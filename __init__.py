from trytond.pool import Pool
from . import taller

__all__ = ['register']


def register():
    Pool.register(
        taller.Marca,
        taller.Modelo,
        taller.Coche,
        taller.Party,
        taller.Productos,
        taller.ModeloProducto,
        taller.BajaCocheStart,
        taller.BajaCocheResultado,
        module='taller', type_='model')
    Pool.register(
        taller.BajaCoche,
        module='taller', type_='wizard')
    Pool.register(
        taller.Repo,
        taller.RepoLista,
        module='taller', type_='report')
