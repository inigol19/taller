from flask import Flask, request, redirect, url_for, render_template, abort, session, g
from flask_tryton import Tryton
import os
from functools import wraps


app = Flask(__name__)
app.config['TRYTON_DATABASE'] = 'taller_db'
app.secret_key = 'wwwwwwwwwwweeeeeeeeeeeeeeeeeeeeeeerrrrrvsdviosdjfosdfnkdvjn'
tryton = Tryton(app, configure_jinja=True)
Marca = tryton.pool.get('taller.marca')
Modelo = tryton.pool.get('taller.modelo')
Coche = tryton.pool.get('taller.coche')
Terceros = tryton.pool.get('party.party')

WebUser = tryton.pool.get('web.user')
UserSession = tryton.pool.get('web.user.session')

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        session_key = None
        if 'session_key' in session:
            session_key = session['session_key']
        g.user = UserSession.get_user(session_key)
        if not g.user:
            return redirect(url_for('login', next=request.path))
        return func(*args, **kwargs)
    return wrapper

@app.route('/logout')
@tryton.transaction(readonly=False)
@login_required
def logout():
    if session['session_key']:
        user_sessions = UserSession.search(
            [('key', '=', session['session_key'])])
        UserSession.delete(user_sessions)
        session.pop('session_key', None)
        session.pop('party', None)
        session.pop('username', None)
        session.pop('name', None)
    return redirect(request.referrer if request.referrer else url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
@tryton.transaction()
def login():
    if request.method == 'POST':
        username = request.form.get('username')
        password = request.form.get('password')
        if username and password:
            user = WebUser.authenticate(username, password)
            if user:
                session['session_key'] = WebUser.new_session(user)
                session['username'] = user.email
                #session['name'] = user.party.name
                #print(session['name'])
                if user.party:
                    session['party'] = user.party.id
                    session['name'] = user.party.name
                next_ = request.form.get('next', None)
                if next_:
                    return redirect(next_)
                return redirect('/')
            else:
                return 'Usuario Incorrecto'
    return render_template('login.html')

@app.route('/signup', methods=['GET', 'POST'])
@tryton.transaction()
def signup():
    if request.method == 'POST':
        #newUser =
        #user = WebUser.authenticate(username, password)
        newTercero = Terceros()
        newTercero.name = request.form.get('name')
        newTercero.save()
        newWebUser =WebUser()
        username = request.form.get('username')
        password = request.form.get('password')
        newWebUser.email = username
        newWebUser.password = password
        newWebUser.party = newTercero
        newWebUser.save()
        return redirect(url_for('listaMarcas'))
    terceros = Terceros.search([])
    return render_template('signup.html', misTerceros=terceros)

@app.route('/')
@tryton.transaction()
def listaMarcas():
    marcas = Marca.search([])
    print(session.get('username'))
    coches = Coche.search([('propietario', '=', session.get('name'))])
    return render_template('marcas.html', misMarcas=marcas, misCoches=coches)


@app.route('/marca/<marca_url>')   #<records("res.user"):users>
@tryton.transaction()
def listaModelos(marca_url):
    modelos = Modelo.search([('marca', '=', marca_url)])
    coches = Coche.search([('marca', '=', marca_url)])

    marcas = Marca.search([('name', '=', marca_url)])
    return render_template('modelos.html', miMarca=marcas[0], misModelos=modelos, misCoches=coches)


@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)

@app.route('/comprar/<record("taller.modelo"):modelo>', methods=['POST', 'GET'])     # que pasaria si cambio modelo por id?? tendria que cambiar algo mas??? <!--devuelve el registro completo del id-->
@tryton.transaction()
def comprar(modelo):
    breakpoint()
    if request.method == 'POST':
        newCoche = Coche()
        newCoche.modelo = modelo
        newCoche.marca = modelo.marca
        newCoche.matricula = request.form.get('matricula', "")
        if session.get('party'):
            miTercero = Terceros(session.get('party'))
            newCoche.propietario = miTercero
        else:
            newCoche.propietario = int(request.form.get('propietario', 0))
        newCoche.save()
        return redirect(url_for('listaModelos', marca_url=modelo.marca.name))


    terceros = Terceros.search([])
    return render_template('comprar.html', miModelo=modelo, misTerceros = terceros)


@app.route('/newMarca/', methods=['POST', 'GET'])   #<records("res.user"):users>
@tryton.transaction()
def addMarca():
    if request.method == 'POST':
        newMarca = Marca()
        newMarca.name = request.form.get('marca', "")
        newMarca.save()
        print(newMarca)
        return redirect(url_for('listaMarcas'))
    return render_template('newMarca.html')

# @app.route('/newModelo/<marca_url>', methods=['POST', 'GET'])   #<records("res.user"):users>
# @tryton.transaction()
# def addModelo(marca_url):
#     #combustibles = modelo.combustible              en newModel <!--{% for combustible in combustibles %}                                                    #<option value={{combustible.id}}>{{combustible}}</option>-->
#
#     # newModelo = Modelo()
#     # newModelo.marca = Modelo.search([('marca', '=', marca_url)])
#
#     if request.method == 'POST':
#         newModelo = Modelo()
#         newModelo.marca = marca[0]
#         newModelo.modelo = request.form.get('marca', "")
#         newModelo.save()
#         print(newMarca)
#         return redirect(url_for('listaModelos'))
#     marca = Marca.search([('name', '=', marca_url)])
#     return render_template('newModelo.html', miMarca=marca[0])

@app.route('/newModelo/<record("taller.marca"):marca>', methods=['POST', 'GET'])     # que pasaria si cambio modelo por id?? tendria que cambiar algo mas??? <!--devuelve el registro completo del id-->
@tryton.transaction()
def addModelo(marca):
    if request.method == 'POST':
        newModelo = Modelo()
        newModelo.marca = marca
        newModelo.modelo = request.form.get('modelo', "")
        newModelo.save()
        return redirect(url_for('listaModelos', marca_url=marca.name))
    combustibles = Modelo.fields_get(['combustible'])['combustible']['selection']
    return render_template('newModelo.html', miMarca=marca, misCombustibles=combustibles)
