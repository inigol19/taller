from proteus import config, Model, Wizard, Report
import csv

config = config.set_trytond('taller_db')

Marca = Model.get('taller.marca')
Modelo = Model.get('taller.modelo')

with open('car_make.csv', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar="'")
    next(spamreader)
    exportMarcas={}
    for row in spamreader:
        miMarca = Marca.find([('name', '=', row[1])])
        if not miMarca:
            newMarca = Marca()
            newMarca.name= row[1]
            newMarca.save()
            exportMarcas[row[0]]= newMarca
        else:
            exportMarcas[row[0]]= miMarca[0]
            print("La marca", miMarca[0].name, "ya estaba registrada")


with open('car_model.csv', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar="'")
    next(spamreader)
    exportModelos=[]
    for row in spamreader:
        #print(exportMarcas[row[1]], row[2])
        newModelo = Modelo()
        newModelo.marca = exportMarcas[row[1]]
        newModelo.modelo = row[2]
        exportModelos.append(newModelo)
    Modelo.save(exportModelos)
