from trytond.model import ModelView, ModelSQL,fields, Unique
from trytond.pyson import Eval, Bool, Greater
import datetime
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.transaction import Transaction
from trytond.report import Report
from trytond.modules.company import CompanyReport

class Marca(ModelSQL, ModelView):
    'Marca'
    __name__ = 'taller.marca'   ##nombre del modelo
    name = fields.Char('Nombre', required=True) ## como este modelo  hay un campo que se llama name, no hace falta usar el _rec_name
    modelos = fields.One2Many('taller.modelo', 'marca', 'Modelos')  ## propiedades del One2Many: modelo, a qué campo hace referencia dentro de la clase, label

class Modelo(ModelSQL, ModelView):
    'Modelo'
    __name__ = 'taller.modelo'
    _rec_name = 'modelo'    ## se utiliza para que cuando se muestre un registro de este moodelo en una vista desde otro modelo, muestre el campo que le digas, este caso el modelo
    marca = fields.Many2One('taller.marca', 'Marca', required=True, ondelete='CASCADE')
    modelo = fields.Char('Modelo', required=True)
    fecha_lanz = fields.Date('Fecha Lanzamiento')
    combustible = fields.Selection([
        ('g', 'gasolina'),
        ('d','diesel'),
        ('h','hibrido'),
        ('e','electrico'),
        (None, '')      ## requerido para poder guardar sin especificar nada = required false
    ], 'tipo de combustible')
    caballos = fields.Integer('numero de caballos')
    precioMod = fields.Integer('precio del modelo')
    #productosDelModelo = fields.Many2One('product.template', 'productos disponibles', ondelete='CASCADE')
    productosDelModelo = fields.Many2Many('modelo-producto', 'modelo', 'producto', 'productos disponibles',
        domain=[
            ('type', '=',  'goods'),
        ])

        #one2Many al coche

    @classmethod
    def default_fecha_lanz(cls):
        return datetime.date.today()

    @classmethod
    def default_marca(cls):
        pool = Pool()
        Marca = pool.get('taller.marca')
        marca1 = Marca.search([],limit=1,
            order=[('name', 'ASC'),('id','ASC')]
        )
        return marca1[0].id

class Coche(ModelSQL, ModelView):
    'Coche'
    __name__ = 'taller.coche'
    matricula = fields.Char('Matricula', required=True,
        states = {
            'readonly': Eval('id',-1) > 0,      #'readonly': Greater(Eval('id',-1), 0),

        })
    propietario = fields.Many2One('party.party', 'Propietario', required=True, ondelete='CASCADE')
    marca = fields.Many2One('taller.marca', 'Marca', ondelete='CASCADE')
    modelo = fields.Many2One('taller.modelo', 'Modelo', ondelete='CASCADE',
        domain=[
            ('marca', '=', Eval('marca',-1))    # 1er marca hace refernecia a la marca del modelo taller.modelo que viene del Many2One, el eval es sobre el priopio registro siempre
        ],
        depends=['marca'],
        states = {
            'required': Bool(Eval('marca',-1)),
            'invisible': ~Bool(Eval('marca',-1)),
        }
    )
    precio = fields.Integer('precio',
        domain= [ 'OR',
            ('precio', '>', 0),
            ('precio', '=', None)
        ])
    fecha_matriculacion = fields.Date('Fecha matriculacion')
    fecha_baja = fields.Date('Fecha baja')
    caballosImp = fields.Function(fields.Integer("nº caballos"), 'getHP', searcher = 'searcherHP')   #("definicion para el usuario de campo funcional", 'campo_funcional', 'filtros')
    fechaImp = fields.Function(fields.Date('fecha de lanzamiento'), 'getFechaImp', searcher = 'searcherFechaLanz')
    # fechaImp2 = fields.Function(fields.Date('fecha2'), 'getMethod2')
    #productos_coche = fields.Many2Many('modelo-producto', 'coche', 'producto', 'productos compatibles con su coche',
    #    domain=[
    #        ('type', '=',  'goods'),
    #        ('modelo', '=', 'producto'),
    #    ])

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints = [        #resstricciones:
            ('matricula unica', Unique(t, t.matricula), 'taller.msg_coche_matricula_unique')
        ]

    @fields.depends('marca', 'modelo')
    def on_change_marca(self):
        if self.marca:
            if len(self.marca.modelos) ==1:
                self.modelo = self.marca.modelos[0]

            if self.modelo:
                #print(self.modelo.marca.name)
                # print(self.marca.name)
                # print(self.marca.length())
                # print(self.modelo)
                if self.modelo.marca.id != self.marca.id:
                    self.modelo = None
                    print("siguefuncionandooooooo2")
            else:
                self.modelo = None

    @fields.depends('marca', 'modelo')
    def on_change_with_precio(self):
        if self.marca and self.modelo:
            print(self.modelo.precioMod)
            #self.precio = self.modelo.precioMod
            return self.modelo.precioMod

    #el que enrealidad funcionaba era esteeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee
    # @fields.depends('marca', 'modelo')
    # def on_change_marca(self):
    #     if self.modelo and self.marca:
    #         #print(self.modelo.marca.name)
    #         if self.modelo.marca.id != self.marca.id:
    #             self.modelo = None
    #             print("si que funciona")


    def getHP(self, name):
        #print("prueba")
        if self.modelo:
            if self.modelo.caballos:
                return self.modelo.caballos
            else:
                return 0
        else:
            return 0

    @classmethod    # este metodo busca condiciones en los filtros del usuario
    def searcherHP(cls, name, clause):  #tiene que devolver un dominio para buscar el valor que queremos
        print("prueba")
        print(name, clause) #name = el nombre del campo donde se ha escrito una condicion y clause = la condicion [de los filtros]
        return [('modelo.caballos', clause[1], clause[2])]  #el dato que queremos traer a la clase coche, lo tenemos en la clase modelo el campo caballos

    @classmethod
    def searcherFechaLanz(cls, name, clause):
        print(name, clause)
        return [('modelo.fecha_lanz', clause[1], clause[2])]

    def getFechaImp(self, name):
        if self.modelo and self.modelo.fecha_lanz:
            return self.modelo.fecha_lanz
        else:
            return datetime.date(1111,11,11)

    # def getMethod2(self, name):
    #     pool = Pool()
    #     Modelo = pool.get('taller.modelo')
        # hp = Modelo.search([],
        #     domain=[
        #     ('self.Modelo.caballos')
        #     ])
        #return 2

class Party(metaclass=PoolMeta):
    __name__ = 'party.party'            #  nos fijamos en la url

    coches = fields.One2Many('taller.coche', 'propietario', 'Coches')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.name.required = True
        cls.name.states.update({             #update solo modifica las clases que hay definidas en el diccionario
            'readonly': Eval('id',-1) > 0
            })

class Productos(metaclass=PoolMeta):
    __name__ = 'product.template'       #nos fijamos en la url

    #modelos_compatibles = fields.One2Many('taller.modelo', 'productosDelModelo', 'Modelos compatibles')
    modelos_compatibles = fields.Many2Many('modelo-producto', 'producto', 'modelo', 'Modelos compatibles',
        states = {
            'invisible': Eval('type','') != 'goods',
        },
        depends=['type'],       #todos los campos evaluados se meten para que hereden propiedades
        )

class ModeloProducto(ModelSQL):     #creamos nueva clase para crear tabla de relacion entre many2many
    "modelo - producto"           #descripcion del modelo nuevo
    __name__ = 'modelo-producto'
    modelo = fields.Many2One('taller.modelo', 'modelo', required=True, ondelete='CASCADE', select = True)       # select = true -> crea un indice que facilita las busquedas
    producto = fields.Many2One('product.template', 'producto', required=True, ondelete='CASCADE', select = True)


########################WIZARD#########################
class BajaCoche(Wizard):
    "Baja coche"
    __name__ = "taller.coche.baja"
    start = StateView('taller.coche.baja.start',
        'taller.coche_baja_start_form', [           #nombreDelModulo.id_registroXML
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Baja', 'baja', 'tryton-ok', default=True),
    ])
    baja = StateTransition()
    result = StateView('taller.coche.baja.result',
        'taller.coche_baja_result_form', [
            Button('Close', 'end', 'tryton-close'),
        ])

    def transition_baja(self):   #transition_"Nombre_de_la_transicion"(204)
        pool = Pool()
        Coche = pool.get('taller.coche')
        coches = Coche.browse(Transaction().context['active_ids'])
        print(coches)
        for coche in coches:
            coche.fecha_baja = self.start.fecha_baja
        Coche.save(coches)
        #self.result.n_coches_adfectados = len(coches)      #se puede hacer
        return "result"

class BajaCocheStart(ModelView):
    "baja de coche"
    __name__ = "taller.coche.baja.start"
    fecha_baja = fields.Date('Fecha baja', required=True)

    @classmethod
    def default_fecha_baja(cls):
        return datetime.date.today()

class BajaCocheResultado(ModelView):
    "resultado baja de coche"
    __name__ = "taller.coche.baja.result"
    n_coches_adfectados = fields.Integer('numero de coches afectados', readonly=True)

    @classmethod
    def default_n_coches_adfectados(cls):
        return len(Transaction().context['active_ids'])

class Repo(CompanyReport):
    ""
    __name__ = "ficha_tecnica_del_coche"


    @classmethod
    def get_context(cls, records, data):

        context = super().get_context(records, data)
        context['empresa'] = "Gesalaga"

        return context

class RepoLista(Report):
    ""
    __name__ = "lista_coches"
