--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4 (Ubuntu 12.4-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.4 (Ubuntu 12.4-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: coche-producto_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."coche-producto_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."coche-producto_id_seq" OWNER TO tryton;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: coche-producto; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."coche-producto" (
    id integer DEFAULT nextval('public."coche-producto_id_seq"'::regclass) NOT NULL,
    coche integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    producto integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "coche-producto_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."coche-producto" OWNER TO tryton;

--
-- Name: TABLE "coche-producto"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."coche-producto" IS 'coche-producto';


--
-- Name: company_company_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.company_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_company_id_seq OWNER TO tryton;

--
-- Name: company_company; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.company_company (
    id integer DEFAULT nextval('public.company_company_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    currency integer NOT NULL,
    footer text,
    header text,
    parent integer,
    party integer NOT NULL,
    timezone character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT company_company_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.company_company OWNER TO tryton;

--
-- Name: TABLE company_company; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.company_company IS 'Company';


--
-- Name: company_employee_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.company_employee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.company_employee_id_seq OWNER TO tryton;

--
-- Name: company_employee; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.company_employee (
    id integer DEFAULT nextval('public.company_employee_id_seq'::regclass) NOT NULL,
    company integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    end_date date,
    party integer NOT NULL,
    start_date date,
    supervisor integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT company_employee_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.company_employee OWNER TO tryton;

--
-- Name: TABLE company_employee; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.company_employee IS 'Employee';


--
-- Name: country_country_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.country_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_country_id_seq OWNER TO tryton;

--
-- Name: country_country; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.country_country (
    id integer DEFAULT nextval('public.country_country_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    code character varying(2),
    code3 character varying(3),
    code_numeric character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT country_country_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.country_country OWNER TO tryton;

--
-- Name: TABLE country_country; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.country_country IS 'Country';


--
-- Name: country_subdivision_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.country_subdivision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_subdivision_id_seq OWNER TO tryton;

--
-- Name: country_subdivision; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.country_subdivision (
    id integer DEFAULT nextval('public.country_subdivision_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    code character varying NOT NULL,
    country integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying NOT NULL,
    parent integer,
    type character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT country_subdivision_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.country_subdivision OWNER TO tryton;

--
-- Name: TABLE country_subdivision; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.country_subdivision IS 'Subdivision';


--
-- Name: country_zip_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.country_zip_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_zip_id_seq OWNER TO tryton;

--
-- Name: country_zip; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.country_zip (
    id integer DEFAULT nextval('public.country_zip_id_seq'::regclass) NOT NULL,
    city character varying,
    country integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    subdivision integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    zip character varying,
    CONSTRAINT country_zip_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.country_zip OWNER TO tryton;

--
-- Name: TABLE country_zip; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.country_zip IS 'Zip';


--
-- Name: cron_company_rel_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.cron_company_rel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cron_company_rel_id_seq OWNER TO tryton;

--
-- Name: cron_company_rel; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.cron_company_rel (
    id integer DEFAULT nextval('public.cron_company_rel_id_seq'::regclass) NOT NULL,
    company integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    cron integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT cron_company_rel_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.cron_company_rel OWNER TO tryton;

--
-- Name: TABLE cron_company_rel; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.cron_company_rel IS 'Cron - Company';


--
-- Name: currency_currency_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.currency_currency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.currency_currency_id_seq OWNER TO tryton;

--
-- Name: currency_currency; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.currency_currency (
    id integer DEFAULT nextval('public.currency_currency_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    code character varying(3) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    digits integer NOT NULL,
    name character varying NOT NULL,
    numeric_code character varying(3),
    rounding numeric NOT NULL,
    symbol character varying(10) NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT currency_currency_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.currency_currency OWNER TO tryton;

--
-- Name: TABLE currency_currency; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.currency_currency IS 'Currency';


--
-- Name: currency_currency_rate_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.currency_currency_rate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.currency_currency_rate_id_seq OWNER TO tryton;

--
-- Name: currency_currency_rate; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.currency_currency_rate (
    id integer DEFAULT nextval('public.currency_currency_rate_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    currency integer,
    date date NOT NULL,
    rate numeric NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT currency_currency_rate_check_currency_rate CHECK ((rate >= (0)::numeric))
);


ALTER TABLE public.currency_currency_rate OWNER TO tryton;

--
-- Name: TABLE currency_currency_rate; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.currency_currency_rate IS 'Currency Rate';


--
-- Name: ir_action_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_action_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_action_id_seq OWNER TO tryton;

--
-- Name: ir_action; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_action (
    id integer DEFAULT nextval('public.ir_action_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    icon integer,
    name character varying NOT NULL,
    type character varying NOT NULL,
    usage character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_action_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_action OWNER TO tryton;

--
-- Name: TABLE ir_action; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_action IS 'Action';


--
-- Name: ir_action-res_group_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."ir_action-res_group_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ir_action-res_group_id_seq" OWNER TO tryton;

--
-- Name: ir_action-res_group; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."ir_action-res_group" (
    id integer DEFAULT nextval('public."ir_action-res_group_id_seq"'::regclass) NOT NULL,
    action integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    "group" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "ir_action-res_group_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."ir_action-res_group" OWNER TO tryton;

--
-- Name: TABLE "ir_action-res_group"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."ir_action-res_group" IS 'Action - Group';


--
-- Name: ir_action_act_window_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_action_act_window_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_action_act_window_id_seq OWNER TO tryton;

--
-- Name: ir_action_act_window; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_action_act_window (
    id integer DEFAULT nextval('public.ir_action_act_window_id_seq'::regclass) NOT NULL,
    action integer NOT NULL,
    context character varying,
    context_domain character varying,
    context_model character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    domain character varying,
    "limit" integer,
    "order" character varying,
    res_model character varying,
    search_value character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_action_act_window_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_action_act_window OWNER TO tryton;

--
-- Name: TABLE ir_action_act_window; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_action_act_window IS 'Action act window';


--
-- Name: ir_action_act_window_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_action_act_window_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_action_act_window_domain_id_seq OWNER TO tryton;

--
-- Name: ir_action_act_window_domain; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_action_act_window_domain (
    id integer DEFAULT nextval('public.ir_action_act_window_domain_id_seq'::regclass) NOT NULL,
    act_window integer NOT NULL,
    active boolean DEFAULT false,
    count boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    domain character varying,
    name character varying,
    sequence integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_action_act_window_domain_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_action_act_window_domain OWNER TO tryton;

--
-- Name: TABLE ir_action_act_window_domain; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_action_act_window_domain IS 'Action act window domain';


--
-- Name: ir_action_act_window_view_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_action_act_window_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_action_act_window_view_id_seq OWNER TO tryton;

--
-- Name: ir_action_act_window_view; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_action_act_window_view (
    id integer DEFAULT nextval('public.ir_action_act_window_view_id_seq'::regclass) NOT NULL,
    act_window integer,
    active boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    sequence integer,
    view integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_action_act_window_view_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_action_act_window_view OWNER TO tryton;

--
-- Name: TABLE ir_action_act_window_view; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_action_act_window_view IS 'Action act window view';


--
-- Name: ir_action_keyword_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_action_keyword_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_action_keyword_id_seq OWNER TO tryton;

--
-- Name: ir_action_keyword; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_action_keyword (
    id integer DEFAULT nextval('public.ir_action_keyword_id_seq'::regclass) NOT NULL,
    action integer,
    create_date timestamp(6) without time zone,
    create_uid integer,
    keyword character varying NOT NULL,
    model character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_action_keyword_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_action_keyword OWNER TO tryton;

--
-- Name: TABLE ir_action_keyword; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_action_keyword IS 'Action keyword';


--
-- Name: ir_action_report_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_action_report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_action_report_id_seq OWNER TO tryton;

--
-- Name: ir_action_report; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_action_report (
    id integer DEFAULT nextval('public.ir_action_report_id_seq'::regclass) NOT NULL,
    action integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    direct_print boolean DEFAULT false,
    email character varying,
    extension character varying,
    model character varying,
    module character varying,
    report character varying,
    report_content_custom bytea,
    report_name character varying NOT NULL,
    single boolean DEFAULT false,
    template_extension character varying NOT NULL,
    translatable boolean DEFAULT false,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_action_report_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_action_report OWNER TO tryton;

--
-- Name: TABLE ir_action_report; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_action_report IS 'Action report';


--
-- Name: ir_action_url_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_action_url_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_action_url_id_seq OWNER TO tryton;

--
-- Name: ir_action_url; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_action_url (
    id integer DEFAULT nextval('public.ir_action_url_id_seq'::regclass) NOT NULL,
    action integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    url character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_action_url_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_action_url OWNER TO tryton;

--
-- Name: TABLE ir_action_url; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_action_url IS 'Action URL';


--
-- Name: ir_action_wizard_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_action_wizard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_action_wizard_id_seq OWNER TO tryton;

--
-- Name: ir_action_wizard; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_action_wizard (
    id integer DEFAULT nextval('public.ir_action_wizard_id_seq'::regclass) NOT NULL,
    action integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    email character varying,
    model character varying,
    "window" boolean DEFAULT false,
    wiz_name character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_action_wizard_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_action_wizard OWNER TO tryton;

--
-- Name: TABLE ir_action_wizard; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_action_wizard IS 'Action wizard';


--
-- Name: ir_attachment_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_attachment_id_seq OWNER TO tryton;

--
-- Name: ir_attachment; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_attachment (
    id integer DEFAULT nextval('public.ir_attachment_id_seq'::regclass) NOT NULL,
    copy_to_resources character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    data bytea,
    description text,
    file_id character varying,
    link character varying,
    name character varying NOT NULL,
    resource character varying NOT NULL,
    type character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_attachment_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_attachment OWNER TO tryton;

--
-- Name: TABLE ir_attachment; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_attachment IS 'Attachment';


--
-- Name: ir_cache_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_cache_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_cache_id_seq OWNER TO tryton;

--
-- Name: ir_cache; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_cache (
    id integer DEFAULT nextval('public.ir_cache_id_seq'::regclass) NOT NULL,
    name character varying NOT NULL,
    "timestamp" timestamp without time zone,
    create_date timestamp(6) without time zone,
    create_uid integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_cache_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_cache OWNER TO tryton;

--
-- Name: TABLE ir_cache; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_cache IS 'Cache';


--
-- Name: ir_calendar_day_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_calendar_day_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_calendar_day_id_seq OWNER TO tryton;

--
-- Name: ir_calendar_day; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_calendar_day (
    id integer DEFAULT nextval('public.ir_calendar_day_id_seq'::regclass) NOT NULL,
    abbreviation character varying NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    index integer NOT NULL,
    name character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer
);


ALTER TABLE public.ir_calendar_day OWNER TO tryton;

--
-- Name: TABLE ir_calendar_day; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_calendar_day IS 'Day';


--
-- Name: ir_calendar_month_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_calendar_month_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_calendar_month_id_seq OWNER TO tryton;

--
-- Name: ir_calendar_month; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_calendar_month (
    id integer DEFAULT nextval('public.ir_calendar_month_id_seq'::regclass) NOT NULL,
    abbreviation character varying NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    index integer NOT NULL,
    name character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer
);


ALTER TABLE public.ir_calendar_month OWNER TO tryton;

--
-- Name: TABLE ir_calendar_month; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_calendar_month IS 'Month';


--
-- Name: ir_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_configuration_id_seq OWNER TO tryton;

--
-- Name: ir_configuration; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_configuration (
    id integer DEFAULT nextval('public.ir_configuration_id_seq'::regclass) NOT NULL,
    language character varying,
    hostname character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_configuration_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_configuration OWNER TO tryton;

--
-- Name: TABLE ir_configuration; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_configuration IS 'Configuration';


--
-- Name: ir_cron_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_cron_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_cron_id_seq OWNER TO tryton;

--
-- Name: ir_cron; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_cron (
    id integer DEFAULT nextval('public.ir_cron_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    day integer,
    hour integer,
    interval_number integer NOT NULL,
    interval_type character varying NOT NULL,
    method character varying NOT NULL,
    minute integer,
    next_call timestamp(0) without time zone,
    weekday integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_cron_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_cron OWNER TO tryton;

--
-- Name: TABLE ir_cron; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_cron IS 'Cron';


--
-- Name: ir_export_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_export_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_export_id_seq OWNER TO tryton;

--
-- Name: ir_export; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_export (
    id integer DEFAULT nextval('public.ir_export_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying,
    resource character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_export_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_export OWNER TO tryton;

--
-- Name: TABLE ir_export; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_export IS 'Export';


--
-- Name: ir_export-res_group_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."ir_export-res_group_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ir_export-res_group_id_seq" OWNER TO tryton;

--
-- Name: ir_export-res_group; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."ir_export-res_group" (
    id integer DEFAULT nextval('public."ir_export-res_group_id_seq"'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    export integer NOT NULL,
    "group" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "ir_export-res_group_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."ir_export-res_group" OWNER TO tryton;

--
-- Name: TABLE "ir_export-res_group"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."ir_export-res_group" IS 'Export Group';


--
-- Name: ir_export-write-res_group_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."ir_export-write-res_group_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ir_export-write-res_group_id_seq" OWNER TO tryton;

--
-- Name: ir_export-write-res_group; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."ir_export-write-res_group" (
    id integer DEFAULT nextval('public."ir_export-write-res_group_id_seq"'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    export integer NOT NULL,
    "group" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "ir_export-write-res_group_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."ir_export-write-res_group" OWNER TO tryton;

--
-- Name: TABLE "ir_export-write-res_group"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."ir_export-write-res_group" IS 'Export Modification Group';


--
-- Name: ir_export_line_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_export_line_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_export_line_id_seq OWNER TO tryton;

--
-- Name: ir_export_line; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_export_line (
    id integer DEFAULT nextval('public.ir_export_line_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    export integer NOT NULL,
    name character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_export_line_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_export_line OWNER TO tryton;

--
-- Name: TABLE ir_export_line; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_export_line IS 'Export line';


--
-- Name: ir_lang_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_lang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_lang_id_seq OWNER TO tryton;

--
-- Name: ir_lang; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_lang (
    id integer DEFAULT nextval('public.ir_lang_id_seq'::regclass) NOT NULL,
    name character varying NOT NULL,
    code character varying NOT NULL,
    translatable boolean DEFAULT false,
    parent character varying,
    active boolean DEFAULT false,
    direction character varying NOT NULL,
    am character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    date character varying NOT NULL,
    decimal_point character varying NOT NULL,
    "grouping" character varying NOT NULL,
    mon_decimal_point character varying NOT NULL,
    mon_grouping character varying NOT NULL,
    mon_thousands_sep character varying,
    n_cs_precedes boolean DEFAULT false,
    n_sep_by_space boolean DEFAULT false,
    n_sign_posn integer NOT NULL,
    negative_sign character varying,
    p_cs_precedes boolean DEFAULT false,
    p_sep_by_space boolean DEFAULT false,
    p_sign_posn integer NOT NULL,
    pm character varying,
    positive_sign character varying,
    thousands_sep character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_lang_check_decimal_point_thousands_sep CHECK (((decimal_point)::text <> (thousands_sep)::text)),
    CONSTRAINT ir_lang_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_lang OWNER TO tryton;

--
-- Name: TABLE ir_lang; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_lang IS 'Language';


--
-- Name: ir_message_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_message_id_seq OWNER TO tryton;

--
-- Name: ir_message; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_message (
    id integer DEFAULT nextval('public.ir_message_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    text text NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_message_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_message OWNER TO tryton;

--
-- Name: TABLE ir_message; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_message IS 'Message';


--
-- Name: ir_model_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_model_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_model_id_seq OWNER TO tryton;

--
-- Name: ir_model; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_model (
    id integer DEFAULT nextval('public.ir_model_id_seq'::regclass) NOT NULL,
    model character varying NOT NULL,
    name character varying,
    info text,
    module character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    global_search_p boolean DEFAULT false,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_model_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_model OWNER TO tryton;

--
-- Name: TABLE ir_model; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_model IS 'Model';


--
-- Name: ir_model_access_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_model_access_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_model_access_id_seq OWNER TO tryton;

--
-- Name: ir_model_access; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_model_access (
    id integer DEFAULT nextval('public.ir_model_access_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    description text,
    "group" integer,
    model integer NOT NULL,
    perm_create boolean DEFAULT false,
    perm_delete boolean DEFAULT false,
    perm_read boolean DEFAULT false,
    perm_write boolean DEFAULT false,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_model_access_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_model_access OWNER TO tryton;

--
-- Name: TABLE ir_model_access; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_model_access IS 'Model access';


--
-- Name: ir_model_button_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_model_button_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_model_button_id_seq OWNER TO tryton;

--
-- Name: ir_model_button; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_model_button (
    id integer DEFAULT nextval('public.ir_model_button_id_seq'::regclass) NOT NULL,
    confirm text,
    create_date timestamp(6) without time zone,
    create_uid integer,
    help text,
    model integer NOT NULL,
    name character varying NOT NULL,
    string character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_model_button_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_model_button OWNER TO tryton;

--
-- Name: TABLE ir_model_button; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_model_button IS 'Model Button';


--
-- Name: ir_model_button-button_reset_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."ir_model_button-button_reset_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ir_model_button-button_reset_id_seq" OWNER TO tryton;

--
-- Name: ir_model_button-button_reset; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."ir_model_button-button_reset" (
    id integer DEFAULT nextval('public."ir_model_button-button_reset_id_seq"'::regclass) NOT NULL,
    button integer NOT NULL,
    button_ruled integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "ir_model_button-button_reset_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."ir_model_button-button_reset" OWNER TO tryton;

--
-- Name: TABLE "ir_model_button-button_reset"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."ir_model_button-button_reset" IS 'Model Button Reset';


--
-- Name: ir_model_button-res_group_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."ir_model_button-res_group_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ir_model_button-res_group_id_seq" OWNER TO tryton;

--
-- Name: ir_model_button-res_group; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."ir_model_button-res_group" (
    id integer DEFAULT nextval('public."ir_model_button-res_group_id_seq"'::regclass) NOT NULL,
    active boolean DEFAULT false,
    button integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    "group" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "ir_model_button-res_group_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."ir_model_button-res_group" OWNER TO tryton;

--
-- Name: TABLE "ir_model_button-res_group"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."ir_model_button-res_group" IS 'Model Button - Group';


--
-- Name: ir_model_button_click_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_model_button_click_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_model_button_click_id_seq OWNER TO tryton;

--
-- Name: ir_model_button_click; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_model_button_click (
    id integer DEFAULT nextval('public.ir_model_button_click_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    button integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    record_id integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    "user" integer,
    CONSTRAINT ir_model_button_click_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_model_button_click OWNER TO tryton;

--
-- Name: TABLE ir_model_button_click; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_model_button_click IS 'Model Button Click';


--
-- Name: ir_model_button_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_model_button_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_model_button_rule_id_seq OWNER TO tryton;

--
-- Name: ir_model_button_rule; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_model_button_rule (
    id integer DEFAULT nextval('public.ir_model_button_rule_id_seq'::regclass) NOT NULL,
    button integer NOT NULL,
    condition character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    description character varying,
    number_user integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    "group" integer,
    CONSTRAINT ir_model_button_rule_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_model_button_rule OWNER TO tryton;

--
-- Name: TABLE ir_model_button_rule; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_model_button_rule IS 'Model Button Rule';


--
-- Name: ir_model_data_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_model_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_model_data_id_seq OWNER TO tryton;

--
-- Name: ir_model_data; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_model_data (
    id integer DEFAULT nextval('public.ir_model_data_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    db_id integer,
    fs_id character varying NOT NULL,
    fs_values text,
    model character varying NOT NULL,
    module character varying NOT NULL,
    noupdate boolean DEFAULT false,
    "values" text,
    write_date timestamp(6) without time zone,
    write_uid integer
);


ALTER TABLE public.ir_model_data OWNER TO tryton;

--
-- Name: TABLE ir_model_data; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_model_data IS 'Model data';


--
-- Name: ir_model_field_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_model_field_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_model_field_id_seq OWNER TO tryton;

--
-- Name: ir_model_field; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_model_field (
    id integer DEFAULT nextval('public.ir_model_field_id_seq'::regclass) NOT NULL,
    model integer NOT NULL,
    name character varying NOT NULL,
    relation character varying,
    field_description character varying,
    ttype character varying,
    help text,
    module character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_model_field_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_model_field OWNER TO tryton;

--
-- Name: TABLE ir_model_field; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_model_field IS 'Model field';


--
-- Name: ir_model_field_access_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_model_field_access_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_model_field_access_id_seq OWNER TO tryton;

--
-- Name: ir_model_field_access; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_model_field_access (
    id integer DEFAULT nextval('public.ir_model_field_access_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    description text,
    field integer NOT NULL,
    "group" integer,
    perm_create boolean DEFAULT false,
    perm_delete boolean DEFAULT false,
    perm_read boolean DEFAULT false,
    perm_write boolean DEFAULT false,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_model_field_access_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_model_field_access OWNER TO tryton;

--
-- Name: TABLE ir_model_field_access; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_model_field_access IS 'Model Field Access';


--
-- Name: ir_module_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_module_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_module_id_seq OWNER TO tryton;

--
-- Name: ir_module; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_module (
    id integer DEFAULT nextval('public.ir_module_id_seq'::regclass) NOT NULL,
    create_uid integer,
    create_date timestamp without time zone,
    write_date timestamp without time zone,
    write_uid integer,
    name character varying NOT NULL,
    state character varying
);


ALTER TABLE public.ir_module OWNER TO tryton;

--
-- Name: TABLE ir_module; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_module IS 'Module';


--
-- Name: ir_module_config_wizard_item_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_module_config_wizard_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_module_config_wizard_item_id_seq OWNER TO tryton;

--
-- Name: ir_module_config_wizard_item; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_module_config_wizard_item (
    id integer DEFAULT nextval('public.ir_module_config_wizard_item_id_seq'::regclass) NOT NULL,
    action integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    sequence integer,
    state character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_module_config_wizard_item_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_module_config_wizard_item OWNER TO tryton;

--
-- Name: TABLE ir_module_config_wizard_item; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_module_config_wizard_item IS 'Config wizard to run after activating a module';


--
-- Name: ir_module_dependency_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_module_dependency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_module_dependency_id_seq OWNER TO tryton;

--
-- Name: ir_module_dependency; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_module_dependency (
    id integer DEFAULT nextval('public.ir_module_dependency_id_seq'::regclass) NOT NULL,
    create_uid integer,
    create_date timestamp without time zone,
    write_date timestamp without time zone,
    write_uid integer,
    name character varying,
    module integer NOT NULL,
    CONSTRAINT ir_module_dependency_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_module_dependency OWNER TO tryton;

--
-- Name: TABLE ir_module_dependency; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_module_dependency IS 'Module dependency';


--
-- Name: ir_note_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_note_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_note_id_seq OWNER TO tryton;

--
-- Name: ir_note; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_note (
    id integer DEFAULT nextval('public.ir_note_id_seq'::regclass) NOT NULL,
    copy_to_resources character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    message text,
    resource character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_note_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_note OWNER TO tryton;

--
-- Name: TABLE ir_note; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_note IS 'Note';


--
-- Name: ir_note_read_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_note_read_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_note_read_id_seq OWNER TO tryton;

--
-- Name: ir_note_read; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_note_read (
    id integer DEFAULT nextval('public.ir_note_read_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    note integer NOT NULL,
    "user" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_note_read_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_note_read OWNER TO tryton;

--
-- Name: TABLE ir_note_read; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_note_read IS 'Note Read';


--
-- Name: ir_queue_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_queue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_queue_id_seq OWNER TO tryton;

--
-- Name: ir_queue; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_queue (
    id integer DEFAULT nextval('public.ir_queue_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    data text,
    dequeued_at timestamp(6) without time zone,
    enqueued_at timestamp(6) without time zone NOT NULL,
    expected_at timestamp(6) without time zone,
    finished_at timestamp(6) without time zone,
    name character varying NOT NULL,
    scheduled_at timestamp(6) without time zone,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_queue_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_queue OWNER TO tryton;

--
-- Name: TABLE ir_queue; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_queue IS 'Queue';


--
-- Name: ir_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_rule_id_seq OWNER TO tryton;

--
-- Name: ir_rule; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_rule (
    id integer DEFAULT nextval('public.ir_rule_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    domain character varying NOT NULL,
    rule_group integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_rule_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_rule OWNER TO tryton;

--
-- Name: TABLE ir_rule; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_rule IS 'Rule';


--
-- Name: ir_rule_group_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_rule_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_rule_group_id_seq OWNER TO tryton;

--
-- Name: ir_rule_group; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_rule_group (
    id integer DEFAULT nextval('public.ir_rule_group_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    default_p boolean DEFAULT false,
    global_p boolean DEFAULT false,
    model integer NOT NULL,
    name character varying NOT NULL,
    perm_create boolean DEFAULT false,
    perm_delete boolean DEFAULT false,
    perm_read boolean DEFAULT false,
    perm_write boolean DEFAULT false,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_rule_group_global_default_exclusive CHECK (((global_p = false) OR (default_p = false))),
    CONSTRAINT ir_rule_group_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_rule_group OWNER TO tryton;

--
-- Name: TABLE ir_rule_group; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_rule_group IS 'Rule group';


--
-- Name: ir_rule_group-res_group_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."ir_rule_group-res_group_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ir_rule_group-res_group_id_seq" OWNER TO tryton;

--
-- Name: ir_rule_group-res_group; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."ir_rule_group-res_group" (
    id integer DEFAULT nextval('public."ir_rule_group-res_group_id_seq"'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    "group" integer NOT NULL,
    rule_group integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "ir_rule_group-res_group_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."ir_rule_group-res_group" OWNER TO tryton;

--
-- Name: TABLE "ir_rule_group-res_group"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."ir_rule_group-res_group" IS 'Rule Group - Group';


--
-- Name: ir_sequence_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_sequence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_sequence_id_seq OWNER TO tryton;

--
-- Name: ir_sequence; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_sequence (
    id integer DEFAULT nextval('public.ir_sequence_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    code character varying NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    last_timestamp integer,
    name character varying NOT NULL,
    number_increment integer,
    number_next_internal integer,
    padding integer,
    prefix character varying,
    suffix character varying,
    timestamp_offset double precision NOT NULL,
    timestamp_rounding double precision NOT NULL,
    type character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    company integer,
    CONSTRAINT ir_sequence_check_timestamp_rounding CHECK ((timestamp_rounding > (0)::double precision)),
    CONSTRAINT ir_sequence_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_sequence OWNER TO tryton;

--
-- Name: TABLE ir_sequence; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_sequence IS 'Sequence';


--
-- Name: ir_sequence_1; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_sequence_1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_sequence_1 OWNER TO tryton;

--
-- Name: ir_sequence_strict_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_sequence_strict_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_sequence_strict_id_seq OWNER TO tryton;

--
-- Name: ir_sequence_strict; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_sequence_strict (
    id integer DEFAULT nextval('public.ir_sequence_strict_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    code character varying NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    last_timestamp integer,
    name character varying NOT NULL,
    number_increment integer,
    number_next_internal integer,
    padding integer,
    prefix character varying,
    suffix character varying,
    timestamp_offset double precision NOT NULL,
    timestamp_rounding double precision NOT NULL,
    type character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    company integer,
    CONSTRAINT ir_sequence_strict_check_timestamp_rounding CHECK ((timestamp_rounding > (0)::double precision)),
    CONSTRAINT ir_sequence_strict_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_sequence_strict OWNER TO tryton;

--
-- Name: TABLE ir_sequence_strict; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_sequence_strict IS 'Sequence Strict';


--
-- Name: ir_sequence_type_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_sequence_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_sequence_type_id_seq OWNER TO tryton;

--
-- Name: ir_sequence_type; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_sequence_type (
    id integer DEFAULT nextval('public.ir_sequence_type_id_seq'::regclass) NOT NULL,
    code character varying NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_sequence_type_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_sequence_type OWNER TO tryton;

--
-- Name: TABLE ir_sequence_type; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_sequence_type IS 'Sequence type';


--
-- Name: ir_sequence_type-res_group_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."ir_sequence_type-res_group_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ir_sequence_type-res_group_id_seq" OWNER TO tryton;

--
-- Name: ir_sequence_type-res_group; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."ir_sequence_type-res_group" (
    id integer DEFAULT nextval('public."ir_sequence_type-res_group_id_seq"'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    "group" integer NOT NULL,
    sequence_type integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "ir_sequence_type-res_group_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."ir_sequence_type-res_group" OWNER TO tryton;

--
-- Name: TABLE "ir_sequence_type-res_group"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."ir_sequence_type-res_group" IS 'Sequence Type - Group';


--
-- Name: ir_session_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_session_id_seq OWNER TO tryton;

--
-- Name: ir_session; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_session (
    id integer DEFAULT nextval('public.ir_session_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    key character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_session_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_session OWNER TO tryton;

--
-- Name: TABLE ir_session; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_session IS 'Session';


--
-- Name: ir_session_wizard_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_session_wizard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_session_wizard_id_seq OWNER TO tryton;

--
-- Name: ir_session_wizard; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_session_wizard (
    id integer DEFAULT nextval('public.ir_session_wizard_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    data text,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_session_wizard_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_session_wizard OWNER TO tryton;

--
-- Name: TABLE ir_session_wizard; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_session_wizard IS 'Session Wizard';


--
-- Name: ir_translation_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_translation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_translation_id_seq OWNER TO tryton;

--
-- Name: ir_translation; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_translation (
    id integer DEFAULT nextval('public.ir_translation_id_seq'::regclass) NOT NULL,
    lang character varying,
    src text,
    name character varying NOT NULL,
    res_id integer NOT NULL,
    value text,
    type character varying NOT NULL,
    module character varying,
    fuzzy boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    overriding_module character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_translation_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_translation OWNER TO tryton;

--
-- Name: TABLE ir_translation; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_translation IS 'Translation';


--
-- Name: ir_trigger_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_trigger_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_trigger_id_seq OWNER TO tryton;

--
-- Name: ir_trigger; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_trigger (
    id integer DEFAULT nextval('public.ir_trigger_id_seq'::regclass) NOT NULL,
    action character varying NOT NULL,
    active boolean DEFAULT false,
    condition character varying NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    limit_number integer NOT NULL,
    minimum_time_delay interval,
    model integer NOT NULL,
    name character varying NOT NULL,
    on_create boolean DEFAULT false,
    on_delete boolean DEFAULT false,
    on_time boolean DEFAULT false,
    on_write boolean DEFAULT false,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_trigger_id_positive CHECK ((id >= 0)),
    CONSTRAINT ir_trigger_on_exclusive CHECK ((NOT ((on_time = true) AND ((on_create = true) OR (on_write = true) OR (on_delete = true)))))
);


ALTER TABLE public.ir_trigger OWNER TO tryton;

--
-- Name: TABLE ir_trigger; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_trigger IS 'Trigger';


--
-- Name: ir_trigger__history___id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_trigger__history___id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_trigger__history___id_seq OWNER TO tryton;

--
-- Name: ir_trigger__history; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_trigger__history (
    id integer,
    __id integer DEFAULT nextval('public.ir_trigger__history___id_seq'::regclass) NOT NULL
);


ALTER TABLE public.ir_trigger__history OWNER TO tryton;

--
-- Name: TABLE ir_trigger__history; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_trigger__history IS 'Trigger';


--
-- Name: ir_trigger_log_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_trigger_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_trigger_log_id_seq OWNER TO tryton;

--
-- Name: ir_trigger_log; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_trigger_log (
    id integer DEFAULT nextval('public.ir_trigger_log_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    record_id integer NOT NULL,
    trigger integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_trigger_log_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_trigger_log OWNER TO tryton;

--
-- Name: TABLE ir_trigger_log; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_trigger_log IS 'Trigger Log';


--
-- Name: ir_ui_icon_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_ui_icon_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_ui_icon_id_seq OWNER TO tryton;

--
-- Name: ir_ui_icon; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_ui_icon (
    id integer DEFAULT nextval('public.ir_ui_icon_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    module character varying NOT NULL,
    name character varying NOT NULL,
    path character varying NOT NULL,
    sequence integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_ui_icon_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_ui_icon OWNER TO tryton;

--
-- Name: TABLE ir_ui_icon; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_ui_icon IS 'Icon';


--
-- Name: ir_ui_menu_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_ui_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_ui_menu_id_seq OWNER TO tryton;

--
-- Name: ir_ui_menu; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_ui_menu (
    id integer DEFAULT nextval('public.ir_ui_menu_id_seq'::regclass) NOT NULL,
    parent integer,
    name character varying NOT NULL,
    icon character varying,
    active boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    sequence integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_ui_menu_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_ui_menu OWNER TO tryton;

--
-- Name: TABLE ir_ui_menu; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_ui_menu IS 'UI menu';


--
-- Name: ir_ui_menu-res_group_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."ir_ui_menu-res_group_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."ir_ui_menu-res_group_id_seq" OWNER TO tryton;

--
-- Name: ir_ui_menu-res_group; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."ir_ui_menu-res_group" (
    id integer DEFAULT nextval('public."ir_ui_menu-res_group_id_seq"'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    "group" integer NOT NULL,
    menu integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "ir_ui_menu-res_group_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."ir_ui_menu-res_group" OWNER TO tryton;

--
-- Name: TABLE "ir_ui_menu-res_group"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."ir_ui_menu-res_group" IS 'UI Menu - Group';


--
-- Name: ir_ui_menu_favorite_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_ui_menu_favorite_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_ui_menu_favorite_id_seq OWNER TO tryton;

--
-- Name: ir_ui_menu_favorite; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_ui_menu_favorite (
    id integer DEFAULT nextval('public.ir_ui_menu_favorite_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    menu integer NOT NULL,
    sequence integer,
    "user" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_ui_menu_favorite_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_ui_menu_favorite OWNER TO tryton;

--
-- Name: TABLE ir_ui_menu_favorite; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_ui_menu_favorite IS 'Menu Favorite';


--
-- Name: ir_ui_view_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_ui_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_ui_view_id_seq OWNER TO tryton;

--
-- Name: ir_ui_view; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_ui_view (
    id integer DEFAULT nextval('public.ir_ui_view_id_seq'::regclass) NOT NULL,
    model character varying,
    type character varying,
    data text,
    field_childs character varying,
    priority integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    domain character varying,
    inherit integer,
    module character varying,
    name character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_ui_view_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_ui_view OWNER TO tryton;

--
-- Name: TABLE ir_ui_view; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_ui_view IS 'View';


--
-- Name: ir_ui_view_search_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_ui_view_search_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_ui_view_search_id_seq OWNER TO tryton;

--
-- Name: ir_ui_view_search; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_ui_view_search (
    id integer DEFAULT nextval('public.ir_ui_view_search_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    domain character varying,
    model character varying NOT NULL,
    name character varying NOT NULL,
    "user" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_ui_view_search_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_ui_view_search OWNER TO tryton;

--
-- Name: TABLE ir_ui_view_search; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_ui_view_search IS 'View Search';


--
-- Name: ir_ui_view_tree_state_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_ui_view_tree_state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_ui_view_tree_state_id_seq OWNER TO tryton;

--
-- Name: ir_ui_view_tree_state; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_ui_view_tree_state (
    id integer DEFAULT nextval('public.ir_ui_view_tree_state_id_seq'::regclass) NOT NULL,
    child_name character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    domain character varying NOT NULL,
    model character varying NOT NULL,
    nodes text,
    selected_nodes text,
    "user" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_ui_view_tree_state_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_ui_view_tree_state OWNER TO tryton;

--
-- Name: TABLE ir_ui_view_tree_state; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_ui_view_tree_state IS 'View Tree State';


--
-- Name: ir_ui_view_tree_width_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.ir_ui_view_tree_width_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ir_ui_view_tree_width_id_seq OWNER TO tryton;

--
-- Name: ir_ui_view_tree_width; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.ir_ui_view_tree_width (
    id integer DEFAULT nextval('public.ir_ui_view_tree_width_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    field character varying NOT NULL,
    model character varying NOT NULL,
    "user" integer NOT NULL,
    width integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT ir_ui_view_tree_width_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.ir_ui_view_tree_width OWNER TO tryton;

--
-- Name: TABLE ir_ui_view_tree_width; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.ir_ui_view_tree_width IS 'View Tree Width';


--
-- Name: modelo-producto_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."modelo-producto_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."modelo-producto_id_seq" OWNER TO tryton;

--
-- Name: modelo-producto; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."modelo-producto" (
    id integer DEFAULT nextval('public."modelo-producto_id_seq"'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    modelo integer NOT NULL,
    producto integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "modelo-producto_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."modelo-producto" OWNER TO tryton;

--
-- Name: TABLE "modelo-producto"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."modelo-producto" IS 'modelo - producto';


--
-- Name: party_address_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_address_id_seq OWNER TO tryton;

--
-- Name: party_address; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_address (
    id integer DEFAULT nextval('public.party_address_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    city character varying,
    country integer,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying,
    party integer NOT NULL,
    party_name character varying,
    sequence integer,
    street text,
    subdivision integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    zip character varying,
    CONSTRAINT party_address_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.party_address OWNER TO tryton;

--
-- Name: TABLE party_address; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_address IS 'Address';


--
-- Name: party_address_format_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_address_format_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_address_format_id_seq OWNER TO tryton;

--
-- Name: party_address_format; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_address_format (
    id integer DEFAULT nextval('public.party_address_format_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    country_code character varying(2),
    create_date timestamp(6) without time zone,
    create_uid integer,
    format_ text NOT NULL,
    language_code character varying(2),
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT party_address_format_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.party_address_format OWNER TO tryton;

--
-- Name: TABLE party_address_format; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_address_format IS 'Address Format';


--
-- Name: party_address_subdivision_type_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_address_subdivision_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_address_subdivision_type_id_seq OWNER TO tryton;

--
-- Name: party_address_subdivision_type; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_address_subdivision_type (
    id integer DEFAULT nextval('public.party_address_subdivision_type_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    country_code character varying(2) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    types character varying,
    write_date timestamp(6) without time zone,
    write_uid integer
);


ALTER TABLE public.party_address_subdivision_type OWNER TO tryton;

--
-- Name: TABLE party_address_subdivision_type; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_address_subdivision_type IS 'Address Subdivision Type';


--
-- Name: party_category_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_category_id_seq OWNER TO tryton;

--
-- Name: party_category; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_category (
    id integer DEFAULT nextval('public.party_category_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying NOT NULL,
    parent integer,
    write_date timestamp(6) without time zone,
    write_uid integer
);


ALTER TABLE public.party_category OWNER TO tryton;

--
-- Name: TABLE party_category; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_category IS 'Category';


--
-- Name: party_category_rel_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_category_rel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_category_rel_id_seq OWNER TO tryton;

--
-- Name: party_category_rel; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_category_rel (
    id integer DEFAULT nextval('public.party_category_rel_id_seq'::regclass) NOT NULL,
    category integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    party integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT party_category_rel_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.party_category_rel OWNER TO tryton;

--
-- Name: TABLE party_category_rel; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_category_rel IS 'Party - Category';


--
-- Name: party_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_configuration_id_seq OWNER TO tryton;

--
-- Name: party_configuration; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_configuration (
    id integer DEFAULT nextval('public.party_configuration_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT party_configuration_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.party_configuration OWNER TO tryton;

--
-- Name: TABLE party_configuration; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_configuration IS 'Party Configuration';


--
-- Name: party_configuration_party_lang_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_configuration_party_lang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_configuration_party_lang_id_seq OWNER TO tryton;

--
-- Name: party_configuration_party_lang; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_configuration_party_lang (
    id integer DEFAULT nextval('public.party_configuration_party_lang_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    party_lang integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    company integer,
    CONSTRAINT party_configuration_party_lang_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.party_configuration_party_lang OWNER TO tryton;

--
-- Name: TABLE party_configuration_party_lang; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_configuration_party_lang IS 'Party Configuration Lang';


--
-- Name: party_configuration_party_sequence_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_configuration_party_sequence_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_configuration_party_sequence_id_seq OWNER TO tryton;

--
-- Name: party_configuration_party_sequence; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_configuration_party_sequence (
    id integer DEFAULT nextval('public.party_configuration_party_sequence_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    party_sequence integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT party_configuration_party_sequence_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.party_configuration_party_sequence OWNER TO tryton;

--
-- Name: TABLE party_configuration_party_sequence; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_configuration_party_sequence IS 'Party Configuration Sequence';


--
-- Name: party_contact_mechanism_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_contact_mechanism_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_contact_mechanism_id_seq OWNER TO tryton;

--
-- Name: party_contact_mechanism; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_contact_mechanism (
    id integer DEFAULT nextval('public.party_contact_mechanism_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    comment text,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying,
    party integer NOT NULL,
    sequence integer,
    type character varying NOT NULL,
    value character varying,
    value_compact character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT party_contact_mechanism_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.party_contact_mechanism OWNER TO tryton;

--
-- Name: TABLE party_contact_mechanism; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_contact_mechanism IS 'Contact Mechanism';


--
-- Name: party_identifier_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_identifier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_identifier_id_seq OWNER TO tryton;

--
-- Name: party_identifier; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_identifier (
    id integer DEFAULT nextval('public.party_identifier_id_seq'::regclass) NOT NULL,
    code character varying NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    party integer NOT NULL,
    sequence integer,
    type character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT party_identifier_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.party_identifier OWNER TO tryton;

--
-- Name: TABLE party_identifier; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_identifier IS 'Party Identifier';


--
-- Name: party_party_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_party_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_party_id_seq OWNER TO tryton;

--
-- Name: party_party; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_party (
    id integer DEFAULT nextval('public.party_party_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    code character varying NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying NOT NULL,
    replaced_by integer,
    write_date timestamp(6) without time zone,
    write_uid integer
);


ALTER TABLE public.party_party OWNER TO tryton;

--
-- Name: TABLE party_party; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_party IS 'Party';


--
-- Name: party_party_lang_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.party_party_lang_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.party_party_lang_id_seq OWNER TO tryton;

--
-- Name: party_party_lang; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.party_party_lang (
    id integer DEFAULT nextval('public.party_party_lang_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    lang integer,
    party integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    company integer,
    CONSTRAINT party_party_lang_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.party_party_lang OWNER TO tryton;

--
-- Name: TABLE party_party_lang; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.party_party_lang IS 'Party Lang';


--
-- Name: product_category_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.product_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_category_id_seq OWNER TO tryton;

--
-- Name: product_category; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.product_category (
    id integer DEFAULT nextval('public.product_category_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying NOT NULL,
    parent integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT product_category_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.product_category OWNER TO tryton;

--
-- Name: TABLE product_category; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.product_category IS 'Product Category';


--
-- Name: product_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.product_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_configuration_id_seq OWNER TO tryton;

--
-- Name: product_configuration; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.product_configuration (
    id integer DEFAULT nextval('public.product_configuration_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    product_sequence integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT product_configuration_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.product_configuration OWNER TO tryton;

--
-- Name: TABLE product_configuration; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.product_configuration IS 'Product Configuration';


--
-- Name: product_configuration_default_cost_price_method_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.product_configuration_default_cost_price_method_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_configuration_default_cost_price_method_id_seq OWNER TO tryton;

--
-- Name: product_configuration_default_cost_price_method; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.product_configuration_default_cost_price_method (
    id integer DEFAULT nextval('public.product_configuration_default_cost_price_method_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    default_cost_price_method character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT product_configuration_default_cost_price_method_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.product_configuration_default_cost_price_method OWNER TO tryton;

--
-- Name: TABLE product_configuration_default_cost_price_method; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.product_configuration_default_cost_price_method IS 'Product Configuration Default Cost Price Method';


--
-- Name: product_cost_price_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.product_cost_price_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_cost_price_id_seq OWNER TO tryton;

--
-- Name: product_cost_price; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.product_cost_price (
    id integer DEFAULT nextval('public.product_cost_price_id_seq'::regclass) NOT NULL,
    company integer,
    cost_price numeric,
    create_date timestamp(6) without time zone,
    create_uid integer,
    product integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT product_cost_price_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.product_cost_price OWNER TO tryton;

--
-- Name: TABLE product_cost_price; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.product_cost_price IS 'Product Cost Price';


--
-- Name: product_cost_price_method_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.product_cost_price_method_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_cost_price_method_id_seq OWNER TO tryton;

--
-- Name: product_cost_price_method; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.product_cost_price_method (
    id integer DEFAULT nextval('public.product_cost_price_method_id_seq'::regclass) NOT NULL,
    company integer,
    cost_price_method character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    template integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT product_cost_price_method_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.product_cost_price_method OWNER TO tryton;

--
-- Name: TABLE product_cost_price_method; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.product_cost_price_method IS 'Product Cost Price Method';


--
-- Name: product_identifier_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.product_identifier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_identifier_id_seq OWNER TO tryton;

--
-- Name: product_identifier; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.product_identifier (
    id integer DEFAULT nextval('public.product_identifier_id_seq'::regclass) NOT NULL,
    code character varying NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    product integer NOT NULL,
    sequence integer,
    type character varying,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT product_identifier_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.product_identifier OWNER TO tryton;

--
-- Name: TABLE product_identifier; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.product_identifier IS 'Product Identifier';


--
-- Name: product_list_price_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.product_list_price_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_list_price_id_seq OWNER TO tryton;

--
-- Name: product_list_price; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.product_list_price (
    id integer DEFAULT nextval('public.product_list_price_id_seq'::regclass) NOT NULL,
    company integer,
    create_date timestamp(6) without time zone,
    create_uid integer,
    list_price numeric,
    template integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT product_list_price_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.product_list_price OWNER TO tryton;

--
-- Name: TABLE product_list_price; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.product_list_price IS 'Product List Price';


--
-- Name: product_product_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.product_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_product_id_seq OWNER TO tryton;

--
-- Name: product_product; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.product_product (
    id integer DEFAULT nextval('public.product_product_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    code character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    description text,
    suffix_code character varying,
    template integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer
);


ALTER TABLE public.product_product OWNER TO tryton;

--
-- Name: TABLE product_product; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.product_product IS 'Product Variant';


--
-- Name: product_template_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.product_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_template_id_seq OWNER TO tryton;

--
-- Name: product_template; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.product_template (
    id integer DEFAULT nextval('public.product_template_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    code character varying,
    consumable boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    default_uom integer NOT NULL,
    name character varying NOT NULL,
    type character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT product_template_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.product_template OWNER TO tryton;

--
-- Name: TABLE product_template; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.product_template IS 'Product Template';


--
-- Name: product_template-product_category_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."product_template-product_category_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."product_template-product_category_id_seq" OWNER TO tryton;

--
-- Name: product_template-product_category; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."product_template-product_category" (
    id integer DEFAULT nextval('public."product_template-product_category_id_seq"'::regclass) NOT NULL,
    category integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    template integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "product_template-product_category_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."product_template-product_category" OWNER TO tryton;

--
-- Name: TABLE "product_template-product_category"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."product_template-product_category" IS 'Template - Category';


--
-- Name: product_uom_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.product_uom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_uom_id_seq OWNER TO tryton;

--
-- Name: product_uom; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.product_uom (
    id integer DEFAULT nextval('public.product_uom_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    category integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    digits integer NOT NULL,
    factor double precision NOT NULL,
    name character varying NOT NULL,
    rate double precision NOT NULL,
    rounding double precision NOT NULL,
    symbol character varying(10) NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT product_uom_id_positive CHECK ((id >= 0)),
    CONSTRAINT product_uom_non_zero_rate_factor CHECK (((rate <> (0)::double precision) OR (factor <> (0)::double precision)))
);


ALTER TABLE public.product_uom OWNER TO tryton;

--
-- Name: TABLE product_uom; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.product_uom IS 'Unit of Measure';


--
-- Name: product_uom_category_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.product_uom_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_uom_category_id_seq OWNER TO tryton;

--
-- Name: product_uom_category; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.product_uom_category (
    id integer DEFAULT nextval('public.product_uom_category_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT product_uom_category_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.product_uom_category OWNER TO tryton;

--
-- Name: TABLE product_uom_category; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.product_uom_category IS 'Unit of Measure Category';


--
-- Name: res_group_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.res_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.res_group_id_seq OWNER TO tryton;

--
-- Name: res_group; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.res_group (
    id integer DEFAULT nextval('public.res_group_id_seq'::regclass) NOT NULL,
    name character varying NOT NULL,
    active boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT res_group_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.res_group OWNER TO tryton;

--
-- Name: TABLE res_group; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.res_group IS 'Group';


--
-- Name: res_user_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.res_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.res_user_id_seq OWNER TO tryton;

--
-- Name: res_user; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.res_user (
    id integer DEFAULT nextval('public.res_user_id_seq'::regclass) NOT NULL,
    name character varying,
    active boolean DEFAULT false,
    login character varying NOT NULL,
    password character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    email character varying,
    language integer,
    menu integer NOT NULL,
    password_hash character varying,
    password_reset character varying,
    password_reset_expire timestamp(6) without time zone,
    signature text,
    write_date timestamp(6) without time zone,
    write_uid integer,
    company integer,
    employee integer,
    main_company integer,
    CONSTRAINT res_user_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.res_user OWNER TO tryton;

--
-- Name: TABLE res_user; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.res_user IS 'User';


--
-- Name: res_user-company_employee_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."res_user-company_employee_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."res_user-company_employee_id_seq" OWNER TO tryton;

--
-- Name: res_user-company_employee; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."res_user-company_employee" (
    id integer DEFAULT nextval('public."res_user-company_employee_id_seq"'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    employee integer NOT NULL,
    "user" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "res_user-company_employee_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."res_user-company_employee" OWNER TO tryton;

--
-- Name: TABLE "res_user-company_employee"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."res_user-company_employee" IS 'User - Employee';


--
-- Name: res_user-ir_action_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."res_user-ir_action_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."res_user-ir_action_id_seq" OWNER TO tryton;

--
-- Name: res_user-ir_action; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."res_user-ir_action" (
    id integer DEFAULT nextval('public."res_user-ir_action_id_seq"'::regclass) NOT NULL,
    action integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    "user" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "res_user-ir_action_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."res_user-ir_action" OWNER TO tryton;

--
-- Name: TABLE "res_user-ir_action"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."res_user-ir_action" IS 'User - Action';


--
-- Name: res_user-res_group_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public."res_user-res_group_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."res_user-res_group_id_seq" OWNER TO tryton;

--
-- Name: res_user-res_group; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public."res_user-res_group" (
    id integer DEFAULT nextval('public."res_user-res_group_id_seq"'::regclass) NOT NULL,
    "user" integer NOT NULL,
    "group" integer NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT "res_user-res_group_id_positive" CHECK ((id >= 0))
);


ALTER TABLE public."res_user-res_group" OWNER TO tryton;

--
-- Name: TABLE "res_user-res_group"; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public."res_user-res_group" IS 'User - Group';


--
-- Name: res_user_application_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.res_user_application_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.res_user_application_id_seq OWNER TO tryton;

--
-- Name: res_user_application; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.res_user_application (
    id integer DEFAULT nextval('public.res_user_application_id_seq'::regclass) NOT NULL,
    application character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    key character varying NOT NULL,
    state character varying,
    "user" integer,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT res_user_application_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.res_user_application OWNER TO tryton;

--
-- Name: TABLE res_user_application; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.res_user_application IS 'User Application';


--
-- Name: res_user_login_attempt_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.res_user_login_attempt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.res_user_login_attempt_id_seq OWNER TO tryton;

--
-- Name: res_user_login_attempt; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.res_user_login_attempt (
    id integer DEFAULT nextval('public.res_user_login_attempt_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    ip_address character varying,
    ip_network character varying,
    login character varying(512),
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT res_user_login_attempt_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.res_user_login_attempt OWNER TO tryton;

--
-- Name: TABLE res_user_login_attempt; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.res_user_login_attempt IS 'Login Attempt

    This class is separated from the res.user one in order to prevent locking
    the res.user table when in a long running process.
    ';


--
-- Name: res_user_warning_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.res_user_warning_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.res_user_warning_id_seq OWNER TO tryton;

--
-- Name: res_user_warning; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.res_user_warning (
    id integer DEFAULT nextval('public.res_user_warning_id_seq'::regclass) NOT NULL,
    always boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying NOT NULL,
    "user" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT res_user_warning_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.res_user_warning OWNER TO tryton;

--
-- Name: TABLE res_user_warning; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.res_user_warning IS 'User Warning';


--
-- Name: taller_coche_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.taller_coche_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taller_coche_id_seq OWNER TO tryton;

--
-- Name: taller_coche; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.taller_coche (
    id integer DEFAULT nextval('public.taller_coche_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    fecha_baja date,
    fecha_matriculacion date,
    marca integer,
    matricula character varying NOT NULL,
    modelo integer,
    precio integer,
    propietario integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT taller_coche_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.taller_coche OWNER TO tryton;

--
-- Name: TABLE taller_coche; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.taller_coche IS 'Coche';


--
-- Name: taller_marca_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.taller_marca_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taller_marca_id_seq OWNER TO tryton;

--
-- Name: taller_marca; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.taller_marca (
    id integer DEFAULT nextval('public.taller_marca_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    name character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT taller_marca_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.taller_marca OWNER TO tryton;

--
-- Name: TABLE taller_marca; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.taller_marca IS 'Marca';


--
-- Name: taller_modelo_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.taller_modelo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.taller_modelo_id_seq OWNER TO tryton;

--
-- Name: taller_modelo; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.taller_modelo (
    id integer DEFAULT nextval('public.taller_modelo_id_seq'::regclass) NOT NULL,
    caballos integer,
    combustible character varying,
    create_date timestamp(6) without time zone,
    create_uid integer,
    fecha_lanz date,
    marca integer NOT NULL,
    modelo character varying NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    "precioMod" integer,
    "productosDelModelo" integer,
    CONSTRAINT taller_modelo_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.taller_modelo OWNER TO tryton;

--
-- Name: TABLE taller_modelo; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.taller_modelo IS 'Modelo';


--
-- Name: web_user_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.web_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.web_user_id_seq OWNER TO tryton;

--
-- Name: web_user; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.web_user (
    id integer DEFAULT nextval('public.web_user_id_seq'::regclass) NOT NULL,
    active boolean DEFAULT false,
    create_date timestamp(6) without time zone,
    create_uid integer,
    email character varying,
    email_token character varying,
    email_valid boolean DEFAULT false,
    party integer,
    password_hash character varying,
    reset_password_token character varying,
    reset_password_token_expire timestamp(6) without time zone,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT web_user_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.web_user OWNER TO tryton;

--
-- Name: TABLE web_user; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.web_user IS 'Web User';


--
-- Name: web_user_authenticate_attempt_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.web_user_authenticate_attempt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.web_user_authenticate_attempt_id_seq OWNER TO tryton;

--
-- Name: web_user_authenticate_attempt; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.web_user_authenticate_attempt (
    id integer DEFAULT nextval('public.web_user_authenticate_attempt_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    ip_address character varying,
    ip_network character varying,
    login character varying(512),
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT web_user_authenticate_attempt_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.web_user_authenticate_attempt OWNER TO tryton;

--
-- Name: TABLE web_user_authenticate_attempt; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.web_user_authenticate_attempt IS 'Web User Authenticate Attempt';


--
-- Name: web_user_session_id_seq; Type: SEQUENCE; Schema: public; Owner: tryton
--

CREATE SEQUENCE public.web_user_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.web_user_session_id_seq OWNER TO tryton;

--
-- Name: web_user_session; Type: TABLE; Schema: public; Owner: tryton
--

CREATE TABLE public.web_user_session (
    id integer DEFAULT nextval('public.web_user_session_id_seq'::regclass) NOT NULL,
    create_date timestamp(6) without time zone,
    create_uid integer,
    key character varying NOT NULL,
    "user" integer NOT NULL,
    write_date timestamp(6) without time zone,
    write_uid integer,
    CONSTRAINT web_user_session_id_positive CHECK ((id >= 0))
);


ALTER TABLE public.web_user_session OWNER TO tryton;

--
-- Name: TABLE web_user_session; Type: COMMENT; Schema: public; Owner: tryton
--

COMMENT ON TABLE public.web_user_session IS 'Web User Session';


--
-- Data for Name: coche-producto; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."coche-producto" (id, coche, create_date, create_uid, producto, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: company_company; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.company_company (id, create_date, create_uid, currency, footer, header, parent, party, timezone, write_date, write_uid) FROM stdin;
1	2020-09-10 09:02:29.489296	1	1			\N	4	\N	\N	\N
\.


--
-- Data for Name: company_employee; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.company_employee (id, company, create_date, create_uid, end_date, party, start_date, supervisor, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: country_country; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.country_country (id, active, code, code3, code_numeric, create_date, create_uid, name, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: country_subdivision; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.country_subdivision (id, active, code, country, create_date, create_uid, name, parent, type, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: country_zip; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.country_zip (id, city, country, create_date, create_uid, subdivision, write_date, write_uid, zip) FROM stdin;
\.


--
-- Data for Name: cron_company_rel; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.cron_company_rel (id, company, create_date, create_uid, cron, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: currency_currency; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.currency_currency (id, active, code, create_date, create_uid, digits, name, numeric_code, rounding, symbol, write_date, write_uid) FROM stdin;
1	t	[]	2020-09-10 09:01:59.038499	1	2	euro		0.01	€	\N	\N
\.


--
-- Data for Name: currency_currency_rate; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.currency_currency_rate (id, create_date, create_uid, currency, date, rate, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_action; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_action (id, active, create_date, create_uid, icon, name, type, usage, write_date, write_uid) FROM stdin;
1	t	2020-09-01 08:49:55.946968	0	\N	Icons	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
2	t	2020-09-01 08:49:55.946968	0	\N	Menu	ir.action.act_window	menu	2020-09-01 08:49:55.946968	0
3	t	2020-09-01 08:49:55.946968	0	\N	Menu	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
4	t	2020-09-01 08:49:55.946968	0	\N	Show View	ir.action.wizard	\N	2020-09-01 08:49:55.946968	0
5	t	2020-09-01 08:49:55.946968	0	\N	Views	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
6	t	2020-09-01 08:49:55.946968	0	\N	View Tree Width	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
7	t	2020-09-01 08:49:55.946968	0	\N	Tree State	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
8	t	2020-09-01 08:49:55.946968	0	\N	View Search	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
9	t	2020-09-01 08:49:55.946968	0	\N	Actions	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
10	t	2020-09-01 08:49:55.946968	0	\N	Reports	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
11	t	2020-09-01 08:49:55.946968	0	\N	Window Actions	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
12	t	2020-09-01 08:49:55.946968	0	\N	Wizards	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
13	t	2020-09-01 08:49:55.946968	0	\N	URLs	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
14	t	2020-09-01 08:49:55.946968	0	\N	Models	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
15	t	2020-09-01 08:49:55.946968	0	\N	Fields	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
16	t	2020-09-01 08:49:55.946968	0	\N	Models Access	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
17	t	2020-09-01 08:49:55.946968	0	\N	Access	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
18	t	2020-09-01 08:49:55.946968	0	\N	Fields Access	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
19	t	2020-09-01 08:49:55.946968	0	\N	Access	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
20	t	2020-09-01 08:49:55.946968	0	\N	Graph	ir.action.report	\N	2020-09-01 08:49:55.946968	0
21	t	2020-09-01 08:49:55.946968	0	\N	Graph	ir.action.wizard	\N	2020-09-01 08:49:55.946968	0
22	t	2020-09-01 08:49:55.946968	0	\N	Workflow Graph	ir.action.report	\N	2020-09-01 08:49:55.946968	0
23	t	2020-09-01 08:49:55.946968	0	\N	Buttons	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
24	t	2020-09-01 08:49:55.946968	0	\N	Clicks	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
25	t	2020-09-01 08:49:55.946968	0	\N	Data	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
26	t	2020-09-01 08:49:55.946968	0	\N	Sequences	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
27	t	2020-09-01 08:49:55.946968	0	\N	Sequences Strict	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
28	t	2020-09-01 08:49:55.946968	0	\N	Sequence Types	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
29	t	2020-09-01 08:49:55.946968	0	\N	Attachments	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
30	t	2020-09-01 08:49:55.946968	0	\N	Notes	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
31	t	2020-09-01 08:49:55.946968	0	\N	Scheduled Actions	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
32	t	2020-09-01 08:49:55.946968	0	\N	Languages	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
33	t	2020-09-01 08:49:55.946968	0	\N	Configure Languages	ir.action.wizard	\N	2020-09-01 08:49:55.946968	0
34	t	2020-09-01 08:49:55.946968	0	\N	Translations	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
35	t	2020-09-01 08:49:55.946968	0	\N	Translations	ir.action.wizard	\N	2020-09-01 08:49:55.946968	0
36	t	2020-09-01 08:49:55.946968	0	\N	Translations	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
37	t	2020-09-01 08:49:55.946968	0	\N	Set Translations	ir.action.wizard	\N	2020-09-01 08:49:55.946968	0
38	t	2020-09-01 08:49:55.946968	0	\N	Clean Translations	ir.action.wizard	\N	2020-09-01 08:49:55.946968	0
39	t	2020-09-01 08:49:55.946968	0	\N	Synchronize Translations	ir.action.wizard	\N	2020-09-01 08:49:55.946968	0
40	t	2020-09-01 08:49:55.946968	0	\N	Export Translations	ir.action.wizard	\N	2020-09-01 08:49:55.946968	0
41	t	2020-09-01 08:49:55.946968	0	\N	Exports	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
42	t	2020-09-01 08:49:55.946968	0	\N	Record Rules	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
43	t	2020-09-01 08:49:55.946968	0	\N	Modules	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
44	t	2020-09-01 08:49:55.946968	0	\N	Config Wizard Items	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
58	t	2020-09-02 07:24:24.586225	0	\N	Parties by Category	ir.action.act_window	\N	2020-09-02 07:24:24.586225	0
45	t	2020-09-01 08:49:55.946968	0	\N	Module Configuration	ir.action.wizard	\N	2020-09-01 08:49:55.946968	0
46	t	2020-09-01 08:49:55.946968	0	\N	Perform Pending Activation/Upgrade	ir.action.wizard	\N	2020-09-01 08:49:55.946968	0
59	t	2020-09-02 07:24:24.586225	0	\N	Labels	ir.action.report	\N	2020-09-02 07:24:24.586225	0
47	t	2020-09-01 08:49:55.946968	0	\N	Configure Modules	ir.action.wizard	\N	2020-09-01 08:49:55.946968	0
48	t	2020-09-01 08:49:55.946968	0	\N	Triggers	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
60	t	2020-09-02 07:24:24.586225	0	\N	Check VIES	ir.action.wizard	\N	2020-09-02 07:24:24.586225	0
49	t	2020-09-01 08:49:55.946968	0	\N	Message	ir.action.act_window	\N	2020-09-01 08:49:55.946968	0
61	t	2020-09-02 07:24:24.586225	0	\N	Replace	ir.action.wizard	\N	2020-09-02 07:24:24.586225	0
50	t	2020-09-01 08:50:28.180942	0	\N	Groups	ir.action.act_window	\N	2020-09-01 08:50:28.180942	0
62	t	2020-09-02 07:24:24.586225	0	\N	Erase	ir.action.wizard	\N	2020-09-02 07:24:24.586225	0
51	t	2020-09-01 08:50:28.180942	0	\N	Users	ir.action.act_window	\N	2020-09-01 08:50:28.180942	0
52	t	2020-09-01 08:50:28.180942	0	\N	Configure Users	ir.action.wizard	\N	2020-09-01 08:50:28.180942	0
63	t	2020-09-02 07:24:24.586225	0	\N	Categories	ir.action.act_window	\N	2020-09-02 07:24:24.586225	0
53	t	2020-09-01 08:50:28.180942	0	\N	Reset Password	ir.action.report	\N	2020-09-01 08:50:28.180942	0
54	t	2020-09-02 07:24:22.107384	0	\N	Countries	ir.action.act_window	\N	2020-09-02 07:24:22.107384	0
64	t	2020-09-02 07:24:24.586225	0	\N	Categories	ir.action.act_window	\N	2020-09-02 07:24:24.586225	0
55	t	2020-09-02 07:24:22.107384	0	\N	Zip	ir.action.act_window	\N	2020-09-02 07:24:22.107384	0
65	t	2020-09-02 07:24:24.586225	0	\N	Addresses	ir.action.act_window	\N	2020-09-02 07:24:24.586225	0
56	t	2020-09-02 07:24:23.639802	0	\N	Currencies	ir.action.act_window	\N	2020-09-02 07:24:23.639802	0
57	t	2020-09-02 07:24:24.586225	0	\N	Parties	ir.action.act_window	\N	2020-09-02 07:24:24.586225	0
66	t	2020-09-02 07:24:24.586225	0	\N	Address Formats	ir.action.act_window	\N	2020-09-02 07:24:24.586225	0
67	t	2020-09-02 07:24:24.586225	0	\N	Address Subdivision Types	ir.action.act_window	\N	2020-09-02 07:24:24.586225	0
68	t	2020-09-02 07:24:24.586225	0	\N	Contact Mechanisms	ir.action.act_window	\N	2020-09-02 07:24:24.586225	0
69	t	2020-09-02 07:24:24.586225	0	\N	Party Configuration	ir.action.act_window	\N	2020-09-02 07:24:24.586225	0
83	t	2020-09-02 07:24:32.977941	0	\N	Categories of Unit of Measure	ir.action.act_window	\N	2020-09-02 07:24:32.977941	0
70	t	2020-09-02 07:24:29.514826	0	\N	Companies	ir.action.act_window	\N	2020-09-02 07:24:29.514826	0
88	t	2020-09-08 08:59:28.457176	0	\N	 lista Coches 	ir.action.act_window	\N	2020-09-08 08:59:28.457176	0
71	t	2020-09-02 07:24:29.514826	0	\N	Companies	ir.action.act_window	\N	2020-09-02 07:24:29.514826	0
84	t	2020-09-02 07:24:32.977941	0	\N	Product Configuration	ir.action.act_window	\N	2020-09-02 07:24:32.977941	0
72	t	2020-09-02 07:24:29.514826	0	\N	Configure Company	ir.action.wizard	\N	2020-09-02 07:24:29.514826	0
98	t	2020-09-16 08:36:14.823954	0	\N	Reset Password	ir.action.report	\N	2020-09-16 08:36:14.823954	0
85	t	2020-09-02 07:56:32.835869	0	\N	Marcas	ir.action.act_window	\N	2020-09-02 07:56:32.835869	0
73	t	2020-09-02 07:24:29.514826	0	\N	Employees	ir.action.act_window	\N	2020-09-02 07:24:29.514826	0
89	t	2020-09-09 07:20:33.036685	0	\N	 lista Coches productos 	ir.action.act_window	\N	2020-09-09 07:20:33.036685	0
74	t	2020-09-02 07:24:29.514826	0	\N	Supervised by	ir.action.act_window	\N	2020-09-02 07:24:29.514826	0
86	t	2020-09-02 08:45:11.63	0	\N	Modelos	ir.action.act_window	\N	2020-09-02 08:45:11.63	0
75	t	2020-09-02 07:24:29.514826	0	\N	Letter	ir.action.report	\N	2020-09-02 07:24:29.514826	0
76	t	2020-09-02 07:24:32.977941	0	\N	Products	ir.action.act_window	\N	2020-09-02 07:24:32.977941	0
77	t	2020-09-02 07:24:32.977941	0	\N	Product by Category	ir.action.act_window	\N	2020-09-02 07:24:32.977941	0
91	t	2020-09-09 08:30:29.70123	0	\N	Baja Transition	ir.action.wizard	\N	2020-09-09 08:30:29.70123	0
78	t	2020-09-02 07:24:32.977941	0	\N	Variants	ir.action.act_window	\N	2020-09-02 07:24:32.977941	0
79	t	2020-09-02 07:24:32.977941	0	\N	Variants	ir.action.act_window	\N	2020-09-02 07:24:32.977941	0
92	t	2020-09-10 08:14:35.515414	0	\N	Report_coche	ir.action.report	\N	2020-09-10 08:14:35.515414	0
80	t	2020-09-02 07:24:32.977941	0	\N	Categories	ir.action.act_window	\N	2020-09-02 07:24:32.977941	0
81	t	2020-09-02 07:24:32.977941	0	\N	Categories	ir.action.act_window	\N	2020-09-02 07:24:32.977941	0
82	t	2020-09-02 07:24:32.977941	0	\N	Units of Measure	ir.action.act_window	\N	2020-09-02 07:24:32.977941	0
95	t	2020-09-14 06:53:25.827367	0	\N	Report_lista_coches	ir.action.report	\N	2020-09-14 06:53:25.827367	0
87	t	2020-09-02 11:14:57.919973	0	\N	Coches	ir.action.act_window	\N	2020-09-02 11:14:57.919973	0
96	t	2020-09-16 08:36:14.823954	0	\N	Web Users	ir.action.act_window	\N	2020-09-16 08:36:14.823954	0
97	t	2020-09-16 08:36:14.823954	0	\N	Email Validation	ir.action.report	\N	2020-09-16 08:36:14.823954	0
\.


--
-- Data for Name: ir_action-res_group; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."ir_action-res_group" (id, action, create_date, create_uid, "group", write_date, write_uid) FROM stdin;
1	46	2020-09-01 08:50:28.180942	0	1	\N	\N
2	39	2020-09-01 08:50:28.180942	0	1	\N	\N
3	40	2020-09-01 08:50:28.180942	0	1	\N	\N
4	33	2020-09-01 08:50:28.180942	0	1	\N	\N
5	61	2020-09-02 07:24:24.586225	0	3	\N	\N
6	62	2020-09-02 07:24:24.586225	0	3	\N	\N
\.


--
-- Data for Name: ir_action_act_window; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_action_act_window (id, action, context, context_domain, context_model, create_date, create_uid, domain, "limit", "order", res_model, search_value, write_date, write_uid) FROM stdin;
1	1	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.ui.icon	[]	2020-09-01 08:49:55.946968	0
2	2	{}	\N	\N	2020-09-01 08:49:55.946968	0	[["parent", "=", null]]	\N	\N	ir.ui.menu	[]	2020-09-01 08:49:55.946968	0
83	83	{}	\N	\N	2020-09-02 07:24:32.977941	0	\N	\N	\N	product.uom.category	[]	2020-09-02 07:24:32.977941	0
3	3	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.ui.menu	[]	2020-09-01 08:49:55.946968	0
5	5	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.ui.view	[]	2020-09-01 08:49:55.946968	0
6	6	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.ui.view_tree_width	[]	2020-09-01 08:49:55.946968	0
84	84	{}	\N	\N	2020-09-02 07:24:32.977941	0	\N	\N	\N	product.configuration	[]	2020-09-02 07:24:32.977941	0
7	7	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.ui.view_tree_state	[]	2020-09-01 08:49:55.946968	0
8	8	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.ui.view_search	[]	2020-09-01 08:49:55.946968	0
9	9	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.action	[]	2020-09-01 08:49:55.946968	0
10	10	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.action.report	[]	2020-09-01 08:49:55.946968	0
11	11	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.action.act_window	[]	2020-09-01 08:49:55.946968	0
12	12	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.action.wizard	[]	2020-09-01 08:49:55.946968	0
13	13	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.action.url	[]	2020-09-01 08:49:55.946968	0
14	14	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.model	[]	2020-09-01 08:49:55.946968	0
15	15	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.model.field	[]	2020-09-01 08:49:55.946968	0
16	16	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.model.access	[]	2020-09-01 08:49:55.946968	0
17	17	{}	\N	\N	2020-09-01 08:49:55.946968	0	[{"__class__": "If", "c": {"__class__": "Equal", "s1": {"__class__": "Eval", "d": [], "v": "active_ids"}, "s2": [{"__class__": "Eval", "d": "", "v": "active_id"}]}, "e": ["model", "in", {"__class__": "Eval", "d": "", "v": "active_ids"}], "t": ["model", "=", {"__class__": "Eval", "d": "", "v": "active_id"}]}]	\N	\N	ir.model.access	[]	2020-09-01 08:49:55.946968	0
18	18	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.model.field.access	[]	2020-09-01 08:49:55.946968	0
19	19	{}	\N	\N	2020-09-01 08:49:55.946968	0	[{"__class__": "If", "c": {"__class__": "Equal", "s1": {"__class__": "Eval", "d": [], "v": "active_ids"}, "s2": [{"__class__": "Eval", "d": "", "v": "active_id"}]}, "e": ["field", "in", {"__class__": "Eval", "d": "", "v": "active_ids"}], "t": ["field", "=", {"__class__": "Eval", "d": "", "v": "active_id"}]}]	\N	\N	ir.model.field.access	[]	2020-09-01 08:49:55.946968	0
23	23	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.model.button	[]	2020-09-01 08:49:55.946968	0
24	24	{}	\N	\N	2020-09-01 08:49:55.946968	0	[{"__class__": "If", "c": {"__class__": "Equal", "s1": {"__class__": "Eval", "d": [], "v": "active_ids"}, "s2": [{"__class__": "Eval", "d": "", "v": "active_id"}]}, "e": ["button", "in", {"__class__": "Eval", "d": "", "v": "active_ids"}], "t": ["button", "=", {"__class__": "Eval", "d": "", "v": "active_id"}]}]	\N	\N	ir.model.button.click	[]	2020-09-01 08:49:55.946968	0
25	25	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.model.data	[]	2020-09-01 08:49:55.946968	0
74	74	{}	\N	\N	2020-09-02 07:24:29.514826	0	[{"__class__": "If", "c": {"__class__": "Equal", "s1": {"__class__": "Eval", "d": [], "v": "active_ids"}, "s2": [{"__class__": "Eval", "d": "", "v": "active_id"}]}, "e": ["supervisor", "in", {"__class__": "Eval", "d": "", "v": "active_ids"}], "t": ["supervisor", "=", {"__class__": "Eval", "d": "", "v": "active_id"}]}]	\N	\N	company.employee	[]	2020-09-02 07:24:29.514826	0
26	26	{"active_test": false}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.sequence	[]	2020-09-01 08:49:55.946968	0
63	63	{}	\N	\N	2020-09-02 07:24:24.586225	0	[["parent", "=", null]]	\N	\N	party.category	[]	2020-09-02 07:24:24.586225	0
27	27	{"active_test": false}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.sequence.strict	[]	2020-09-01 08:49:55.946968	0
28	28	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.sequence.type	[]	2020-09-01 08:49:55.946968	0
29	29	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.attachment	[]	2020-09-01 08:49:55.946968	0
64	64	{}	\N	\N	2020-09-02 07:24:24.586225	0	\N	\N	\N	party.category	[]	2020-09-02 07:24:24.586225	0
30	30	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.note	[]	2020-09-01 08:49:55.946968	0
31	31		\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.cron	[]	2020-09-01 08:49:55.946968	0
32	32	{"active_test": false}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.lang	[]	2020-09-01 08:49:55.946968	0
65	65	{}	\N	\N	2020-09-02 07:24:24.586225	0	\N	\N	\N	party.address	[]	2020-09-02 07:24:24.586225	0
34	34	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.translation	[]	2020-09-01 08:49:55.946968	0
36	36	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.translation	[]	2020-09-01 08:49:55.946968	0
76	76	{}	\N	\N	2020-09-02 07:24:32.977941	0	\N	\N	\N	product.template	[]	2020-09-02 07:24:32.977941	0
41	41	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.export	[]	2020-09-01 08:49:55.946968	0
66	66	{}	\N	\N	2020-09-02 07:24:24.586225	0	\N	\N	\N	party.address.format	[]	2020-09-02 07:24:24.586225	0
42	42	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.rule.group	[]	2020-09-01 08:49:55.946968	0
43	43	{}	\N	\N	2020-09-01 08:49:55.946968	0	[["name", "!=", "tests"]]	\N	\N	ir.module	[]	2020-09-01 08:49:55.946968	0
44	44	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.module.config_wizard.item	[]	2020-09-01 08:49:55.946968	0
67	67	{}	\N	\N	2020-09-02 07:24:24.586225	0	\N	\N	\N	party.address.subdivision_type	[]	2020-09-02 07:24:24.586225	0
48	48	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.trigger	[]	2020-09-01 08:49:55.946968	0
49	49	{}	\N	\N	2020-09-01 08:49:55.946968	0	\N	\N	\N	ir.message	[]	2020-09-01 08:49:55.946968	0
88	88	{}	\N	\N	2020-09-08 08:59:28.457176	0	[{"__class__": "If", "c": {"__class__": "Equal", "s1": {"__class__": "Eval", "d": [], "v": "active_ids"}, "s2": [{"__class__": "Eval", "d": "", "v": "active_id"}]}, "e": ["propietario", "in", {"__class__": "Eval", "d": "", "v": "active_ids"}], "t": ["propietario", "=", {"__class__": "Eval", "d": "", "v": "active_id"}]}]	\N	\N	taller.coche	[]	2020-09-08 08:59:28.457176	0
50	50	{}	\N	\N	2020-09-01 08:50:28.180942	0	\N	\N	\N	res.group	[]	2020-09-01 08:50:28.180942	0
68	68	{}	\N	\N	2020-09-02 07:24:24.586225	0	\N	\N	\N	party.contact_mechanism	[]	2020-09-02 07:24:24.586225	0
51	51	{}	\N	\N	2020-09-01 08:50:28.180942	0	\N	\N	\N	res.user	[]	2020-09-01 08:50:28.180942	0
54	54	{}	\N	\N	2020-09-02 07:24:22.107384	0	\N	\N	\N	country.country	[]	2020-09-02 07:24:22.107384	0
77	77	{"categories": [{"__class__": "Eval", "d": "", "v": "active_id"}]}	\N	\N	2020-09-02 07:24:32.977941	0	[["categories_all", "child_of", [{"__class__": "Eval", "d": "", "v": "active_id"}], "parent"]]	\N	\N	product.template	[]	2020-09-02 07:24:32.977941	0
55	55	{}	\N	\N	2020-09-02 07:24:22.107384	0	[["country", "in", {"__class__": "Eval", "d": "", "v": "active_ids"}]]	\N	\N	country.zip	[]	2020-09-02 07:24:22.107384	0
69	69	{}	\N	\N	2020-09-02 07:24:24.586225	0	\N	\N	\N	party.configuration	[]	2020-09-02 07:24:24.586225	0
56	56	{}	\N	\N	2020-09-02 07:24:23.639802	0	\N	\N	\N	currency.currency	[]	2020-09-02 07:24:23.639802	0
57	57	{}	\N	\N	2020-09-02 07:24:24.586225	0	\N	\N	\N	party.party	[]	2020-09-02 07:24:24.586225	0
85	85	{}	\N	\N	2020-09-02 07:56:32.835869	0	\N	\N	\N	taller.marca	[]	2020-09-02 07:56:32.835869	0
58	58	{"categories": [{"__class__": "Eval", "d": "", "v": "active_id"}]}	\N	\N	2020-09-02 07:24:24.586225	0	[["categories", "child_of", [{"__class__": "Eval", "d": "", "v": "active_id"}], "parent"]]	\N	\N	party.party	[]	2020-09-02 07:24:24.586225	0
70	70	{}	\N	\N	2020-09-02 07:24:29.514826	0	[["parent", "=", null]]	\N	\N	company.company	[]	2020-09-02 07:24:29.514826	0
79	79	{}	\N	\N	2020-09-02 07:24:32.977941	0	[{"__class__": "If", "c": {"__class__": "Equal", "s1": {"__class__": "Eval", "d": [], "v": "active_ids"}, "s2": [{"__class__": "Eval", "d": "", "v": "active_id"}]}, "e": ["template", "in", {"__class__": "Eval", "d": "", "v": "active_ids"}], "t": ["template", "=", {"__class__": "Eval", "d": "", "v": "active_id"}]}]	\N	\N	product.product	[]	2020-09-02 07:24:32.977941	0
71	71	{}	\N	\N	2020-09-02 07:24:29.514826	0	\N	\N	\N	company.company	[]	2020-09-02 07:24:29.514826	0
73	73	{}	\N	\N	2020-09-02 07:24:29.514826	0	\N	\N	\N	company.employee	[]	2020-09-02 07:24:29.514826	0
78	78	{}	\N	\N	2020-09-02 07:24:32.977941	0	\N	\N	\N	product.product	[]	2020-09-02 07:24:32.977941	0
80	80	{}	\N	\N	2020-09-02 07:24:32.977941	0	[["parent", "=", null]]	\N	\N	product.category	[]	2020-09-02 07:24:32.977941	0
86	86	{}	\N	\N	2020-09-02 08:45:11.63	0	\N	\N	\N	taller.modelo	[]	2020-09-02 08:45:11.63	0
81	81	{}	\N	\N	2020-09-02 07:24:32.977941	0	\N	\N	\N	product.category	[]	2020-09-02 07:24:32.977941	0
82	82	{}	\N	\N	2020-09-02 07:24:32.977941	0	\N	\N	\N	product.uom	[]	2020-09-02 07:24:32.977941	0
87	87	{}	\N	\N	2020-09-02 11:14:57.919973	0	\N	\N	\N	taller.coche	[]	2020-09-02 11:14:57.919973	0
89	89	{}	\N	\N	2020-09-09 07:20:33.036685	0	[{"__class__": "If", "c": {"__class__": "Equal", "s1": {"__class__": "Eval", "d": [], "v": "active_ids"}, "s2": [{"__class__": "Eval", "d": "", "v": "active_id"}]}, "e": ["modelo.productosDelModelo", "in", {"__class__": "Eval", "d": "", "v": "active_ids"}], "t": ["modelo.productosDelModelo", "=", {"__class__": "Eval", "d": "", "v": "active_id"}]}]	\N	\N	taller.coche	[]	2020-09-09 07:20:33.036685	0
96	96	{}	\N	\N	2020-09-16 08:36:14.823954	0	\N	\N	\N	web.user	[]	2020-09-16 08:36:14.823954	0
\.


--
-- Data for Name: ir_action_act_window_domain; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_action_act_window_domain (id, act_window, active, count, create_date, create_uid, domain, name, sequence, write_date, write_uid) FROM stdin;
1	25	t	t	2020-09-01 08:49:55.946968	0	[["out_of_sync", "=", true]]	Out of Sync	10	\N	\N
2	25	t	f	2020-09-01 08:49:55.946968	0		All	9999	\N	\N
3	34	t	f	2020-09-01 08:49:55.946968	0	[["module", "!=", null]]	Modules	10	\N	\N
4	34	t	f	2020-09-01 08:49:55.946968	0	[["module", "=", null]]	Local	20	\N	\N
\.


--
-- Data for Name: ir_action_act_window_view; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_action_act_window_view (id, act_window, active, create_date, create_uid, sequence, view, write_date, write_uid) FROM stdin;
1	1	t	2020-09-01 08:49:55.946968	0	10	1	\N	\N
2	1	t	2020-09-01 08:49:55.946968	0	20	2	\N	\N
3	2	t	2020-09-01 08:49:55.946968	0	1	3	\N	\N
4	3	t	2020-09-01 08:49:55.946968	0	10	4	\N	\N
5	3	t	2020-09-01 08:49:55.946968	0	20	5	\N	\N
6	5	t	2020-09-01 08:49:55.946968	0	1	9	\N	\N
7	5	t	2020-09-01 08:49:55.946968	0	2	8	\N	\N
8	6	t	2020-09-01 08:49:55.946968	0	1	11	\N	\N
9	6	t	2020-09-01 08:49:55.946968	0	2	10	\N	\N
10	7	t	2020-09-01 08:49:55.946968	0	10	13	\N	\N
11	7	t	2020-09-01 08:49:55.946968	0	20	12	\N	\N
12	8	t	2020-09-01 08:49:55.946968	0	10	15	\N	\N
13	8	t	2020-09-01 08:49:55.946968	0	20	14	\N	\N
14	9	t	2020-09-01 08:49:55.946968	0	1	17	\N	\N
15	9	t	2020-09-01 08:49:55.946968	0	2	16	\N	\N
16	10	t	2020-09-01 08:49:55.946968	0	1	21	\N	\N
17	10	t	2020-09-01 08:49:55.946968	0	2	20	\N	\N
18	11	t	2020-09-01 08:49:55.946968	0	1	23	\N	\N
19	11	t	2020-09-01 08:49:55.946968	0	2	22	\N	\N
20	12	t	2020-09-01 08:49:55.946968	0	1	31	\N	\N
21	12	t	2020-09-01 08:49:55.946968	0	2	30	\N	\N
22	13	t	2020-09-01 08:49:55.946968	0	1	33	\N	\N
23	13	t	2020-09-01 08:49:55.946968	0	2	32	\N	\N
24	14	t	2020-09-01 08:49:55.946968	0	1	35	\N	\N
25	14	t	2020-09-01 08:49:55.946968	0	2	34	\N	\N
26	15	t	2020-09-01 08:49:55.946968	0	1	37	\N	\N
27	15	t	2020-09-01 08:49:55.946968	0	2	36	\N	\N
28	16	t	2020-09-01 08:49:55.946968	0	1	38	\N	\N
29	16	t	2020-09-01 08:49:55.946968	0	2	39	\N	\N
30	18	t	2020-09-01 08:49:55.946968	0	10	40	\N	\N
31	18	t	2020-09-01 08:49:55.946968	0	20	41	\N	\N
32	23	t	2020-09-01 08:49:55.946968	0	10	43	\N	\N
33	23	t	2020-09-01 08:49:55.946968	0	20	44	\N	\N
34	25	t	2020-09-01 08:49:55.946968	0	10	49	\N	\N
35	25	t	2020-09-01 08:49:55.946968	0	20	50	\N	\N
36	26	t	2020-09-01 08:49:55.946968	0	1	52	\N	\N
37	26	t	2020-09-01 08:49:55.946968	0	2	51	\N	\N
38	27	t	2020-09-01 08:49:55.946968	0	1	54	\N	\N
39	27	t	2020-09-01 08:49:55.946968	0	2	53	\N	\N
40	28	t	2020-09-01 08:49:55.946968	0	1	56	\N	\N
41	28	t	2020-09-01 08:49:55.946968	0	2	55	\N	\N
42	29	t	2020-09-01 08:49:55.946968	0	1	58	\N	\N
43	29	t	2020-09-01 08:49:55.946968	0	2	57	\N	\N
44	30	t	2020-09-01 08:49:55.946968	0	1	60	\N	\N
45	30	t	2020-09-01 08:49:55.946968	0	2	59	\N	\N
46	31	t	2020-09-01 08:49:55.946968	0	1	61	\N	\N
47	31	t	2020-09-01 08:49:55.946968	0	2	62	\N	\N
48	32	t	2020-09-01 08:49:55.946968	0	1	63	\N	\N
49	32	t	2020-09-01 08:49:55.946968	0	2	64	\N	\N
50	34	t	2020-09-01 08:49:55.946968	0	1	67	\N	\N
51	34	t	2020-09-01 08:49:55.946968	0	2	66	\N	\N
52	41	t	2020-09-01 08:49:55.946968	0	1	76	\N	\N
53	41	t	2020-09-01 08:49:55.946968	0	2	75	\N	\N
54	42	t	2020-09-01 08:49:55.946968	0	1	80	\N	\N
55	42	t	2020-09-01 08:49:55.946968	0	2	79	\N	\N
56	43	t	2020-09-01 08:49:55.946968	0	1	84	\N	\N
57	43	t	2020-09-01 08:49:55.946968	0	2	83	\N	\N
58	44	t	2020-09-01 08:49:55.946968	0	10	87	\N	\N
59	48	t	2020-09-01 08:49:55.946968	0	10	94	\N	\N
60	48	t	2020-09-01 08:49:55.946968	0	20	93	\N	\N
61	49	t	2020-09-01 08:49:55.946968	0	10	95	\N	\N
62	49	t	2020-09-01 08:49:55.946968	0	20	96	\N	\N
63	50	t	2020-09-01 08:50:28.180942	0	1	98	\N	\N
64	50	t	2020-09-01 08:50:28.180942	0	2	97	\N	\N
65	51	t	2020-09-01 08:50:28.180942	0	1	101	\N	\N
66	51	t	2020-09-01 08:50:28.180942	0	2	99	\N	\N
67	54	t	2020-09-02 07:24:22.107384	0	10	111	\N	\N
68	54	t	2020-09-02 07:24:22.107384	0	20	110	\N	\N
69	55	t	2020-09-02 07:24:22.107384	0	10	115	\N	\N
70	55	t	2020-09-02 07:24:22.107384	0	20	114	\N	\N
71	56	t	2020-09-02 07:24:23.639802	0	10	117	\N	\N
72	56	t	2020-09-02 07:24:23.639802	0	20	116	\N	\N
73	57	t	2020-09-02 07:24:24.586225	0	10	121	\N	\N
74	57	t	2020-09-02 07:24:24.586225	0	20	122	\N	\N
75	63	t	2020-09-02 07:24:24.586225	0	10	130	\N	\N
76	63	t	2020-09-02 07:24:24.586225	0	20	129	\N	\N
77	64	t	2020-09-02 07:24:24.586225	0	10	131	\N	\N
78	64	t	2020-09-02 07:24:24.586225	0	20	129	\N	\N
79	65	t	2020-09-02 07:24:24.586225	0	10	132	\N	\N
80	65	t	2020-09-02 07:24:24.586225	0	20	134	\N	\N
81	66	t	2020-09-02 07:24:24.586225	0	10	135	\N	\N
82	66	t	2020-09-02 07:24:24.586225	0	20	136	\N	\N
83	67	t	2020-09-02 07:24:24.586225	0	10	137	\N	\N
84	67	t	2020-09-02 07:24:24.586225	0	20	138	\N	\N
85	68	t	2020-09-02 07:24:24.586225	0	10	139	\N	\N
86	68	t	2020-09-02 07:24:24.586225	0	20	141	\N	\N
87	69	t	2020-09-02 07:24:24.586225	0	1	142	\N	\N
88	70	t	2020-09-02 07:24:29.514826	0	10	144	\N	\N
89	70	t	2020-09-02 07:24:29.514826	0	20	143	\N	\N
90	71	t	2020-09-02 07:24:29.514826	0	10	145	\N	\N
91	71	t	2020-09-02 07:24:29.514826	0	20	143	\N	\N
92	73	t	2020-09-02 07:24:29.514826	0	10	150	\N	\N
93	73	t	2020-09-02 07:24:29.514826	0	20	149	\N	\N
94	74	t	2020-09-02 07:24:29.514826	0	10	150	\N	\N
95	74	t	2020-09-02 07:24:29.514826	0	20	149	\N	\N
96	76	t	2020-09-02 07:24:32.977941	0	10	154	\N	\N
97	76	t	2020-09-02 07:24:32.977941	0	20	155	\N	\N
98	77	t	2020-09-02 07:24:32.977941	0	10	154	\N	\N
99	77	t	2020-09-02 07:24:32.977941	0	20	155	\N	\N
100	78	t	2020-09-02 07:24:32.977941	0	10	156	\N	\N
101	78	t	2020-09-02 07:24:32.977941	0	20	158	\N	\N
102	79	t	2020-09-02 07:24:32.977941	0	10	156	\N	\N
103	79	t	2020-09-02 07:24:32.977941	0	20	158	\N	\N
104	80	t	2020-09-02 07:24:32.977941	0	10	164	\N	\N
105	80	t	2020-09-02 07:24:32.977941	0	20	165	\N	\N
106	81	t	2020-09-02 07:24:32.977941	0	10	163	\N	\N
107	81	t	2020-09-02 07:24:32.977941	0	20	165	\N	\N
108	82	t	2020-09-02 07:24:32.977941	0	10	166	\N	\N
109	82	t	2020-09-02 07:24:32.977941	0	20	167	\N	\N
110	83	t	2020-09-02 07:24:32.977941	0	10	168	\N	\N
111	83	t	2020-09-02 07:24:32.977941	0	20	169	\N	\N
112	84	t	2020-09-02 07:24:32.977941	0	10	170	\N	\N
113	85	t	2020-09-02 07:56:32.835869	0	10	172	\N	\N
114	85	t	2020-09-02 07:56:32.835869	0	20	171	\N	\N
115	86	t	2020-09-02 08:45:11.63	0	10	174	\N	\N
116	86	t	2020-09-02 08:45:11.63	0	20	173	\N	\N
117	87	t	2020-09-02 11:14:57.919973	0	10	176	\N	\N
118	87	t	2020-09-02 11:14:57.919973	0	20	175	\N	\N
119	96	t	2020-09-16 08:36:14.823954	0	10	189	\N	\N
120	96	t	2020-09-16 08:36:14.823954	0	20	188	\N	\N
\.


--
-- Data for Name: ir_action_keyword; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_action_keyword (id, action, create_date, create_uid, keyword, model, write_date, write_uid) FROM stdin;
1	1	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,3	\N	\N
2	3	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,4	\N	\N
3	5	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,5	\N	\N
4	6	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,6	\N	\N
5	7	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,7	\N	\N
6	8	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,8	\N	\N
7	9	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,10	\N	\N
8	10	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,11	\N	\N
9	11	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,12	\N	\N
10	12	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,13	\N	\N
11	13	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,14	\N	\N
12	14	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,16	\N	\N
13	15	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,17	\N	\N
14	16	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,18	\N	\N
15	17	2020-09-01 08:49:55.946968	0	form_relate	ir.model,-1	\N	\N
16	18	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,19	\N	\N
17	19	2020-09-01 08:49:55.946968	0	form_relate	ir.model.field,-1	\N	\N
18	21	2020-09-01 08:49:55.946968	0	form_print	ir.model,-1	\N	\N
19	22	2020-09-01 08:49:55.946968	0	form_print	ir.model,-1	\N	\N
20	23	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,20	\N	\N
21	24	2020-09-01 08:49:55.946968	0	form_relate	ir.model.button,-1	\N	\N
22	25	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,21	\N	\N
23	26	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,23	\N	\N
24	27	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,24	\N	\N
25	28	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,25	\N	\N
26	29	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,26	\N	\N
27	30	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,27	\N	\N
28	31	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,29	\N	\N
29	32	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,31	\N	\N
30	34	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,32	\N	\N
31	35	2020-09-01 08:49:55.946968	0	form_relate	ir.action.report,-1	\N	\N
32	37	2020-09-01 08:49:55.946968	0	form_action	ir.action.report,-1	\N	\N
33	37	2020-09-01 08:49:55.946968	0	form_action	ir.ui.view,-1	\N	\N
34	37	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,33	\N	\N
35	38	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,34	\N	\N
36	39	2020-09-01 08:49:55.946968	0	form_action	ir.action.report,-1	\N	\N
37	39	2020-09-01 08:49:55.946968	0	form_action	ir.ui.view,-1	\N	\N
38	39	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,35	\N	\N
39	40	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,36	\N	\N
40	41	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,37	\N	\N
41	42	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,38	\N	\N
42	43	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,40	\N	\N
43	44	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,41	\N	\N
44	45	2020-09-01 08:49:55.946968	0	form_action	ir.module.config_wizard.item,-1	\N	\N
45	46	2020-09-01 08:49:55.946968	0	form_action	ir.module,-1	\N	\N
46	46	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,42	\N	\N
47	48	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,43	\N	\N
48	49	2020-09-01 08:49:55.946968	0	tree_open	ir.ui.menu,44	\N	\N
49	50	2020-09-01 08:50:28.180942	0	tree_open	ir.ui.menu,46	\N	\N
50	51	2020-09-01 08:50:28.180942	0	tree_open	ir.ui.menu,47	\N	\N
51	54	2020-09-02 07:24:22.107384	0	tree_open	ir.ui.menu,48	\N	\N
52	55	2020-09-02 07:24:22.107384	0	form_relate	country.country,-1	\N	\N
53	56	2020-09-02 07:24:23.639802	0	tree_open	ir.ui.menu,50	\N	\N
54	57	2020-09-02 07:24:24.586225	0	tree_open	ir.ui.menu,53	\N	\N
55	58	2020-09-02 07:24:24.586225	0	tree_open	party.category,-1	\N	\N
56	59	2020-09-02 07:24:24.586225	0	form_print	party.party,-1	\N	\N
57	60	2020-09-02 07:24:24.586225	0	form_action	party.party,-1	\N	\N
58	61	2020-09-02 07:24:24.586225	0	form_action	party.party,-1	\N	\N
59	62	2020-09-02 07:24:24.586225	0	form_action	party.party,-1	\N	\N
60	63	2020-09-02 07:24:24.586225	0	tree_open	ir.ui.menu,54	\N	\N
61	64	2020-09-02 07:24:24.586225	0	tree_open	ir.ui.menu,55	\N	\N
62	65	2020-09-02 07:24:24.586225	0	tree_open	ir.ui.menu,56	\N	\N
63	66	2020-09-02 07:24:24.586225	0	tree_open	ir.ui.menu,57	\N	\N
64	67	2020-09-02 07:24:24.586225	0	tree_open	ir.ui.menu,58	\N	\N
65	68	2020-09-02 07:24:24.586225	0	tree_open	ir.ui.menu,59	\N	\N
66	69	2020-09-02 07:24:24.586225	0	tree_open	ir.ui.menu,60	\N	\N
67	70	2020-09-02 07:24:29.514826	0	tree_open	ir.ui.menu,62	\N	\N
68	71	2020-09-02 07:24:29.514826	0	tree_open	ir.ui.menu,63	\N	\N
69	73	2020-09-02 07:24:29.514826	0	tree_open	ir.ui.menu,64	\N	\N
70	74	2020-09-02 07:24:29.514826	0	form_relate	company.employee,-1	\N	\N
71	75	2020-09-02 07:24:29.514826	0	form_print	party.party,-1	\N	\N
72	76	2020-09-02 07:24:32.977941	0	tree_open	ir.ui.menu,67	\N	\N
73	77	2020-09-02 07:24:32.977941	0	tree_open	product.category,-1	\N	\N
74	78	2020-09-02 07:24:32.977941	0	tree_open	ir.ui.menu,68	\N	\N
75	79	2020-09-02 07:24:32.977941	0	form_relate	product.template,-1	\N	\N
76	80	2020-09-02 07:24:32.977941	0	tree_open	ir.ui.menu,69	\N	\N
77	81	2020-09-02 07:24:32.977941	0	tree_open	ir.ui.menu,70	\N	\N
78	82	2020-09-02 07:24:32.977941	0	tree_open	ir.ui.menu,71	\N	\N
79	83	2020-09-02 07:24:32.977941	0	tree_open	ir.ui.menu,72	\N	\N
80	84	2020-09-02 07:24:32.977941	0	tree_open	ir.ui.menu,73	\N	\N
81	85	2020-09-02 07:56:32.835869	0	tree_open	ir.ui.menu,75	\N	\N
82	86	2020-09-02 08:45:11.63	0	tree_open	ir.ui.menu,76	\N	\N
83	87	2020-09-02 11:19:59.170481	0	tree_open	ir.ui.menu,77	\N	\N
84	88	2020-09-08 08:59:28.457176	0	form_relate	party.party,-1	\N	\N
85	89	2020-09-09 07:20:33.036685	0	form_relate	product.template,-1	\N	\N
87	91	2020-09-09 08:30:29.70123	0	form_action	taller.coche,-1	\N	\N
88	92	2020-09-10 08:14:35.515414	0	form_print	taller.coche,-1	\N	\N
89	95	2020-09-14 06:53:25.827367	0	form_print	taller.coche,-1	\N	\N
90	96	2020-09-16 08:36:14.823954	0	tree_open	ir.ui.menu,78	\N	\N
\.


--
-- Data for Name: ir_action_report; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_action_report (id, action, create_date, create_uid, direct_print, email, extension, model, module, report, report_content_custom, report_name, single, template_extension, translatable, write_date, write_uid) FROM stdin;
20	20	2020-09-01 08:49:55.946968	0	f	\N		ir.model	ir	\N	\N	ir.model.graph	f	odt	t	2020-09-01 08:49:55.946968	0
22	22	2020-09-01 08:49:55.946968	0	f	\N		ir.model	ir	\N	\N	ir.model.workflow_graph	f	odt	t	2020-09-01 08:49:55.946968	0
53	53	2020-09-01 08:50:28.180942	0	f	\N		res.user	res	res/email_reset_password.html	\N	res.user.email_reset_password	f	html	t	2020-09-01 08:50:28.180942	0
59	59	2020-09-02 07:24:24.586225	0	f	\N		party.party	party	party/label.fodt	\N	party.label	f	odt	t	2020-09-02 07:24:24.586225	0
75	75	2020-09-02 07:24:29.514826	0	f	\N		party.party	company	company/letter.fodt	\N	party.letter	f	odt	t	2020-09-02 07:24:29.514826	0
92	92	2020-09-10 08:14:35.515414	0	f	\N	pdf	taller.coche	taller	taller/reports/ficha_tecnica.odt	\N	ficha_tecnica_del_coche	f	odt	t	2020-09-10 08:14:35.515414	0
95	95	2020-09-14 06:53:25.827367	0	f	\N	xls	taller.coche	taller	taller/reports/lista_coches.ods	\N	Lista_de_mis_coches	f	ods	t	2020-09-14 07:21:43.519096	0
97	97	2020-09-16 08:36:14.823954	0	f	\N		web.user	web_user	web_user/email_validation.html	\N	web.user.email_validation	f	html	t	2020-09-16 08:36:14.823954	0
98	98	2020-09-16 08:36:14.823954	0	f	\N		web.user	web_user	web_user/email_reset_password.html	\N	web.user.email_reset_password	f	html	t	2020-09-16 08:36:14.823954	0
\.


--
-- Data for Name: ir_action_url; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_action_url (id, action, create_date, create_uid, url, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_action_wizard; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_action_wizard (id, action, create_date, create_uid, email, model, "window", wiz_name, write_date, write_uid) FROM stdin;
4	4	2020-09-01 08:49:55.946968	0	\N	\N	f	ir.ui.view.show	2020-09-01 08:49:55.946968	0
21	21	2020-09-01 08:49:55.946968	0	\N	ir.model	f	ir.model.print_model_graph	2020-09-01 08:49:55.946968	0
33	33	2020-09-01 08:49:55.946968	0	\N	\N	t	ir.lang.config	2020-09-01 08:49:55.946968	0
35	35	2020-09-01 08:49:55.946968	0	\N	ir.action.report	f	ir.translation.report	2020-09-01 08:49:55.946968	0
37	37	2020-09-01 08:49:55.946968	0	\N	\N	f	ir.translation.set	2020-09-01 08:49:55.946968	0
38	38	2020-09-01 08:49:55.946968	0	\N	\N	f	ir.translation.clean	2020-09-01 08:49:55.946968	0
39	39	2020-09-01 08:49:55.946968	0	\N	\N	f	ir.translation.update	2020-09-01 08:49:55.946968	0
40	40	2020-09-01 08:49:55.946968	0	\N	\N	f	ir.translation.export	2020-09-01 08:49:55.946968	0
45	45	2020-09-01 08:49:55.946968	0	\N	\N	t	ir.module.config_wizard	2020-09-01 08:49:55.946968	0
46	46	2020-09-01 08:49:55.946968	0	\N	\N	f	ir.module.activate_upgrade	2020-09-01 08:49:55.946968	0
47	47	2020-09-01 08:49:55.946968	0	\N	\N	f	ir.module.config	2020-09-01 08:49:55.946968	0
52	52	2020-09-01 08:50:28.180942	0	\N	\N	t	res.user.config	2020-09-01 08:50:28.180942	0
60	60	2020-09-02 07:24:24.586225	0	\N	party.party	f	party.check_vies	2020-09-02 07:24:24.586225	0
61	61	2020-09-02 07:24:24.586225	0	\N	party.party	f	party.replace	2020-09-02 07:24:24.586225	0
62	62	2020-09-02 07:24:24.586225	0	\N	party.party	f	party.erase	2020-09-02 07:24:24.586225	0
72	72	2020-09-02 07:24:29.514826	0	\N	\N	t	company.company.config	2020-09-02 07:24:29.514826	0
91	91	2020-09-09 08:30:29.70123	0	\N	\N	f	taller.coche.baja	2020-09-09 08:32:39.366492	0
\.


--
-- Data for Name: ir_attachment; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_attachment (id, copy_to_resources, create_date, create_uid, data, description, file_id, link, name, resource, type, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_cache; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_cache (id, name, "timestamp", create_date, create_uid, write_date, write_uid) FROM stdin;
16	ir_model_data.get_id	2020-09-07 13:54:57.457097	\N	\N	\N	\N
15	ir_model_data.has_model	2020-09-07 13:54:57.464912	\N	\N	\N	\N
13	ir.model.field.get_name	2020-09-16 08:36:17.444012	\N	\N	\N	\N
11	ir.model.get_name	2020-09-16 08:36:17.446571	\N	\N	\N	\N
14	ir.translation	2020-09-16 08:36:17.449068	\N	\N	\N	\N
12	ir.message	2020-09-16 08:36:17.451987	\N	\N	\N	\N
7	ir_rule.domain_get	2020-09-29 10:22:58.079883	\N	\N	\N	\N
8	res_user.get_groups	2020-09-29 10:22:58.084024	\N	\N	\N	\N
6	ir_model_field_access.check	2020-09-29 10:22:58.087857	\N	\N	\N	\N
5	res_user._get_login	2020-09-29 10:22:58.089896	\N	\N	\N	\N
10	modelview.fields_view_get	2020-09-29 10:22:58.091788	\N	\N	\N	\N
9	ir_model_access.get_access	2020-09-29 10:22:58.093776	\N	\N	\N	\N
1	ir.translation.get_language	2020-09-29 10:24:51.966501	\N	\N	\N	\N
2	res_user.get_preferences	2020-09-29 10:24:51.969582	\N	\N	\N	\N
3	ir.lang.code	2020-09-29 10:24:51.971301	\N	\N	\N	\N
4	ir.lang	2020-09-29 10:24:51.972988	\N	\N	\N	\N
\.


--
-- Data for Name: ir_calendar_day; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_calendar_day (id, abbreviation, create_date, create_uid, index, name, write_date, write_uid) FROM stdin;
1	Mon	2020-09-01 08:49:55.946968	0	0	Monday	\N	\N
2	Tue	2020-09-01 08:49:55.946968	0	1	Tuesday	\N	\N
3	Wed	2020-09-01 08:49:55.946968	0	2	Wednesday	\N	\N
4	Thu	2020-09-01 08:49:55.946968	0	3	Thursday	\N	\N
5	Fri	2020-09-01 08:49:55.946968	0	4	Friday	\N	\N
6	Sat	2020-09-01 08:49:55.946968	0	5	Saturday	\N	\N
7	Sun	2020-09-01 08:49:55.946968	0	6	Sunday	\N	\N
\.


--
-- Data for Name: ir_calendar_month; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_calendar_month (id, abbreviation, create_date, create_uid, index, name, write_date, write_uid) FROM stdin;
1	Jan	2020-09-01 08:49:55.946968	0	1	January	\N	\N
2	Feb	2020-09-01 08:49:55.946968	0	2	February	\N	\N
3	Mar	2020-09-01 08:49:55.946968	0	3	March	\N	\N
4	Apr	2020-09-01 08:49:55.946968	0	4	April	\N	\N
5	May	2020-09-01 08:49:55.946968	0	5	May	\N	\N
6	Jun	2020-09-01 08:49:55.946968	0	6	June	\N	\N
7	Jul	2020-09-01 08:49:55.946968	0	7	July	\N	\N
8	Aug	2020-09-01 08:49:55.946968	0	8	August	\N	\N
9	Sep	2020-09-01 08:49:55.946968	0	9	September	\N	\N
10	Oct	2020-09-01 08:49:55.946968	0	10	October	\N	\N
11	Nov	2020-09-01 08:49:55.946968	0	11	November	\N	\N
12	Dec	2020-09-01 08:49:55.946968	0	12	December	\N	\N
\.


--
-- Data for Name: ir_configuration; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_configuration (id, language, hostname, create_date, create_uid, write_date, write_uid) FROM stdin;
1	en	\N	2020-09-01 08:50:46.285882	0	2020-09-29 10:22:48.015388	0
\.


--
-- Data for Name: ir_cron; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_cron (id, active, create_date, create_uid, day, hour, interval_number, interval_type, method, minute, next_call, weekday, write_date, write_uid) FROM stdin;
1	t	2020-09-01 08:50:28.180942	0	\N	\N	5	minutes	ir.trigger|trigger_time	\N	\N	\N	\N	\N
\.


--
-- Data for Name: ir_export; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_export (id, create_date, create_uid, name, resource, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_export-res_group; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."ir_export-res_group" (id, create_date, create_uid, export, "group", write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_export-write-res_group; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."ir_export-write-res_group" (id, create_date, create_uid, export, "group", write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_export_line; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_export_line (id, create_date, create_uid, export, name, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_lang; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_lang (id, name, code, translatable, parent, active, direction, am, create_date, create_uid, date, decimal_point, "grouping", mon_decimal_point, mon_grouping, mon_thousands_sep, n_cs_precedes, n_sep_by_space, n_sign_posn, negative_sign, p_cs_precedes, p_sep_by_space, p_sign_posn, pm, positive_sign, thousands_sep, write_date, write_uid) FROM stdin;
2	Bulgarian	bg	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d.%m.%Y	,	[3, 3, 0]	,	[3, 3, 0]	 	f	t	1	-	f	t	1			.	\N	\N
3	Català	ca	f	\N	t	ltr	a. m.	2020-09-01 08:49:55.946968	0	%d/%m/%Y	,	[3, 3, 0]	,	[3, 3, 0]	.	f	t	1	-	f	t	1	p. m.		 	\N	\N
4	Czech	cs	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d.%m.%Y	,	[3, 3, 0]	,	[3, 3, 0]	 	f	t	1	-	f	t	1			 	\N	\N
5	German	de	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d.%m.%Y	,	[3, 3, 0]	,	[3, 3, 0]	.	f	t	1	-	f	t	1			.	\N	\N
6	Spanish	es	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d/%m/%Y	,	[3, 3, 0]	,	[3, 3, 0]	.	f	t	1	-	f	t	1			.	\N	\N
7	Spanish (Latin American)	es_419	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d/%m/%Y	.	[3, 3, 0]	.	[3, 3, 0]	,	f	t	1	-	f	t	1			,	\N	\N
8	Estonian	et	f	\N	t	ltr	\N	2020-09-01 08:49:55.946968	0	%d.%m.%Y	,	[3, 3]	,	[3, 3]	 	f	t	1	-	f	t	1	\N		 	\N	\N
9	Persian	fa	f	\N	t	rtl		2020-09-01 08:49:55.946968	0	%Y/%m/%d	.	[3, 0]	٫	[3, 0]	٬	f	t	1	-	f	t	1			,	\N	\N
10	Finnish	fi	f	\N	t	ltr	\N	2020-09-01 08:49:55.946968	0	%d.%m.%Y	,	[3, 3, 0]	,	[3, 3, 0]	 	f	t	1	-	f	t	1	\N		 	\N	\N
11	French	fr	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d.%m.%Y	,	[3, 0]	,	[3, 0]	 	f	t	1	-	f	t	1			 	\N	\N
12	Hungarian	hu	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%Y-%m-%d	,	[3, 3, 0]	,	[3, 3, 0]	.	f	t	1	-	f	t	1			.	\N	\N
13	Indonesian	id	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d/%m/%Y	,	[3, 3]	,	[3, 3]	.	t	f	1	-	t	f	1			.	\N	\N
14	Italian	it	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d/%m/%Y	,	[]	,	[3, 3, 0]	.	t	t	1	-	t	t	1				\N	\N
15	Lao	lo	f	\N	t	ltr	AM	2020-09-01 08:49:55.946968	0	%d/%m/%Y	.	[3, 3, 0]	.	[3, 3, 0]	,	t	t	4	-	t	t	4	PM		,	\N	\N
16	Lithuanian	lt	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%Y-%m-%d	,	[3, 3, 0]	,	[3, 3, 0]	.	f	t	1	-	f	t	1			.	\N	\N
17	Dutch	nl	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d-%m-%Y	,	[]	,	[3, 3, 0]	 	t	t	2	-	t	t	1				\N	\N
18	Polish	pl	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d.%m.%Y	,	[3, 3, 0]	,	[3, 0, 0]	 	f	t	1	-	f	t	1			 	\N	\N
19	Portuguese	pt	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d-%m-%Y	,	[]	,	[3, 3, 0]	.	t	t	1	-	t	t	1				\N	\N
20	Russian	ru	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d.%m.%Y	,	[3, 3, 0]	.	[3, 3, 0]	 	f	t	1	-	f	t	1			 	\N	\N
21	Slovenian	sl	f	\N	t	ltr		2020-09-01 08:49:55.946968	0	%d.%m.%Y	,	[]	,	[3, 3, 0]	 	f	t	1	-	f	t	1			 	\N	\N
22	Turkish	tr	f	\N	t	ltr	\N	2020-09-01 08:49:55.946968	0	%d-%m-%Y	,	[3, 3, 0]	,	[3, 3, 0]	.	f	t	1	-	f	t	1	\N		.	\N	\N
23	Chinese Simplified	zh_CN	f	\N	t	ltr	上午	2020-09-01 08:49:55.946968	0	%Y-%m-%d	.	[3, 0]	.	[3, 0]	,	t	f	4	-	t	f	4	下午		,	\N	\N
1	English	en	t	\N	t	ltr	AM	2020-09-01 08:49:55.946968	0	%m/%d/%Y	.	[3, 3, 0]	.	[3, 3, 0]	,	t	f	1	-	t	f	1	PM		,	2020-09-29 10:24:51.934718	0
\.


--
-- Data for Name: ir_message; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_message (id, create_date, create_uid, text, write_date, write_uid) FROM stdin;
1	2020-09-01 08:49:55.946968	0	ID	\N	\N
2	2020-09-01 08:49:55.946968	0	Created by	\N	\N
3	2020-09-01 08:49:55.946968	0	Created at	\N	\N
4	2020-09-01 08:49:55.946968	0	Edited by	\N	\N
5	2020-09-01 08:49:55.946968	0	Edited at	\N	\N
6	2020-09-01 08:49:55.946968	0	Record Name	\N	\N
7	2020-09-01 08:49:55.946968	0	Active	\N	\N
8	2020-09-01 08:49:55.946968	0	Uncheck to exclude from future use.	\N	\N
9	2020-09-01 08:49:55.946968	0	Name	\N	\N
10	2020-09-01 08:49:55.946968	0	String	\N	\N
11	2020-09-01 08:49:55.946968	0	Help	\N	\N
12	2020-09-01 08:49:55.946968	0	Type	\N	\N
13	2020-09-01 08:49:55.946968	0	Boolean	\N	\N
14	2020-09-01 08:49:55.946968	0	Integer	\N	\N
15	2020-09-01 08:49:55.946968	0	Char	\N	\N
16	2020-09-01 08:49:55.946968	0	Float	\N	\N
17	2020-09-01 08:49:55.946968	0	Numeric	\N	\N
18	2020-09-01 08:49:55.946968	0	Date	\N	\N
19	2020-09-01 08:49:55.946968	0	DateTime	\N	\N
20	2020-09-01 08:49:55.946968	0	Selection	\N	\N
21	2020-09-01 08:49:55.946968	0	MultiSelection	\N	\N
22	2020-09-01 08:49:55.946968	0	Digits	\N	\N
23	2020-09-01 08:49:55.946968	0	Domain	\N	\N
24	2020-09-01 08:49:55.946968	0	A couple of key and label separated by ":" per line.	\N	\N
25	2020-09-01 08:49:55.946968	0	Selection Sorted	\N	\N
26	2020-09-01 08:49:55.946968	0	If the selection must be sorted on label.	\N	\N
27	2020-09-01 08:49:55.946968	0	Selection JSON	\N	\N
28	2020-09-01 08:49:55.946968	0	Sequence	\N	\N
29	2020-09-01 08:49:55.946968	0	ID must be positive.	\N	\N
30	2020-09-01 08:49:55.946968	0	You are not allowed to modify this record.	\N	\N
31	2020-09-01 08:49:55.946968	0	You are not allowed to delete this record.	\N	\N
32	2020-09-01 08:49:55.946968	0	This record is part of the base configuration.	\N	\N
33	2020-09-01 08:49:55.946968	0	Relation not found: "%(value)r" in "%(model)s".	\N	\N
34	2020-09-01 08:49:55.946968	0	Too many relations found: "%(value)r" in "%(model)s".	\N	\N
35	2020-09-01 08:49:55.946968	0	Syntax error for reference: "%(value)r" in "%(field)s".	\N	\N
36	2020-09-01 08:49:55.946968	0	Syntax error for XML id: "%(value)r" in "%(field)s".	\N	\N
37	2020-09-01 08:49:55.946968	0	The value for field "%(field)s" in "%(model)s" is not valid according to its domain.	\N	\N
38	2020-09-01 08:49:55.946968	0	A value is required for field "%(field)s" in "%(model)s".	\N	\N
39	2020-09-01 08:49:55.946968	0	The value for field "%(field)s" in "%(model)s" is too long (%(size)i > %(max_size)i).	\N	\N
40	2020-09-01 08:49:55.946968	0	The number of digits in the value "%(value)s" for field "%(field)s" in "%(model)s" exceeds the limit of "%(digits)i".	\N	\N
41	2020-09-01 08:49:55.946968	0	The value "%(value)s" for field "%(field)s" in "%(model)s" is not one of the allowed options.	\N	\N
42	2020-09-01 08:49:55.946968	0	The time value "%(value)s" for field "%(field)s" in "%(model)s" is not valid.	\N	\N
43	2020-09-01 08:49:55.946968	0	The value "%(value)s" for field "%(field)s" in "%(model)s" does not exist.	\N	\N
44	2020-09-01 08:49:55.946968	0	The records could not be deleted because they are used by field "%(field)s" of "%(model)s".	\N	\N
45	2020-09-01 08:49:55.946968	0	You are not allowed to access "%(model)s".	\N	\N
46	2020-09-01 08:49:55.946968	0	You are not allowed to access "%(model)s.%(field)s".	\N	\N
47	2020-09-01 08:49:55.946968	0	You are not allowed to create records of "%(model)s" because they fail on at least one of these rules:\n%(rules)s	\N	\N
48	2020-09-01 08:49:55.946968	0	You are not allowed to read records "%(ids)s" of "%(model)s" because of at least one of these rules:\n%(rules)s	\N	\N
49	2020-09-01 08:49:55.946968	0	You are trying to read records "%(ids)s" of "%(model)s" that don't exist anymore.	\N	\N
50	2020-09-01 08:49:55.946968	0	You are not allowed to write to records "%(ids)s" of "%(model)s" because of at least one of these rules:\n%(rules)s	\N	\N
51	2020-09-01 08:49:55.946968	0	You are trying to write to records "%(ids)s" of "%(model)s" that don't exist anymore.	\N	\N
52	2020-09-01 08:49:55.946968	0	You are not allowed to delete records "%(ids)s" of "%(model)s" because of at lease one of those rules:\n%(rules)s	\N	\N
53	2020-09-01 08:49:55.946968	0	Invalid domain in schema "%(schema)s".	\N	\N
54	2020-09-01 08:49:55.946968	0	Invalid selection in schema "%(schema)s".	\N	\N
55	2020-09-01 08:49:55.946968	0	Recursion error: Record "%(rec_name)s" with parent "%(parent_rec_name)s" was configured as ancestor of itself.	\N	\N
56	2020-09-01 08:49:55.946968	0	Missing search function for field "%(field)s" in "%(model)s".	\N	\N
57	2020-09-01 08:49:55.946968	0	Missing setter function for field "%(field)s" in "%(model)s".	\N	\N
58	2020-09-01 08:49:55.946968	0	Calling button "%(button)s on "%(model)s" is not allowed.	\N	\N
59	2020-09-01 08:49:55.946968	0	Invalid XML for view "%(name)s".	\N	\N
60	2020-09-01 08:49:55.946968	0	Wrong wizard model in keyword action "%(name)s".	\N	\N
61	2020-09-01 08:49:55.946968	0	Invalid email definition for report "%(name)s".	\N	\N
62	2020-09-01 08:49:55.946968	0	Invalid view "%(view)s" for action "%(action)s".	\N	\N
63	2020-09-01 08:49:55.946968	0	Invalid domain or search criteria "%(domain)s" for action "%(action)s".	\N	\N
64	2020-09-01 08:49:55.946968	0	Invalid context "%(context)s" for action "%(action)s".	\N	\N
65	2020-09-01 08:49:55.946968	0	The condition "%(condition)s" is not a valid PYSON expression for button rule "%(rule)s".	\N	\N
66	2020-09-01 08:49:55.946968	0	Missing sequence.	\N	\N
67	2020-09-01 08:49:55.946968	0	Invalid prefix "%(affix)s" for sequence "%(sequence)s".	\N	\N
68	2020-09-01 08:49:55.946968	0	Invalid suffix "%(affix)s" for sequence "%(sequence)s".	\N	\N
69	2020-09-01 08:49:55.946968	0	The "Last Timestamp" cannot be in the future for sequence "%s".	\N	\N
70	2020-09-01 08:49:55.946968	0	Invalid grouping "%(grouping)s" for language "%(language)s".	\N	\N
71	2020-09-01 08:49:55.946968	0	Invalid date format "%(format)s" for language "%(language)s".	\N	\N
72	2020-09-01 08:49:55.946968	0	The default language must be translatable.	\N	\N
73	2020-09-01 08:49:55.946968	0	The default language can not be deleted.	\N	\N
74	2020-09-01 08:49:55.946968	0	Invalid domain in rule "%(name)s".	\N	\N
75	2020-09-01 08:49:55.946968	0	You can not export translation "%(name)s" because it has been overridden by module "%(overriding_module)s".	\N	\N
76	2020-09-01 08:49:55.946968	0	You can not remove a module that is activated or that is about to be activated.	\N	\N
77	2020-09-01 08:49:55.946968	0	Some activated modules depend on the ones you are trying to deactivate:	\N	\N
78	2020-09-01 08:49:55.946968	0	Condition "%(condition)s" is not a valid PYSON expression for trigger "%(trigger)s".	\N	\N
79	2020-09-01 08:49:55.946968	0	Failed to save, please retry.	\N	\N
80	2020-09-01 08:49:55.946968	0	Y	\N	\N
81	2020-09-01 08:49:55.946968	0	M	\N	\N
82	2020-09-01 08:49:55.946968	0	w	\N	\N
83	2020-09-01 08:49:55.946968	0	d	\N	\N
84	2020-09-01 08:49:55.946968	0	h	\N	\N
85	2020-09-01 08:49:55.946968	0	m	\N	\N
86	2020-09-01 08:49:55.946968	0	s	\N	\N
87	2020-09-01 08:49:55.946968	0	The resources to which this record must be copied.	\N	\N
88	2020-09-01 08:49:55.946968	0	Attachments	\N	\N
89	2020-09-01 08:49:55.946968	0	Notes	\N	\N
90	2020-09-01 08:50:28.180942	0	The password is too short.	\N	\N
91	2020-09-01 08:50:28.180942	0	The password is forbidden.	\N	\N
92	2020-09-01 08:50:28.180942	0	The same characters appear in the password too many times.	\N	\N
93	2020-09-01 08:50:28.180942	0	The password cannot be the same as user's name.	\N	\N
94	2020-09-01 08:50:28.180942	0	The password cannot be the same as user's login.	\N	\N
95	2020-09-01 08:50:28.180942	0	The password cannot be the same as user's email address.	\N	\N
96	2020-09-01 08:50:28.180942	0	For logging purposes users cannot be deleted, instead they should be deactivated.	\N	\N
97	2020-09-01 08:50:28.180942	0	Password for %(login)s	\N	\N
98	2020-09-02 07:24:23.639802	0	No rate found for currency "%(currency)s" on "%(date)s".	\N	\N
99	2020-09-02 07:24:23.639802	0	A currency can only have one rate by date.	\N	\N
100	2020-09-02 07:24:23.639802	0	A currency rate must be positive.	\N	\N
101	2020-09-02 07:24:24.586225	0	The code on party must be unique.	\N	\N
102	2020-09-02 07:24:24.586225	0	You cannot change the party of contact mechanism "%(contact)s".	\N	\N
103	2020-09-02 07:24:24.586225	0	The phone number "%(phone)s" for party "%(party)s" is not valid.	\N	\N
104	2020-09-02 07:24:24.586225	0	The %(type)s "%(code)s" for party "%(party)s" is not valid.	\N	\N
105	2020-09-02 07:24:24.586225	0	The VIES service is unavailable, try again later.	\N	\N
106	2020-09-02 07:24:24.586225	0	Parties have different names: "%(source_name)s" vs "%(destination_name)s".	\N	\N
107	2020-09-02 07:24:24.586225	0	Parties have different tax identifiers: "%(source_code)s" vs "%(destination_code)s".	\N	\N
108	2020-09-02 07:24:24.586225	0	Party "%(party)s" cannot be erased because they are still active.	\N	\N
109	2020-09-02 07:24:24.586225	0	You cannot change the party of address "%(address)s".	\N	\N
110	2020-09-02 07:24:24.586225	0	Invalid format "%(format)s" with exception "%(exception)s".	\N	\N
111	2020-09-02 07:24:24.586225	0	The name of party category must be unique by parent.	\N	\N
112	2020-09-02 07:24:24.586225	0	The country code on subdivision type must be unique.	\N	\N
113	2020-09-02 07:24:32.977941	0	You cannot modify the factor of UOM "%(uom)s".	\N	\N
114	2020-09-02 07:24:32.977941	0	You cannot modify the rate of UOM "%(uom)s".	\N	\N
115	2020-09-02 07:24:32.977941	0	You cannot modify the category of UOM "%(uom)s".	\N	\N
116	2020-09-02 07:24:32.977941	0	If the UOM is still not used, you can delete it otherwise you can deactivate it and create a new one.	\N	\N
117	2020-09-02 07:24:32.977941	0	Incompatible factor and rate values on UOM "%(uom)s".	\N	\N
118	2020-09-02 07:24:32.977941	0	Rate and factor can not be both equal to zero.	\N	\N
119	2020-09-02 07:24:32.977941	0	The %(type)s "%(code)s" for product "%(product)s" is not valid.	\N	\N
120	2020-09-02 07:24:32.977941	0	Code of active product must be unique.	\N	\N
121	2020-09-04 07:51:32.521302	0	 Matricula repetida 	\N	\N
122	2020-09-16 08:36:14.823954	0	E-mail of active web user must be unique.	\N	\N
123	2020-09-16 08:36:14.823954	0	Web user session key must be unique.	\N	\N
\.


--
-- Data for Name: ir_model; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_model (id, model, name, info, module, create_date, create_uid, global_search_p, write_date, write_uid) FROM stdin;
127	web.user	Web User	Web User	web_user	\N	\N	f	\N	\N
128	web.user.authenticate.attempt	Web User Authenticate Attempt	Web User Authenticate Attempt	web_user	\N	\N	f	\N	\N
129	web.user.session	Web User Session	Web User Session	web_user	\N	\N	f	\N	\N
123	modelo-producto	modelo - producto	modelo - producto	taller	\N	\N	f	\N	\N
125	taller.coche.baja.start	baja de coche	baja de coche	taller	\N	\N	f	\N	\N
126	taller.coche.baja.result	resultado baja de coche	resultado baja de coche	taller	\N	\N	f	\N	\N
1	ir.configuration	Configuration	Configuration	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
2	ir.translation	Translation	Translation	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
3	ir.translation.set.start	Set Translation	Set Translation	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
4	ir.translation.set.succeed	Set Translation	Set Translation	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
5	ir.translation.clean.start	Clean translation	Clean translation	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
6	ir.translation.clean.succeed	Clean translation	Clean translation	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
7	ir.translation.update.start	Update translation	Update translation	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
8	ir.translation.export.start	Export translation	Export translation	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
9	ir.translation.export.result	Export translation	Export translation	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
10	ir.sequence.type	Sequence type	Sequence type	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
11	ir.sequence	Sequence	Sequence	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
12	ir.sequence.strict	Sequence Strict	Sequence Strict	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
13	ir.ui.menu	UI menu	UI menu	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
14	ir.ui.menu.favorite	Menu Favorite	Menu Favorite	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
15	ir.ui.view	View	View	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
16	ir.ui.view.show.start	Show view	Show view	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
17	ir.ui.view_tree_width	View Tree Width	View Tree Width	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
18	ir.ui.view_tree_state	View Tree State	View Tree State	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
19	ir.ui.view_search	View Search	View Search	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
20	ir.ui.icon	Icon	Icon	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
21	ir.action	Action	Action	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
22	ir.action.keyword	Action keyword	Action keyword	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
23	ir.action.report	Action report	Action report	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
24	ir.action.act_window	Action act window	Action act window	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
25	ir.action.act_window.view	Action act window view	Action act window view	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
26	ir.action.act_window.domain	Action act window domain	Action act window domain	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
27	ir.action.wizard	Action wizard	Action wizard	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
28	ir.action.url	Action URL	Action URL	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
29	ir.model	Model	Model	ir	2020-09-01 08:50:03.481569	0	\N	\N	\N
30	ir.model.field	Model field	Model field	ir	\N	\N	f	\N	\N
31	ir.model.access	Model access	Model access	ir	\N	\N	f	\N	\N
32	ir.model.field.access	Model Field Access	Model Field Access	ir	\N	\N	f	\N	\N
33	ir.model.button	Model Button	Model Button	ir	\N	\N	f	\N	\N
34	ir.model.button.rule	Model Button Rule	Model Button Rule	ir	\N	\N	f	\N	\N
35	ir.model.button.click	Model Button Click	Model Button Click	ir	\N	\N	f	\N	\N
36	ir.model.button-button.reset	Model Button Reset	Model Button Reset	ir	\N	\N	f	\N	\N
37	ir.model.data	Model data	Model data	ir	\N	\N	f	\N	\N
38	ir.model.print_model_graph.start	Print Model Graph	Print Model Graph	ir	\N	\N	f	\N	\N
39	ir.attachment	Attachment	Attachment	ir	\N	\N	f	\N	\N
40	ir.note	Note	Note	ir	\N	\N	f	\N	\N
41	ir.note.read	Note Read	Note Read	ir	\N	\N	f	\N	\N
42	ir.cron	Cron	Cron	ir	\N	\N	f	\N	\N
43	ir.lang	Language	Language	ir	\N	\N	f	\N	\N
44	ir.lang.config.start	Language Configuration Start	Language Configuration Start	ir	\N	\N	f	\N	\N
45	ir.export	Export	Export	ir	\N	\N	f	\N	\N
46	ir.export.line	Export line	Export line	ir	\N	\N	f	\N	\N
47	ir.rule.group	Rule group	Rule group	ir	\N	\N	f	\N	\N
48	ir.rule	Rule	Rule	ir	\N	\N	f	\N	\N
49	ir.module	Module	Module	ir	\N	\N	f	\N	\N
50	ir.module.dependency	Module dependency	Module dependency	ir	\N	\N	f	\N	\N
51	ir.module.config_wizard.item	Config wizard to run after activating a module	Config wizard to run after activating a module	ir	\N	\N	f	\N	\N
52	ir.module.config_wizard.first	Module Config Wizard First	Module Config Wizard First	ir	\N	\N	f	\N	\N
53	ir.module.config_wizard.other	Module Config Wizard Other	Module Config Wizard Other	ir	\N	\N	f	\N	\N
54	ir.module.config_wizard.done	Module Config Wizard Done	Module Config Wizard Done	ir	\N	\N	f	\N	\N
55	ir.module.activate_upgrade.start	Module Activate Upgrade Start	Module Activate Upgrade Start	ir	\N	\N	f	\N	\N
56	ir.module.activate_upgrade.done	Module Activate Upgrade Done	Module Activate Upgrade Done	ir	\N	\N	f	\N	\N
57	ir.cache	Cache	Cache	ir	\N	\N	f	\N	\N
58	ir.date	Date	Date	ir	\N	\N	f	\N	\N
59	ir.trigger	Trigger	Trigger	ir	\N	\N	f	\N	\N
60	ir.trigger.log	Trigger Log	Trigger Log	ir	\N	\N	f	\N	\N
61	ir.session	Session	Session	ir	\N	\N	f	\N	\N
62	ir.session.wizard	Session Wizard	Session Wizard	ir	\N	\N	f	\N	\N
63	ir.queue	Queue	Queue	ir	\N	\N	f	\N	\N
64	ir.calendar.month	Month	Month	ir	\N	\N	f	\N	\N
65	ir.calendar.day	Day	Day	ir	\N	\N	f	\N	\N
66	ir.message	Message	Message	ir	\N	\N	f	\N	\N
67	res.group	Group	Group	res	\N	\N	f	\N	\N
68	res.user	User	User	res	\N	\N	f	\N	\N
69	res.user.login.attempt	Login Attempt	Login Attempt\n\n    This class is separated from the res.user one in order to prevent locking\n    the res.user table when in a long running process.\n    	res	\N	\N	f	\N	\N
70	res.user-ir.action	User - Action	User - Action	res	\N	\N	f	\N	\N
71	res.user-res.group	User - Group	User - Group	res	\N	\N	f	\N	\N
72	res.user.warning	User Warning	User Warning	res	\N	\N	f	\N	\N
73	res.user.application	User Application	User Application	res	\N	\N	f	\N	\N
74	res.user.config.start	User Config Init	User Config Init	res	\N	\N	f	\N	\N
75	ir.ui.menu-res.group	UI Menu - Group	UI Menu - Group	res	\N	\N	f	\N	\N
76	ir.action-res.group	Action - Group	Action - Group	res	\N	\N	f	\N	\N
77	ir.model.button-res.group	Model Button - Group	Model Button - Group	res	\N	\N	f	\N	\N
78	ir.rule.group-res.group	Rule Group - Group	Rule Group - Group	res	\N	\N	f	\N	\N
79	ir.sequence.type-res.group	Sequence Type - Group	Sequence Type - Group	res	\N	\N	f	\N	\N
80	ir.export-res.group	Export Group	Export Group	res	\N	\N	f	\N	\N
81	ir.export-write-res.group	Export Modification Group	Export Modification Group	res	\N	\N	f	\N	\N
82	country.country	Country	Country	country	\N	\N	f	\N	\N
83	country.subdivision	Subdivision	Subdivision	country	\N	\N	f	\N	\N
84	country.zip	Zip	Zip	country	\N	\N	f	\N	\N
85	currency.currency	Currency	Currency	currency	\N	\N	f	\N	\N
86	currency.currency.rate	Currency Rate	Currency Rate	currency	\N	\N	f	\N	\N
87	party.category	Category	Category	party	\N	\N	f	\N	\N
88	party.party	Party	Party	party	\N	\N	f	\N	\N
89	party.party.lang	Party Lang	Party Lang	party	\N	\N	f	\N	\N
90	party.party-party.category	Party - Category	Party - Category	party	\N	\N	f	\N	\N
91	party.identifier	Party Identifier	Party Identifier	party	\N	\N	f	\N	\N
92	party.check_vies.result	Check VIES	Check VIES	party	\N	\N	f	\N	\N
93	party.replace.ask	Replace Party	Replace Party	party	\N	\N	f	\N	\N
94	party.erase.ask	Erase Party	Erase Party	party	\N	\N	f	\N	\N
95	party.address	Address	Address	party	\N	\N	f	\N	\N
96	party.address.format	Address Format	Address Format	party	\N	\N	f	\N	\N
97	party.address.subdivision_type	Address Subdivision Type	Address Subdivision Type	party	\N	\N	f	\N	\N
98	party.contact_mechanism	Contact Mechanism	Contact Mechanism	party	\N	\N	f	\N	\N
99	party.configuration	Party Configuration	Party Configuration	party	\N	\N	f	\N	\N
100	party.configuration.party_sequence	Party Configuration Sequence	Party Configuration Sequence	party	\N	\N	f	\N	\N
101	party.configuration.party_lang	Party Configuration Lang	Party Configuration Lang	party	\N	\N	f	\N	\N
102	company.company	Company	Company	company	\N	\N	f	\N	\N
103	company.employee	Employee	Employee	company	\N	\N	f	\N	\N
104	company.company.config.start	Company Config	Company Config	company	\N	\N	f	\N	\N
105	res.user-company.employee	User - Employee	User - Employee	company	\N	\N	f	\N	\N
106	ir.cron-company.company	Cron - Company	Cron - Company	company	\N	\N	f	\N	\N
107	product.uom.category	Unit of Measure Category	Unit of Measure Category	product	\N	\N	f	\N	\N
108	product.uom	Unit of Measure	Unit of Measure	product	\N	\N	f	\N	\N
109	product.category	Product Category	Product Category	product	\N	\N	f	\N	\N
110	product.template	Product Template	Product Template	product	\N	\N	f	\N	\N
111	product.product	Product Variant	Product Variant	product	\N	\N	f	\N	\N
112	product.identifier	Product Identifier	Product Identifier	product	\N	\N	f	\N	\N
113	product.list_price	Product List Price	Product List Price	product	\N	\N	f	\N	\N
114	product.cost_price_method	Product Cost Price Method	Product Cost Price Method	product	\N	\N	f	\N	\N
115	product.cost_price	Product Cost Price	Product Cost Price	product	\N	\N	f	\N	\N
116	product.template-product.category	Template - Category	Template - Category	product	\N	\N	f	\N	\N
117	product.template-product.category.all	Template - Category All	Template - Category All	product	\N	\N	f	\N	\N
118	product.configuration	Product Configuration	Product Configuration	product	\N	\N	f	\N	\N
119	product.configuration.default_cost_price_method	Product Configuration Default Cost Price Method	Product Configuration Default Cost Price Method	product	\N	\N	f	\N	\N
120	taller.marca	Marca	Marca	taller	\N	\N	f	\N	\N
121	taller.modelo	Modelo	Modelo	taller	\N	\N	f	\N	\N
122	taller.coche	Coche	Coche	taller	\N	\N	f	\N	\N
\.


--
-- Data for Name: ir_model_access; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_model_access (id, active, create_date, create_uid, description, "group", model, perm_create, perm_delete, perm_read, perm_write, write_date, write_uid) FROM stdin;
1	t	2020-09-01 08:50:28.180942	0	\N	\N	67	f	f	t	f	\N	\N
2	t	2020-09-01 08:50:28.180942	0	\N	1	67	t	t	t	t	\N	\N
3	t	2020-09-01 08:50:28.180942	0	\N	\N	68	f	f	t	f	\N	\N
4	t	2020-09-01 08:50:28.180942	0	\N	1	68	t	t	t	t	\N	\N
5	t	2020-09-01 08:50:28.180942	0	\N	\N	10	f	f	t	f	\N	\N
6	t	2020-09-01 08:50:28.180942	0	\N	1	10	t	t	t	t	\N	\N
7	t	2020-09-01 08:50:28.180942	0	\N	\N	20	f	f	t	f	\N	\N
8	t	2020-09-01 08:50:28.180942	0	\N	1	20	t	t	t	t	\N	\N
9	t	2020-09-01 08:50:28.180942	0	\N	\N	13	f	f	t	f	\N	\N
10	t	2020-09-01 08:50:28.180942	0	\N	1	13	t	t	t	t	\N	\N
11	t	2020-09-01 08:50:28.180942	0	\N	\N	15	f	f	t	f	\N	\N
12	t	2020-09-01 08:50:28.180942	0	\N	1	15	t	t	t	t	\N	\N
13	t	2020-09-01 08:50:28.180942	0	\N	\N	21	f	f	t	f	\N	\N
14	t	2020-09-01 08:50:28.180942	0	\N	1	21	t	t	t	t	\N	\N
15	t	2020-09-01 08:50:28.180942	0	\N	\N	22	f	f	t	f	\N	\N
16	t	2020-09-01 08:50:28.180942	0	\N	1	22	t	t	t	t	\N	\N
17	t	2020-09-01 08:50:28.180942	0	\N	\N	23	f	f	t	f	\N	\N
18	t	2020-09-01 08:50:28.180942	0	\N	1	23	t	t	t	t	\N	\N
19	t	2020-09-01 08:50:28.180942	0	\N	\N	24	f	f	t	f	\N	\N
20	t	2020-09-01 08:50:28.180942	0	\N	1	24	t	t	t	t	\N	\N
21	t	2020-09-01 08:50:28.180942	0	\N	\N	25	f	f	t	f	\N	\N
22	t	2020-09-01 08:50:28.180942	0	\N	1	25	t	t	t	t	\N	\N
23	t	2020-09-01 08:50:28.180942	0	\N	\N	26	f	f	t	f	\N	\N
24	t	2020-09-01 08:50:28.180942	0	\N	1	26	t	t	t	t	\N	\N
25	t	2020-09-01 08:50:28.180942	0	\N	\N	27	f	f	t	f	\N	\N
26	t	2020-09-01 08:50:28.180942	0	\N	1	27	t	t	t	t	\N	\N
27	t	2020-09-01 08:50:28.180942	0	\N	\N	28	f	f	t	f	\N	\N
28	t	2020-09-01 08:50:28.180942	0	\N	1	28	t	t	t	t	\N	\N
29	t	2020-09-01 08:50:28.180942	0	\N	\N	29	f	f	t	f	\N	\N
30	t	2020-09-01 08:50:28.180942	0	\N	1	29	t	t	t	t	\N	\N
31	t	2020-09-01 08:50:28.180942	0	\N	\N	30	f	f	t	f	\N	\N
32	t	2020-09-01 08:50:28.180942	0	\N	1	30	t	t	t	t	\N	\N
33	t	2020-09-01 08:50:28.180942	0	\N	\N	31	f	f	t	f	\N	\N
34	t	2020-09-01 08:50:28.180942	0	\N	1	31	t	t	t	t	\N	\N
35	t	2020-09-01 08:50:28.180942	0	\N	\N	33	f	f	t	f	\N	\N
36	t	2020-09-01 08:50:28.180942	0	\N	1	33	t	t	t	t	\N	\N
37	t	2020-09-01 08:50:28.180942	0	\N	\N	34	f	f	t	f	\N	\N
38	t	2020-09-01 08:50:28.180942	0	\N	1	34	t	t	t	t	\N	\N
39	t	2020-09-01 08:50:28.180942	0	\N	\N	35	f	f	t	f	\N	\N
40	t	2020-09-01 08:50:28.180942	0	\N	1	35	t	t	t	t	\N	\N
41	t	2020-09-01 08:50:28.180942	0	\N	\N	37	f	f	t	f	\N	\N
42	t	2020-09-01 08:50:28.180942	0	\N	\N	42	f	f	t	f	\N	\N
43	t	2020-09-01 08:50:28.180942	0	\N	1	42	t	t	t	t	\N	\N
44	t	2020-09-01 08:50:28.180942	0	\N	\N	63	f	f	f	f	\N	\N
45	t	2020-09-01 08:50:28.180942	0	\N	1	63	f	f	t	f	\N	\N
46	t	2020-09-01 08:50:28.180942	0	\N	\N	43	f	f	t	f	\N	\N
47	t	2020-09-01 08:50:28.180942	0	\N	1	43	t	t	t	t	\N	\N
48	t	2020-09-01 08:50:28.180942	0	\N	\N	2	f	f	t	f	\N	\N
49	t	2020-09-01 08:50:28.180942	0	\N	1	2	t	t	t	t	\N	\N
50	t	2020-09-01 08:50:28.180942	0	\N	\N	47	f	f	t	f	\N	\N
51	t	2020-09-01 08:50:28.180942	0	\N	1	47	t	t	t	t	\N	\N
52	t	2020-09-01 08:50:28.180942	0	\N	\N	48	f	f	t	f	\N	\N
53	t	2020-09-01 08:50:28.180942	0	\N	1	48	t	t	t	t	\N	\N
54	t	2020-09-01 08:50:28.180942	0	\N	\N	49	f	f	t	f	\N	\N
55	t	2020-09-01 08:50:28.180942	0	\N	1	49	t	t	t	t	\N	\N
56	t	2020-09-01 08:50:28.180942	0	\N	\N	50	f	f	t	f	\N	\N
57	t	2020-09-01 08:50:28.180942	0	\N	1	50	t	t	t	t	\N	\N
58	t	2020-09-01 08:50:28.180942	0	\N	\N	59	f	f	t	f	\N	\N
59	t	2020-09-01 08:50:28.180942	0	\N	1	59	t	t	t	t	\N	\N
60	t	2020-09-01 08:50:28.180942	0	\N	\N	60	f	f	t	f	\N	\N
61	t	2020-09-01 08:50:28.180942	0	\N	1	60	t	t	t	t	\N	\N
62	t	2020-09-01 08:50:28.180942	0	\N	\N	18	f	f	f	f	\N	\N
63	t	2020-09-01 08:50:28.180942	0	\N	1	18	t	t	t	t	\N	\N
64	t	2020-09-01 08:50:28.180942	0	\N	\N	66	f	f	t	f	\N	\N
65	t	2020-09-01 08:50:28.180942	0	\N	1	66	t	t	t	t	\N	\N
66	t	2020-09-02 07:24:23.639802	0	\N	\N	85	f	f	t	f	\N	\N
67	t	2020-09-02 07:24:23.639802	0	\N	2	85	t	t	t	t	\N	\N
68	t	2020-09-02 07:24:23.639802	0	\N	\N	86	f	f	t	f	\N	\N
69	t	2020-09-02 07:24:23.639802	0	\N	2	86	t	t	t	t	\N	\N
70	t	2020-09-02 07:24:24.586225	0	\N	\N	87	f	f	t	f	\N	\N
71	t	2020-09-02 07:24:24.586225	0	\N	3	87	t	t	t	t	\N	\N
72	t	2020-09-02 07:24:24.586225	0	\N	\N	96	f	f	t	f	\N	\N
73	t	2020-09-02 07:24:24.586225	0	\N	3	96	t	t	t	t	\N	\N
74	t	2020-09-02 07:24:24.586225	0	\N	\N	97	f	f	t	f	\N	\N
75	t	2020-09-02 07:24:24.586225	0	\N	3	97	t	t	t	t	\N	\N
76	t	2020-09-02 07:24:24.586225	0	\N	\N	99	f	f	t	f	\N	\N
77	t	2020-09-02 07:24:24.586225	0	\N	3	99	t	t	t	t	\N	\N
78	t	2020-09-02 07:24:29.514826	0	\N	\N	102	f	f	t	f	\N	\N
79	t	2020-09-02 07:24:29.514826	0	\N	4	102	t	t	t	t	\N	\N
80	t	2020-09-02 07:24:29.514826	0	\N	\N	103	f	f	t	f	\N	\N
81	t	2020-09-02 07:24:29.514826	0	\N	5	103	t	t	t	t	\N	\N
82	t	2020-09-02 07:24:32.977941	0	\N	\N	111	f	f	t	f	\N	\N
83	t	2020-09-02 07:24:32.977941	0	\N	6	111	t	t	t	t	\N	\N
84	t	2020-09-02 07:24:32.977941	0	\N	\N	110	f	f	t	f	\N	\N
85	t	2020-09-02 07:24:32.977941	0	\N	6	110	t	t	t	t	\N	\N
86	t	2020-09-02 07:24:32.977941	0	\N	\N	112	f	f	t	f	\N	\N
87	t	2020-09-02 07:24:32.977941	0	\N	6	112	t	t	t	t	\N	\N
88	t	2020-09-02 07:24:32.977941	0	\N	\N	109	f	f	t	f	\N	\N
89	t	2020-09-02 07:24:32.977941	0	\N	6	109	t	t	t	t	\N	\N
90	t	2020-09-02 07:24:32.977941	0	\N	\N	108	f	f	t	f	\N	\N
91	t	2020-09-02 07:24:32.977941	0	\N	6	108	t	t	t	t	\N	\N
92	t	2020-09-02 07:24:32.977941	0	\N	\N	107	f	f	t	f	\N	\N
93	t	2020-09-02 07:24:32.977941	0	\N	6	107	t	t	t	t	\N	\N
94	t	2020-09-02 07:24:32.977941	0	\N	\N	118	f	f	t	f	\N	\N
95	t	2020-09-02 07:24:32.977941	0	\N	6	118	t	t	t	t	\N	\N
96	t	2020-09-16 08:36:14.823954	0	\N	\N	127	f	f	f	f	\N	\N
97	t	2020-09-16 08:36:14.823954	0	\N	1	127	t	t	t	t	\N	\N
\.


--
-- Data for Name: ir_model_button; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_model_button (id, confirm, create_date, create_uid, help, model, name, string, write_date, write_uid) FROM stdin;
1	\N	2020-09-01 08:49:55.946968	0	\N	15	show	Show	\N	\N
2	\N	2020-09-01 08:49:55.946968	0	\N	37	sync	Sync	\N	\N
3	\N	2020-09-01 08:49:55.946968	0	\N	42	run_once	Run Once	\N	\N
4	Are you sure you want to load languages' translations?	2020-09-01 08:49:55.946968	0	\N	43	load_translations	Load translations	\N	\N
5	Are you sure you want to remove languages' translations?	2020-09-01 08:49:55.946968	0	\N	43	unload_translations	Unload translations	\N	\N
6	\N	2020-09-01 08:49:55.946968	0	\N	49	activate	Mark for Activation	\N	\N
7	\N	2020-09-01 08:49:55.946968	0	\N	49	activate_cancel	Cancel Activation	\N	\N
8	\N	2020-09-01 08:49:55.946968	0	\N	49	deactivate	Mark for Deactivation (beta)	\N	\N
9	\N	2020-09-01 08:49:55.946968	0	\N	49	deactivate_cancel	Cancel Deactivation	\N	\N
10	\N	2020-09-01 08:49:55.946968	0	\N	49	upgrade	Mark for Upgrade	\N	\N
11	\N	2020-09-01 08:49:55.946968	0	\N	49	upgrade_cancel	Cancel Upgrade	\N	\N
12	\N	2020-09-01 08:50:28.180942	0	Send by email a new temporary password to the user	68	reset_password	Reset Password	\N	\N
13	\N	2020-09-01 08:50:28.180942	0	\N	73	validate_	Validate	\N	\N
14	\N	2020-09-01 08:50:28.180942	0	\N	73	cancel	Cancel	\N	\N
15	\N	2020-09-16 08:36:14.823954	0	\N	127	validate_email	Validate E-mail	\N	\N
16	\N	2020-09-16 08:36:14.823954	0	\N	127	reset_password	Reset Password	\N	\N
\.


--
-- Data for Name: ir_model_button-button_reset; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."ir_model_button-button_reset" (id, button, button_ruled, create_date, create_uid, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_model_button-res_group; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."ir_model_button-res_group" (id, active, button, create_date, create_uid, "group", write_date, write_uid) FROM stdin;
1	t	6	2020-09-01 08:50:28.180942	0	1	\N	\N
2	t	7	2020-09-01 08:50:28.180942	0	1	\N	\N
3	t	8	2020-09-01 08:50:28.180942	0	1	\N	\N
4	t	9	2020-09-01 08:50:28.180942	0	1	\N	\N
5	t	10	2020-09-01 08:50:28.180942	0	1	\N	\N
6	t	11	2020-09-01 08:50:28.180942	0	1	\N	\N
7	t	3	2020-09-01 08:50:28.180942	0	1	\N	\N
8	t	2	2020-09-01 08:50:28.180942	0	1	\N	\N
9	t	1	2020-09-01 08:50:28.180942	0	1	\N	\N
\.


--
-- Data for Name: ir_model_button_click; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_model_button_click (id, active, button, create_date, create_uid, record_id, write_date, write_uid, "user") FROM stdin;
\.


--
-- Data for Name: ir_model_button_rule; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_model_button_rule (id, button, condition, create_date, create_uid, description, number_user, write_date, write_uid, "group") FROM stdin;
\.


--
-- Data for Name: ir_model_data; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_model_data (id, create_date, create_uid, db_id, fs_id, fs_values, model, module, noupdate, "values", write_date, write_uid) FROM stdin;
1	2020-09-01 08:49:55.946968	0	1	lang_en	[["am","AM"],["code","en"],["date","%m/%d/%Y"],["decimal_point","."],["grouping","[3, 3, 0]"],["mon_decimal_point","."],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep",","],["n_cs_precedes",true],["n_sep_by_space",false],["n_sign_posn",1],["name","English"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",false],["p_sign_posn",1],["pm","PM"],["positive_sign",""],["thousands_sep",","]]	ir.lang	ir	f	[["am","AM"],["code","en"],["date","%m/%d/%Y"],["decimal_point","."],["grouping","[3, 3, 0]"],["mon_decimal_point","."],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep",","],["n_cs_precedes",true],["n_sep_by_space",false],["n_sign_posn",1],["name","English"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",false],["p_sign_posn",1],["pm","PM"],["positive_sign",""],["thousands_sep",","]]	\N	\N
2	2020-09-01 08:49:55.946968	0	1	board_icon	[["name","tryton-board"],["path","ui/icons/tryton-board.svg"]]	ir.ui.icon	ir	f	[["name","tryton-board"],["path","ui/icons/tryton-board.svg"]]	\N	\N
3	2020-09-01 08:49:55.946968	0	2	calendar_icon	[["name","tryton-calendar"],["path","ui/icons/tryton-calendar.svg"]]	ir.ui.icon	ir	f	[["name","tryton-calendar"],["path","ui/icons/tryton-calendar.svg"]]	\N	\N
4	2020-09-01 08:49:55.946968	0	3	folder_icon	[["name","tryton-folder"],["path","ui/icons/tryton-folder.svg"]]	ir.ui.icon	ir	f	[["name","tryton-folder"],["path","ui/icons/tryton-folder.svg"]]	\N	\N
5	2020-09-01 08:49:55.946968	0	4	form_icon	[["name","tryton-form"],["path","ui/icons/tryton-form.svg"]]	ir.ui.icon	ir	f	[["name","tryton-form"],["path","ui/icons/tryton-form.svg"]]	\N	\N
6	2020-09-01 08:49:55.946968	0	5	graph_icon	[["name","tryton-graph"],["path","ui/icons/tryton-graph.svg"]]	ir.ui.icon	ir	f	[["name","tryton-graph"],["path","ui/icons/tryton-graph.svg"]]	\N	\N
7	2020-09-01 08:49:55.946968	0	6	list_icon	[["name","tryton-list"],["path","ui/icons/tryton-list.svg"]]	ir.ui.icon	ir	f	[["name","tryton-list"],["path","ui/icons/tryton-list.svg"]]	\N	\N
8	2020-09-01 08:49:55.946968	0	7	settings_icon	[["name","tryton-settings"],["path","ui/icons/tryton-settings.svg"]]	ir.ui.icon	ir	f	[["name","tryton-settings"],["path","ui/icons/tryton-settings.svg"]]	\N	\N
9	2020-09-01 08:49:55.946968	0	8	tree_icon	[["name","tryton-tree"],["path","ui/icons/tryton-tree.svg"]]	ir.ui.icon	ir	f	[["name","tryton-tree"],["path","ui/icons/tryton-tree.svg"]]	\N	\N
10	2020-09-01 08:49:55.946968	0	1	menu_administration	[["icon","tryton-settings"],["name","Administration"],["sequence",9999]]	ir.ui.menu	ir	f	[["icon","tryton-settings"],["name","Administration"],["sequence",9999]]	\N	\N
11	2020-09-01 08:49:55.946968	0	2	menu_ui	[["icon","tryton-folder"],["name","User Interface"],["parent",1]]	ir.ui.menu	ir	f	[["icon","tryton-folder"],["name","User Interface"],["parent",1]]	\N	\N
12	2020-09-01 08:49:55.946968	0	1	icon_view_tree	[["model","ir.ui.icon"],["name","icon_view_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.ui.icon"],["name","icon_view_list"],["type","tree"]]	\N	\N
13	2020-09-01 08:49:55.946968	0	2	icon_view_form	[["model","ir.ui.icon"],["name","icon_view_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.ui.icon"],["name","icon_view_form"],["type","form"]]	\N	\N
14	2020-09-01 08:49:55.946968	0	1	act_icon_form	[["name","Icons"],["res_model","ir.ui.icon"]]	ir.action.act_window	ir	f	[["name","Icons"],["res_model","ir.ui.icon"]]	\N	\N
15	2020-09-01 08:49:55.946968	0	1	act_icon_form_view1	[["act_window",1],["sequence",10],["view",1]]	ir.action.act_window.view	ir	f	[["act_window",1],["sequence",10],["view",1]]	\N	\N
16	2020-09-01 08:49:55.946968	0	2	act_icon_form_view2	[["act_window",1],["sequence",20],["view",2]]	ir.action.act_window.view	ir	f	[["act_window",1],["sequence",20],["view",2]]	\N	\N
17	2020-09-01 08:49:55.946968	0	3	menu_icon_form	[["action","ir.action.act_window,1"],["icon","tryton-list"],["name","Icons"],["parent",2]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,1"],["icon","tryton-list"],["name","Icons"],["parent",2]]	\N	\N
18	2020-09-01 08:49:55.946968	0	3	menu_view_tree_tree	[["field_childs","childs"],["model","ir.ui.menu"],["name","ui_menu_tree"],["priority",20],["type","tree"]]	ir.ui.view	ir	f	[["field_childs","childs"],["model","ir.ui.menu"],["name","ui_menu_tree"],["priority",20],["type","tree"]]	\N	\N
19	2020-09-01 08:49:55.946968	0	4	menu_view_list	[["model","ir.ui.menu"],["name","ui_menu_list"],["priority",10],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.ui.menu"],["name","ui_menu_list"],["priority",10],["type","tree"]]	\N	\N
20	2020-09-01 08:49:55.946968	0	5	menu_view_form	[["model","ir.ui.menu"],["name","ui_menu_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.ui.menu"],["name","ui_menu_form"],["type","form"]]	\N	\N
21	2020-09-01 08:49:55.946968	0	2	act_menu_tree	[["domain","[[\\"parent\\", \\"=\\", null]]"],["name","Menu"],["res_model","ir.ui.menu"],["usage","menu"]]	ir.action.act_window	ir	f	[["domain","[[\\"parent\\", \\"=\\", null]]"],["name","Menu"],["res_model","ir.ui.menu"],["usage","menu"]]	\N	\N
22	2020-09-01 08:49:55.946968	0	3	act_menu_tree_view1	[["act_window",2],["sequence",1],["view",3]]	ir.action.act_window.view	ir	f	[["act_window",2],["sequence",1],["view",3]]	\N	\N
23	2020-09-01 08:49:55.946968	0	3	act_menu_list	[["name","Menu"],["res_model","ir.ui.menu"]]	ir.action.act_window	ir	f	[["name","Menu"],["res_model","ir.ui.menu"]]	\N	\N
24	2020-09-01 08:49:55.946968	0	4	act_menu_list_view1	[["act_window",3],["sequence",10],["view",4]]	ir.action.act_window.view	ir	f	[["act_window",3],["sequence",10],["view",4]]	\N	\N
25	2020-09-01 08:49:55.946968	0	5	act_menu_list_view2	[["act_window",3],["sequence",20],["view",5]]	ir.action.act_window.view	ir	f	[["act_window",3],["sequence",20],["view",5]]	\N	\N
26	2020-09-01 08:49:55.946968	0	4	menu_menu_list	[["action","ir.action.act_window,3"],["icon","tryton-list"],["name","Menu"],["parent",2]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,3"],["icon","tryton-list"],["name","Menu"],["parent",2]]	\N	\N
27	2020-09-01 08:49:55.946968	0	6	menu_favorite_view_list	[["model","ir.ui.menu.favorite"],["name","ui_menu_favorite_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.ui.menu.favorite"],["name","ui_menu_favorite_list"],["type","tree"]]	\N	\N
28	2020-09-01 08:49:55.946968	0	7	menu_favorite_view_form	[["model","ir.ui.menu.favorite"],["name","ui_menu_favorite_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.ui.menu.favorite"],["name","ui_menu_favorite_form"],["type","form"]]	\N	\N
29	2020-09-01 08:49:55.946968	0	4	act_view_show	[["name","Show View"],["wiz_name","ir.ui.view.show"]]	ir.action.wizard	ir	f	[["name","Show View"],["wiz_name","ir.ui.view.show"]]	\N	\N
30	2020-09-01 08:49:55.946968	0	8	view_view_form	[["model","ir.ui.view"],["name","ui_view_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.ui.view"],["name","ui_view_form"],["type","form"]]	\N	\N
31	2020-09-01 08:49:55.946968	0	9	view_view_tree	[["model","ir.ui.view"],["name","ui_view_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.ui.view"],["name","ui_view_list"],["type","tree"]]	\N	\N
32	2020-09-01 08:49:55.946968	0	5	act_view_form	[["name","Views"],["res_model","ir.ui.view"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","Views"],["res_model","ir.ui.view"],["type","ir.action.act_window"]]	\N	\N
33	2020-09-01 08:49:55.946968	0	6	act_view_form_view1	[["act_window",5],["sequence",1],["view",9]]	ir.action.act_window.view	ir	f	[["act_window",5],["sequence",1],["view",9]]	\N	\N
34	2020-09-01 08:49:55.946968	0	7	act_view_form_view2	[["act_window",5],["sequence",2],["view",8]]	ir.action.act_window.view	ir	f	[["act_window",5],["sequence",2],["view",8]]	\N	\N
35	2020-09-01 08:49:55.946968	0	5	menu_view	[["action","ir.action.act_window,5"],["icon","tryton-list"],["name","Views"],["parent",2]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,5"],["icon","tryton-list"],["name","Views"],["parent",2]]	\N	\N
36	2020-09-01 08:49:55.946968	0	1	view_show_button	[["model",15],["name","show"],["string","Show"]]	ir.model.button	ir	f	[["model",15],["name","show"],["string","Show"]]	\N	\N
37	2020-09-01 08:49:55.946968	0	10	view_tree_width_view_form	[["model","ir.ui.view_tree_width"],["name","ui_view_tree_width_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.ui.view_tree_width"],["name","ui_view_tree_width_form"],["type","form"]]	\N	\N
38	2020-09-01 08:49:55.946968	0	11	view_tree_width_view_tree	[["model","ir.ui.view_tree_width"],["name","ui_view_tree_width_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.ui.view_tree_width"],["name","ui_view_tree_width_list"],["type","tree"]]	\N	\N
39	2020-09-01 08:49:55.946968	0	6	act_view_tree_width_form	[["name","View Tree Width"],["res_model","ir.ui.view_tree_width"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","View Tree Width"],["res_model","ir.ui.view_tree_width"],["type","ir.action.act_window"]]	\N	\N
40	2020-09-01 08:49:55.946968	0	8	act_view_tree_width_form_view1	[["act_window",6],["sequence",1],["view",11]]	ir.action.act_window.view	ir	f	[["act_window",6],["sequence",1],["view",11]]	\N	\N
41	2020-09-01 08:49:55.946968	0	9	act_view_tree_width_form_view2	[["act_window",6],["sequence",2],["view",10]]	ir.action.act_window.view	ir	f	[["act_window",6],["sequence",2],["view",10]]	\N	\N
42	2020-09-01 08:49:55.946968	0	6	menu_view_tree_width	[["action","ir.action.act_window,6"],["icon","tryton-list"],["name","View Tree Width"],["parent",2]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,6"],["icon","tryton-list"],["name","View Tree Width"],["parent",2]]	\N	\N
43	2020-09-01 08:49:55.946968	0	12	view_tree_state_form	[["model","ir.ui.view_tree_state"],["name","ui_view_tree_state_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.ui.view_tree_state"],["name","ui_view_tree_state_form"],["type","form"]]	\N	\N
44	2020-09-01 08:49:55.946968	0	13	view_tree_state_tree	[["model","ir.ui.view_tree_state"],["name","ui_view_tree_state_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.ui.view_tree_state"],["name","ui_view_tree_state_list"],["type","tree"]]	\N	\N
45	2020-09-01 08:49:55.946968	0	7	act_view_tree_state	[["name","Tree State"],["res_model","ir.ui.view_tree_state"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","Tree State"],["res_model","ir.ui.view_tree_state"],["type","ir.action.act_window"]]	\N	\N
46	2020-09-01 08:49:55.946968	0	10	act_view_tree_state_tree	[["act_window",7],["sequence",10],["view",13]]	ir.action.act_window.view	ir	f	[["act_window",7],["sequence",10],["view",13]]	\N	\N
47	2020-09-01 08:49:55.946968	0	11	act_view_tree_state_form	[["act_window",7],["sequence",20],["view",12]]	ir.action.act_window.view	ir	f	[["act_window",7],["sequence",20],["view",12]]	\N	\N
48	2020-09-01 08:49:55.946968	0	7	menu_view_tree_state	[["action","ir.action.act_window,7"],["icon","tryton-list"],["name","Tree State"],["parent",2]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,7"],["icon","tryton-list"],["name","Tree State"],["parent",2]]	\N	\N
49	2020-09-01 08:49:55.946968	0	14	view_search_form	[["model","ir.ui.view_search"],["name","ui_view_search_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.ui.view_search"],["name","ui_view_search_form"],["type","form"]]	\N	\N
50	2020-09-01 08:49:55.946968	0	15	view_search_tree	[["model","ir.ui.view_search"],["name","ui_view_search_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.ui.view_search"],["name","ui_view_search_list"],["type","tree"]]	\N	\N
51	2020-09-01 08:49:55.946968	0	8	act_view_search	[["name","View Search"],["res_model","ir.ui.view_search"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","View Search"],["res_model","ir.ui.view_search"],["type","ir.action.act_window"]]	\N	\N
52	2020-09-01 08:49:55.946968	0	12	act_view_search_tree	[["act_window",8],["sequence",10],["view",15]]	ir.action.act_window.view	ir	f	[["act_window",8],["sequence",10],["view",15]]	\N	\N
53	2020-09-01 08:49:55.946968	0	13	act_view_search_form	[["act_window",8],["sequence",20],["view",14]]	ir.action.act_window.view	ir	f	[["act_window",8],["sequence",20],["view",14]]	\N	\N
54	2020-09-01 08:49:55.946968	0	8	menu_view_search	[["action","ir.action.act_window,8"],["icon","tryton-list"],["name","View Search"],["parent",2]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,8"],["icon","tryton-list"],["name","View Search"],["parent",2]]	\N	\N
55	2020-09-01 08:49:55.946968	0	9	menu_action	[["icon","tryton-folder"],["name","Actions"],["parent",2]]	ir.ui.menu	ir	f	[["icon","tryton-folder"],["name","Actions"],["parent",2]]	\N	\N
56	2020-09-01 08:49:55.946968	0	16	action_view_form	[["model","ir.action"],["name","action_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.action"],["name","action_form"],["type","form"]]	\N	\N
57	2020-09-01 08:49:55.946968	0	17	action_view_tree	[["model","ir.action"],["name","action_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.action"],["name","action_list"],["type","tree"]]	\N	\N
58	2020-09-01 08:49:55.946968	0	9	act_action_form	[["name","Actions"],["res_model","ir.action"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","Actions"],["res_model","ir.action"],["type","ir.action.act_window"]]	\N	\N
59	2020-09-01 08:49:55.946968	0	14	act_action_form_view1	[["act_window",9],["sequence",1],["view",17]]	ir.action.act_window.view	ir	f	[["act_window",9],["sequence",1],["view",17]]	\N	\N
60	2020-09-01 08:49:55.946968	0	15	act_action_form_view2	[["act_window",9],["sequence",2],["view",16]]	ir.action.act_window.view	ir	f	[["act_window",9],["sequence",2],["view",16]]	\N	\N
61	2020-09-01 08:49:55.946968	0	10	menu_act_action	[["action","ir.action.act_window,9"],["icon","tryton-list"],["name","Actions"],["parent",9]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,9"],["icon","tryton-list"],["name","Actions"],["parent",9]]	\N	\N
62	2020-09-01 08:49:55.946968	0	18	action_keyword_view_list	[["model","ir.action.keyword"],["name","action_keyword_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.action.keyword"],["name","action_keyword_list"],["type","tree"]]	\N	\N
63	2020-09-01 08:49:55.946968	0	19	action_keyword_view_form	[["model","ir.action.keyword"],["name","action_keyword_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.action.keyword"],["name","action_keyword_form"],["type","form"]]	\N	\N
64	2020-09-01 08:49:55.946968	0	20	action_report_view_form	[["model","ir.action.report"],["name","action_report_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.action.report"],["name","action_report_form"],["type","form"]]	\N	\N
95	2020-09-01 08:49:55.946968	0	34	model_view_form	[["model","ir.model"],["name","model_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.model"],["name","model_form"],["type","form"]]	\N	\N
65	2020-09-01 08:49:55.946968	0	21	action_report_view_tree	[["model","ir.action.report"],["name","action_report_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.action.report"],["name","action_report_list"],["type","tree"]]	\N	\N
66	2020-09-01 08:49:55.946968	0	10	act_action_report_form	[["name","Reports"],["res_model","ir.action.report"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","Reports"],["res_model","ir.action.report"],["type","ir.action.act_window"]]	\N	\N
67	2020-09-01 08:49:55.946968	0	16	act_action_report_form_view1	[["act_window",10],["sequence",1],["view",21]]	ir.action.act_window.view	ir	f	[["act_window",10],["sequence",1],["view",21]]	\N	\N
68	2020-09-01 08:49:55.946968	0	17	act_action_report_form_view2	[["act_window",10],["sequence",2],["view",20]]	ir.action.act_window.view	ir	f	[["act_window",10],["sequence",2],["view",20]]	\N	\N
69	2020-09-01 08:49:55.946968	0	11	menu_action_report_form	[["action","ir.action.act_window,10"],["icon","tryton-list"],["name","Reports"],["parent",9]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,10"],["icon","tryton-list"],["name","Reports"],["parent",9]]	\N	\N
70	2020-09-01 08:49:55.946968	0	22	action_act_window_view_form	[["model","ir.action.act_window"],["name","action_act_window_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.action.act_window"],["name","action_act_window_form"],["type","form"]]	\N	\N
71	2020-09-01 08:49:55.946968	0	23	action_act_window_view_tree	[["model","ir.action.act_window"],["name","action_act_window_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.action.act_window"],["name","action_act_window_list"],["type","tree"]]	\N	\N
72	2020-09-01 08:49:55.946968	0	11	act_action_act_window_form	[["name","Window Actions"],["res_model","ir.action.act_window"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","Window Actions"],["res_model","ir.action.act_window"],["type","ir.action.act_window"]]	\N	\N
73	2020-09-01 08:49:55.946968	0	18	act_action_act_window_view1	[["act_window",11],["sequence",1],["view",23]]	ir.action.act_window.view	ir	f	[["act_window",11],["sequence",1],["view",23]]	\N	\N
74	2020-09-01 08:49:55.946968	0	19	act_action_act_window_view2	[["act_window",11],["sequence",2],["view",22]]	ir.action.act_window.view	ir	f	[["act_window",11],["sequence",2],["view",22]]	\N	\N
75	2020-09-01 08:49:55.946968	0	12	menu_action_act_window	[["action","ir.action.act_window,11"],["icon","tryton-list"],["name","Window Actions"],["parent",9]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,11"],["icon","tryton-list"],["name","Window Actions"],["parent",9]]	\N	\N
76	2020-09-01 08:49:55.946968	0	24	act_window_view_view_form	[["model","ir.action.act_window.view"],["name","action_act_window_view_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.action.act_window.view"],["name","action_act_window_view_form"],["type","form"]]	\N	\N
77	2020-09-01 08:49:55.946968	0	25	act_window_view_view_list	[["model","ir.action.act_window.view"],["name","action_act_window_view_list"],["priority",10],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.action.act_window.view"],["name","action_act_window_view_list"],["priority",10],["type","tree"]]	\N	\N
78	2020-09-01 08:49:55.946968	0	26	act_window_view_view_list2	[["model","ir.action.act_window.view"],["name","action_act_window_view_list2"],["priority",20],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.action.act_window.view"],["name","action_act_window_view_list2"],["priority",20],["type","tree"]]	\N	\N
79	2020-09-01 08:49:55.946968	0	27	act_window_domain_view_form	[["model","ir.action.act_window.domain"],["name","action_act_window_domain_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.action.act_window.domain"],["name","action_act_window_domain_form"],["type","form"]]	\N	\N
80	2020-09-01 08:49:55.946968	0	28	act_window_domain_view_list	[["model","ir.action.act_window.domain"],["name","action_act_window_domain_list"],["priority",10],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.action.act_window.domain"],["name","action_act_window_domain_list"],["priority",10],["type","tree"]]	\N	\N
81	2020-09-01 08:49:55.946968	0	29	act_window_domain_view_list2	[["model","ir.action.act_window.domain"],["name","action_act_window_domain_list2"],["priority",20],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.action.act_window.domain"],["name","action_act_window_domain_list2"],["priority",20],["type","tree"]]	\N	\N
82	2020-09-01 08:49:55.946968	0	30	action_wizard_view_form	[["model","ir.action.wizard"],["name","action_wizard_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.action.wizard"],["name","action_wizard_form"],["type","form"]]	\N	\N
83	2020-09-01 08:49:55.946968	0	31	action_wizard_view_tree	[["model","ir.action.wizard"],["name","action_wizard_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.action.wizard"],["name","action_wizard_list"],["type","tree"]]	\N	\N
84	2020-09-01 08:49:55.946968	0	12	act_action_wizard_form	[["name","Wizards"],["res_model","ir.action.wizard"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","Wizards"],["res_model","ir.action.wizard"],["type","ir.action.act_window"]]	\N	\N
85	2020-09-01 08:49:55.946968	0	20	act_action_wizard_form_view1	[["act_window",12],["sequence",1],["view",31]]	ir.action.act_window.view	ir	f	[["act_window",12],["sequence",1],["view",31]]	\N	\N
86	2020-09-01 08:49:55.946968	0	21	act_action_wizard_form_view2	[["act_window",12],["sequence",2],["view",30]]	ir.action.act_window.view	ir	f	[["act_window",12],["sequence",2],["view",30]]	\N	\N
87	2020-09-01 08:49:55.946968	0	13	menu_action_wizard	[["action","ir.action.act_window,12"],["icon","tryton-list"],["name","Wizards"],["parent",9]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,12"],["icon","tryton-list"],["name","Wizards"],["parent",9]]	\N	\N
88	2020-09-01 08:49:55.946968	0	32	action_url_view_form	[["model","ir.action.url"],["name","action_url_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.action.url"],["name","action_url_form"],["type","form"]]	\N	\N
89	2020-09-01 08:49:55.946968	0	33	action_url_view_tree	[["model","ir.action.url"],["name","action_url_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.action.url"],["name","action_url_list"],["type","tree"]]	\N	\N
90	2020-09-01 08:49:55.946968	0	13	act_action_url_form	[["name","URLs"],["res_model","ir.action.url"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","URLs"],["res_model","ir.action.url"],["type","ir.action.act_window"]]	\N	\N
91	2020-09-01 08:49:55.946968	0	22	act_action_url_form_view1	[["act_window",13],["sequence",1],["view",33]]	ir.action.act_window.view	ir	f	[["act_window",13],["sequence",1],["view",33]]	\N	\N
92	2020-09-01 08:49:55.946968	0	23	act_action_url_form_view2	[["act_window",13],["sequence",2],["view",32]]	ir.action.act_window.view	ir	f	[["act_window",13],["sequence",2],["view",32]]	\N	\N
93	2020-09-01 08:49:55.946968	0	14	menu_action_url	[["action","ir.action.act_window,13"],["icon","tryton-list"],["name","URLs"],["parent",9]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,13"],["icon","tryton-list"],["name","URLs"],["parent",9]]	\N	\N
94	2020-09-01 08:49:55.946968	0	15	menu_models	[["icon","tryton-folder"],["name","Models"],["parent",1]]	ir.ui.menu	ir	f	[["icon","tryton-folder"],["name","Models"],["parent",1]]	\N	\N
222	2020-09-01 08:49:55.946968	0	1	config_wizard_item_lang	[["action",33]]	ir.module.config_wizard.item	ir	f	[["action",33]]	\N	\N
96	2020-09-01 08:49:55.946968	0	35	model_view_tree	[["model","ir.model"],["name","model_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.model"],["name","model_list"],["type","tree"]]	\N	\N
97	2020-09-01 08:49:55.946968	0	14	act_model_form	[["name","Models"],["res_model","ir.model"]]	ir.action.act_window	ir	f	[["name","Models"],["res_model","ir.model"]]	\N	\N
98	2020-09-01 08:49:55.946968	0	24	act_model_form_view1	[["act_window",14],["sequence",1],["view",35]]	ir.action.act_window.view	ir	f	[["act_window",14],["sequence",1],["view",35]]	\N	\N
99	2020-09-01 08:49:55.946968	0	25	act_model_form_view2	[["act_window",14],["sequence",2],["view",34]]	ir.action.act_window.view	ir	f	[["act_window",14],["sequence",2],["view",34]]	\N	\N
100	2020-09-01 08:49:55.946968	0	16	menu_model_form	[["action","ir.action.act_window,14"],["icon","tryton-list"],["name","Models"],["parent",15]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,14"],["icon","tryton-list"],["name","Models"],["parent",15]]	\N	\N
101	2020-09-01 08:49:55.946968	0	36	model_fields_view_form	[["model","ir.model.field"],["name","model_field_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.model.field"],["name","model_field_form"],["type","form"]]	\N	\N
102	2020-09-01 08:49:55.946968	0	37	model_fields_view_tree	[["model","ir.model.field"],["name","model_field_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.model.field"],["name","model_field_list"],["type","tree"]]	\N	\N
103	2020-09-01 08:49:55.946968	0	15	act_model_fields_form	[["name","Fields"],["res_model","ir.model.field"]]	ir.action.act_window	ir	f	[["name","Fields"],["res_model","ir.model.field"]]	\N	\N
104	2020-09-01 08:49:55.946968	0	26	act_model_fields_form_view1	[["act_window",15],["sequence",1],["view",37]]	ir.action.act_window.view	ir	f	[["act_window",15],["sequence",1],["view",37]]	\N	\N
105	2020-09-01 08:49:55.946968	0	27	act_model_fields_form_view2	[["act_window",15],["sequence",2],["view",36]]	ir.action.act_window.view	ir	f	[["act_window",15],["sequence",2],["view",36]]	\N	\N
106	2020-09-01 08:49:55.946968	0	17	model_model_fields_form	[["action","ir.action.act_window,15"],["icon","tryton-list"],["name","Fields"],["parent",15]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,15"],["icon","tryton-list"],["name","Fields"],["parent",15]]	\N	\N
107	2020-09-01 08:49:55.946968	0	38	model_access_view_tree	[["model","ir.model.access"],["name","model_access_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.model.access"],["name","model_access_list"],["type","tree"]]	\N	\N
108	2020-09-01 08:49:55.946968	0	39	model_access_view_form	[["model","ir.model.access"],["name","model_access_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.model.access"],["name","model_access_form"],["type","form"]]	\N	\N
109	2020-09-01 08:49:55.946968	0	16	act_model_access_form	[["name","Models Access"],["res_model","ir.model.access"]]	ir.action.act_window	ir	f	[["name","Models Access"],["res_model","ir.model.access"]]	\N	\N
110	2020-09-01 08:49:55.946968	0	28	act_model_access_form_view1	[["act_window",16],["sequence",1],["view",38]]	ir.action.act_window.view	ir	f	[["act_window",16],["sequence",1],["view",38]]	\N	\N
111	2020-09-01 08:49:55.946968	0	29	act_model_access_form_view2	[["act_window",16],["sequence",2],["view",39]]	ir.action.act_window.view	ir	f	[["act_window",16],["sequence",2],["view",39]]	\N	\N
112	2020-09-01 08:49:55.946968	0	18	menu_model_access_form	[["action","ir.action.act_window,16"],["icon","tryton-list"],["name","Models Access"],["parent",15]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,16"],["icon","tryton-list"],["name","Models Access"],["parent",15]]	\N	\N
113	2020-09-01 08:49:55.946968	0	17	act_model_access_form_relate_model	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"model\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"model\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name","Access"],["res_model","ir.model.access"]]	ir.action.act_window	ir	f	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"model\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"model\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name","Access"],["res_model","ir.model.access"]]	\N	\N
114	2020-09-01 08:49:55.946968	0	15	act_model_access_form_relate_model_keyword1	[["action",17],["keyword","form_relate"],["model","ir.model,-1"]]	ir.action.keyword	ir	f	[["action",17],["keyword","form_relate"],["model","ir.model,-1"]]	\N	\N
115	2020-09-01 08:49:55.946968	0	40	model_field_access_view_tree	[["model","ir.model.field.access"],["name","model_field_access_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.model.field.access"],["name","model_field_access_list"],["type","tree"]]	\N	\N
116	2020-09-01 08:49:55.946968	0	41	model_field_access_view_form	[["model","ir.model.field.access"],["name","model_field_access_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.model.field.access"],["name","model_field_access_form"],["type","form"]]	\N	\N
117	2020-09-01 08:49:55.946968	0	18	act_model_field_access_form	[["name","Fields Access"],["res_model","ir.model.field.access"]]	ir.action.act_window	ir	f	[["name","Fields Access"],["res_model","ir.model.field.access"]]	\N	\N
118	2020-09-01 08:49:55.946968	0	30	act_model_field_access_form_view1	[["act_window",18],["sequence",10],["view",40]]	ir.action.act_window.view	ir	f	[["act_window",18],["sequence",10],["view",40]]	\N	\N
119	2020-09-01 08:49:55.946968	0	31	act_model_field_access_form_view2	[["act_window",18],["sequence",20],["view",41]]	ir.action.act_window.view	ir	f	[["act_window",18],["sequence",20],["view",41]]	\N	\N
120	2020-09-01 08:49:55.946968	0	19	menu_model_field_access_form	[["action","ir.action.act_window,18"],["icon","tryton-list"],["name","Fields Access"],["parent",18]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,18"],["icon","tryton-list"],["name","Fields Access"],["parent",18]]	\N	\N
121	2020-09-01 08:49:55.946968	0	19	act_model_field_access_form_relate_field	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"field\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"field\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name","Access"],["res_model","ir.model.field.access"]]	ir.action.act_window	ir	f	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"field\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"field\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name","Access"],["res_model","ir.model.field.access"]]	\N	\N
122	2020-09-01 08:49:55.946968	0	17	act_modelfield__access_form_relate_field_keyword1	[["action",19],["keyword","form_relate"],["model","ir.model.field,-1"]]	ir.action.keyword	ir	f	[["action",19],["keyword","form_relate"],["model","ir.model.field,-1"]]	\N	\N
123	2020-09-01 08:49:55.946968	0	20	report_model_graph	[["model","ir.model"],["name","Graph"],["report_name","ir.model.graph"]]	ir.action.report	ir	f	[["model","ir.model"],["name","Graph"],["report_name","ir.model.graph"]]	\N	\N
124	2020-09-01 08:49:55.946968	0	42	print_model_graph_start_view_form	[["model","ir.model.print_model_graph.start"],["name","model_print_model_graph_start_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.model.print_model_graph.start"],["name","model_print_model_graph_start_form"],["type","form"]]	\N	\N
125	2020-09-01 08:49:55.946968	0	21	print_model_graph	[["model","ir.model"],["name","Graph"],["wiz_name","ir.model.print_model_graph"]]	ir.action.wizard	ir	f	[["model","ir.model"],["name","Graph"],["wiz_name","ir.model.print_model_graph"]]	\N	\N
126	2020-09-01 08:49:55.946968	0	18	print_model_graph_keyword	[["action",21],["keyword","form_print"],["model","ir.model,-1"]]	ir.action.keyword	ir	f	[["action",21],["keyword","form_print"],["model","ir.model,-1"]]	\N	\N
127	2020-09-01 08:49:55.946968	0	22	report_model_workflow_graph	[["model","ir.model"],["name","Workflow Graph"],["report_name","ir.model.workflow_graph"]]	ir.action.report	ir	f	[["model","ir.model"],["name","Workflow Graph"],["report_name","ir.model.workflow_graph"]]	\N	\N
128	2020-09-01 08:49:55.946968	0	19	print_model_workflow_graph_keyword	[["action",22],["keyword","form_print"],["model","ir.model,-1"]]	ir.action.keyword	ir	f	[["action",22],["keyword","form_print"],["model","ir.model,-1"]]	\N	\N
129	2020-09-01 08:49:55.946968	0	43	model_button_view_list	[["model","ir.model.button"],["name","model_button_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.model.button"],["name","model_button_list"],["type","tree"]]	\N	\N
130	2020-09-01 08:49:55.946968	0	44	model_button_view_form	[["model","ir.model.button"],["name","model_button_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.model.button"],["name","model_button_form"],["type","form"]]	\N	\N
131	2020-09-01 08:49:55.946968	0	23	act_model_button_form	[["name","Buttons"],["res_model","ir.model.button"]]	ir.action.act_window	ir	f	[["name","Buttons"],["res_model","ir.model.button"]]	\N	\N
132	2020-09-01 08:49:55.946968	0	32	act_model_button_form_view1	[["act_window",23],["sequence",10],["view",43]]	ir.action.act_window.view	ir	f	[["act_window",23],["sequence",10],["view",43]]	\N	\N
133	2020-09-01 08:49:55.946968	0	33	act_model_button_form_view2	[["act_window",23],["sequence",20],["view",44]]	ir.action.act_window.view	ir	f	[["act_window",23],["sequence",20],["view",44]]	\N	\N
134	2020-09-01 08:49:55.946968	0	20	menu_model_button_form	[["action","ir.action.act_window,23"],["icon","tryton-list"],["name","Buttons"],["parent",18]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,23"],["icon","tryton-list"],["name","Buttons"],["parent",18]]	\N	\N
135	2020-09-01 08:49:55.946968	0	45	model_button_rule_view_list	[["model","ir.model.button.rule"],["name","model_button_rule_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.model.button.rule"],["name","model_button_rule_list"],["type","tree"]]	\N	\N
136	2020-09-01 08:49:55.946968	0	46	model_button_rule_view_form	[["model","ir.model.button.rule"],["name","model_button_rule_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.model.button.rule"],["name","model_button_rule_form"],["type","form"]]	\N	\N
137	2020-09-01 08:49:55.946968	0	47	model_button_click_view_list	[["model","ir.model.button.click"],["name","model_button_click_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.model.button.click"],["name","model_button_click_list"],["type","tree"]]	\N	\N
138	2020-09-01 08:49:55.946968	0	48	model_button_click_view_form	[["model","ir.model.button.click"],["name","model_button_click_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.model.button.click"],["name","model_button_click_form"],["type","form"]]	\N	\N
139	2020-09-01 08:49:55.946968	0	24	act_model_button_click_form_relate_model_button	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"button\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"button\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name","Clicks"],["res_model","ir.model.button.click"]]	ir.action.act_window	ir	f	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"button\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"button\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name","Clicks"],["res_model","ir.model.button.click"]]	\N	\N
140	2020-09-01 08:49:55.946968	0	21	act_model_button_click_form_relate_model_button_keyword1	[["action",24],["keyword","form_relate"],["model","ir.model.button,-1"]]	ir.action.keyword	ir	f	[["action",24],["keyword","form_relate"],["model","ir.model.button,-1"]]	\N	\N
141	2020-09-01 08:49:55.946968	0	49	model_data_view_list	[["model","ir.model.data"],["name","model_data_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.model.data"],["name","model_data_list"],["type","tree"]]	\N	\N
142	2020-09-01 08:49:55.946968	0	50	model_data_view_form	[["model","ir.model.data"],["name","model_data_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.model.data"],["name","model_data_form"],["type","form"]]	\N	\N
143	2020-09-01 08:49:55.946968	0	25	act_model_data_form	[["name","Data"],["res_model","ir.model.data"]]	ir.action.act_window	ir	f	[["name","Data"],["res_model","ir.model.data"]]	\N	\N
144	2020-09-01 08:49:55.946968	0	34	act_model_data_form_view1	[["act_window",25],["sequence",10],["view",49]]	ir.action.act_window.view	ir	f	[["act_window",25],["sequence",10],["view",49]]	\N	\N
145	2020-09-01 08:49:55.946968	0	35	act_model_data_form_view2	[["act_window",25],["sequence",20],["view",50]]	ir.action.act_window.view	ir	f	[["act_window",25],["sequence",20],["view",50]]	\N	\N
146	2020-09-01 08:49:55.946968	0	1	act_model_data_form_domain_out_of_sync	[["act_window",25],["count",true],["domain","[[\\"out_of_sync\\", \\"=\\", true]]"],["name","Out of Sync"],["sequence",10]]	ir.action.act_window.domain	ir	f	[["act_window",25],["count",true],["domain","[[\\"out_of_sync\\", \\"=\\", true]]"],["name","Out of Sync"],["sequence",10]]	\N	\N
147	2020-09-01 08:49:55.946968	0	2	act_model_data_form_domain_all	[["act_window",25],["domain",""],["name","All"],["sequence",9999]]	ir.action.act_window.domain	ir	f	[["act_window",25],["domain",""],["name","All"],["sequence",9999]]	\N	\N
148	2020-09-01 08:49:55.946968	0	21	menu_model_data_form	[["action","ir.action.act_window,25"],["icon","tryton-list"],["name","Data"],["parent",16]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,25"],["icon","tryton-list"],["name","Data"],["parent",16]]	\N	\N
149	2020-09-01 08:49:55.946968	0	2	model_data_sync_button	[["model",37],["name","sync"],["string","Sync"]]	ir.model.button	ir	f	[["model",37],["name","sync"],["string","Sync"]]	\N	\N
150	2020-09-01 08:49:55.946968	0	22	menu_sequences	[["icon","tryton-folder"],["name","Sequences"],["parent",1]]	ir.ui.menu	ir	f	[["icon","tryton-folder"],["name","Sequences"],["parent",1]]	\N	\N
151	2020-09-01 08:49:55.946968	0	51	sequence_view_form	[["model","ir.sequence"],["name","sequence_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.sequence"],["name","sequence_form"],["type","form"]]	\N	\N
152	2020-09-01 08:49:55.946968	0	52	sequence_view_tree	[["model","ir.sequence"],["name","sequence_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.sequence"],["name","sequence_list"],["type","tree"]]	\N	\N
153	2020-09-01 08:49:55.946968	0	26	act_sequence_form	[["context","{\\"active_test\\": false}"],["name","Sequences"],["res_model","ir.sequence"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["context","{\\"active_test\\": false}"],["name","Sequences"],["res_model","ir.sequence"],["type","ir.action.act_window"]]	\N	\N
154	2020-09-01 08:49:55.946968	0	36	act_sequence_form_view1	[["act_window",26],["sequence",1],["view",52]]	ir.action.act_window.view	ir	f	[["act_window",26],["sequence",1],["view",52]]	\N	\N
155	2020-09-01 08:49:55.946968	0	37	act_sequence_form_view2	[["act_window",26],["sequence",2],["view",51]]	ir.action.act_window.view	ir	f	[["act_window",26],["sequence",2],["view",51]]	\N	\N
156	2020-09-01 08:49:55.946968	0	23	menu_sequence_form	[["action","ir.action.act_window,26"],["icon","tryton-list"],["name","Sequences"],["parent",22]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,26"],["icon","tryton-list"],["name","Sequences"],["parent",22]]	\N	\N
157	2020-09-01 08:49:55.946968	0	53	sequence_strict_view_form	[["inherit",51],["model","ir.sequence.strict"],["type",null]]	ir.ui.view	ir	f	[["inherit",51],["model","ir.sequence.strict"],["type",null]]	\N	\N
158	2020-09-01 08:49:55.946968	0	54	sequence_strict_view_tree	[["inherit",52],["model","ir.sequence.strict"],["type",null]]	ir.ui.view	ir	f	[["inherit",52],["model","ir.sequence.strict"],["type",null]]	\N	\N
159	2020-09-01 08:49:55.946968	0	27	act_sequence_strict_form	[["context","{\\"active_test\\": false}"],["name","Sequences Strict"],["res_model","ir.sequence.strict"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["context","{\\"active_test\\": false}"],["name","Sequences Strict"],["res_model","ir.sequence.strict"],["type","ir.action.act_window"]]	\N	\N
160	2020-09-01 08:49:55.946968	0	38	act_sequence_strict_form_view1	[["act_window",27],["sequence",1],["view",54]]	ir.action.act_window.view	ir	f	[["act_window",27],["sequence",1],["view",54]]	\N	\N
161	2020-09-01 08:49:55.946968	0	39	act_sequence_strict_form_view2	[["act_window",27],["sequence",2],["view",53]]	ir.action.act_window.view	ir	f	[["act_window",27],["sequence",2],["view",53]]	\N	\N
162	2020-09-01 08:49:55.946968	0	24	menu_sequence_strict_form	[["action","ir.action.act_window,27"],["icon","tryton-list"],["name","Sequences Strict"],["parent",22]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,27"],["icon","tryton-list"],["name","Sequences Strict"],["parent",22]]	\N	\N
163	2020-09-01 08:49:55.946968	0	55	sequence_type_view_form	[["model","ir.sequence.type"],["name","sequence_type_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.sequence.type"],["name","sequence_type_form"],["type","form"]]	\N	\N
164	2020-09-01 08:49:55.946968	0	56	sequence_type_view_tree	[["model","ir.sequence.type"],["name","sequence_type_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.sequence.type"],["name","sequence_type_list"],["type","tree"]]	\N	\N
165	2020-09-01 08:49:55.946968	0	28	act_sequence_type_form	[["name","Sequence Types"],["res_model","ir.sequence.type"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","Sequence Types"],["res_model","ir.sequence.type"],["type","ir.action.act_window"]]	\N	\N
166	2020-09-01 08:49:55.946968	0	40	act_sequence_type_form_view1	[["act_window",28],["sequence",1],["view",56]]	ir.action.act_window.view	ir	f	[["act_window",28],["sequence",1],["view",56]]	\N	\N
167	2020-09-01 08:49:55.946968	0	41	act_sequence_type_form_view2	[["act_window",28],["sequence",2],["view",55]]	ir.action.act_window.view	ir	f	[["act_window",28],["sequence",2],["view",55]]	\N	\N
168	2020-09-01 08:49:55.946968	0	25	menu_ir_sequence_type	[["action","ir.action.act_window,28"],["icon","tryton-list"],["name","Sequence Types"],["parent",22]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,28"],["icon","tryton-list"],["name","Sequence Types"],["parent",22]]	\N	\N
169	2020-09-01 08:49:55.946968	0	57	attachment_view_form	[["model","ir.attachment"],["name","attachment_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.attachment"],["name","attachment_form"],["type","form"]]	\N	\N
170	2020-09-01 08:49:55.946968	0	58	attachment_view_tree	[["model","ir.attachment"],["name","attachment_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.attachment"],["name","attachment_list"],["type","tree"]]	\N	\N
171	2020-09-01 08:49:55.946968	0	29	act_attachment_form	[["name","Attachments"],["res_model","ir.attachment"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","Attachments"],["res_model","ir.attachment"],["type","ir.action.act_window"]]	\N	\N
172	2020-09-01 08:49:55.946968	0	42	act_attachment_form_view1	[["act_window",29],["sequence",1],["view",58]]	ir.action.act_window.view	ir	f	[["act_window",29],["sequence",1],["view",58]]	\N	\N
173	2020-09-01 08:49:55.946968	0	43	act_attachment_form_view2	[["act_window",29],["sequence",2],["view",57]]	ir.action.act_window.view	ir	f	[["act_window",29],["sequence",2],["view",57]]	\N	\N
174	2020-09-01 08:49:55.946968	0	26	menu_attachment_form	[["action","ir.action.act_window,29"],["icon","tryton-list"],["name","Attachments"],["parent",15]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,29"],["icon","tryton-list"],["name","Attachments"],["parent",15]]	\N	\N
175	2020-09-01 08:49:55.946968	0	59	note_view_form	[["model","ir.note"],["name","note_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.note"],["name","note_form"],["type","form"]]	\N	\N
176	2020-09-01 08:49:55.946968	0	60	note_view_list	[["model","ir.note"],["name","note_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.note"],["name","note_list"],["type","tree"]]	\N	\N
177	2020-09-01 08:49:55.946968	0	30	act_note_form	[["name","Notes"],["res_model","ir.note"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","Notes"],["res_model","ir.note"],["type","ir.action.act_window"]]	\N	\N
178	2020-09-01 08:49:55.946968	0	44	act_note_form_view1	[["act_window",30],["sequence",1],["view",60]]	ir.action.act_window.view	ir	f	[["act_window",30],["sequence",1],["view",60]]	\N	\N
179	2020-09-01 08:49:55.946968	0	45	act_note_form_view2	[["act_window",30],["sequence",2],["view",59]]	ir.action.act_window.view	ir	f	[["act_window",30],["sequence",2],["view",59]]	\N	\N
180	2020-09-01 08:49:55.946968	0	27	menu_note_form	[["action","ir.action.act_window,30"],["icon","tryton-list"],["name","Notes"],["parent",15]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,30"],["icon","tryton-list"],["name","Notes"],["parent",15]]	\N	\N
181	2020-09-01 08:49:55.946968	0	28	menu_scheduler	[["icon","tryton-folder"],["name","Scheduler"],["parent",1]]	ir.ui.menu	ir	f	[["icon","tryton-folder"],["name","Scheduler"],["parent",1]]	\N	\N
182	2020-09-01 08:49:55.946968	0	61	cron_view_tree	[["model","ir.cron"],["name","cron_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.cron"],["name","cron_list"],["type","tree"]]	\N	\N
183	2020-09-01 08:49:55.946968	0	62	cron_view_form	[["model","ir.cron"],["name","cron_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.cron"],["name","cron_form"],["type","form"]]	\N	\N
184	2020-09-01 08:49:55.946968	0	31	act_cron_form	[["context",""],["name","Scheduled Actions"],["res_model","ir.cron"]]	ir.action.act_window	ir	f	[["context",""],["name","Scheduled Actions"],["res_model","ir.cron"]]	\N	\N
185	2020-09-01 08:49:55.946968	0	46	act_cron_form_view1	[["act_window",31],["sequence",1],["view",61]]	ir.action.act_window.view	ir	f	[["act_window",31],["sequence",1],["view",61]]	\N	\N
186	2020-09-01 08:49:55.946968	0	47	act_cron_form_view2	[["act_window",31],["sequence",2],["view",62]]	ir.action.act_window.view	ir	f	[["act_window",31],["sequence",2],["view",62]]	\N	\N
187	2020-09-01 08:49:55.946968	0	29	menu_cron_form	[["action","ir.action.act_window,31"],["icon","tryton-list"],["name","Scheduled Actions"],["parent",28]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,31"],["icon","tryton-list"],["name","Scheduled Actions"],["parent",28]]	\N	\N
188	2020-09-01 08:49:55.946968	0	3	cron_run_once_button	[["model",42],["name","run_once"],["string","Run Once"]]	ir.model.button	ir	f	[["model",42],["name","run_once"],["string","Run Once"]]	\N	\N
189	2020-09-01 08:49:55.946968	0	30	menu_localization	[["icon","tryton-folder"],["name","Localization"],["parent",1]]	ir.ui.menu	ir	f	[["icon","tryton-folder"],["name","Localization"],["parent",1]]	\N	\N
190	2020-09-01 08:49:55.946968	0	2	lang_bg	[["am",""],["code","bg"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","\\u00a0"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Bulgarian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	ir.lang	ir	f	[["am",""],["code","bg"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","\\u00a0"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Bulgarian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	\N	\N
191	2020-09-01 08:49:55.946968	0	3	lang_ca	[["am","a. m."],["code","ca"],["date","%d/%m/%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Catal\\u00e0"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm","p. m."],["positive_sign",""],["thousands_sep"," "]]	ir.lang	ir	f	[["am","a. m."],["code","ca"],["date","%d/%m/%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Catal\\u00e0"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm","p. m."],["positive_sign",""],["thousands_sep"," "]]	\N	\N
192	2020-09-01 08:49:55.946968	0	4	lang_cs	[["am",""],["code","cs"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","\\u00a0"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Czech"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep"," "]]	ir.lang	ir	f	[["am",""],["code","cs"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","\\u00a0"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Czech"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep"," "]]	\N	\N
193	2020-09-01 08:49:55.946968	0	5	lang_de	[["am",""],["code","de"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","German"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	ir.lang	ir	f	[["am",""],["code","de"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","German"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	\N	\N
194	2020-09-01 08:49:55.946968	0	6	lang_es	[["am",""],["code","es"],["date","%d/%m/%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Spanish"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	ir.lang	ir	f	[["am",""],["code","es"],["date","%d/%m/%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Spanish"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	\N	\N
195	2020-09-01 08:49:55.946968	0	7	lang_es_419	[["am",""],["code","es_419"],["date","%d/%m/%Y"],["decimal_point","."],["grouping","[3, 3, 0]"],["mon_decimal_point","."],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep",","],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Spanish (Latin American)"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep",","]]	ir.lang	ir	f	[["am",""],["code","es_419"],["date","%d/%m/%Y"],["decimal_point","."],["grouping","[3, 3, 0]"],["mon_decimal_point","."],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep",","],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Spanish (Latin American)"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep",","]]	\N	\N
196	2020-09-01 08:49:55.946968	0	8	lang_et	[["code","et"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3]"],["mon_decimal_point",","],["mon_grouping","[3, 3]"],["mon_thousands_sep","\\u00a0"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Estonian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["positive_sign",""],["thousands_sep","\\u00a0"]]	ir.lang	ir	f	[["code","et"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3]"],["mon_decimal_point",","],["mon_grouping","[3, 3]"],["mon_thousands_sep","\\u00a0"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Estonian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["positive_sign",""],["thousands_sep","\\u00a0"]]	\N	\N
316	2020-09-01 08:49:55.946968	0	11	November	[["abbreviation","Nov"],["index",11],["name","November"]]	ir.calendar.month	ir	f	[["abbreviation","Nov"],["index",11],["name","November"]]	\N	\N
197	2020-09-01 08:49:55.946968	0	9	lang_fa	[["am",""],["code","fa"],["date","%Y/%m/%d"],["decimal_point","."],["direction","rtl"],["grouping","[3, 0]"],["mon_decimal_point","\\u066b"],["mon_grouping","[3, 0]"],["mon_thousands_sep","\\u066c"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Persian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep",","]]	ir.lang	ir	f	[["am",""],["code","fa"],["date","%Y/%m/%d"],["decimal_point","."],["direction","rtl"],["grouping","[3, 0]"],["mon_decimal_point","\\u066b"],["mon_grouping","[3, 0]"],["mon_thousands_sep","\\u066c"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Persian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep",","]]	\N	\N
198	2020-09-01 08:49:55.946968	0	10	lang_fi	[["code","fi"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","\\u00a0"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Finnish"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["positive_sign",""],["thousands_sep","\\u00a0"]]	ir.lang	ir	f	[["code","fi"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","\\u00a0"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Finnish"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["positive_sign",""],["thousands_sep","\\u00a0"]]	\N	\N
199	2020-09-01 08:49:55.946968	0	11	lang_fr	[["am",""],["code","fr"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 0]"],["mon_thousands_sep"," "],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","French"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep"," "]]	ir.lang	ir	f	[["am",""],["code","fr"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 0]"],["mon_thousands_sep"," "],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","French"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep"," "]]	\N	\N
200	2020-09-01 08:49:55.946968	0	12	lang_hu	[["am",""],["code","hu"],["date","%Y-%m-%d"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Hungarian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	ir.lang	ir	f	[["am",""],["code","hu"],["date","%Y-%m-%d"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Hungarian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	\N	\N
201	2020-09-01 08:49:55.946968	0	13	lang_id	[["am",""],["code","id"],["date","%d/%m/%Y"],["decimal_point",","],["grouping","[3, 3]"],["mon_decimal_point",","],["mon_grouping","[3, 3]"],["mon_thousands_sep","."],["n_cs_precedes",true],["n_sep_by_space",false],["n_sign_posn",1],["name","Indonesian"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",false],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	ir.lang	ir	f	[["am",""],["code","id"],["date","%d/%m/%Y"],["decimal_point",","],["grouping","[3, 3]"],["mon_decimal_point",","],["mon_grouping","[3, 3]"],["mon_thousands_sep","."],["n_cs_precedes",true],["n_sep_by_space",false],["n_sign_posn",1],["name","Indonesian"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",false],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	\N	\N
202	2020-09-01 08:49:55.946968	0	14	lang_it	[["am",""],["code","it"],["date","%d/%m/%Y"],["decimal_point",","],["grouping","[]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",true],["n_sep_by_space",true],["n_sign_posn",1],["name","Italian"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep",""]]	ir.lang	ir	f	[["am",""],["code","it"],["date","%d/%m/%Y"],["decimal_point",","],["grouping","[]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",true],["n_sep_by_space",true],["n_sign_posn",1],["name","Italian"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep",""]]	\N	\N
203	2020-09-01 08:49:55.946968	0	15	lang_lo	[["am","AM"],["code","lo"],["date","%d/%m/%Y"],["decimal_point","."],["grouping","[3, 3, 0]"],["mon_decimal_point","."],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep",","],["n_cs_precedes",true],["n_sep_by_space",true],["n_sign_posn",4],["name","Lao"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",true],["p_sign_posn",4],["pm","PM"],["positive_sign",""],["thousands_sep",","]]	ir.lang	ir	f	[["am","AM"],["code","lo"],["date","%d/%m/%Y"],["decimal_point","."],["grouping","[3, 3, 0]"],["mon_decimal_point","."],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep",","],["n_cs_precedes",true],["n_sep_by_space",true],["n_sign_posn",4],["name","Lao"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",true],["p_sign_posn",4],["pm","PM"],["positive_sign",""],["thousands_sep",","]]	\N	\N
204	2020-09-01 08:49:55.946968	0	16	lang_lt	[["am",""],["code","lt"],["date","%Y-%m-%d"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Lithuanian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	ir.lang	ir	f	[["am",""],["code","lt"],["date","%Y-%m-%d"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Lithuanian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep","."]]	\N	\N
205	2020-09-01 08:49:55.946968	0	17	lang_nl	[["am",""],["code","nl"],["date","%d-%m-%Y"],["decimal_point",","],["grouping","[]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep"," "],["n_cs_precedes",true],["n_sep_by_space",true],["n_sign_posn",2],["name","Dutch"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep",""]]	ir.lang	ir	f	[["am",""],["code","nl"],["date","%d-%m-%Y"],["decimal_point",","],["grouping","[]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep"," "],["n_cs_precedes",true],["n_sep_by_space",true],["n_sign_posn",2],["name","Dutch"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep",""]]	\N	\N
206	2020-09-01 08:49:55.946968	0	18	lang_pl	[["am",""],["code","pl"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 0, 0]"],["mon_thousands_sep"," "],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Polish"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep"," "]]	ir.lang	ir	f	[["am",""],["code","pl"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 0, 0]"],["mon_thousands_sep"," "],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Polish"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep"," "]]	\N	\N
207	2020-09-01 08:49:55.946968	0	19	lang_pt	[["am",""],["code","pt"],["date","%d-%m-%Y"],["decimal_point",","],["grouping","[]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",true],["n_sep_by_space",true],["n_sign_posn",1],["name","Portuguese"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep",""]]	ir.lang	ir	f	[["am",""],["code","pt"],["date","%d-%m-%Y"],["decimal_point",","],["grouping","[]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",true],["n_sep_by_space",true],["n_sign_posn",1],["name","Portuguese"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep",""]]	\N	\N
208	2020-09-01 08:49:55.946968	0	20	lang_ru	[["am",""],["code","ru"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point","."],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","\\u00a0"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Russian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep"," "]]	ir.lang	ir	f	[["am",""],["code","ru"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point","."],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","\\u00a0"],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Russian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep"," "]]	\N	\N
209	2020-09-01 08:49:55.946968	0	21	lang_sl	[["am",""],["code","sl"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep"," "],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Slovenian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep"," "]]	ir.lang	ir	f	[["am",""],["code","sl"],["date","%d.%m.%Y"],["decimal_point",","],["grouping","[]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep"," "],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Slovenian"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["pm",""],["positive_sign",""],["thousands_sep"," "]]	\N	\N
210	2020-09-01 08:49:55.946968	0	22	lang_tr	[["code","tr"],["date","%d-%m-%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Turkish"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["positive_sign",""],["thousands_sep","."]]	ir.lang	ir	f	[["code","tr"],["date","%d-%m-%Y"],["decimal_point",","],["grouping","[3, 3, 0]"],["mon_decimal_point",","],["mon_grouping","[3, 3, 0]"],["mon_thousands_sep","."],["n_cs_precedes",false],["n_sep_by_space",true],["n_sign_posn",1],["name","Turkish"],["negative_sign","-"],["p_cs_precedes",false],["p_sep_by_space",true],["p_sign_posn",1],["positive_sign",""],["thousands_sep","."]]	\N	\N
211	2020-09-01 08:49:55.946968	0	23	lang_zh_CN	[["am","\\u4e0a\\u5348"],["code","zh_CN"],["date","%Y-%m-%d"],["decimal_point","."],["grouping","[3, 0]"],["mon_decimal_point","."],["mon_grouping","[3, 0]"],["mon_thousands_sep",","],["n_cs_precedes",true],["n_sep_by_space",false],["n_sign_posn",4],["name","Chinese Simplified"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",false],["p_sign_posn",4],["pm","\\u4e0b\\u5348"],["positive_sign",""],["thousands_sep",","]]	ir.lang	ir	f	[["am","\\u4e0a\\u5348"],["code","zh_CN"],["date","%Y-%m-%d"],["decimal_point","."],["grouping","[3, 0]"],["mon_decimal_point","."],["mon_grouping","[3, 0]"],["mon_thousands_sep",","],["n_cs_precedes",true],["n_sep_by_space",false],["n_sign_posn",4],["name","Chinese Simplified"],["negative_sign","-"],["p_cs_precedes",true],["p_sep_by_space",false],["p_sign_posn",4],["pm","\\u4e0b\\u5348"],["positive_sign",""],["thousands_sep",","]]	\N	\N
212	2020-09-01 08:49:55.946968	0	63	lang_view_tree	[["model","ir.lang"],["name","lang_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.lang"],["name","lang_list"],["type","tree"]]	\N	\N
213	2020-09-01 08:49:55.946968	0	64	lang_view_form	[["model","ir.lang"],["name","lang_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.lang"],["name","lang_form"],["type","form"]]	\N	\N
214	2020-09-01 08:49:55.946968	0	32	act_lang_form	[["context","{\\"active_test\\": false}"],["name","Languages"],["res_model","ir.lang"]]	ir.action.act_window	ir	f	[["context","{\\"active_test\\": false}"],["name","Languages"],["res_model","ir.lang"]]	\N	\N
215	2020-09-01 08:49:55.946968	0	48	act_lang_form_view1	[["act_window",32],["sequence",1],["view",63]]	ir.action.act_window.view	ir	f	[["act_window",32],["sequence",1],["view",63]]	\N	\N
216	2020-09-01 08:49:55.946968	0	49	act_lang_form_view2	[["act_window",32],["sequence",2],["view",64]]	ir.action.act_window.view	ir	f	[["act_window",32],["sequence",2],["view",64]]	\N	\N
217	2020-09-01 08:49:55.946968	0	31	menu_lang_form	[["action","ir.action.act_window,32"],["icon","tryton-list"],["name","Languages"],["parent",30]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,32"],["icon","tryton-list"],["name","Languages"],["parent",30]]	\N	\N
218	2020-09-01 08:49:55.946968	0	4	lang_load_translations_button	[["confirm","Are you sure you want to load languages' translations?"],["model",43],["name","load_translations"],["string","Load translations"]]	ir.model.button	ir	f	[["confirm","Are you sure you want to load languages' translations?"],["model",43],["name","load_translations"],["string","Load translations"]]	\N	\N
219	2020-09-01 08:49:55.946968	0	5	lang_unload_translations_button	[["confirm","Are you sure you want to remove languages' translations?"],["model",43],["name","unload_translations"],["string","Unload translations"]]	ir.model.button	ir	f	[["confirm","Are you sure you want to remove languages' translations?"],["model",43],["name","unload_translations"],["string","Unload translations"]]	\N	\N
220	2020-09-01 08:49:55.946968	0	65	lang_config_start_view_form	[["model","ir.lang.config.start"],["name","lang_config_start_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.lang.config.start"],["name","lang_config_start_form"],["type","form"]]	\N	\N
221	2020-09-01 08:49:55.946968	0	33	act_lang_config	[["name","Configure Languages"],["window",true],["wiz_name","ir.lang.config"]]	ir.action.wizard	ir	f	[["name","Configure Languages"],["window",true],["wiz_name","ir.lang.config"]]	\N	\N
223	2020-09-01 08:49:55.946968	0	66	translation_view_form	[["model","ir.translation"],["name","translation_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.translation"],["name","translation_form"],["type","form"]]	\N	\N
224	2020-09-01 08:49:55.946968	0	67	translation_view_tree	[["model","ir.translation"],["name","translation_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.translation"],["name","translation_list"],["type","tree"]]	\N	\N
225	2020-09-01 08:49:55.946968	0	34	act_translation_form	[["domain",null],["name","Translations"],["res_model","ir.translation"]]	ir.action.act_window	ir	f	[["domain",null],["name","Translations"],["res_model","ir.translation"]]	\N	\N
226	2020-09-01 08:49:55.946968	0	3	act_translation_form_domain_module	[["act_window",34],["domain","[[\\"module\\", \\"!=\\", null]]"],["name","Modules"],["sequence",10]]	ir.action.act_window.domain	ir	f	[["act_window",34],["domain","[[\\"module\\", \\"!=\\", null]]"],["name","Modules"],["sequence",10]]	\N	\N
227	2020-09-01 08:49:55.946968	0	4	act_translation_form_domain_local	[["act_window",34],["domain","[[\\"module\\", \\"=\\", null]]"],["name","Local"],["sequence",20]]	ir.action.act_window.domain	ir	f	[["act_window",34],["domain","[[\\"module\\", \\"=\\", null]]"],["name","Local"],["sequence",20]]	\N	\N
228	2020-09-01 08:49:55.946968	0	50	act_translation_form_view1	[["act_window",34],["sequence",1],["view",67]]	ir.action.act_window.view	ir	f	[["act_window",34],["sequence",1],["view",67]]	\N	\N
229	2020-09-01 08:49:55.946968	0	51	act_translation_form_view2	[["act_window",34],["sequence",2],["view",66]]	ir.action.act_window.view	ir	f	[["act_window",34],["sequence",2],["view",66]]	\N	\N
230	2020-09-01 08:49:55.946968	0	32	menu_translation_form	[["action","ir.action.act_window,34"],["icon","tryton-list"],["name","Translations"],["parent",30]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,34"],["icon","tryton-list"],["name","Translations"],["parent",30]]	\N	\N
231	2020-09-01 08:49:55.946968	0	35	wizard_translation_report	[["model","ir.action.report"],["name","Translations"],["wiz_name","ir.translation.report"]]	ir.action.wizard	ir	f	[["model","ir.action.report"],["name","Translations"],["wiz_name","ir.translation.report"]]	\N	\N
232	2020-09-01 08:49:55.946968	0	31	wizard_translation_report_keyword1	[["action",35],["keyword","form_relate"],["model","ir.action.report,-1"]]	ir.action.keyword	ir	f	[["action",35],["keyword","form_relate"],["model","ir.action.report,-1"]]	\N	\N
233	2020-09-01 08:49:55.946968	0	36	act_translation_report	[["name","Translations"],["res_model","ir.translation"]]	ir.action.act_window	ir	f	[["name","Translations"],["res_model","ir.translation"]]	\N	\N
234	2020-09-01 08:49:55.946968	0	68	translation_set_start_view_form	[["model","ir.translation.set.start"],["name","translation_set_start_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.translation.set.start"],["name","translation_set_start_form"],["type","form"]]	\N	\N
235	2020-09-01 08:49:55.946968	0	69	translation_set_succeed_view_form	[["model","ir.translation.set.succeed"],["name","translation_set_succeed_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.translation.set.succeed"],["name","translation_set_succeed_form"],["type","form"]]	\N	\N
236	2020-09-01 08:49:55.946968	0	37	act_translation_set	[["name","Set Translations"],["wiz_name","ir.translation.set"]]	ir.action.wizard	ir	f	[["name","Set Translations"],["wiz_name","ir.translation.set"]]	\N	\N
237	2020-09-01 08:49:55.946968	0	32	act_translation_set_keyword_report	[["action",37],["keyword","form_action"],["model","ir.action.report,-1"]]	ir.action.keyword	ir	f	[["action",37],["keyword","form_action"],["model","ir.action.report,-1"]]	\N	\N
238	2020-09-01 08:49:55.946968	0	33	act_translation_set_keyword_view	[["action",37],["keyword","form_action"],["model","ir.ui.view,-1"]]	ir.action.keyword	ir	f	[["action",37],["keyword","form_action"],["model","ir.ui.view,-1"]]	\N	\N
239	2020-09-01 08:49:55.946968	0	33	menu_translation_set	[["action","ir.action.wizard,37"],["icon","tryton-launch"],["name","Set Translations"],["parent",30],["sequence",20]]	ir.ui.menu	ir	f	[["action","ir.action.wizard,37"],["icon","tryton-launch"],["name","Set Translations"],["parent",30],["sequence",20]]	\N	\N
240	2020-09-01 08:49:55.946968	0	70	translation_clean_start_view_form	[["model","ir.translation.clean.start"],["name","translation_clean_start_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.translation.clean.start"],["name","translation_clean_start_form"],["type","form"]]	\N	\N
241	2020-09-01 08:49:55.946968	0	71	translation_clean_succeed_view_form	[["model","ir.translation.clean.succeed"],["name","translation_clean_succeed_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.translation.clean.succeed"],["name","translation_clean_succeed_form"],["type","form"]]	\N	\N
242	2020-09-01 08:49:55.946968	0	38	act_translation_clean	[["name","Clean Translations"],["wiz_name","ir.translation.clean"]]	ir.action.wizard	ir	f	[["name","Clean Translations"],["wiz_name","ir.translation.clean"]]	\N	\N
243	2020-09-01 08:49:55.946968	0	34	menu_translation_clean	[["action","ir.action.wizard,38"],["icon","tryton-launch"],["name","Clean Translations"],["parent",30],["sequence",30]]	ir.ui.menu	ir	f	[["action","ir.action.wizard,38"],["icon","tryton-launch"],["name","Clean Translations"],["parent",30],["sequence",30]]	\N	\N
244	2020-09-01 08:49:55.946968	0	72	translation_update_start_view_form	[["model","ir.translation.update.start"],["name","translation_update_start_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.translation.update.start"],["name","translation_update_start_form"],["type","form"]]	\N	\N
245	2020-09-01 08:49:55.946968	0	39	act_translation_update	[["name","Synchronize Translations"],["wiz_name","ir.translation.update"]]	ir.action.wizard	ir	f	[["name","Synchronize Translations"],["wiz_name","ir.translation.update"]]	\N	\N
246	2020-09-01 08:49:55.946968	0	36	act_translation_update_keyword_report	[["action",39],["keyword","form_action"],["model","ir.action.report,-1"]]	ir.action.keyword	ir	f	[["action",39],["keyword","form_action"],["model","ir.action.report,-1"]]	\N	\N
247	2020-09-01 08:49:55.946968	0	37	act_translation_update_keyword_view	[["action",39],["keyword","form_action"],["model","ir.ui.view,-1"]]	ir.action.keyword	ir	f	[["action",39],["keyword","form_action"],["model","ir.ui.view,-1"]]	\N	\N
248	2020-09-01 08:49:55.946968	0	35	menu_translation_update	[["action","ir.action.wizard,39"],["icon","tryton-launch"],["name","Synchronize Translations"],["parent",30],["sequence",40]]	ir.ui.menu	ir	f	[["action","ir.action.wizard,39"],["icon","tryton-launch"],["name","Synchronize Translations"],["parent",30],["sequence",40]]	\N	\N
249	2020-09-01 08:49:55.946968	0	73	translation_export_start_view_form	[["model","ir.translation.export.start"],["name","translation_export_start_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.translation.export.start"],["name","translation_export_start_form"],["type","form"]]	\N	\N
250	2020-09-01 08:49:55.946968	0	74	translation_export_result_view_form	[["model","ir.translation.export.result"],["name","translation_export_result_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.translation.export.result"],["name","translation_export_result_form"],["type","form"]]	\N	\N
251	2020-09-01 08:49:55.946968	0	40	act_translation_export	[["name","Export Translations"],["wiz_name","ir.translation.export"]]	ir.action.wizard	ir	f	[["name","Export Translations"],["wiz_name","ir.translation.export"]]	\N	\N
252	2020-09-01 08:49:55.946968	0	36	menu_translation_export	[["action","ir.action.wizard,40"],["icon","tryton-launch"],["name","Export Translations"],["parent",30],["sequence",50]]	ir.ui.menu	ir	f	[["action","ir.action.wizard,40"],["icon","tryton-launch"],["name","Export Translations"],["parent",30],["sequence",50]]	\N	\N
253	2020-09-01 08:49:55.946968	0	75	export_view_form	[["model","ir.export"],["name","export_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.export"],["name","export_form"],["type","form"]]	\N	\N
254	2020-09-01 08:49:55.946968	0	76	export_view_tree	[["model","ir.export"],["name","export_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.export"],["name","export_list"],["type","tree"]]	\N	\N
255	2020-09-01 08:49:55.946968	0	41	act_export_form	[["name","Exports"],["res_model","ir.export"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["name","Exports"],["res_model","ir.export"],["type","ir.action.act_window"]]	\N	\N
256	2020-09-01 08:49:55.946968	0	52	act_export_form_view1	[["act_window",41],["sequence",1],["view",76]]	ir.action.act_window.view	ir	f	[["act_window",41],["sequence",1],["view",76]]	\N	\N
257	2020-09-01 08:49:55.946968	0	53	act_export_form_view2	[["act_window",41],["sequence",2],["view",75]]	ir.action.act_window.view	ir	f	[["act_window",41],["sequence",2],["view",75]]	\N	\N
258	2020-09-01 08:49:55.946968	0	37	menu_export_form	[["action","ir.action.act_window,41"],["icon","tryton-list"],["name","Exports"],["parent",15]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,41"],["icon","tryton-list"],["name","Exports"],["parent",15]]	\N	\N
259	2020-09-01 08:49:55.946968	0	77	export_line_view_form	[["model","ir.export.line"],["name","export_line_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.export.line"],["name","export_line_form"],["type","form"]]	\N	\N
260	2020-09-01 08:49:55.946968	0	78	export_line_view_tree	[["model","ir.export.line"],["name","export_line_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.export.line"],["name","export_line_list"],["type","tree"]]	\N	\N
261	2020-09-01 08:49:55.946968	0	79	rule_group_view_form	[["model","ir.rule.group"],["name","rule_group_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.rule.group"],["name","rule_group_form"],["type","form"]]	\N	\N
262	2020-09-01 08:49:55.946968	0	80	rule_group_view_tree	[["model","ir.rule.group"],["name","rule_group_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.rule.group"],["name","rule_group_list"],["type","tree"]]	\N	\N
263	2020-09-01 08:49:55.946968	0	42	act_rule_group_form	[["name","Record Rules"],["res_model","ir.rule.group"]]	ir.action.act_window	ir	f	[["name","Record Rules"],["res_model","ir.rule.group"]]	\N	\N
264	2020-09-01 08:49:55.946968	0	54	act_rule_group_form_view1	[["act_window",42],["sequence",1],["view",80]]	ir.action.act_window.view	ir	f	[["act_window",42],["sequence",1],["view",80]]	\N	\N
265	2020-09-01 08:49:55.946968	0	55	act_rule_group_form_view2	[["act_window",42],["sequence",2],["view",79]]	ir.action.act_window.view	ir	f	[["act_window",42],["sequence",2],["view",79]]	\N	\N
266	2020-09-01 08:49:55.946968	0	38	menu_rule_group_form	[["action","ir.action.act_window,42"],["icon","tryton-list"],["name","Record Rules"],["parent",15]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,42"],["icon","tryton-list"],["name","Record Rules"],["parent",15]]	\N	\N
267	2020-09-01 08:49:55.946968	0	81	rule_view_form	[["model","ir.rule"],["name","rule_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.rule"],["name","rule_form"],["type","form"]]	\N	\N
268	2020-09-01 08:49:55.946968	0	82	rule_view_tree	[["model","ir.rule"],["name","rule_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.rule"],["name","rule_list"],["type","tree"]]	\N	\N
269	2020-09-01 08:49:55.946968	0	39	menu_modules	[["icon","tryton-folder"],["name","Modules"],["parent",1]]	ir.ui.menu	ir	f	[["icon","tryton-folder"],["name","Modules"],["parent",1]]	\N	\N
270	2020-09-01 08:49:55.946968	0	83	module_view_form	[["model","ir.module"],["name","module_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.module"],["name","module_form"],["type","form"]]	\N	\N
271	2020-09-01 08:49:55.946968	0	84	module_view_tree	[["model","ir.module"],["name","module_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.module"],["name","module_list"],["type","tree"]]	\N	\N
272	2020-09-01 08:49:55.946968	0	43	act_module_form	[["domain","[[\\"name\\", \\"!=\\", \\"tests\\"]]"],["name","Modules"],["res_model","ir.module"],["type","ir.action.act_window"]]	ir.action.act_window	ir	f	[["domain","[[\\"name\\", \\"!=\\", \\"tests\\"]]"],["name","Modules"],["res_model","ir.module"],["type","ir.action.act_window"]]	\N	\N
273	2020-09-01 08:49:55.946968	0	56	act_module_form_view1	[["act_window",43],["sequence",1],["view",84]]	ir.action.act_window.view	ir	f	[["act_window",43],["sequence",1],["view",84]]	\N	\N
274	2020-09-01 08:49:55.946968	0	57	act_module_form_view2	[["act_window",43],["sequence",2],["view",83]]	ir.action.act_window.view	ir	f	[["act_window",43],["sequence",2],["view",83]]	\N	\N
275	2020-09-01 08:49:55.946968	0	40	menu_module_form	[["action","ir.action.act_window,43"],["icon","tryton-list"],["name","Modules"],["parent",39]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,43"],["icon","tryton-list"],["name","Modules"],["parent",39]]	\N	\N
276	2020-09-01 08:49:55.946968	0	6	module_activate_button	[["model",49],["name","activate"],["string","Mark for Activation"]]	ir.model.button	ir	f	[["model",49],["name","activate"],["string","Mark for Activation"]]	\N	\N
277	2020-09-01 08:49:55.946968	0	7	module_activate_cancel_button	[["model",49],["name","activate_cancel"],["string","Cancel Activation"]]	ir.model.button	ir	f	[["model",49],["name","activate_cancel"],["string","Cancel Activation"]]	\N	\N
278	2020-09-01 08:49:55.946968	0	8	module_deactivate_button	[["model",49],["name","deactivate"],["string","Mark for Deactivation (beta)"]]	ir.model.button	ir	f	[["model",49],["name","deactivate"],["string","Mark for Deactivation (beta)"]]	\N	\N
279	2020-09-01 08:49:55.946968	0	9	module_deactivate_cancel_button	[["model",49],["name","deactivate_cancel"],["string","Cancel Deactivation"]]	ir.model.button	ir	f	[["model",49],["name","deactivate_cancel"],["string","Cancel Deactivation"]]	\N	\N
280	2020-09-01 08:49:55.946968	0	10	module_upgrade_button	[["model",49],["name","upgrade"],["string","Mark for Upgrade"]]	ir.model.button	ir	f	[["model",49],["name","upgrade"],["string","Mark for Upgrade"]]	\N	\N
281	2020-09-01 08:49:55.946968	0	11	module_upgrade_cancel_button	[["model",49],["name","upgrade_cancel"],["string","Cancel Upgrade"]]	ir.model.button	ir	f	[["model",49],["name","upgrade_cancel"],["string","Cancel Upgrade"]]	\N	\N
282	2020-09-01 08:49:55.946968	0	85	module_dependency_view_form	[["model","ir.module.dependency"],["name","module_dependency_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.module.dependency"],["name","module_dependency_form"],["type","form"]]	\N	\N
283	2020-09-01 08:49:55.946968	0	86	module_dependency_view_list	[["model","ir.module.dependency"],["name","module_dependency_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.module.dependency"],["name","module_dependency_list"],["type","tree"]]	\N	\N
317	2020-09-01 08:49:55.946968	0	12	December	[["abbreviation","Dec"],["index",12],["name","December"]]	ir.calendar.month	ir	f	[["abbreviation","Dec"],["index",12],["name","December"]]	\N	\N
284	2020-09-01 08:49:55.946968	0	87	config_wizard_item_view_tree	[["model","ir.module.config_wizard.item"],["name","module_config_wizard_item_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.module.config_wizard.item"],["name","module_config_wizard_item_list"],["type","tree"]]	\N	\N
285	2020-09-01 08:49:55.946968	0	44	act_config_wizard_item_form	[["name","Config Wizard Items"],["res_model","ir.module.config_wizard.item"]]	ir.action.act_window	ir	f	[["name","Config Wizard Items"],["res_model","ir.module.config_wizard.item"]]	\N	\N
286	2020-09-01 08:49:55.946968	0	58	act_config_wizard_item_form_view1	[["act_window",44],["sequence",10],["view",87]]	ir.action.act_window.view	ir	f	[["act_window",44],["sequence",10],["view",87]]	\N	\N
287	2020-09-01 08:49:55.946968	0	41	menu_config_wizard_item_form	[["action","ir.action.act_window,44"],["icon","tryton-list"],["name","Config Wizard Items"],["parent",39]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,44"],["icon","tryton-list"],["name","Config Wizard Items"],["parent",39]]	\N	\N
288	2020-09-01 08:49:55.946968	0	45	act_module_config_wizard	[["name","Module Configuration"],["window",true],["wiz_name","ir.module.config_wizard"]]	ir.action.wizard	ir	f	[["name","Module Configuration"],["window",true],["wiz_name","ir.module.config_wizard"]]	\N	\N
289	2020-09-01 08:49:55.946968	0	44	act_module_config_wizard_keyword	[["action",45],["keyword","form_action"],["model","ir.module.config_wizard.item,-1"]]	ir.action.keyword	ir	f	[["action",45],["keyword","form_action"],["model","ir.module.config_wizard.item,-1"]]	\N	\N
290	2020-09-01 08:49:55.946968	0	88	module_config_wizard_first_view_form	[["model","ir.module.config_wizard.first"],["name","module_config_wizard_first_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.module.config_wizard.first"],["name","module_config_wizard_first_form"],["type","form"]]	\N	\N
291	2020-09-01 08:49:55.946968	0	89	module_config_wizard_other_view_form	[["model","ir.module.config_wizard.other"],["name","module_config_wizard_other_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.module.config_wizard.other"],["name","module_config_wizard_other_form"],["type","form"]]	\N	\N
292	2020-09-01 08:49:55.946968	0	90	module_config_wizard_done_view_form	[["model","ir.module.config_wizard.done"],["name","module_config_wizard_done_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.module.config_wizard.done"],["name","module_config_wizard_done_form"],["type","form"]]	\N	\N
293	2020-09-01 08:49:55.946968	0	46	act_module_activate_upgrade	[["name","Perform Pending Activation/Upgrade"],["wiz_name","ir.module.activate_upgrade"]]	ir.action.wizard	ir	f	[["name","Perform Pending Activation/Upgrade"],["wiz_name","ir.module.activate_upgrade"]]	\N	\N
294	2020-09-01 08:49:55.946968	0	45	act_module_activate_upgrade_keyword1	[["action",46],["keyword","form_action"],["model","ir.module,-1"]]	ir.action.keyword	ir	f	[["action",46],["keyword","form_action"],["model","ir.module,-1"]]	\N	\N
295	2020-09-01 08:49:55.946968	0	91	module_activate_upgrade_start_view_form	[["model","ir.module.activate_upgrade.start"],["name","module_activate_upgrade_start_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.module.activate_upgrade.start"],["name","module_activate_upgrade_start_form"],["type","form"]]	\N	\N
296	2020-09-01 08:49:55.946968	0	92	module_activate_upgrade_done_view_form	[["model","ir.module.activate_upgrade.done"],["name","module_activate_upgrade_done_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.module.activate_upgrade.done"],["name","module_activate_upgrade_done_form"],["type","form"]]	\N	\N
297	2020-09-01 08:49:55.946968	0	42	menu_module_activate_upgrade	[["action","ir.action.wizard,46"],["icon","tryton-launch"],["name","Perform Pending Activation/Upgrade"],["parent",39]]	ir.ui.menu	ir	f	[["action","ir.action.wizard,46"],["icon","tryton-launch"],["name","Perform Pending Activation/Upgrade"],["parent",39]]	\N	\N
298	2020-09-01 08:49:55.946968	0	47	act_module_config	[["name","Configure Modules"],["wiz_name","ir.module.config"]]	ir.action.wizard	ir	f	[["name","Configure Modules"],["wiz_name","ir.module.config"]]	\N	\N
299	2020-09-01 08:49:55.946968	0	2	config_wizard_item_module	[["action",47],["sequence",10000]]	ir.module.config_wizard.item	ir	f	[["action",47],["sequence",10000]]	\N	\N
300	2020-09-01 08:49:55.946968	0	93	trigger_view_form	[["model","ir.trigger"],["name","trigger_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.trigger"],["name","trigger_form"],["type","form"]]	\N	\N
301	2020-09-01 08:49:55.946968	0	94	trigger_view_tree	[["model","ir.trigger"],["name","trigger_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.trigger"],["name","trigger_list"],["type","tree"]]	\N	\N
302	2020-09-01 08:49:55.946968	0	48	act_trigger_form	[["name","Triggers"],["res_model","ir.trigger"]]	ir.action.act_window	ir	f	[["name","Triggers"],["res_model","ir.trigger"]]	\N	\N
303	2020-09-01 08:49:55.946968	0	59	act_trigger_form_view1	[["act_window",48],["sequence",10],["view",94]]	ir.action.act_window.view	ir	f	[["act_window",48],["sequence",10],["view",94]]	\N	\N
304	2020-09-01 08:49:55.946968	0	60	act_trigger_form_view2	[["act_window",48],["sequence",20],["view",93]]	ir.action.act_window.view	ir	f	[["act_window",48],["sequence",20],["view",93]]	\N	\N
305	2020-09-01 08:49:55.946968	0	43	menu_trigger_form	[["action","ir.action.act_window,48"],["icon","tryton-list"],["name","Triggers"],["parent",15]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,48"],["icon","tryton-list"],["name","Triggers"],["parent",15]]	\N	\N
306	2020-09-01 08:49:55.946968	0	1	January	[["abbreviation","Jan"],["index",1],["name","January"]]	ir.calendar.month	ir	f	[["abbreviation","Jan"],["index",1],["name","January"]]	\N	\N
307	2020-09-01 08:49:55.946968	0	2	February	[["abbreviation","Feb"],["index",2],["name","February"]]	ir.calendar.month	ir	f	[["abbreviation","Feb"],["index",2],["name","February"]]	\N	\N
308	2020-09-01 08:49:55.946968	0	3	March	[["abbreviation","Mar"],["index",3],["name","March"]]	ir.calendar.month	ir	f	[["abbreviation","Mar"],["index",3],["name","March"]]	\N	\N
309	2020-09-01 08:49:55.946968	0	4	April	[["abbreviation","Apr"],["index",4],["name","April"]]	ir.calendar.month	ir	f	[["abbreviation","Apr"],["index",4],["name","April"]]	\N	\N
310	2020-09-01 08:49:55.946968	0	5	May	[["abbreviation","May"],["index",5],["name","May"]]	ir.calendar.month	ir	f	[["abbreviation","May"],["index",5],["name","May"]]	\N	\N
311	2020-09-01 08:49:55.946968	0	6	June	[["abbreviation","Jun"],["index",6],["name","June"]]	ir.calendar.month	ir	f	[["abbreviation","Jun"],["index",6],["name","June"]]	\N	\N
312	2020-09-01 08:49:55.946968	0	7	July	[["abbreviation","Jul"],["index",7],["name","July"]]	ir.calendar.month	ir	f	[["abbreviation","Jul"],["index",7],["name","July"]]	\N	\N
313	2020-09-01 08:49:55.946968	0	8	August	[["abbreviation","Aug"],["index",8],["name","August"]]	ir.calendar.month	ir	f	[["abbreviation","Aug"],["index",8],["name","August"]]	\N	\N
314	2020-09-01 08:49:55.946968	0	9	September	[["abbreviation","Sep"],["index",9],["name","September"]]	ir.calendar.month	ir	f	[["abbreviation","Sep"],["index",9],["name","September"]]	\N	\N
315	2020-09-01 08:49:55.946968	0	10	October	[["abbreviation","Oct"],["index",10],["name","October"]]	ir.calendar.month	ir	f	[["abbreviation","Oct"],["index",10],["name","October"]]	\N	\N
318	2020-09-01 08:49:55.946968	0	1	Monday	[["abbreviation","Mon"],["index",0],["name","Monday"]]	ir.calendar.day	ir	f	[["abbreviation","Mon"],["index",0],["name","Monday"]]	\N	\N
319	2020-09-01 08:49:55.946968	0	2	Tuesday	[["abbreviation","Tue"],["index",1],["name","Tuesday"]]	ir.calendar.day	ir	f	[["abbreviation","Tue"],["index",1],["name","Tuesday"]]	\N	\N
320	2020-09-01 08:49:55.946968	0	3	Wednesday	[["abbreviation","Wed"],["index",2],["name","Wednesday"]]	ir.calendar.day	ir	f	[["abbreviation","Wed"],["index",2],["name","Wednesday"]]	\N	\N
321	2020-09-01 08:49:55.946968	0	4	Thursday	[["abbreviation","Thu"],["index",3],["name","Thursday"]]	ir.calendar.day	ir	f	[["abbreviation","Thu"],["index",3],["name","Thursday"]]	\N	\N
322	2020-09-01 08:49:55.946968	0	5	Friday	[["abbreviation","Fri"],["index",4],["name","Friday"]]	ir.calendar.day	ir	f	[["abbreviation","Fri"],["index",4],["name","Friday"]]	\N	\N
323	2020-09-01 08:49:55.946968	0	6	Saturday	[["abbreviation","Sat"],["index",5],["name","Saturday"]]	ir.calendar.day	ir	f	[["abbreviation","Sat"],["index",5],["name","Saturday"]]	\N	\N
324	2020-09-01 08:49:55.946968	0	7	Sunday	[["abbreviation","Sun"],["index",6],["name","Sunday"]]	ir.calendar.day	ir	f	[["abbreviation","Sun"],["index",6],["name","Sunday"]]	\N	\N
325	2020-09-01 08:49:55.946968	0	95	message_view_tree	[["model","ir.message"],["name","message_list"],["type","tree"]]	ir.ui.view	ir	f	[["model","ir.message"],["name","message_list"],["type","tree"]]	\N	\N
326	2020-09-01 08:49:55.946968	0	96	message_view_form	[["model","ir.message"],["name","message_form"],["type","form"]]	ir.ui.view	ir	f	[["model","ir.message"],["name","message_form"],["type","form"]]	\N	\N
327	2020-09-01 08:49:55.946968	0	49	act_message_form	[["name","Message"],["res_model","ir.message"]]	ir.action.act_window	ir	f	[["name","Message"],["res_model","ir.message"]]	\N	\N
328	2020-09-01 08:49:55.946968	0	61	act_message_form_view1	[["act_window",49],["sequence",10],["view",95]]	ir.action.act_window.view	ir	f	[["act_window",49],["sequence",10],["view",95]]	\N	\N
329	2020-09-01 08:49:55.946968	0	62	act_message_form_view2	[["act_window",49],["sequence",20],["view",96]]	ir.action.act_window.view	ir	f	[["act_window",49],["sequence",20],["view",96]]	\N	\N
330	2020-09-01 08:49:55.946968	0	44	menu_message_form	[["action","ir.action.act_window,49"],["icon","tryton-list"],["name","Messages"],["parent",30],["sequence",10]]	ir.ui.menu	ir	f	[["action","ir.action.act_window,49"],["icon","tryton-list"],["name","Messages"],["parent",30],["sequence",10]]	\N	\N
331	2020-09-01 08:49:55.946968	0	1	msg_ID	[["text","ID"]]	ir.message	ir	f	[["text","ID"]]	\N	\N
332	2020-09-01 08:49:55.946968	0	2	msg_created_by	[["text","Created by"]]	ir.message	ir	f	[["text","Created by"]]	\N	\N
333	2020-09-01 08:49:55.946968	0	3	msg_created_at	[["text","Created at"]]	ir.message	ir	f	[["text","Created at"]]	\N	\N
334	2020-09-01 08:49:55.946968	0	4	msg_edited_by	[["text","Edited by"]]	ir.message	ir	f	[["text","Edited by"]]	\N	\N
335	2020-09-01 08:49:55.946968	0	5	msg_edited_at	[["text","Edited at"]]	ir.message	ir	f	[["text","Edited at"]]	\N	\N
336	2020-09-01 08:49:55.946968	0	6	msg_record_name	[["text","Record Name"]]	ir.message	ir	f	[["text","Record Name"]]	\N	\N
337	2020-09-01 08:49:55.946968	0	7	msg_active	[["text","Active"]]	ir.message	ir	f	[["text","Active"]]	\N	\N
338	2020-09-01 08:49:55.946968	0	8	msg_active_help	[["text","Uncheck to exclude from future use."]]	ir.message	ir	f	[["text","Uncheck to exclude from future use."]]	\N	\N
339	2020-09-01 08:49:55.946968	0	9	msg_dict_schema_name	[["text","Name"]]	ir.message	ir	f	[["text","Name"]]	\N	\N
340	2020-09-01 08:49:55.946968	0	10	msg_dict_schema_string	[["text","String"]]	ir.message	ir	f	[["text","String"]]	\N	\N
341	2020-09-01 08:49:55.946968	0	11	msg_dict_schema_help	[["text","Help"]]	ir.message	ir	f	[["text","Help"]]	\N	\N
342	2020-09-01 08:49:55.946968	0	12	msg_dict_schema_type	[["text","Type"]]	ir.message	ir	f	[["text","Type"]]	\N	\N
343	2020-09-01 08:49:55.946968	0	13	msg_dict_schema_boolean	[["text","Boolean"]]	ir.message	ir	f	[["text","Boolean"]]	\N	\N
344	2020-09-01 08:49:55.946968	0	14	msg_dict_schema_integer	[["text","Integer"]]	ir.message	ir	f	[["text","Integer"]]	\N	\N
345	2020-09-01 08:49:55.946968	0	15	msg_dict_schema_char	[["text","Char"]]	ir.message	ir	f	[["text","Char"]]	\N	\N
346	2020-09-01 08:49:55.946968	0	16	msg_dict_schema_float	[["text","Float"]]	ir.message	ir	f	[["text","Float"]]	\N	\N
347	2020-09-01 08:49:55.946968	0	17	msg_dict_schema_numeric	[["text","Numeric"]]	ir.message	ir	f	[["text","Numeric"]]	\N	\N
348	2020-09-01 08:49:55.946968	0	18	msg_dict_schema_date	[["text","Date"]]	ir.message	ir	f	[["text","Date"]]	\N	\N
349	2020-09-01 08:49:55.946968	0	19	msg_dict_schema_datetime	[["text","DateTime"]]	ir.message	ir	f	[["text","DateTime"]]	\N	\N
350	2020-09-01 08:49:55.946968	0	20	msg_dict_schema_selection	[["text","Selection"]]	ir.message	ir	f	[["text","Selection"]]	\N	\N
351	2020-09-01 08:49:55.946968	0	21	msg_dict_schema_multiselection	[["text","MultiSelection"]]	ir.message	ir	f	[["text","MultiSelection"]]	\N	\N
352	2020-09-01 08:49:55.946968	0	22	msg_dict_schema_digits	[["text","Digits"]]	ir.message	ir	f	[["text","Digits"]]	\N	\N
353	2020-09-01 08:49:55.946968	0	23	msg_dict_schema_domain	[["text","Domain"]]	ir.message	ir	f	[["text","Domain"]]	\N	\N
354	2020-09-01 08:49:55.946968	0	24	msg_dict_schema_selection_help	[["text","A couple of key and label separated by \\":\\" per line."]]	ir.message	ir	f	[["text","A couple of key and label separated by \\":\\" per line."]]	\N	\N
355	2020-09-01 08:49:55.946968	0	25	msg_dict_schema_selection_sorted	[["text","Selection Sorted"]]	ir.message	ir	f	[["text","Selection Sorted"]]	\N	\N
356	2020-09-01 08:49:55.946968	0	26	msg_dict_schema_selection_sorted_help	[["text","If the selection must be sorted on label."]]	ir.message	ir	f	[["text","If the selection must be sorted on label."]]	\N	\N
357	2020-09-01 08:49:55.946968	0	27	msg_dict_schema_selection_json	[["text","Selection JSON"]]	ir.message	ir	f	[["text","Selection JSON"]]	\N	\N
358	2020-09-01 08:49:55.946968	0	28	msg_sequence	[["text","Sequence"]]	ir.message	ir	f	[["text","Sequence"]]	\N	\N
359	2020-09-01 08:49:55.946968	0	29	msg_id_positive	[["text","ID must be positive."]]	ir.message	ir	f	[["text","ID must be positive."]]	\N	\N
360	2020-09-01 08:49:55.946968	0	30	msg_write_xml_record	[["text","You are not allowed to modify this record."]]	ir.message	ir	f	[["text","You are not allowed to modify this record."]]	\N	\N
361	2020-09-01 08:49:55.946968	0	31	msg_delete_xml_record	[["text","You are not allowed to delete this record."]]	ir.message	ir	f	[["text","You are not allowed to delete this record."]]	\N	\N
362	2020-09-01 08:49:55.946968	0	32	msg_base_config_record	[["text","This record is part of the base configuration."]]	ir.message	ir	f	[["text","This record is part of the base configuration."]]	\N	\N
363	2020-09-01 08:49:55.946968	0	33	msg_relation_not_found	[["text","Relation not found: \\"%(value)r\\" in \\"%(model)s\\"."]]	ir.message	ir	f	[["text","Relation not found: \\"%(value)r\\" in \\"%(model)s\\"."]]	\N	\N
364	2020-09-01 08:49:55.946968	0	34	msg_too_many_relations_found	[["text","Too many relations found: \\"%(value)r\\" in \\"%(model)s\\"."]]	ir.message	ir	f	[["text","Too many relations found: \\"%(value)r\\" in \\"%(model)s\\"."]]	\N	\N
365	2020-09-01 08:49:55.946968	0	35	msg_reference_syntax_error	[["text","Syntax error for reference: \\"%(value)r\\" in \\"%(field)s\\"."]]	ir.message	ir	f	[["text","Syntax error for reference: \\"%(value)r\\" in \\"%(field)s\\"."]]	\N	\N
366	2020-09-01 08:49:55.946968	0	36	msg_xml_id_syntax_error	[["text","Syntax error for XML id: \\"%(value)r\\" in \\"%(field)s\\"."]]	ir.message	ir	f	[["text","Syntax error for XML id: \\"%(value)r\\" in \\"%(field)s\\"."]]	\N	\N
367	2020-09-01 08:49:55.946968	0	37	msg_domain_validation_record	[["text","The value for field \\"%(field)s\\" in \\"%(model)s\\" is not valid according to its domain."]]	ir.message	ir	f	[["text","The value for field \\"%(field)s\\" in \\"%(model)s\\" is not valid according to its domain."]]	\N	\N
368	2020-09-01 08:49:55.946968	0	38	msg_required_validation_record	[["text","A value is required for field \\"%(field)s\\" in \\"%(model)s\\"."]]	ir.message	ir	f	[["text","A value is required for field \\"%(field)s\\" in \\"%(model)s\\"."]]	\N	\N
369	2020-09-01 08:49:55.946968	0	39	msg_size_validation_record	[["text","The value for field \\"%(field)s\\" in \\"%(model)s\\" is too long (%(size)i > %(max_size)i)."]]	ir.message	ir	f	[["text","The value for field \\"%(field)s\\" in \\"%(model)s\\" is too long (%(size)i > %(max_size)i)."]]	\N	\N
370	2020-09-01 08:49:55.946968	0	40	msg_digits_validation_record	[["text","The number of digits in the value \\"%(value)s\\" for field \\"%(field)s\\" in \\"%(model)s\\" exceeds the limit of \\"%(digits)i\\"."]]	ir.message	ir	f	[["text","The number of digits in the value \\"%(value)s\\" for field \\"%(field)s\\" in \\"%(model)s\\" exceeds the limit of \\"%(digits)i\\"."]]	\N	\N
371	2020-09-01 08:49:55.946968	0	41	msg_selection_validation_record	[["text","The value \\"%(value)s\\" for field \\"%(field)s\\" in \\"%(model)s\\" is not one of the allowed options."]]	ir.message	ir	f	[["text","The value \\"%(value)s\\" for field \\"%(field)s\\" in \\"%(model)s\\" is not one of the allowed options."]]	\N	\N
372	2020-09-01 08:49:55.946968	0	42	msg_time_format_validation_record	[["text","The time value \\"%(value)s\\" for field \\"%(field)s\\" in \\"%(model)s\\" is not valid."]]	ir.message	ir	f	[["text","The time value \\"%(value)s\\" for field \\"%(field)s\\" in \\"%(model)s\\" is not valid."]]	\N	\N
373	2020-09-01 08:49:55.946968	0	43	msg_foreign_model_missing	[["text","The value \\"%(value)s\\" for field \\"%(field)s\\" in \\"%(model)s\\" does not exist."]]	ir.message	ir	f	[["text","The value \\"%(value)s\\" for field \\"%(field)s\\" in \\"%(model)s\\" does not exist."]]	\N	\N
374	2020-09-01 08:49:55.946968	0	44	msg_foreign_model_exist	[["text","The records could not be deleted because they are used by field \\"%(field)s\\" of \\"%(model)s\\"."]]	ir.message	ir	f	[["text","The records could not be deleted because they are used by field \\"%(field)s\\" of \\"%(model)s\\"."]]	\N	\N
375	2020-09-01 08:49:55.946968	0	45	msg_access_rule_error	[["text","You are not allowed to access \\"%(model)s\\"."]]	ir.message	ir	f	[["text","You are not allowed to access \\"%(model)s\\"."]]	\N	\N
376	2020-09-01 08:49:55.946968	0	46	msg_access_rule_field_error	[["text","You are not allowed to access \\"%(model)s.%(field)s\\"."]]	ir.message	ir	f	[["text","You are not allowed to access \\"%(model)s.%(field)s\\"."]]	\N	\N
377	2020-09-01 08:49:55.946968	0	47	msg_create_rule_error	[["text","You are not allowed to create records of \\"%(model)s\\" because they fail on at least one of these rules:\\n%(rules)s"]]	ir.message	ir	f	[["text","You are not allowed to create records of \\"%(model)s\\" because they fail on at least one of these rules:\\n%(rules)s"]]	\N	\N
378	2020-09-01 08:49:55.946968	0	48	msg_read_rule_error	[["text","You are not allowed to read records \\"%(ids)s\\" of \\"%(model)s\\" because of at least one of these rules:\\n%(rules)s"]]	ir.message	ir	f	[["text","You are not allowed to read records \\"%(ids)s\\" of \\"%(model)s\\" because of at least one of these rules:\\n%(rules)s"]]	\N	\N
379	2020-09-01 08:49:55.946968	0	49	msg_read_error	[["text","You are trying to read records \\"%(ids)s\\" of \\"%(model)s\\" that don't exist anymore."]]	ir.message	ir	f	[["text","You are trying to read records \\"%(ids)s\\" of \\"%(model)s\\" that don't exist anymore."]]	\N	\N
380	2020-09-01 08:49:55.946968	0	50	msg_write_rule_error	[["text","You are not allowed to write to records \\"%(ids)s\\" of \\"%(model)s\\" because of at least one of these rules:\\n%(rules)s"]]	ir.message	ir	f	[["text","You are not allowed to write to records \\"%(ids)s\\" of \\"%(model)s\\" because of at least one of these rules:\\n%(rules)s"]]	\N	\N
381	2020-09-01 08:49:55.946968	0	51	msg_write_error	[["text","You are trying to write to records \\"%(ids)s\\" of \\"%(model)s\\" that don't exist anymore."]]	ir.message	ir	f	[["text","You are trying to write to records \\"%(ids)s\\" of \\"%(model)s\\" that don't exist anymore."]]	\N	\N
382	2020-09-01 08:49:55.946968	0	52	msg_delete_rule_error	[["text","You are not allowed to delete records \\"%(ids)s\\" of \\"%(model)s\\" because of at lease one of those rules:\\n%(rules)s"]]	ir.message	ir	f	[["text","You are not allowed to delete records \\"%(ids)s\\" of \\"%(model)s\\" because of at lease one of those rules:\\n%(rules)s"]]	\N	\N
383	2020-09-01 08:49:55.946968	0	53	msg_dict_schema_invalid_domain	[["text","Invalid domain in schema \\"%(schema)s\\"."]]	ir.message	ir	f	[["text","Invalid domain in schema \\"%(schema)s\\"."]]	\N	\N
384	2020-09-01 08:49:55.946968	0	54	msg_dict_schema_invalid_selection	[["text","Invalid selection in schema \\"%(schema)s\\"."]]	ir.message	ir	f	[["text","Invalid selection in schema \\"%(schema)s\\"."]]	\N	\N
385	2020-09-01 08:49:55.946968	0	55	msg_recursion_error	[["text","Recursion error: Record \\"%(rec_name)s\\" with parent \\"%(parent_rec_name)s\\" was configured as ancestor of itself."]]	ir.message	ir	f	[["text","Recursion error: Record \\"%(rec_name)s\\" with parent \\"%(parent_rec_name)s\\" was configured as ancestor of itself."]]	\N	\N
386	2020-09-01 08:49:55.946968	0	56	msg_search_function_missing	[["text","Missing search function for field \\"%(field)s\\" in \\"%(model)s\\"."]]	ir.message	ir	f	[["text","Missing search function for field \\"%(field)s\\" in \\"%(model)s\\"."]]	\N	\N
387	2020-09-01 08:49:55.946968	0	57	msg_setter_function_missing	[["text","Missing setter function for field \\"%(field)s\\" in \\"%(model)s\\"."]]	ir.message	ir	f	[["text","Missing setter function for field \\"%(field)s\\" in \\"%(model)s\\"."]]	\N	\N
388	2020-09-01 08:49:55.946968	0	58	msg_access_button_error	[["text","Calling button \\"%(button)s on \\"%(model)s\\" is not allowed."]]	ir.message	ir	f	[["text","Calling button \\"%(button)s on \\"%(model)s\\" is not allowed."]]	\N	\N
389	2020-09-01 08:49:55.946968	0	59	msg_view_invalid_xml	[["text","Invalid XML for view \\"%(name)s\\"."]]	ir.message	ir	f	[["text","Invalid XML for view \\"%(name)s\\"."]]	\N	\N
390	2020-09-01 08:49:55.946968	0	60	msg_action_wrong_wizard_model	[["text","Wrong wizard model in keyword action \\"%(name)s\\"."]]	ir.message	ir	f	[["text","Wrong wizard model in keyword action \\"%(name)s\\"."]]	\N	\N
391	2020-09-01 08:49:55.946968	0	61	msg_report_invalid_email	[["text","Invalid email definition for report \\"%(name)s\\"."]]	ir.message	ir	f	[["text","Invalid email definition for report \\"%(name)s\\"."]]	\N	\N
392	2020-09-01 08:49:55.946968	0	62	msg_action_invalid_views	[["text","Invalid view \\"%(view)s\\" for action \\"%(action)s\\"."]]	ir.message	ir	f	[["text","Invalid view \\"%(view)s\\" for action \\"%(action)s\\"."]]	\N	\N
393	2020-09-01 08:49:55.946968	0	63	msg_action_invalid_domain	[["text","Invalid domain or search criteria \\"%(domain)s\\" for action \\"%(action)s\\"."]]	ir.message	ir	f	[["text","Invalid domain or search criteria \\"%(domain)s\\" for action \\"%(action)s\\"."]]	\N	\N
394	2020-09-01 08:49:55.946968	0	64	msg_action_invalid_context	[["text","Invalid context \\"%(context)s\\" for action \\"%(action)s\\"."]]	ir.message	ir	f	[["text","Invalid context \\"%(context)s\\" for action \\"%(action)s\\"."]]	\N	\N
395	2020-09-01 08:49:55.946968	0	65	msg_model_invalid_condition	[["text","The condition \\"%(condition)s\\" is not a valid PYSON expression for button rule \\"%(rule)s\\"."]]	ir.message	ir	f	[["text","The condition \\"%(condition)s\\" is not a valid PYSON expression for button rule \\"%(rule)s\\"."]]	\N	\N
396	2020-09-01 08:49:55.946968	0	66	msg_sequence_missing	[["text","Missing sequence."]]	ir.message	ir	f	[["text","Missing sequence."]]	\N	\N
397	2020-09-01 08:49:55.946968	0	67	msg_sequence_invalid_prefix	[["text","Invalid prefix \\"%(affix)s\\" for sequence \\"%(sequence)s\\"."]]	ir.message	ir	f	[["text","Invalid prefix \\"%(affix)s\\" for sequence \\"%(sequence)s\\"."]]	\N	\N
398	2020-09-01 08:49:55.946968	0	68	msg_sequence_invalid_suffix	[["text","Invalid suffix \\"%(affix)s\\" for sequence \\"%(sequence)s\\"."]]	ir.message	ir	f	[["text","Invalid suffix \\"%(affix)s\\" for sequence \\"%(sequence)s\\"."]]	\N	\N
399	2020-09-01 08:49:55.946968	0	69	msg_sequence_last_timestamp_future	[["text","The \\"Last Timestamp\\" cannot be in the future for sequence \\"%s\\"."]]	ir.message	ir	f	[["text","The \\"Last Timestamp\\" cannot be in the future for sequence \\"%s\\"."]]	\N	\N
400	2020-09-01 08:49:55.946968	0	70	msg_language_invalid_grouping	[["text","Invalid grouping \\"%(grouping)s\\" for language \\"%(language)s\\"."]]	ir.message	ir	f	[["text","Invalid grouping \\"%(grouping)s\\" for language \\"%(language)s\\"."]]	\N	\N
401	2020-09-01 08:49:55.946968	0	71	msg_language_invalid_date	[["text","Invalid date format \\"%(format)s\\" for language \\"%(language)s\\"."]]	ir.message	ir	f	[["text","Invalid date format \\"%(format)s\\" for language \\"%(language)s\\"."]]	\N	\N
402	2020-09-01 08:49:55.946968	0	72	msg_language_default_translatable	[["text","The default language must be translatable."]]	ir.message	ir	f	[["text","The default language must be translatable."]]	\N	\N
403	2020-09-01 08:49:55.946968	0	73	msg_language_delete_default	[["text","The default language can not be deleted."]]	ir.message	ir	f	[["text","The default language can not be deleted."]]	\N	\N
404	2020-09-01 08:49:55.946968	0	74	msg_rule_invalid_domain	[["text","Invalid domain in rule \\"%(name)s\\"."]]	ir.message	ir	f	[["text","Invalid domain in rule \\"%(name)s\\"."]]	\N	\N
405	2020-09-01 08:49:55.946968	0	75	msg_translation_overridden	[["text","You can not export translation \\"%(name)s\\" because it has been overridden by module \\"%(overriding_module)s\\"."]]	ir.message	ir	f	[["text","You can not export translation \\"%(name)s\\" because it has been overridden by module \\"%(overriding_module)s\\"."]]	\N	\N
406	2020-09-01 08:49:55.946968	0	76	msg_module_delete_state	[["text","You can not remove a module that is activated or that is about to be activated."]]	ir.message	ir	f	[["text","You can not remove a module that is activated or that is about to be activated."]]	\N	\N
407	2020-09-01 08:49:55.946968	0	77	msg_module_deactivate_dependency	[["text","Some activated modules depend on the ones you are trying to deactivate:"]]	ir.message	ir	f	[["text","Some activated modules depend on the ones you are trying to deactivate:"]]	\N	\N
408	2020-09-01 08:49:55.946968	0	78	msg_trigger_invalid_condition	[["text","Condition \\"%(condition)s\\" is not a valid PYSON expression for trigger \\"%(trigger)s\\"."]]	ir.message	ir	f	[["text","Condition \\"%(condition)s\\" is not a valid PYSON expression for trigger \\"%(trigger)s\\"."]]	\N	\N
409	2020-09-01 08:49:55.946968	0	79	msg_html_editor_save_fail	[["text","Failed to save, please retry."]]	ir.message	ir	f	[["text","Failed to save, please retry."]]	\N	\N
410	2020-09-01 08:49:55.946968	0	80	msg_timedelta_Y	[["text","Y"]]	ir.message	ir	f	[["text","Y"]]	\N	\N
411	2020-09-01 08:49:55.946968	0	81	msg_timedelta_M	[["text","M"]]	ir.message	ir	f	[["text","M"]]	\N	\N
412	2020-09-01 08:49:55.946968	0	82	msg_timedelta_w	[["text","w"]]	ir.message	ir	f	[["text","w"]]	\N	\N
413	2020-09-01 08:49:55.946968	0	83	msg_timedelta_d	[["text","d"]]	ir.message	ir	f	[["text","d"]]	\N	\N
414	2020-09-01 08:49:55.946968	0	84	msg_timedelta_h	[["text","h"]]	ir.message	ir	f	[["text","h"]]	\N	\N
415	2020-09-01 08:49:55.946968	0	85	msg_timedelta_m	[["text","m"]]	ir.message	ir	f	[["text","m"]]	\N	\N
416	2020-09-01 08:49:55.946968	0	86	msg_timedelta_s	[["text","s"]]	ir.message	ir	f	[["text","s"]]	\N	\N
417	2020-09-01 08:49:55.946968	0	87	msg_resource_copy_help	[["text","The resources to which this record must be copied."]]	ir.message	ir	f	[["text","The resources to which this record must be copied."]]	\N	\N
418	2020-09-01 08:49:55.946968	0	88	msg_attachments	[["text","Attachments"]]	ir.message	ir	f	[["text","Attachments"]]	\N	\N
419	2020-09-01 08:49:55.946968	0	89	msg_notes	[["text","Notes"]]	ir.message	ir	f	[["text","Notes"]]	\N	\N
420	2020-09-01 08:50:28.180942	0	45	menu_res	[["icon","tryton-folder"],["name","Users"],["parent",1]]	ir.ui.menu	res	f	[["icon","tryton-folder"],["name","Users"],["parent",1]]	\N	\N
421	2020-09-01 08:50:28.180942	0	1	group_admin	[["name","Administration"]]	res.group	res	f	[["name","Administration"]]	\N	\N
422	2020-09-01 08:50:28.180942	0	97	group_view_form	[["model","res.group"],["name","group_form"],["type","form"]]	ir.ui.view	res	f	[["model","res.group"],["name","group_form"],["type","form"]]	\N	\N
423	2020-09-01 08:50:28.180942	0	98	group_view_tree	[["model","res.group"],["name","group_list"],["type","tree"]]	ir.ui.view	res	f	[["model","res.group"],["name","group_list"],["type","tree"]]	\N	\N
424	2020-09-01 08:50:28.180942	0	50	act_group_form	[["name","Groups"],["res_model","res.group"],["type","ir.action.act_window"]]	ir.action.act_window	res	f	[["name","Groups"],["res_model","res.group"],["type","ir.action.act_window"]]	\N	\N
425	2020-09-01 08:50:28.180942	0	63	act_group_form_view1	[["act_window",50],["sequence",1],["view",98]]	ir.action.act_window.view	res	f	[["act_window",50],["sequence",1],["view",98]]	\N	\N
426	2020-09-01 08:50:28.180942	0	64	act_group_form_view2	[["act_window",50],["sequence",2],["view",97]]	ir.action.act_window.view	res	f	[["act_window",50],["sequence",2],["view",97]]	\N	\N
427	2020-09-01 08:50:28.180942	0	46	menu_group_form	[["action","ir.action.act_window,50"],["icon","tryton-list"],["name","Groups"],["parent",45]]	ir.ui.menu	res	f	[["action","ir.action.act_window,50"],["icon","tryton-list"],["name","Groups"],["parent",45]]	\N	\N
428	2020-09-01 08:50:28.180942	0	1	menu_group_form_admin	[["group",1],["menu",46]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",46]]	\N	\N
582	2020-09-01 08:50:28.180942	0	2	module_activate_cancel_button_group_admin	[["button",7],["group",1]]	ir.model.button-res.group	res	f	[["button",7],["group",1]]	\N	\N
429	2020-09-01 08:50:28.180942	0	1	access_group	[["model",67],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",67],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
430	2020-09-01 08:50:28.180942	0	2	access_group_admin	[["group",1],["model",67],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",67],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
431	2020-09-01 08:50:28.180942	0	1	user_admin	[["login","admin"],["menu",2],["name","Administrator"],["signature","Administrator"]]	res.user	res	f	[["login","admin"],["menu",2],["name","Administrator"],["signature","Administrator"]]	\N	\N
432	2020-09-01 08:50:28.180942	0	1	user_admin_group_admin	[["group",1],["user",1]]	res.user-res.group	res	f	[["group",1],["user",1]]	\N	\N
433	2020-09-01 08:50:28.180942	0	99	user_view_form	[["model","res.user"],["name","user_form"],["type","form"]]	ir.ui.view	res	f	[["model","res.user"],["name","user_form"],["type","form"]]	\N	\N
434	2020-09-01 08:50:28.180942	0	100	user_view_form_preferences	[["model","res.user"],["name","user_form_preferences"],["priority",20],["type","form"]]	ir.ui.view	res	f	[["model","res.user"],["name","user_form_preferences"],["priority",20],["type","form"]]	\N	\N
435	2020-09-01 08:50:28.180942	0	101	user_view_tree	[["model","res.user"],["name","user_list"],["type","tree"]]	ir.ui.view	res	f	[["model","res.user"],["name","user_list"],["type","tree"]]	\N	\N
436	2020-09-01 08:50:28.180942	0	51	act_user_form	[["name","Users"],["res_model","res.user"],["type","ir.action.act_window"]]	ir.action.act_window	res	f	[["name","Users"],["res_model","res.user"],["type","ir.action.act_window"]]	\N	\N
437	2020-09-01 08:50:28.180942	0	65	act_user_form_view1	[["act_window",51],["sequence",1],["view",101]]	ir.action.act_window.view	res	f	[["act_window",51],["sequence",1],["view",101]]	\N	\N
438	2020-09-01 08:50:28.180942	0	66	act_user_form_view2	[["act_window",51],["sequence",2],["view",99]]	ir.action.act_window.view	res	f	[["act_window",51],["sequence",2],["view",99]]	\N	\N
439	2020-09-01 08:50:28.180942	0	47	menu_user_form	[["action","ir.action.act_window,51"],["icon","tryton-list"],["name","Users"],["parent",45]]	ir.ui.menu	res	f	[["action","ir.action.act_window,51"],["icon","tryton-list"],["name","Users"],["parent",45]]	\N	\N
440	2020-09-01 08:50:28.180942	0	2	menu_user_form_group_admin	[["group",1],["menu",47]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",47]]	\N	\N
441	2020-09-01 08:50:28.180942	0	3	access_user	[["model",68],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",68],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
442	2020-09-01 08:50:28.180942	0	4	access_user_admin	[["group",1],["model",68],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",68],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
443	2020-09-01 08:50:28.180942	0	12	user_reset_password_button	[["help","Send by email a new temporary password to the user"],["model",68],["name","reset_password"],["string","Reset Password"]]	ir.model.button	res	f	[["help","Send by email a new temporary password to the user"],["model",68],["name","reset_password"],["string","Reset Password"]]	\N	\N
444	2020-09-01 08:50:28.180942	0	102	user_config_start_view_form	[["model","res.user.config.start"],["name","user_config_start_form"],["type","form"]]	ir.ui.view	res	f	[["model","res.user.config.start"],["name","user_config_start_form"],["type","form"]]	\N	\N
445	2020-09-01 08:50:28.180942	0	52	act_user_config	[["name","Configure Users"],["window",true],["wiz_name","res.user.config"]]	ir.action.wizard	res	f	[["name","Configure Users"],["window",true],["wiz_name","res.user.config"]]	\N	\N
446	2020-09-01 08:50:28.180942	0	3	config_wizard_item_user	[["action",52]]	ir.module.config_wizard.item	res	f	[["action",52]]	\N	\N
447	2020-09-01 08:50:28.180942	0	103	user_warning_view_form	[["model","res.user.warning"],["name","user_warning_form"],["type","form"]]	ir.ui.view	res	f	[["model","res.user.warning"],["name","user_warning_form"],["type","form"]]	\N	\N
448	2020-09-01 08:50:28.180942	0	104	user_warning_view_tree	[["model","res.user.warning"],["name","user_warning_tree"],["type","tree"]]	ir.ui.view	res	f	[["model","res.user.warning"],["name","user_warning_tree"],["type","tree"]]	\N	\N
449	2020-09-01 08:50:28.180942	0	1	rule_group_user_warning	[["global_p",true],["model",72],["name","Own warning"]]	ir.rule.group	res	f	[["global_p",true],["model",72],["name","Own warning"]]	\N	\N
450	2020-09-01 08:50:28.180942	0	1	rule_user_warning1	[["domain","[[\\"user\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",1]]	ir.rule	res	f	[["domain","[[\\"user\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",1]]	\N	\N
451	2020-09-01 08:50:28.180942	0	105	user_application_view_form	[["model","res.user.application"],["name","user_application_form"],["type","form"]]	ir.ui.view	res	f	[["model","res.user.application"],["name","user_application_form"],["type","form"]]	\N	\N
452	2020-09-01 08:50:28.180942	0	106	user_application_view_list	[["model","res.user.application"],["name","user_application_list"],["type","tree"]]	ir.ui.view	res	f	[["model","res.user.application"],["name","user_application_list"],["type","tree"]]	\N	\N
453	2020-09-01 08:50:28.180942	0	2	rule_group_user_application	[["default_p",true],["global_p",false],["model",73],["name","Own user application"]]	ir.rule.group	res	f	[["default_p",true],["global_p",false],["model",73],["name","Own user application"]]	\N	\N
454	2020-09-01 08:50:28.180942	0	2	rule_user_application1	[["domain","[[\\"user\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",2]]	ir.rule	res	f	[["domain","[[\\"user\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",2]]	\N	\N
455	2020-09-01 08:50:28.180942	0	3	rule_group_user_application_admin	[["default_p",false],["global_p",false],["model",73],["name","Any user application"]]	ir.rule.group	res	f	[["default_p",false],["global_p",false],["model",73],["name","Any user application"]]	\N	\N
456	2020-09-01 08:50:28.180942	0	3	rule_user_application_admin1	[["domain","[]"],["rule_group",3]]	ir.rule	res	f	[["domain","[]"],["rule_group",3]]	\N	\N
457	2020-09-01 08:50:28.180942	0	1	rule_user_application_admin_admin	[["group",1],["rule_group",3]]	ir.rule.group-res.group	res	f	[["group",1],["rule_group",3]]	\N	\N
458	2020-09-01 08:50:28.180942	0	53	report_email_reset_password	[["model","res.user"],["name","Reset Password"],["report","res/email_reset_password.html"],["report_name","res.user.email_reset_password"],["template_extension","html"]]	ir.action.report	res	f	[["model","res.user"],["name","Reset Password"],["report","res/email_reset_password.html"],["report_name","res.user.email_reset_password"],["template_extension","html"]]	\N	\N
459	2020-09-01 08:50:28.180942	0	13	user_application_validate_button	[["model",73],["name","validate_"],["string","Validate"]]	ir.model.button	res	f	[["model",73],["name","validate_"],["string","Validate"]]	\N	\N
460	2020-09-01 08:50:28.180942	0	14	user_application_cancel_button	[["model",73],["name","cancel"],["string","Cancel"]]	ir.model.button	res	f	[["model",73],["name","cancel"],["string","Cancel"]]	\N	\N
461	2020-09-01 08:50:28.180942	0	5	access_ir_sequence_type	[["model",10],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",10],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
462	2020-09-01 08:50:28.180942	0	6	access_ir_sequence_type_admin	[["group",1],["model",10],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",10],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
463	2020-09-01 08:50:28.180942	0	7	access_ir_ui_icon	[["model",20],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",20],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
464	2020-09-01 08:50:28.180942	0	8	access_ir_ui_icon_admin	[["group",1],["model",20],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",20],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
465	2020-09-01 08:50:28.180942	0	9	access_ir_ui_menu	[["model",13],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",13],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
466	2020-09-01 08:50:28.180942	0	10	access_ir_ui_menu_admin	[["group",1],["model",13],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",13],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
467	2020-09-01 08:50:28.180942	0	11	access_ir_ui_view	[["model",15],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",15],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
468	2020-09-01 08:50:28.180942	0	12	access_ir_ui_view_admin	[["group",1],["model",15],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",15],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
469	2020-09-01 08:50:28.180942	0	13	access_ir_action	[["model",21],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",21],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
470	2020-09-01 08:50:28.180942	0	14	access_ir_action_admin	[["group",1],["model",21],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",21],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
471	2020-09-01 08:50:28.180942	0	15	access_ir_action_keyword	[["model",22],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",22],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
472	2020-09-01 08:50:28.180942	0	16	access_ir_action_keyword_admin	[["group",1],["model",22],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",22],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
473	2020-09-01 08:50:28.180942	0	17	access_ir_action_report	[["model",23],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",23],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
474	2020-09-01 08:50:28.180942	0	18	access_ir_action_report_admin	[["group",1],["model",23],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",23],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
475	2020-09-01 08:50:28.180942	0	19	access_ir_action_act_window	[["model",24],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",24],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
476	2020-09-01 08:50:28.180942	0	20	access_ir_action_act_window_admin	[["group",1],["model",24],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",24],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
477	2020-09-01 08:50:28.180942	0	21	access_ir_action_act_window_view	[["model",25],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",25],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
478	2020-09-01 08:50:28.180942	0	22	access_ir_action_act_window_view_admin	[["group",1],["model",25],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",25],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
479	2020-09-01 08:50:28.180942	0	23	access_ir_action_act_window_domain	[["model",26],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",26],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
480	2020-09-01 08:50:28.180942	0	24	access_ir_action_act_window_domain_admin	[["group",1],["model",26],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",26],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
481	2020-09-01 08:50:28.180942	0	25	access_ir_action_wizard	[["model",27],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",27],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
482	2020-09-01 08:50:28.180942	0	26	access_ir_action_wizard_admin	[["group",1],["model",27],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",27],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
483	2020-09-01 08:50:28.180942	0	27	access_ir_action_url	[["model",28],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",28],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
484	2020-09-01 08:50:28.180942	0	28	access_ir_action_url_admin	[["group",1],["model",28],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",28],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
485	2020-09-01 08:50:28.180942	0	29	access_ir_model	[["model",29],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",29],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
486	2020-09-01 08:50:28.180942	0	30	access_ir_model_admin	[["group",1],["model",29],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",29],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
487	2020-09-01 08:50:28.180942	0	31	access_ir_model_field	[["model",30],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",30],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
488	2020-09-01 08:50:28.180942	0	32	access_ir_model_field_admin	[["group",1],["model",30],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",30],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
489	2020-09-01 08:50:28.180942	0	33	access_ir_model_access	[["model",31],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",31],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
490	2020-09-01 08:50:28.180942	0	34	access_ir_model_access_ir_admin	[["group",1],["model",31],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",31],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
491	2020-09-01 08:50:28.180942	0	35	access_ir_model_button	[["model",33],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",33],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
492	2020-09-01 08:50:28.180942	0	36	access_ir_model_button_admin	[["group",1],["model",33],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",33],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
493	2020-09-01 08:50:28.180942	0	37	access_ir_model_button_rule	[["model",34],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",34],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
494	2020-09-01 08:50:28.180942	0	38	access_ir_model_button_rule_admin	[["group",1],["model",34],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",34],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
495	2020-09-01 08:50:28.180942	0	39	access_ir_model_button_click	[["model",35],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",35],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
496	2020-09-01 08:50:28.180942	0	40	access_ir_model_button_click_admin	[["group",1],["model",35],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",35],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
497	2020-09-01 08:50:28.180942	0	41	access_ir_model_data	[["model",37],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",37],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
498	2020-09-01 08:50:28.180942	0	42	access_ir_cron	[["model",42],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",42],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
499	2020-09-01 08:50:28.180942	0	43	access_ir_cron_admin	[["group",1],["model",42],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",42],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
500	2020-09-01 08:50:28.180942	0	44	access_ir_queue	[["model",63],["perm_create",false],["perm_delete",false],["perm_read",false],["perm_write",false]]	ir.model.access	res	f	[["model",63],["perm_create",false],["perm_delete",false],["perm_read",false],["perm_write",false]]	\N	\N
501	2020-09-01 08:50:28.180942	0	45	access_ir_queue_admin	[["group",1],["model",63],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["group",1],["model",63],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
502	2020-09-01 08:50:28.180942	0	46	access_ir_lang	[["model",43],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",43],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
503	2020-09-01 08:50:28.180942	0	47	access_ir_lang_admin	[["group",1],["model",43],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",43],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
504	2020-09-01 08:50:28.180942	0	48	access_ir_translation	[["model",2],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",2],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
505	2020-09-01 08:50:28.180942	0	49	access_ir_translation_admin	[["group",1],["model",2],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",2],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
506	2020-09-01 08:50:28.180942	0	50	access_ir_rule_group	[["model",47],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",47],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
507	2020-09-01 08:50:28.180942	0	51	access_ir_rule_group_admin	[["group",1],["model",47],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",47],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
508	2020-09-01 08:50:28.180942	0	52	access_ir_rule	[["model",48],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",48],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
509	2020-09-01 08:50:28.180942	0	53	access_ir_rule_admin	[["group",1],["model",48],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",48],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
583	2020-09-01 08:50:28.180942	0	3	module_deactivate_button_group_admin	[["button",8],["group",1]]	ir.model.button-res.group	res	f	[["button",8],["group",1]]	\N	\N
510	2020-09-01 08:50:28.180942	0	54	access_ir_module	[["model",49],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",49],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
511	2020-09-01 08:50:28.180942	0	55	access_ir_module_admin	[["group",1],["model",49],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",49],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
512	2020-09-01 08:50:28.180942	0	56	access_ir_module_dependency	[["model",50],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",50],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
513	2020-09-01 08:50:28.180942	0	57	access_ir_module_dependency_admin	[["group",1],["model",50],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",50],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
514	2020-09-01 08:50:28.180942	0	58	access_ir_trigger	[["model",59],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",59],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
515	2020-09-01 08:50:28.180942	0	59	access_ir_trigger_admin	[["group",1],["model",59],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",59],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
516	2020-09-01 08:50:28.180942	0	60	access_ir_trigger_log	[["model",60],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",60],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
517	2020-09-01 08:50:28.180942	0	61	access_ir_trigger_log_admin	[["group",1],["model",60],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",60],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
518	2020-09-01 08:50:28.180942	0	4	rule_group_menu	[["global_p",true],["model",13],["name","User in groups"]]	ir.rule.group	res	f	[["global_p",true],["model",13],["name","User in groups"]]	\N	\N
519	2020-09-01 08:50:28.180942	0	4	rule_menu1	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",4]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",4]]	\N	\N
520	2020-09-01 08:50:28.180942	0	5	rule_menu2	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",4]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",4]]	\N	\N
521	2020-09-01 08:50:28.180942	0	5	rule_group_action	[["global_p",true],["model",21],["name","User in groups"]]	ir.rule.group	res	f	[["global_p",true],["model",21],["name","User in groups"]]	\N	\N
522	2020-09-01 08:50:28.180942	0	6	rule_action1	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",5]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",5]]	\N	\N
523	2020-09-01 08:50:28.180942	0	7	rule_action2	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",5]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",5]]	\N	\N
524	2020-09-01 08:50:28.180942	0	6	rule_group_action_keyword	[["global_p",true],["model",22],["name","User in groups"]]	ir.rule.group	res	f	[["global_p",true],["model",22],["name","User in groups"]]	\N	\N
525	2020-09-01 08:50:28.180942	0	8	rule_action_keyword1	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",6]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",6]]	\N	\N
526	2020-09-01 08:50:28.180942	0	9	rule_action_keyword2	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",6]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",6]]	\N	\N
527	2020-09-01 08:50:28.180942	0	7	rule_group_action_report	[["global_p",true],["model",23],["name","User in groups"]]	ir.rule.group	res	f	[["global_p",true],["model",23],["name","User in groups"]]	\N	\N
528	2020-09-01 08:50:28.180942	0	10	rule_action_report1	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",7]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",7]]	\N	\N
529	2020-09-01 08:50:28.180942	0	11	rule_action_report2	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",7]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",7]]	\N	\N
530	2020-09-01 08:50:28.180942	0	8	rule_group_action_act_window	[["global_p",true],["model",24],["name","User in groups"]]	ir.rule.group	res	f	[["global_p",true],["model",24],["name","User in groups"]]	\N	\N
531	2020-09-01 08:50:28.180942	0	12	rule_action_act_window1	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",8]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",8]]	\N	\N
532	2020-09-01 08:50:28.180942	0	13	rule_action_act_window2	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",8]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",8]]	\N	\N
533	2020-09-01 08:50:28.180942	0	9	rule_group_action_wizard	[["global_p",true],["model",27],["name","User in groups"]]	ir.rule.group	res	f	[["global_p",true],["model",27],["name","User in groups"]]	\N	\N
534	2020-09-01 08:50:28.180942	0	14	rule_action_wizard1	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",9]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",9]]	\N	\N
535	2020-09-01 08:50:28.180942	0	15	rule_action_wizard2	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",9]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",9]]	\N	\N
536	2020-09-01 08:50:28.180942	0	10	rule_group_action_url	[["global_p",true],["model",28],["name","User in groups"]]	ir.rule.group	res	f	[["global_p",true],["model",28],["name","User in groups"]]	\N	\N
537	2020-09-01 08:50:28.180942	0	16	rule_action_url1	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",10]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",10]]	\N	\N
538	2020-09-01 08:50:28.180942	0	17	rule_action_url2	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",10]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"=\\", null]]"],["rule_group",10]]	\N	\N
539	2020-09-01 08:50:28.180942	0	1	act_module_activate_upgrade_group_admin	[["action",46],["group",1]]	ir.action-res.group	res	f	[["action",46],["group",1]]	\N	\N
540	2020-09-01 08:50:28.180942	0	2	act_translation_update_group_admin	[["action",39],["group",1]]	ir.action-res.group	res	f	[["action",39],["group",1]]	\N	\N
541	2020-09-01 08:50:28.180942	0	3	act_translation_export_group_admin	[["action",40],["group",1]]	ir.action-res.group	res	f	[["action",40],["group",1]]	\N	\N
542	2020-09-01 08:50:28.180942	0	4	act_lang_config	[["action",33],["group",1]]	ir.action-res.group	res	f	[["action",33],["group",1]]	\N	\N
543	2020-09-01 08:50:28.180942	0	3	menu_administration_group_admin	[["group",1],["menu",1]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",1]]	\N	\N
544	2020-09-01 08:50:28.180942	0	4	menu_ui_group_admin	[["group",1],["menu",2]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",2]]	\N	\N
545	2020-09-01 08:50:28.180942	0	5	menu_icon_form_group_admin	[["group",1],["menu",3]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",3]]	\N	\N
546	2020-09-01 08:50:28.180942	0	6	menu_menu_tree_group_admin	[["group",1],["menu",4]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",4]]	\N	\N
547	2020-09-01 08:50:28.180942	0	7	menu_view_group_admin	[["group",1],["menu",5]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",5]]	\N	\N
548	2020-09-01 08:50:28.180942	0	8	menu_view_tree_width_group_admin	[["group",1],["menu",6]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",6]]	\N	\N
549	2020-09-01 08:50:28.180942	0	9	menu_action_group_admin	[["group",1],["menu",9]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",9]]	\N	\N
550	2020-09-01 08:50:28.180942	0	10	menu_act_action_group_admin	[["group",1],["menu",10]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",10]]	\N	\N
551	2020-09-01 08:50:28.180942	0	11	menu_action_report_form_group_admin	[["group",1],["menu",11]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",11]]	\N	\N
552	2020-09-01 08:50:28.180942	0	12	menu_action_act_window_group_admin	[["group",1],["menu",12]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",12]]	\N	\N
553	2020-09-01 08:50:28.180942	0	13	menu_action_wizard_group_admin	[["group",1],["menu",13]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",13]]	\N	\N
554	2020-09-01 08:50:28.180942	0	14	menu_action_url_group_admin	[["group",1],["menu",14]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",14]]	\N	\N
555	2020-09-01 08:50:28.180942	0	15	menu_models_group_admin	[["group",1],["menu",15]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",15]]	\N	\N
556	2020-09-01 08:50:28.180942	0	16	menu_model_form_group_admin	[["group",1],["menu",16]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",16]]	\N	\N
557	2020-09-01 08:50:28.180942	0	17	model_model_fields_form_group_admin	[["group",1],["menu",17]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",17]]	\N	\N
558	2020-09-01 08:50:28.180942	0	18	menu_model_access_form_group_admin	[["group",1],["menu",18]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",18]]	\N	\N
559	2020-09-01 08:50:28.180942	0	19	menu_model_field_access_form_group_admin	[["group",1],["menu",19]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",19]]	\N	\N
560	2020-09-01 08:50:28.180942	0	20	menu_sequences_group_admin	[["group",1],["menu",22]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",22]]	\N	\N
561	2020-09-01 08:50:28.180942	0	21	menu_sequence_form_group_admin	[["group",1],["menu",23]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",23]]	\N	\N
562	2020-09-01 08:50:28.180942	0	22	menu_sequence_strict_form_group_admin	[["group",1],["menu",24]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",24]]	\N	\N
563	2020-09-01 08:50:28.180942	0	23	menu_ir_sequence_type_group_admin	[["group",1],["menu",25]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",25]]	\N	\N
564	2020-09-01 08:50:28.180942	0	24	menu_attachment_form_group_admin	[["group",1],["menu",26]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",26]]	\N	\N
565	2020-09-01 08:50:28.180942	0	25	menu_scheduler_group_admin	[["group",1],["menu",28]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",28]]	\N	\N
566	2020-09-01 08:50:28.180942	0	26	menu_cron_form_group_admin	[["group",1],["menu",29]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",29]]	\N	\N
567	2020-09-01 08:50:28.180942	0	27	menu_localization_group_admin	[["group",1],["menu",30]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",30]]	\N	\N
568	2020-09-01 08:50:28.180942	0	28	menu_lang_form_group_admin	[["group",1],["menu",31]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",31]]	\N	\N
569	2020-09-01 08:50:28.180942	0	29	menu_translation_form_group_admin	[["group",1],["menu",32]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",32]]	\N	\N
570	2020-09-01 08:50:28.180942	0	30	menu_translation_set_group_admin	[["group",1],["menu",33]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",33]]	\N	\N
571	2020-09-01 08:50:28.180942	0	31	menu_translation_clean_group_admin	[["group",1],["menu",34]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",34]]	\N	\N
572	2020-09-01 08:50:28.180942	0	32	menu_translation_update_group_admin	[["group",1],["menu",35]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",35]]	\N	\N
573	2020-09-01 08:50:28.180942	0	33	menu_translation_export_group_admin	[["group",1],["menu",36]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",36]]	\N	\N
574	2020-09-01 08:50:28.180942	0	34	menu_export_form_group_admin	[["group",1],["menu",37]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",37]]	\N	\N
575	2020-09-01 08:50:28.180942	0	35	menu_rule_group_form_group_admin	[["group",1],["menu",38]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",38]]	\N	\N
576	2020-09-01 08:50:28.180942	0	36	menu_modules_group_admin	[["group",1],["menu",39]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",39]]	\N	\N
577	2020-09-01 08:50:28.180942	0	37	menu_module_form_group_admin	[["group",1],["menu",40]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",40]]	\N	\N
578	2020-09-01 08:50:28.180942	0	38	menu_config_wizard_item_form_group_admin	[["group",1],["menu",41]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",41]]	\N	\N
579	2020-09-01 08:50:28.180942	0	39	menu_module_activate_upgrade_group_admin	[["group",1],["menu",42]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",42]]	\N	\N
580	2020-09-01 08:50:28.180942	0	40	menu_trigger_form_group_admin	[["group",1],["menu",43]]	ir.ui.menu-res.group	res	f	[["group",1],["menu",43]]	\N	\N
581	2020-09-01 08:50:28.180942	0	1	module_activate_button_group_admin	[["button",6],["group",1]]	ir.model.button-res.group	res	f	[["button",6],["group",1]]	\N	\N
584	2020-09-01 08:50:28.180942	0	4	module_deactivate_cancel_button_group_admin	[["button",9],["group",1]]	ir.model.button-res.group	res	f	[["button",9],["group",1]]	\N	\N
585	2020-09-01 08:50:28.180942	0	5	module_upgrade_button_group_admin	[["button",10],["group",1]]	ir.model.button-res.group	res	f	[["button",10],["group",1]]	\N	\N
586	2020-09-01 08:50:28.180942	0	6	module_upgrade_cancel_button_group_admin	[["button",11],["group",1]]	ir.model.button-res.group	res	f	[["button",11],["group",1]]	\N	\N
587	2020-09-01 08:50:28.180942	0	7	cron_run_once_button_group_admin	[["button",3],["group",1]]	ir.model.button-res.group	res	f	[["button",3],["group",1]]	\N	\N
588	2020-09-01 08:50:28.180942	0	8	model_data_sync_button_group_admin	[["button",2],["group",1]]	ir.model.button-res.group	res	f	[["button",2],["group",1]]	\N	\N
589	2020-09-01 08:50:28.180942	0	9	view_show_button_group_admin	[["button",1],["group",1]]	ir.model.button-res.group	res	f	[["button",1],["group",1]]	\N	\N
590	2020-09-01 08:50:28.180942	0	107	sequence_type_view_form	[["inherit",55],["model","ir.sequence.type"],["name","sequence_type_form"]]	ir.ui.view	res	f	[["inherit",55],["model","ir.sequence.type"],["name","sequence_type_form"]]	\N	\N
591	2020-09-01 08:50:28.180942	0	11	rule_group_sequence	[["global_p",true],["model",11],["name","User in groups"],["perm_read",false]]	ir.rule.group	res	f	[["global_p",true],["model",11],["name","User in groups"],["perm_read",false]]	\N	\N
592	2020-09-01 08:50:28.180942	0	18	rule_sequence	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",11]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",11]]	\N	\N
593	2020-09-01 08:50:28.180942	0	12	rule_group_sequence_strict	[["global_p",true],["model",12],["name","User in groups"],["perm_read",false]]	ir.rule.group	res	f	[["global_p",true],["model",12],["name","User in groups"],["perm_read",false]]	\N	\N
594	2020-09-01 08:50:28.180942	0	19	rule_sequence_strict	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",12]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Get\\", \\"d\\": [], \\"k\\": \\"groups\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",12]]	\N	\N
595	2020-09-01 08:50:28.180942	0	1	cron_trigger_time	[["interval_number",5],["interval_type","minutes"],["method","ir.trigger|trigger_time"]]	ir.cron	res	f	[["interval_number",5],["interval_type","minutes"],["method","ir.trigger|trigger_time"]]	\N	\N
596	2020-09-01 08:50:28.180942	0	62	rule_default_view_tree_state	[["model",18],["perm_create",false],["perm_delete",false],["perm_read",false],["perm_write",false]]	ir.model.access	res	f	[["model",18],["perm_create",false],["perm_delete",false],["perm_read",false],["perm_write",false]]	\N	\N
597	2020-09-01 08:50:28.180942	0	63	rule_group_view_tree_state	[["group",1],["model",18],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",18],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
598	2020-09-01 08:50:28.180942	0	13	rule_group_view_search	[["default_p",true],["global_p",false],["model",19],["name","Own view search"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.rule.group	res	f	[["default_p",true],["global_p",false],["model",19],["name","Own view search"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
599	2020-09-01 08:50:28.180942	0	20	rule_group_view_search1	[["domain","[[\\"user\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",13]]	ir.rule	res	f	[["domain","[[\\"user\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",13]]	\N	\N
600	2020-09-01 08:50:28.180942	0	14	rule_group_view_search_admin	[["default_p",false],["global_p",false],["model",19],["name","Any view search"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.rule.group	res	f	[["default_p",false],["global_p",false],["model",19],["name","Any view search"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
601	2020-09-01 08:50:28.180942	0	2	rule_group_view_search_admin_group_admin	[["group",1],["rule_group",14]]	ir.rule.group-res.group	res	f	[["group",1],["rule_group",14]]	\N	\N
602	2020-09-01 08:50:28.180942	0	15	rule_group_session	[["global_p",true],["model",61],["name","Own session"]]	ir.rule.group	res	f	[["global_p",true],["model",61],["name","Own session"]]	\N	\N
603	2020-09-01 08:50:28.180942	0	21	rule_session1	[["domain","[[\\"create_uid\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",15]]	ir.rule	res	f	[["domain","[[\\"create_uid\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",15]]	\N	\N
604	2020-09-01 08:50:28.180942	0	16	rule_group_session_wizard	[["global_p",true],["model",62],["name","Own session"]]	ir.rule.group	res	f	[["global_p",true],["model",62],["name","Own session"]]	\N	\N
605	2020-09-01 08:50:28.180942	0	22	rule_session_wizard1	[["domain","[[\\"create_uid\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",16]]	ir.rule	res	f	[["domain","[[\\"create_uid\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",16]]	\N	\N
606	2020-09-01 08:50:28.180942	0	17	rule_group_ui_menu_favorite	[["global_p",true],["model",14],["name","Own favorite"]]	ir.rule.group	res	f	[["global_p",true],["model",14],["name","Own favorite"]]	\N	\N
607	2020-09-01 08:50:28.180942	0	23	rule_ui_menu_favorite1	[["domain","[[\\"create_uid\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",17]]	ir.rule	res	f	[["domain","[[\\"create_uid\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": -1, \\"k\\": \\"id\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",17]]	\N	\N
608	2020-09-01 08:50:28.180942	0	64	access_message	[["model",66],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	res	f	[["model",66],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
609	2020-09-01 08:50:28.180942	0	65	access_message_group_admin	[["group",1],["model",66],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	res	f	[["group",1],["model",66],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
610	2020-09-01 08:50:28.180942	0	108	export_view_form	[["inherit",75],["model","ir.export"],["name","export_form"]]	ir.ui.view	res	f	[["inherit",75],["model","ir.export"],["name","export_form"]]	\N	\N
611	2020-09-01 08:50:28.180942	0	109	export_view_list	[["inherit",76],["model","ir.export"],["name","export_list"]]	ir.ui.view	res	f	[["inherit",76],["model","ir.export"],["name","export_list"]]	\N	\N
612	2020-09-01 08:50:28.180942	0	18	rule_group_export_read	[["default_p",true],["global_p",false],["model",45],["name","User in groups"],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.rule.group	res	f	[["default_p",true],["global_p",false],["model",45],["name","User in groups"],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
613	2020-09-01 08:50:28.180942	0	24	rule_group_export_read1	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"user.groups\\"}]]"],["rule_group",18]]	ir.rule	res	f	[["domain","[[\\"groups\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"user.groups\\"}]]"],["rule_group",18]]	\N	\N
614	2020-09-01 08:50:28.180942	0	25	rule_group_export_read2	[["domain","[[\\"create_uid\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": -1, \\"v\\": \\"user.id\\"}]]"],["rule_group",18]]	ir.rule	res	f	[["domain","[[\\"create_uid\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": -1, \\"v\\": \\"user.id\\"}]]"],["rule_group",18]]	\N	\N
615	2020-09-01 08:50:28.180942	0	19	rule_group_export_write	[["default_p",true],["global_p",false],["model",45],["name","User in modification groups"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.rule.group	res	f	[["default_p",true],["global_p",false],["model",45],["name","User in modification groups"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
616	2020-09-01 08:50:28.180942	0	26	rule_group_export_write1	[["domain","[[\\"write_groups\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"user.groups\\"}]]"],["rule_group",19]]	ir.rule	res	f	[["domain","[[\\"write_groups\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"user.groups\\"}]]"],["rule_group",19]]	\N	\N
617	2020-09-01 08:50:28.180942	0	27	rule_group_export_write2	[["domain","[[\\"create_uid\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": -1, \\"v\\": \\"user.id\\"}]]"],["rule_group",19]]	ir.rule	res	f	[["domain","[[\\"create_uid\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": -1, \\"v\\": \\"user.id\\"}]]"],["rule_group",19]]	\N	\N
618	2020-09-01 08:50:28.180942	0	20	rule_group_export_any	[["default_p",false],["global_p",false],["model",45],["name","Any export"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.rule.group	res	f	[["default_p",false],["global_p",false],["model",45],["name","Any export"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
619	2020-09-01 08:50:28.180942	0	28	rule_group_export_any1	[["domain","[]"],["rule_group",20]]	ir.rule	res	f	[["domain","[]"],["rule_group",20]]	\N	\N
620	2020-09-01 08:50:28.180942	0	3	rule_group_export_any_admin	[["group",1],["rule_group",20]]	ir.rule.group-res.group	res	f	[["group",1],["rule_group",20]]	\N	\N
621	2020-09-01 08:50:28.180942	0	21	rule_group_export_line_read	[["default_p",true],["global_p",false],["model",46],["name","User in groups"],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.rule.group	res	f	[["default_p",true],["global_p",false],["model",46],["name","User in groups"],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
622	2020-09-01 08:50:28.180942	0	29	rule_group_export_line_read1	[["domain","[[\\"export.groups\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"user.groups\\"}]]"],["rule_group",21]]	ir.rule	res	f	[["domain","[[\\"export.groups\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"user.groups\\"}]]"],["rule_group",21]]	\N	\N
623	2020-09-01 08:50:28.180942	0	30	rule_group_export_line_read2	[["domain","[[\\"export.create_uid\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": -1, \\"v\\": \\"user.id\\"}]]"],["rule_group",21]]	ir.rule	res	f	[["domain","[[\\"export.create_uid\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": -1, \\"v\\": \\"user.id\\"}]]"],["rule_group",21]]	\N	\N
624	2020-09-01 08:50:28.180942	0	22	rule_group_export_line_write	[["default_p",true],["global_p",false],["model",46],["name","User in modification groups"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.rule.group	res	f	[["default_p",true],["global_p",false],["model",46],["name","User in modification groups"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
625	2020-09-01 08:50:28.180942	0	31	rule_group_export_line_write1	[["domain","[[\\"export.write_groups\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"user.groups\\"}]]"],["rule_group",22]]	ir.rule	res	f	[["domain","[[\\"export.write_groups\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"user.groups\\"}]]"],["rule_group",22]]	\N	\N
626	2020-09-01 08:50:28.180942	0	32	rule_group_export_line_write2	[["domain","[[\\"export.create_uid\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": -1, \\"v\\": \\"user.id\\"}]]"],["rule_group",22]]	ir.rule	res	f	[["domain","[[\\"export.create_uid\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": -1, \\"v\\": \\"user.id\\"}]]"],["rule_group",22]]	\N	\N
627	2020-09-01 08:50:28.180942	0	23	rule_group_export_line_any	[["default_p",false],["global_p",false],["model",46],["name","Any export"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.rule.group	res	f	[["default_p",false],["global_p",false],["model",46],["name","Any export"],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
628	2020-09-01 08:50:28.180942	0	33	rule_group_export_line_any1	[["domain","[]"],["rule_group",23]]	ir.rule	res	f	[["domain","[]"],["rule_group",23]]	\N	\N
629	2020-09-01 08:50:28.180942	0	4	rule_group_export_line_any_admin	[["group",1],["rule_group",23]]	ir.rule.group-res.group	res	f	[["group",1],["rule_group",23]]	\N	\N
630	2020-09-01 08:50:28.180942	0	90	msg_password_length	[["text","The password is too short."]]	ir.message	res	f	[["text","The password is too short."]]	\N	\N
631	2020-09-01 08:50:28.180942	0	91	msg_password_forbidden	[["text","The password is forbidden."]]	ir.message	res	f	[["text","The password is forbidden."]]	\N	\N
632	2020-09-01 08:50:28.180942	0	92	msg_password_entropy	[["text","The same characters appear in the password too many times."]]	ir.message	res	f	[["text","The same characters appear in the password too many times."]]	\N	\N
633	2020-09-01 08:50:28.180942	0	93	msg_password_name	[["text","The password cannot be the same as user's name."]]	ir.message	res	f	[["text","The password cannot be the same as user's name."]]	\N	\N
634	2020-09-01 08:50:28.180942	0	94	msg_password_login	[["text","The password cannot be the same as user's login."]]	ir.message	res	f	[["text","The password cannot be the same as user's login."]]	\N	\N
635	2020-09-01 08:50:28.180942	0	95	msg_password_email	[["text","The password cannot be the same as user's email address."]]	ir.message	res	f	[["text","The password cannot be the same as user's email address."]]	\N	\N
636	2020-09-01 08:50:28.180942	0	96	msg_user_delete_forbidden	[["text","For logging purposes users cannot be deleted, instead they should be deactivated."]]	ir.message	res	f	[["text","For logging purposes users cannot be deleted, instead they should be deactivated."]]	\N	\N
637	2020-09-01 08:50:28.180942	0	97	msg_user_password	[["text","Password for %(login)s"]]	ir.message	res	f	[["text","Password for %(login)s"]]	\N	\N
638	2020-09-02 07:24:22.107384	0	9	country_icon	[["name","tryton-country"],["path","icons/tryton-country.svg"]]	ir.ui.icon	country	f	[["name","tryton-country"],["path","icons/tryton-country.svg"]]	\N	\N
639	2020-09-02 07:24:22.107384	0	110	country_view_form	[["model","country.country"],["name","country_form"],["type","form"]]	ir.ui.view	country	f	[["model","country.country"],["name","country_form"],["type","form"]]	\N	\N
640	2020-09-02 07:24:22.107384	0	111	country_view_tree	[["model","country.country"],["name","country_tree"],["type","tree"]]	ir.ui.view	country	f	[["model","country.country"],["name","country_tree"],["type","tree"]]	\N	\N
641	2020-09-02 07:24:22.107384	0	54	act_country_form	[["name","Countries"],["res_model","country.country"]]	ir.action.act_window	country	f	[["name","Countries"],["res_model","country.country"]]	\N	\N
642	2020-09-02 07:24:22.107384	0	67	act_country_form_view1	[["act_window",54],["sequence",10],["view",111]]	ir.action.act_window.view	country	f	[["act_window",54],["sequence",10],["view",111]]	\N	\N
643	2020-09-02 07:24:22.107384	0	68	act_country_form_view2	[["act_window",54],["sequence",20],["view",110]]	ir.action.act_window.view	country	f	[["act_window",54],["sequence",20],["view",110]]	\N	\N
644	2020-09-02 07:24:22.107384	0	48	menu_country_form	[["action","ir.action.act_window,54"],["icon","tryton-country"],["name","Countries"],["parent",1]]	ir.ui.menu	country	f	[["action","ir.action.act_window,54"],["icon","tryton-country"],["name","Countries"],["parent",1]]	\N	\N
645	2020-09-02 07:24:22.107384	0	41	menu_country_form_group_admin	[["group",1],["menu",48]]	ir.ui.menu-res.group	country	f	[["group",1],["menu",48]]	\N	\N
646	2020-09-02 07:24:22.107384	0	112	subdivision_view_form	[["model","country.subdivision"],["name","subdivision_form"],["type","form"]]	ir.ui.view	country	f	[["model","country.subdivision"],["name","subdivision_form"],["type","form"]]	\N	\N
647	2020-09-02 07:24:22.107384	0	113	subdivision_view_tree	[["model","country.subdivision"],["name","subdivision_tree"],["type","tree"]]	ir.ui.view	country	f	[["model","country.subdivision"],["name","subdivision_tree"],["type","tree"]]	\N	\N
648	2020-09-02 07:24:22.107384	0	114	zip_view_form	[["model","country.zip"],["name","zip_form"],["type","form"]]	ir.ui.view	country	f	[["model","country.zip"],["name","zip_form"],["type","form"]]	\N	\N
649	2020-09-02 07:24:22.107384	0	115	zip_view_list	[["model","country.zip"],["name","zip_list"],["type","tree"]]	ir.ui.view	country	f	[["model","country.zip"],["name","zip_list"],["type","tree"]]	\N	\N
650	2020-09-02 07:24:22.107384	0	55	act_zip_form	[["domain","[[\\"country\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}]]"],["name","Zip"],["res_model","country.zip"]]	ir.action.act_window	country	f	[["domain","[[\\"country\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}]]"],["name","Zip"],["res_model","country.zip"]]	\N	\N
651	2020-09-02 07:24:22.107384	0	69	act_zip_form_view1	[["act_window",55],["sequence",10],["view",115]]	ir.action.act_window.view	country	f	[["act_window",55],["sequence",10],["view",115]]	\N	\N
652	2020-09-02 07:24:22.107384	0	70	act_zip_form_view2	[["act_window",55],["sequence",20],["view",114]]	ir.action.act_window.view	country	f	[["act_window",55],["sequence",20],["view",114]]	\N	\N
653	2020-09-02 07:24:22.107384	0	52	act_zip_form_keyword1	[["action",55],["keyword","form_relate"],["model","country.country,-1"]]	ir.action.keyword	country	f	[["action",55],["keyword","form_relate"],["model","country.country,-1"]]	\N	\N
654	2020-09-02 07:24:23.639802	0	2	group_currency_admin	[["name","Currency Administration"]]	res.group	currency	f	[["name","Currency Administration"]]	\N	\N
655	2020-09-02 07:24:23.639802	0	2	user_admin_group_currency_admin	[["group",2],["user",1]]	res.user-res.group	currency	f	[["group",2],["user",1]]	\N	\N
656	2020-09-02 07:24:23.639802	0	10	currency_icon	[["name","tryton-currency"],["path","icons/tryton-currency.svg"]]	ir.ui.icon	currency	f	[["name","tryton-currency"],["path","icons/tryton-currency.svg"]]	\N	\N
657	2020-09-02 07:24:23.639802	0	49	menu_currency	[["icon","tryton-currency"],["name","Currency"],["sequence",3]]	ir.ui.menu	currency	f	[["icon","tryton-currency"],["name","Currency"],["sequence",3]]	\N	\N
658	2020-09-02 07:24:23.639802	0	42	menu_currency_group_currency_admin	[["group",2],["menu",49]]	ir.ui.menu-res.group	currency	f	[["group",2],["menu",49]]	\N	\N
659	2020-09-02 07:24:23.639802	0	116	currency_view_form	[["model","currency.currency"],["name","currency_form"],["type","form"]]	ir.ui.view	currency	f	[["model","currency.currency"],["name","currency_form"],["type","form"]]	\N	\N
660	2020-09-02 07:24:23.639802	0	117	currency_view_tree	[["model","currency.currency"],["name","currency_tree"],["type","tree"]]	ir.ui.view	currency	f	[["model","currency.currency"],["name","currency_tree"],["type","tree"]]	\N	\N
661	2020-09-02 07:24:23.639802	0	56	act_currency_form	[["name","Currencies"],["res_model","currency.currency"]]	ir.action.act_window	currency	f	[["name","Currencies"],["res_model","currency.currency"]]	\N	\N
662	2020-09-02 07:24:23.639802	0	71	act_currency_form_view1	[["act_window",56],["sequence",10],["view",117]]	ir.action.act_window.view	currency	f	[["act_window",56],["sequence",10],["view",117]]	\N	\N
663	2020-09-02 07:24:23.639802	0	72	act_currency_form_view2	[["act_window",56],["sequence",20],["view",116]]	ir.action.act_window.view	currency	f	[["act_window",56],["sequence",20],["view",116]]	\N	\N
664	2020-09-02 07:24:23.639802	0	50	menu_currency_form	[["action","ir.action.act_window,56"],["icon","tryton-list"],["name","Currencies"],["parent",49]]	ir.ui.menu	currency	f	[["action","ir.action.act_window,56"],["icon","tryton-list"],["name","Currencies"],["parent",49]]	\N	\N
665	2020-09-02 07:24:23.639802	0	66	access_currency	[["model",85],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	currency	f	[["model",85],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
666	2020-09-02 07:24:23.639802	0	67	access_currency_currency_admin	[["group",2],["model",85],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	currency	f	[["group",2],["model",85],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
667	2020-09-02 07:24:23.639802	0	118	currency_rate_view_list	[["model","currency.currency.rate"],["name","currency_rate_list"],["type","tree"]]	ir.ui.view	currency	f	[["model","currency.currency.rate"],["name","currency_rate_list"],["type","tree"]]	\N	\N
668	2020-09-02 07:24:23.639802	0	119	currency_rate_view_form	[["model","currency.currency.rate"],["name","currency_rate_form"],["type","form"]]	ir.ui.view	currency	f	[["model","currency.currency.rate"],["name","currency_rate_form"],["type","form"]]	\N	\N
669	2020-09-02 07:24:23.639802	0	120	currency_rate_view_graph	[["model","currency.currency.rate"],["name","currency_rate_graph"],["type","graph"]]	ir.ui.view	currency	f	[["model","currency.currency.rate"],["name","currency_rate_graph"],["type","graph"]]	\N	\N
670	2020-09-02 07:24:23.639802	0	68	access_currency_rate	[["model",86],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	currency	f	[["model",86],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
671	2020-09-02 07:24:23.639802	0	69	access_currency_rate_currency_admin	[["group",2],["model",86],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	currency	f	[["group",2],["model",86],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
672	2020-09-02 07:24:23.639802	0	98	msg_no_rate	[["text","No rate found for currency \\"%(currency)s\\" on \\"%(date)s\\"."]]	ir.message	currency	f	[["text","No rate found for currency \\"%(currency)s\\" on \\"%(date)s\\"."]]	\N	\N
673	2020-09-02 07:24:23.639802	0	99	msg_currency_unique_rate_date	[["text","A currency can only have one rate by date."]]	ir.message	currency	f	[["text","A currency can only have one rate by date."]]	\N	\N
674	2020-09-02 07:24:23.639802	0	100	msg_currency_rate_positive	[["text","A currency rate must be positive."]]	ir.message	currency	f	[["text","A currency rate must be positive."]]	\N	\N
675	2020-09-02 07:24:24.586225	0	3	group_party_admin	[["name","Party Administration"]]	res.group	party	f	[["name","Party Administration"]]	\N	\N
676	2020-09-02 07:24:24.586225	0	3	user_admin_group_party_admin	[["group",3],["user",1]]	res.user-res.group	party	f	[["group",3],["user",1]]	\N	\N
677	2020-09-02 07:24:24.586225	0	11	party_icon	[["name","tryton-party"],["path","icons/tryton-party.svg"]]	ir.ui.icon	party	f	[["name","tryton-party"],["path","icons/tryton-party.svg"]]	\N	\N
678	2020-09-02 07:24:24.586225	0	51	menu_party	[["icon","tryton-party"],["name","Party"],["sequence",0]]	ir.ui.menu	party	f	[["icon","tryton-party"],["name","Party"],["sequence",0]]	\N	\N
679	2020-09-02 07:24:24.586225	0	52	menu_configuration	[["icon","tryton-settings"],["name","Configuration"],["parent",51],["sequence",0]]	ir.ui.menu	party	f	[["icon","tryton-settings"],["name","Configuration"],["parent",51],["sequence",0]]	\N	\N
680	2020-09-02 07:24:24.586225	0	43	menu_party_group_party_admin	[["group",3],["menu",52]]	ir.ui.menu-res.group	party	f	[["group",3],["menu",52]]	\N	\N
681	2020-09-02 07:24:24.586225	0	121	party_view_tree	[["model","party.party"],["name","party_tree"],["type","tree"]]	ir.ui.view	party	f	[["model","party.party"],["name","party_tree"],["type","tree"]]	\N	\N
682	2020-09-02 07:24:24.586225	0	122	party_view_form	[["model","party.party"],["name","party_form"],["type","form"]]	ir.ui.view	party	f	[["model","party.party"],["name","party_form"],["type","form"]]	\N	\N
683	2020-09-02 07:24:24.586225	0	57	act_party_form	[["name","Parties"],["res_model","party.party"]]	ir.action.act_window	party	f	[["name","Parties"],["res_model","party.party"]]	\N	\N
684	2020-09-02 07:24:24.586225	0	73	act_party_form_view1	[["act_window",57],["sequence",10],["view",121]]	ir.action.act_window.view	party	f	[["act_window",57],["sequence",10],["view",121]]	\N	\N
685	2020-09-02 07:24:24.586225	0	74	act_party_form_view2	[["act_window",57],["sequence",20],["view",122]]	ir.action.act_window.view	party	f	[["act_window",57],["sequence",20],["view",122]]	\N	\N
686	2020-09-02 07:24:24.586225	0	53	menu_party_form	[["action","ir.action.act_window,57"],["icon","tryton-list"],["name","Parties"],["parent",51],["sequence",1]]	ir.ui.menu	party	f	[["action","ir.action.act_window,57"],["icon","tryton-list"],["name","Parties"],["parent",51],["sequence",1]]	\N	\N
687	2020-09-02 07:24:24.586225	0	58	act_party_by_category	[["context","{\\"categories\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}"],["domain","[[\\"categories\\", \\"child_of\\", [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}], \\"parent\\"]]"],["name","Parties by Category"],["res_model","party.party"]]	ir.action.act_window	party	f	[["context","{\\"categories\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}"],["domain","[[\\"categories\\", \\"child_of\\", [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}], \\"parent\\"]]"],["name","Parties by Category"],["res_model","party.party"]]	\N	\N
688	2020-09-02 07:24:24.586225	0	55	act_party_by_category_keyword1	[["action",58],["keyword","tree_open"],["model","party.category,-1"]]	ir.action.keyword	party	f	[["action",58],["keyword","tree_open"],["model","party.category,-1"]]	\N	\N
689	2020-09-02 07:24:24.586225	0	59	report_label	[["model","party.party"],["name","Labels"],["report","party/label.fodt"],["report_name","party.label"]]	ir.action.report	party	f	[["model","party.party"],["name","Labels"],["report","party/label.fodt"],["report_name","party.label"]]	\N	\N
690	2020-09-02 07:24:24.586225	0	56	report_label_party	[["action",59],["keyword","form_print"],["model","party.party,-1"]]	ir.action.keyword	party	f	[["action",59],["keyword","form_print"],["model","party.party,-1"]]	\N	\N
691	2020-09-02 07:24:24.586225	0	1	sequence_type_party	[["code","party.party"],["name","Party"]]	ir.sequence.type	party	f	[["code","party.party"],["name","Party"]]	\N	\N
692	2020-09-02 07:24:24.586225	0	1	sequence_type_party_group_admin	[["group",1],["sequence_type",1]]	ir.sequence.type-res.group	party	f	[["group",1],["sequence_type",1]]	\N	\N
693	2020-09-02 07:24:24.586225	0	2	sequence_type_party_group_party_admin	[["group",3],["sequence_type",1]]	ir.sequence.type-res.group	party	f	[["group",3],["sequence_type",1]]	\N	\N
694	2020-09-02 07:24:24.586225	0	1	sequence_party	[["code","party.party"],["name","Party"]]	ir.sequence	party	f	[["code","party.party"],["name","Party"]]	\N	\N
695	2020-09-02 07:24:24.586225	0	123	identifier_form	[["model","party.identifier"],["name","identifier_form"],["type","form"]]	ir.ui.view	party	f	[["model","party.identifier"],["name","identifier_form"],["type","form"]]	\N	\N
696	2020-09-02 07:24:24.586225	0	124	identifier_list	[["model","party.identifier"],["name","identifier_list"],["priority",10],["type","tree"]]	ir.ui.view	party	f	[["model","party.identifier"],["name","identifier_list"],["priority",10],["type","tree"]]	\N	\N
697	2020-09-02 07:24:24.586225	0	125	identifier_list_sequence	[["model","party.identifier"],["name","identifier_list_sequence"],["priority",20],["type","tree"]]	ir.ui.view	party	f	[["model","party.identifier"],["name","identifier_list_sequence"],["priority",20],["type","tree"]]	\N	\N
698	2020-09-02 07:24:24.586225	0	60	wizard_check_vies	[["model","party.party"],["name","Check VIES"],["wiz_name","party.check_vies"]]	ir.action.wizard	party	f	[["model","party.party"],["name","Check VIES"],["wiz_name","party.check_vies"]]	\N	\N
699	2020-09-02 07:24:24.586225	0	57	check_vies_keyword	[["action",60],["keyword","form_action"],["model","party.party,-1"]]	ir.action.keyword	party	f	[["action",60],["keyword","form_action"],["model","party.party,-1"]]	\N	\N
700	2020-09-02 07:24:24.586225	0	126	check_vies_result	[["model","party.check_vies.result"],["name","check_vies_result"],["type","form"]]	ir.ui.view	party	f	[["model","party.check_vies.result"],["name","check_vies_result"],["type","form"]]	\N	\N
701	2020-09-02 07:24:24.586225	0	61	wizard_replace	[["model","party.party"],["name","Replace"],["wiz_name","party.replace"]]	ir.action.wizard	party	f	[["model","party.party"],["name","Replace"],["wiz_name","party.replace"]]	\N	\N
702	2020-09-02 07:24:24.586225	0	5	wizard_replace-group_party_admin	[["action",61],["group",3]]	ir.action-res.group	party	f	[["action",61],["group",3]]	\N	\N
872	2020-09-02 07:24:29.514826	0	73	act_employee_form	[["name","Employees"],["res_model","company.employee"]]	ir.action.act_window	company	f	[["name","Employees"],["res_model","company.employee"]]	\N	\N
703	2020-09-02 07:24:24.586225	0	58	wizard_replace_keyword1	[["action",61],["keyword","form_action"],["model","party.party,-1"]]	ir.action.keyword	party	f	[["action",61],["keyword","form_action"],["model","party.party,-1"]]	\N	\N
704	2020-09-02 07:24:24.586225	0	127	replace_ask_view_form	[["model","party.replace.ask"],["name","replace_ask_form"],["type","form"]]	ir.ui.view	party	f	[["model","party.replace.ask"],["name","replace_ask_form"],["type","form"]]	\N	\N
705	2020-09-02 07:24:24.586225	0	62	wizard_erase	[["model","party.party"],["name","Erase"],["wiz_name","party.erase"]]	ir.action.wizard	party	f	[["model","party.party"],["name","Erase"],["wiz_name","party.erase"]]	\N	\N
706	2020-09-02 07:24:24.586225	0	6	wizard_erase-group_party_admin	[["action",62],["group",3]]	ir.action-res.group	party	f	[["action",62],["group",3]]	\N	\N
707	2020-09-02 07:24:24.586225	0	59	wizard_erase_keyword1	[["action",62],["keyword","form_action"],["model","party.party,-1"]]	ir.action.keyword	party	f	[["action",62],["keyword","form_action"],["model","party.party,-1"]]	\N	\N
708	2020-09-02 07:24:24.586225	0	128	erase_ask_view_form	[["model","party.erase.ask"],["name","erase_ask_form"],["type","form"]]	ir.ui.view	party	f	[["model","party.erase.ask"],["name","erase_ask_form"],["type","form"]]	\N	\N
709	2020-09-02 07:24:24.586225	0	129	category_view_form	[["model","party.category"],["name","category_form"],["type","form"]]	ir.ui.view	party	f	[["model","party.category"],["name","category_form"],["type","form"]]	\N	\N
710	2020-09-02 07:24:24.586225	0	130	category_view_tree	[["field_childs","childs"],["model","party.category"],["name","category_tree"],["type","tree"]]	ir.ui.view	party	f	[["field_childs","childs"],["model","party.category"],["name","category_tree"],["type","tree"]]	\N	\N
711	2020-09-02 07:24:24.586225	0	131	category_view_list	[["model","party.category"],["name","category_list"],["priority",10],["type","tree"]]	ir.ui.view	party	f	[["model","party.category"],["name","category_list"],["priority",10],["type","tree"]]	\N	\N
712	2020-09-02 07:24:24.586225	0	63	act_category_tree	[["domain","[[\\"parent\\", \\"=\\", null]]"],["name","Categories"],["res_model","party.category"]]	ir.action.act_window	party	f	[["domain","[[\\"parent\\", \\"=\\", null]]"],["name","Categories"],["res_model","party.category"]]	\N	\N
713	2020-09-02 07:24:24.586225	0	75	act_category_tree_view1	[["act_window",63],["sequence",10],["view",130]]	ir.action.act_window.view	party	f	[["act_window",63],["sequence",10],["view",130]]	\N	\N
714	2020-09-02 07:24:24.586225	0	76	act_category_tree_view2	[["act_window",63],["sequence",20],["view",129]]	ir.action.act_window.view	party	f	[["act_window",63],["sequence",20],["view",129]]	\N	\N
715	2020-09-02 07:24:24.586225	0	54	menu_category_tree	[["action","ir.action.act_window,63"],["icon","tryton-tree"],["name","Categories"],["parent",51]]	ir.ui.menu	party	f	[["action","ir.action.act_window,63"],["icon","tryton-tree"],["name","Categories"],["parent",51]]	\N	\N
716	2020-09-02 07:24:24.586225	0	64	act_category_list	[["name","Categories"],["res_model","party.category"]]	ir.action.act_window	party	f	[["name","Categories"],["res_model","party.category"]]	\N	\N
717	2020-09-02 07:24:24.586225	0	77	act_category_list_view1	[["act_window",64],["sequence",10],["view",131]]	ir.action.act_window.view	party	f	[["act_window",64],["sequence",10],["view",131]]	\N	\N
718	2020-09-02 07:24:24.586225	0	78	act_category_list_view2	[["act_window",64],["sequence",20],["view",129]]	ir.action.act_window.view	party	f	[["act_window",64],["sequence",20],["view",129]]	\N	\N
719	2020-09-02 07:24:24.586225	0	55	menu_category_list	[["action","ir.action.act_window,64"],["icon","tryton-list"],["name","Categories"],["parent",54]]	ir.ui.menu	party	f	[["action","ir.action.act_window,64"],["icon","tryton-list"],["name","Categories"],["parent",54]]	\N	\N
720	2020-09-02 07:24:24.586225	0	70	access_party_category	[["model",87],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	party	f	[["model",87],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
721	2020-09-02 07:24:24.586225	0	71	access_party_category_admin	[["group",3],["model",87],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	party	f	[["group",3],["model",87],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
722	2020-09-02 07:24:24.586225	0	132	address_view_tree	[["model","party.address"],["name","address_tree"],["priority",10],["type","tree"]]	ir.ui.view	party	f	[["model","party.address"],["name","address_tree"],["priority",10],["type","tree"]]	\N	\N
723	2020-09-02 07:24:24.586225	0	133	address_view_tree_sequence	[["model","party.address"],["name","address_tree_sequence"],["priority",20],["type","tree"]]	ir.ui.view	party	f	[["model","party.address"],["name","address_tree_sequence"],["priority",20],["type","tree"]]	\N	\N
724	2020-09-02 07:24:24.586225	0	134	address_view_form	[["model","party.address"],["name","address_form"],["type","form"]]	ir.ui.view	party	f	[["model","party.address"],["name","address_form"],["type","form"]]	\N	\N
725	2020-09-02 07:24:24.586225	0	65	act_address_form	[["name","Addresses"],["res_model","party.address"]]	ir.action.act_window	party	f	[["name","Addresses"],["res_model","party.address"]]	\N	\N
726	2020-09-02 07:24:24.586225	0	79	act_address_form_view1	[["act_window",65],["sequence",10],["view",132]]	ir.action.act_window.view	party	f	[["act_window",65],["sequence",10],["view",132]]	\N	\N
727	2020-09-02 07:24:24.586225	0	80	act_address_form_view2	[["act_window",65],["sequence",20],["view",134]]	ir.action.act_window.view	party	f	[["act_window",65],["sequence",20],["view",134]]	\N	\N
728	2020-09-02 07:24:24.586225	0	56	menu_address_form	[["action","ir.action.act_window,65"],["icon","tryton-list"],["name","Addresses"],["parent",51],["sequence",2]]	ir.ui.menu	party	f	[["action","ir.action.act_window,65"],["icon","tryton-list"],["name","Addresses"],["parent",51],["sequence",2]]	\N	\N
729	2020-09-02 07:24:24.586225	0	135	address_format_view_list	[["model","party.address.format"],["name","address_format_list"],["type","tree"]]	ir.ui.view	party	f	[["model","party.address.format"],["name","address_format_list"],["type","tree"]]	\N	\N
730	2020-09-02 07:24:24.586225	0	136	address_format_view_form	[["model","party.address.format"],["name","address_format_form"],["type","form"]]	ir.ui.view	party	f	[["model","party.address.format"],["name","address_format_form"],["type","form"]]	\N	\N
731	2020-09-02 07:24:24.586225	0	66	act_address_format_form	[["name","Address Formats"],["res_model","party.address.format"]]	ir.action.act_window	party	f	[["name","Address Formats"],["res_model","party.address.format"]]	\N	\N
732	2020-09-02 07:24:24.586225	0	81	act_address_format_form_view1	[["act_window",66],["sequence",10],["view",135]]	ir.action.act_window.view	party	f	[["act_window",66],["sequence",10],["view",135]]	\N	\N
733	2020-09-02 07:24:24.586225	0	82	act_address_format_form_view2	[["act_window",66],["sequence",20],["view",136]]	ir.action.act_window.view	party	f	[["act_window",66],["sequence",20],["view",136]]	\N	\N
871	2020-09-02 07:24:29.514826	0	150	employee_view_tree	[["model","company.employee"],["name","employee_tree"],["type","tree"]]	ir.ui.view	company	f	[["model","company.employee"],["name","employee_tree"],["type","tree"]]	\N	\N
734	2020-09-02 07:24:24.586225	0	57	menu_address_format_form	[["action","ir.action.act_window,66"],["icon","tryton-list"],["name","Address Formats"],["parent",52],["sequence",10]]	ir.ui.menu	party	f	[["action","ir.action.act_window,66"],["icon","tryton-list"],["name","Address Formats"],["parent",52],["sequence",10]]	\N	\N
735	2020-09-02 07:24:24.586225	0	72	access_address_format	[["model",96],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	party	f	[["model",96],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
736	2020-09-02 07:24:24.586225	0	73	access_address_format_admin	[["group",3],["model",96],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	party	f	[["group",3],["model",96],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
737	2020-09-02 07:24:24.586225	0	137	address_subdivision_type_view_list	[["model","party.address.subdivision_type"],["name","address_subdivision_type_list"],["type","tree"]]	ir.ui.view	party	f	[["model","party.address.subdivision_type"],["name","address_subdivision_type_list"],["type","tree"]]	\N	\N
738	2020-09-02 07:24:24.586225	0	138	address_subdivision_type_view_form	[["model","party.address.subdivision_type"],["name","address_subdivision_type_form"],["type","form"]]	ir.ui.view	party	f	[["model","party.address.subdivision_type"],["name","address_subdivision_type_form"],["type","form"]]	\N	\N
739	2020-09-02 07:24:24.586225	0	67	act_address_subdivision_type_form	[["name","Address Subdivision Types"],["res_model","party.address.subdivision_type"]]	ir.action.act_window	party	f	[["name","Address Subdivision Types"],["res_model","party.address.subdivision_type"]]	\N	\N
740	2020-09-02 07:24:24.586225	0	83	act_address_subdivision_type_form_view1	[["act_window",67],["sequence",10],["view",137]]	ir.action.act_window.view	party	f	[["act_window",67],["sequence",10],["view",137]]	\N	\N
741	2020-09-02 07:24:24.586225	0	84	act_address_subdivision_type_form_view2	[["act_window",67],["sequence",20],["view",138]]	ir.action.act_window.view	party	f	[["act_window",67],["sequence",20],["view",138]]	\N	\N
742	2020-09-02 07:24:24.586225	0	58	menu_address_subdivision_type_form	[["action","ir.action.act_window,67"],["icon","tryton-list"],["name","Address Subdivision Types"],["parent",52],["sequence",20]]	ir.ui.menu	party	f	[["action","ir.action.act_window,67"],["icon","tryton-list"],["name","Address Subdivision Types"],["parent",52],["sequence",20]]	\N	\N
743	2020-09-02 07:24:24.586225	0	74	access_address_subdivision_type	[["model",97],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	party	f	[["model",97],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
744	2020-09-02 07:24:24.586225	0	75	access_address_subdivision_type_admin	[["group",3],["model",97],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	party	f	[["group",3],["model",97],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
745	2020-09-02 07:24:24.586225	0	1	address_format_ar	[["country_code","AR"],["format_","${party_name}\\n${name}\\n${street}\\n${subdivision}\\n${ZIP}, ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","AR"],["format_","${party_name}\\n${name}\\n${street}\\n${subdivision}\\n${ZIP}, ${city}\\n${COUNTRY}"]]	\N	\N
746	2020-09-02 07:24:24.586225	0	2	address_format_au	[["country_code","AU"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${subdivision}\\n${CITY} ${SUBDIVISION} ${ZIP}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","AU"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${subdivision}\\n${CITY} ${SUBDIVISION} ${ZIP}\\n${COUNTRY}"]]	\N	\N
747	2020-09-02 07:24:24.586225	0	3	address_format_at	[["country_code","AT"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","AT"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
748	2020-09-02 07:24:24.586225	0	4	address_format_bd	[["country_code","BD"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${city}-${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","BD"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${city}-${zip}\\n${COUNTRY}"]]	\N	\N
749	2020-09-02 07:24:24.586225	0	5	address_format_by	[["country_code","BY"],["format_","${party_name}\\n${name}\\n${street}\\n${zip}, ${city}\\n${subdivision}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","BY"],["format_","${party_name}\\n${name}\\n${street}\\n${zip}, ${city}\\n${subdivision}\\n${COUNTRY}"]]	\N	\N
750	2020-09-02 07:24:24.586225	0	6	address_format_be	[["country_code","BE"],["format_","${attn}\\n${party_name}\\n${name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","BE"],["format_","${attn}\\n${party_name}\\n${name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
751	2020-09-02 07:24:24.586225	0	7	address_format_br	[["country_code","BR"],["format_","${party_name}\\n${street}\\n${name}\\n${city} - ${subdivision_code}\\n${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","BR"],["format_","${party_name}\\n${street}\\n${name}\\n${city} - ${subdivision_code}\\n${zip}\\n${COUNTRY}"]]	\N	\N
752	2020-09-02 07:24:24.586225	0	8	address_format_bg	[["country_code","BG"],["format_","${party_name}\\n${street}\\n${name}\\n${zip} ${city}\\n${subdivision}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","BG"],["format_","${party_name}\\n${street}\\n${name}\\n${zip} ${city}\\n${subdivision}\\n${COUNTRY}"]]	\N	\N
753	2020-09-02 07:24:24.586225	0	9	address_format_ca_fr	[["country_code","CA"],["format_","${attn}\\n${party_name}\\n${name}\\n${street}\\n${city} (${subdivision}) ${zip}\\n${COUNTRY}"],["language_code","fr"]]	party.address.format	party	t	[["country_code","CA"],["format_","${attn}\\n${party_name}\\n${name}\\n${street}\\n${city} (${subdivision}) ${zip}\\n${COUNTRY}"],["language_code","fr"]]	\N	\N
754	2020-09-02 07:24:24.586225	0	10	address_format_ca	[["country_code","CA"],["format_","${ATTN}\\n${PARTY_NAME}\\n${NAME}\\n${STREET}\\n${CITY} ${SUBDIVISION_CODE} ${ZIP}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","CA"],["format_","${ATTN}\\n${PARTY_NAME}\\n${NAME}\\n${STREET}\\n${CITY} ${SUBDIVISION_CODE} ${ZIP}\\n${COUNTRY}"]]	\N	\N
755	2020-09-02 07:24:24.586225	0	11	address_format_cl	[["country_code","CL"],["format_","${party_name}\\n${street}\\n${name}\\n${zip}\\n${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","CL"],["format_","${party_name}\\n${street}\\n${name}\\n${zip}\\n${city}\\n${COUNTRY}"]]	\N	\N
756	2020-09-02 07:24:24.586225	0	12	address_format_cn_zh_CN	[["country_code","CN"],["format_","${COUNTRY} ${ZIP}\\n${subdivision}${city}${street}${name}\\n${party_name}"],["language_code","zh"]]	party.address.format	party	t	[["country_code","CN"],["format_","${COUNTRY} ${ZIP}\\n${subdivision}${city}${street}${name}\\n${party_name}"],["language_code","zh"]]	\N	\N
757	2020-09-02 07:24:24.586225	0	13	address_format_cn	[["country_code","CN"],["format_","${COUNTRY} ${ZIP}\\n${subdivision}, ${city}, ${street}, ${name}\\n${party_name}"]]	party.address.format	party	t	[["country_code","CN"],["format_","${COUNTRY} ${ZIP}\\n${subdivision}, ${city}, ${street}, ${name}\\n${party_name}"]]	\N	\N
758	2020-09-02 07:24:24.586225	0	14	address_format_hr	[["country_code","HR"],["format_","${party_name}\\n${street}\\n${COUNTRY_CODE}-${ZIP} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","HR"],["format_","${party_name}\\n${street}\\n${COUNTRY_CODE}-${ZIP} ${city}\\n${COUNTRY}"]]	\N	\N
759	2020-09-02 07:24:24.586225	0	15	address_format_cz	[["country_code","CZ"],["format_","${party_name}\\n${attn}\\n${street}\\n${COUNTRY_CODE}-${ZIP} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","CZ"],["format_","${party_name}\\n${attn}\\n${street}\\n${COUNTRY_CODE}-${ZIP} ${city}\\n${COUNTRY}"]]	\N	\N
760	2020-09-02 07:24:24.586225	0	16	address_format_dk	[["country_code","DK"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","DK"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
761	2020-09-02 07:24:24.586225	0	17	address_format_ee	[["country_code","EE"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","EE"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
762	2020-09-02 07:24:24.586225	0	18	address_format_fi	[["country_code","FI"],["format_","${attn}\\n${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","FI"],["format_","${attn}\\n${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
763	2020-09-02 07:24:24.586225	0	19	address_format_fr	[["country_code","FR"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${ZIP} ${CITY}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","FR"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${ZIP} ${CITY}\\n${COUNTRY}"]]	\N	\N
764	2020-09-02 07:24:24.586225	0	20	address_format_de	[["country_code","DE"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","DE"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
765	2020-09-02 07:24:24.586225	0	21	address_format_gr	[["country_code","GR"],["format_","${party_name}\\n${street}\\n${COUNTRY_CODE}-${ZIP} ${CITY}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","GR"],["format_","${party_name}\\n${street}\\n${COUNTRY_CODE}-${ZIP} ${CITY}\\n${COUNTRY}"]]	\N	\N
766	2020-09-02 07:24:24.586225	0	22	address_format_hk	[["country_code","HK"],["format_","${party_name}\\n${name}\\n${street}\\n${subdivision}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","HK"],["format_","${party_name}\\n${name}\\n${street}\\n${subdivision}\\n${COUNTRY}"]]	\N	\N
767	2020-09-02 07:24:24.586225	0	23	address_format_hu	[["country_code","HU"],["format_","${party_name}\\n${city}\\n${street}\\n${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","HU"],["format_","${party_name}\\n${city}\\n${street}\\n${zip}\\n${COUNTRY}"]]	\N	\N
768	2020-09-02 07:24:24.586225	0	24	address_format_is	[["country_code","IS"],["format_","${party_name}\\n${street}\\n${name}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","IS"],["format_","${party_name}\\n${street}\\n${name}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
769	2020-09-02 07:24:24.586225	0	25	address_format_in	[["country_code","IN"],["format_","${party_name}\\n${name}\\n${street}\\n${CITY} ${zip}\\n${subdivision}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","IN"],["format_","${party_name}\\n${name}\\n${street}\\n${CITY} ${zip}\\n${subdivision}\\n${COUNTRY}"]]	\N	\N
770	2020-09-02 07:24:24.586225	0	26	address_format_id	[["country_code","ID"],["format_","${party_name}\\n${name}\\n${street}\\n${city} ${zip}\\n${subdivision}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","ID"],["format_","${party_name}\\n${name}\\n${street}\\n${city} ${zip}\\n${subdivision}\\n${COUNTRY}"]]	\N	\N
771	2020-09-02 07:24:24.586225	0	27	address_format_ir	[["country_code","IR"],["format_","${party_name}\\n${name}\\n${city}\\n${street}\\n${subdivision}\\n${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","IR"],["format_","${party_name}\\n${name}\\n${city}\\n${street}\\n${subdivision}\\n${zip}\\n${COUNTRY}"]]	\N	\N
772	2020-09-02 07:24:24.586225	0	28	address_format_iq	[["country_code","IQ"],["format_","${party_name}\\n${street}\\n${name}\\n${subdivision}\\n${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","IQ"],["format_","${party_name}\\n${street}\\n${name}\\n${subdivision}\\n${zip}\\n${COUNTRY}"]]	\N	\N
773	2020-09-02 07:24:24.586225	0	29	address_format_ie	[["country_code","IE"],["format_","${party_name}\\n${street}\\n${city} ${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","IE"],["format_","${party_name}\\n${street}\\n${city} ${zip}\\n${COUNTRY}"]]	\N	\N
774	2020-09-02 07:24:24.586225	0	30	address_format_il	[["country_code","IL"],["format_","${party_name}\\n${street}\\n${city} ${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","IL"],["format_","${party_name}\\n${street}\\n${city} ${zip}\\n${COUNTRY}"]]	\N	\N
775	2020-09-02 07:24:24.586225	0	31	address_format_it	[["country_code","IT"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${zip} ${city} ${SUBDIVISION_CODE}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","IT"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${zip} ${city} ${SUBDIVISION_CODE}\\n${COUNTRY}"]]	\N	\N
776	2020-09-02 07:24:24.586225	0	32	address_format_jp_jp	[["country_code","JP"],["format_","${COUNTRY}\\n${zip}\\n${subdivision}${city}${street}\\n${party_name}"],["language_code","jp"]]	party.address.format	party	t	[["country_code","JP"],["format_","${COUNTRY}\\n${zip}\\n${subdivision}${city}${street}\\n${party_name}"],["language_code","jp"]]	\N	\N
777	2020-09-02 07:24:24.586225	0	33	address_format_jp	[["country_code","JP"],["format_","${party_name}\\n${street}\\n${city}, ${SUBDIVISION} ${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","JP"],["format_","${party_name}\\n${street}\\n${city}, ${SUBDIVISION} ${zip}\\n${COUNTRY}"]]	\N	\N
778	2020-09-02 07:24:24.586225	0	34	address_format_kr_ko	[["country_code","KR"],["format_","${COUNTRY}\\n${street}\\n${party_name}\\n${zip}"],["language_code","ko"]]	party.address.format	party	t	[["country_code","KR"],["format_","${COUNTRY}\\n${street}\\n${party_name}\\n${zip}"],["language_code","ko"]]	\N	\N
779	2020-09-02 07:24:24.586225	0	35	address_format_kr	[["country_code","KR"],["format_","${party_name}\\n${street}\\n${city}, ${subdivision} ${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","KR"],["format_","${party_name}\\n${street}\\n${city}, ${subdivision} ${zip}\\n${COUNTRY}"]]	\N	\N
780	2020-09-02 07:24:24.586225	0	36	address_format_lv	[["country_code","LV"],["format_","${party_name}\\n${street}\\n${city}\\n${subdivision}\\n${COUNTRY_CODE}-${ZIP}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","LV"],["format_","${party_name}\\n${street}\\n${city}\\n${subdivision}\\n${COUNTRY_CODE}-${ZIP}\\n${COUNTRY}"]]	\N	\N
781	2020-09-02 07:24:24.586225	0	37	address_format_mo_zh_CN	[["country_code","MO"],["format_","${COUNTRY}\\n${city}\\n${street}\\n${party_name}"],["language_code","zh"]]	party.address.format	party	t	[["country_code","MO"],["format_","${COUNTRY}\\n${city}\\n${street}\\n${party_name}"],["language_code","zh"]]	\N	\N
782	2020-09-02 07:24:24.586225	0	38	address_format_mo	[["country_code","MO"],["format_","${party_name}\\n${street}\\n${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","MO"],["format_","${party_name}\\n${street}\\n${city}\\n${COUNTRY}"]]	\N	\N
783	2020-09-02 07:24:24.586225	0	39	address_format_my	[["country_code","MY"],["format_","${attn}\\n${party_name}\\n${name}\\n${street}\\n${zip} ${CITY}\\n${SUBDIVISION}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","MY"],["format_","${attn}\\n${party_name}\\n${name}\\n${street}\\n${zip} ${CITY}\\n${SUBDIVISION}\\n${COUNTRY}"]]	\N	\N
784	2020-09-02 07:24:24.586225	0	40	address_format_mx	[["country_code","MX"],["format_","${attn}\\n${party_name}\\n${street}\\n${name}\\n${zip}, ${city}, ${subdivision}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","MX"],["format_","${attn}\\n${party_name}\\n${street}\\n${name}\\n${zip}, ${city}, ${subdivision}\\n${COUNTRY}"]]	\N	\N
785	2020-09-02 07:24:24.586225	0	41	address_format_nl	[["country_code","NL"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${zip} ${CITY}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","NL"],["format_","${party_name}\\n${attn}\\n${name}\\n${street}\\n${zip} ${CITY}\\n${COUNTRY}"]]	\N	\N
786	2020-09-02 07:24:24.586225	0	42	address_format_nz	[["country_code","NZ"],["format_","${party_name}\\n${street}\\n${city} ${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","NZ"],["format_","${party_name}\\n${street}\\n${city} ${zip}\\n${COUNTRY}"]]	\N	\N
787	2020-09-02 07:24:24.586225	0	43	address_format_no	[["country_code","NO"],["format_","${party_name}\\n${street}\\n${zip} ${CITY}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","NO"],["format_","${party_name}\\n${street}\\n${zip} ${CITY}\\n${COUNTRY}"]]	\N	\N
788	2020-09-02 07:24:24.586225	0	44	address_format_om	[["country_code","OM"],["format_","${party_name}\\n${street}\\n${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","OM"],["format_","${party_name}\\n${street}\\n${city}\\n${COUNTRY}"]]	\N	\N
789	2020-09-02 07:24:24.586225	0	45	address_format_pk	[["country_code","PK"],["format_","${party_name}\\n${street}\\n${city}\\n${zip}\\n${subdivision}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","PK"],["format_","${party_name}\\n${street}\\n${city}\\n${zip}\\n${subdivision}\\n${COUNTRY}"]]	\N	\N
790	2020-09-02 07:24:24.586225	0	46	address_format_pe	[["country_code","PE"],["format_","${party_name}\\n${street}\\n${name}\\n${city}\\n${subdivision}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","PE"],["format_","${party_name}\\n${street}\\n${name}\\n${city}\\n${subdivision}\\n${COUNTRY}"]]	\N	\N
791	2020-09-02 07:24:24.586225	0	47	address_format_ph	[["country_code","PH"],["format_","${party_name}\\n${street}\\n${zip} ${CITY}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","PH"],["format_","${party_name}\\n${street}\\n${zip} ${CITY}\\n${COUNTRY}"]]	\N	\N
792	2020-09-02 07:24:24.586225	0	48	address_format_pl	[["country_code","PL"],["format_","${attn} ${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","PL"],["format_","${attn} ${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
793	2020-09-02 07:24:24.586225	0	49	address_format_pt	[["country_code","PT"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","PT"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
794	2020-09-02 07:24:24.586225	0	50	address_format_qa	[["country_code","QA"],["format_","${party_name}\\n${street}\\n${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","QA"],["format_","${party_name}\\n${street}\\n${city}\\n${COUNTRY}"]]	\N	\N
795	2020-09-02 07:24:24.586225	0	51	address_format_ro	[["country_code","RO"],["format_","${attn} ${party_name}\\n${street}\\n${city}\\n${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","RO"],["format_","${attn} ${party_name}\\n${street}\\n${city}\\n${zip}\\n${COUNTRY}"]]	\N	\N
796	2020-09-02 07:24:24.586225	0	52	address_format_ru	[["country_code","RU"],["format_","${party_name}\\n${street}\\n${city}\\n${subdivision}\\n${COUNTRY}\\n${zip}"]]	party.address.format	party	t	[["country_code","RU"],["format_","${party_name}\\n${street}\\n${city}\\n${subdivision}\\n${COUNTRY}\\n${zip}"]]	\N	\N
797	2020-09-02 07:24:24.586225	0	53	address_format_sa	[["country_code","SA"],["format_","${party_name}\\n${street}\\n${city} ${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","SA"],["format_","${party_name}\\n${street}\\n${city} ${zip}\\n${COUNTRY}"]]	\N	\N
798	2020-09-02 07:24:24.586225	0	54	address_format_rs	[["country_code","RS"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","RS"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
799	2020-09-02 07:24:24.586225	0	55	address_format_sg	[["country_code","SG"],["format_","${party_name}\\n${street}\\n${name}\\n${CITY} ${ZIP}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","SG"],["format_","${party_name}\\n${street}\\n${name}\\n${CITY} ${ZIP}\\n${COUNTRY}"]]	\N	\N
800	2020-09-02 07:24:24.586225	0	56	address_format_sk	[["country_code","SK"],["format_","${attn}\\n${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","SK"],["format_","${attn}\\n${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
801	2020-09-02 07:24:24.586225	0	57	address_format_sl	[["country_code","SL"],["format_","${party_name}\\n${attn}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","SL"],["format_","${party_name}\\n${attn}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
802	2020-09-02 07:24:24.586225	0	58	address_format_es	[["country_code","ES"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${subdivision}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","ES"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${subdivision}\\n${COUNTRY}"]]	\N	\N
803	2020-09-02 07:24:24.586225	0	59	address_format_lk	[["country_code","LK"],["format_","${party_name}\\n${street}\\n${CITY}\\n${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","LK"],["format_","${party_name}\\n${street}\\n${CITY}\\n${zip}\\n${COUNTRY}"]]	\N	\N
804	2020-09-02 07:24:24.586225	0	60	address_format_se	[["country_code","SE"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","SE"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
805	2020-09-02 07:24:24.586225	0	61	address_format_ch	[["country_code","CH"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","CH"],["format_","${party_name}\\n${street}\\n${zip} ${city}\\n${COUNTRY}"]]	\N	\N
806	2020-09-02 07:24:24.586225	0	62	address_format_tw_zh_CN	[["country_code","TW"],["format_","${COUNTRY}\\n${zip}\\n${street}\\n${party_name}"],["language_code","zh"]]	party.address.format	party	t	[["country_code","TW"],["format_","${COUNTRY}\\n${zip}\\n${street}\\n${party_name}"],["language_code","zh"]]	\N	\N
807	2020-09-02 07:24:24.586225	0	63	address_format_tw	[["country_code","TW"],["format_","${party_name}\\n${street}\\n${city}, ${subdivision} ${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","TW"],["format_","${party_name}\\n${street}\\n${city}, ${subdivision} ${zip}\\n${COUNTRY}"]]	\N	\N
808	2020-09-02 07:24:24.586225	0	64	address_format_th	[["country_code","TH"],["format_","${party_name}\\n${street}\\n${name}\\n${subdivision}\\n${COUNTRY}\\n${zip}"]]	party.address.format	party	t	[["country_code","TH"],["format_","${party_name}\\n${street}\\n${name}\\n${subdivision}\\n${COUNTRY}\\n${zip}"]]	\N	\N
809	2020-09-02 07:24:24.586225	0	65	address_format_tr	[["country_code","TR"],["format_","${party_name}\\n${attn}\\n${street}\\n${name}\\n${zip} ${city} ${subdivision}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","TR"],["format_","${party_name}\\n${attn}\\n${street}\\n${name}\\n${zip} ${city} ${subdivision}\\n${COUNTRY}"]]	\N	\N
810	2020-09-02 07:24:24.586225	0	66	address_format_ua	[["country_code","UA"],["format_","${party_name}\\n${street}\\n${city}\\n${subdivision}\\n${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","UA"],["format_","${party_name}\\n${street}\\n${city}\\n${subdivision}\\n${zip}\\n${COUNTRY}"]]	\N	\N
811	2020-09-02 07:24:24.586225	0	67	address_format_gb	[["country_code","GB"],["format_","${party_name}\\n${street}\\n${CITY}\\n${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","GB"],["format_","${party_name}\\n${street}\\n${CITY}\\n${zip}\\n${COUNTRY}"]]	\N	\N
812	2020-09-02 07:24:24.586225	0	68	address_format_us	[["country_code","US"],["format_","${attn}\\n${party_name}\\n${street}\\n${city}, ${subdivision_code} ${zip}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","US"],["format_","${attn}\\n${party_name}\\n${street}\\n${city}, ${subdivision_code} ${zip}\\n${COUNTRY}"]]	\N	\N
813	2020-09-02 07:24:24.586225	0	69	address_format_vn	[["country_code","VN"],["format_","${party_name}\\n${street}\\n${city}\\n${subdivision}\\n${COUNTRY}"]]	party.address.format	party	t	[["country_code","VN"],["format_","${party_name}\\n${street}\\n${city}\\n${subdivision}\\n${COUNTRY}"]]	\N	\N
814	2020-09-02 07:24:24.586225	0	1	address_subdivision_type_id	[["country_code","ID"],["types",["autonomous province","province","special district","special region"]]]	party.address.subdivision_type	party	t	[["country_code","ID"],["types",["autonomous province","province","special district","special region"]]]	\N	\N
815	2020-09-02 07:24:24.586225	0	2	address_subdivision_it	[["country_code","IT"],["types",["province"]]]	party.address.subdivision_type	party	t	[["country_code","IT"],["types",["province"]]]	\N	\N
816	2020-09-02 07:24:24.586225	0	3	address_subdivision_es	[["country_code","ES"],["types",["province"]]]	party.address.subdivision_type	party	t	[["country_code","ES"],["types",["province"]]]	\N	\N
817	2020-09-02 07:24:24.586225	0	139	contact_mechanism_view_tree	[["model","party.contact_mechanism"],["name","contact_mechanism_tree"],["priority",10],["type","tree"]]	ir.ui.view	party	f	[["model","party.contact_mechanism"],["name","contact_mechanism_tree"],["priority",10],["type","tree"]]	\N	\N
818	2020-09-02 07:24:24.586225	0	140	contact_mechanism_view_tree_sequence	[["model","party.contact_mechanism"],["name","contact_mechanism_tree_sequence"],["priority",20],["type","tree"]]	ir.ui.view	party	f	[["model","party.contact_mechanism"],["name","contact_mechanism_tree_sequence"],["priority",20],["type","tree"]]	\N	\N
819	2020-09-02 07:24:24.586225	0	141	contact_mechanism_view_form	[["model","party.contact_mechanism"],["name","contact_mechanism_form"],["type","form"]]	ir.ui.view	party	f	[["model","party.contact_mechanism"],["name","contact_mechanism_form"],["type","form"]]	\N	\N
820	2020-09-02 07:24:24.586225	0	68	act_contact_mechanism_form	[["name","Contact Mechanisms"],["res_model","party.contact_mechanism"]]	ir.action.act_window	party	f	[["name","Contact Mechanisms"],["res_model","party.contact_mechanism"]]	\N	\N
821	2020-09-02 07:24:24.586225	0	85	act_contact_mechanism_form_view1	[["act_window",68],["sequence",10],["view",139]]	ir.action.act_window.view	party	f	[["act_window",68],["sequence",10],["view",139]]	\N	\N
822	2020-09-02 07:24:24.586225	0	86	act_contact_mechanism_form_view2	[["act_window",68],["sequence",20],["view",141]]	ir.action.act_window.view	party	f	[["act_window",68],["sequence",20],["view",141]]	\N	\N
823	2020-09-02 07:24:24.586225	0	59	menu_contact_mechanism_form	[["action","ir.action.act_window,68"],["icon","tryton-list"],["name","Contact Mechanisms"],["parent",51],["sequence",3]]	ir.ui.menu	party	f	[["action","ir.action.act_window,68"],["icon","tryton-list"],["name","Contact Mechanisms"],["parent",51],["sequence",3]]	\N	\N
824	2020-09-02 07:24:24.586225	0	142	party_configuration_view_form	[["model","party.configuration"],["name","configuration_form"],["type","form"]]	ir.ui.view	party	f	[["model","party.configuration"],["name","configuration_form"],["type","form"]]	\N	\N
825	2020-09-02 07:24:24.586225	0	69	act_party_configuration_form	[["name","Party Configuration"],["res_model","party.configuration"]]	ir.action.act_window	party	f	[["name","Party Configuration"],["res_model","party.configuration"]]	\N	\N
826	2020-09-02 07:24:24.586225	0	87	act_party_configuration_view1	[["act_window",69],["sequence",1],["view",142]]	ir.action.act_window.view	party	f	[["act_window",69],["sequence",1],["view",142]]	\N	\N
827	2020-09-02 07:24:24.586225	0	60	menu_party_configuration	[["action","ir.action.act_window,69"],["icon","tryton-list"],["name","Party Configuration"],["parent",52],["sequence",0]]	ir.ui.menu	party	f	[["action","ir.action.act_window,69"],["icon","tryton-list"],["name","Party Configuration"],["parent",52],["sequence",0]]	\N	\N
828	2020-09-02 07:24:24.586225	0	44	menu_party_configuration_group_party_admin	[["group",3],["menu",60]]	ir.ui.menu-res.group	party	f	[["group",3],["menu",60]]	\N	\N
829	2020-09-02 07:24:24.586225	0	76	access_party_configuration	[["model",99],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	party	f	[["model",99],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
830	2020-09-02 07:24:24.586225	0	77	access_party_configuration_party_admin	[["group",3],["model",99],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	party	f	[["group",3],["model",99],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
831	2020-09-02 07:24:24.586225	0	1	configuration_party_sequence	[["party_sequence",1]]	party.configuration.party_sequence	party	t	[["party_sequence",1]]	\N	\N
832	2020-09-02 07:24:24.586225	0	101	msg_party_code_unique	[["text","The code on party must be unique."]]	ir.message	party	f	[["text","The code on party must be unique."]]	\N	\N
833	2020-09-02 07:24:24.586225	0	102	msg_contact_mechanism_change_party	[["text","You cannot change the party of contact mechanism \\"%(contact)s\\"."]]	ir.message	party	f	[["text","You cannot change the party of contact mechanism \\"%(contact)s\\"."]]	\N	\N
834	2020-09-02 07:24:24.586225	0	103	msg_invalid_phone_number	[["text","The phone number \\"%(phone)s\\" for party \\"%(party)s\\" is not valid."]]	ir.message	party	f	[["text","The phone number \\"%(phone)s\\" for party \\"%(party)s\\" is not valid."]]	\N	\N
835	2020-09-02 07:24:24.586225	0	104	msg_invalid_code	[["text","The %(type)s \\"%(code)s\\" for party \\"%(party)s\\" is not valid."]]	ir.message	party	f	[["text","The %(type)s \\"%(code)s\\" for party \\"%(party)s\\" is not valid."]]	\N	\N
836	2020-09-02 07:24:24.586225	0	105	msg_vies_unavailable	[["text","The VIES service is unavailable, try again later."]]	ir.message	party	f	[["text","The VIES service is unavailable, try again later."]]	\N	\N
837	2020-09-02 07:24:24.586225	0	106	msg_different_name	[["text","Parties have different names: \\"%(source_name)s\\" vs \\"%(destination_name)s\\"."]]	ir.message	party	f	[["text","Parties have different names: \\"%(source_name)s\\" vs \\"%(destination_name)s\\"."]]	\N	\N
838	2020-09-02 07:24:24.586225	0	107	msg_different_tax_identifier	[["text","Parties have different tax identifiers: \\"%(source_code)s\\" vs \\"%(destination_code)s\\"."]]	ir.message	party	f	[["text","Parties have different tax identifiers: \\"%(source_code)s\\" vs \\"%(destination_code)s\\"."]]	\N	\N
839	2020-09-02 07:24:24.586225	0	108	msg_erase_active_party	[["text","Party \\"%(party)s\\" cannot be erased because they are still active."]]	ir.message	party	f	[["text","Party \\"%(party)s\\" cannot be erased because they are still active."]]	\N	\N
840	2020-09-02 07:24:24.586225	0	109	msg_address_change_party	[["text","You cannot change the party of address \\"%(address)s\\"."]]	ir.message	party	f	[["text","You cannot change the party of address \\"%(address)s\\"."]]	\N	\N
841	2020-09-02 07:24:24.586225	0	110	msg_invalid_format	[["text","Invalid format \\"%(format)s\\" with exception \\"%(exception)s\\"."]]	ir.message	party	f	[["text","Invalid format \\"%(format)s\\" with exception \\"%(exception)s\\"."]]	\N	\N
842	2020-09-02 07:24:24.586225	0	111	msg_category_name_unique	[["text","The name of party category must be unique by parent."]]	ir.message	party	f	[["text","The name of party category must be unique by parent."]]	\N	\N
843	2020-09-02 07:24:24.586225	0	112	msg_address_subdivision_country_code_unique	[["text","The country code on subdivision type must be unique."]]	ir.message	party	f	[["text","The country code on subdivision type must be unique."]]	\N	\N
844	2020-09-02 07:24:29.514826	0	4	group_company_admin	[["name","Company Administration"]]	res.group	company	f	[["name","Company Administration"]]	\N	\N
845	2020-09-02 07:24:29.514826	0	4	user_admin_company_admin	[["group",4],["user",1]]	res.user-res.group	company	f	[["group",4],["user",1]]	\N	\N
846	2020-09-02 07:24:29.514826	0	5	group_employee_admin	[["name","Employee Administration"]]	res.group	company	f	[["name","Employee Administration"]]	\N	\N
847	2020-09-02 07:24:29.514826	0	5	user_admin_employee_admin	[["group",5],["user",1]]	res.user-res.group	company	f	[["group",5],["user",1]]	\N	\N
848	2020-09-02 07:24:29.514826	0	12	company_icon	[["name","tryton-company"],["path","icons/tryton-company.svg"]]	ir.ui.icon	company	f	[["name","tryton-company"],["path","icons/tryton-company.svg"]]	\N	\N
849	2020-09-02 07:24:29.514826	0	61	menu_company	[["icon","tryton-company"],["name","Company"],["sequence",2]]	ir.ui.menu	company	f	[["icon","tryton-company"],["name","Company"],["sequence",2]]	\N	\N
850	2020-09-02 07:24:29.514826	0	45	menu_currency_group_company_admin	[["group",4],["menu",61]]	ir.ui.menu-res.group	company	f	[["group",4],["menu",61]]	\N	\N
851	2020-09-02 07:24:29.514826	0	46	menu_currency_group_employee_admin	[["group",5],["menu",61]]	ir.ui.menu-res.group	company	f	[["group",5],["menu",61]]	\N	\N
852	2020-09-02 07:24:29.514826	0	143	company_view_form	[["inherit",null],["model","company.company"],["name","company_form"],["type","form"]]	ir.ui.view	company	f	[["inherit",null],["model","company.company"],["name","company_form"],["type","form"]]	\N	\N
853	2020-09-02 07:24:29.514826	0	144	company_view_tree	[["field_childs","childs"],["model","company.company"],["name","company_tree"],["type","tree"]]	ir.ui.view	company	f	[["field_childs","childs"],["model","company.company"],["name","company_tree"],["type","tree"]]	\N	\N
854	2020-09-02 07:24:29.514826	0	145	company_view_list	[["model","company.company"],["name","company_list"],["priority",10],["type","tree"]]	ir.ui.view	company	f	[["model","company.company"],["name","company_list"],["priority",10],["type","tree"]]	\N	\N
855	2020-09-02 07:24:29.514826	0	70	act_company_tree	[["domain","[[\\"parent\\", \\"=\\", null]]"],["name","Companies"],["res_model","company.company"]]	ir.action.act_window	company	f	[["domain","[[\\"parent\\", \\"=\\", null]]"],["name","Companies"],["res_model","company.company"]]	\N	\N
856	2020-09-02 07:24:29.514826	0	88	act_company_tree_view1	[["act_window",70],["sequence",10],["view",144]]	ir.action.act_window.view	company	f	[["act_window",70],["sequence",10],["view",144]]	\N	\N
857	2020-09-02 07:24:29.514826	0	89	act_company_tree_view2	[["act_window",70],["sequence",20],["view",143]]	ir.action.act_window.view	company	f	[["act_window",70],["sequence",20],["view",143]]	\N	\N
858	2020-09-02 07:24:29.514826	0	62	menu_company_tree	[["action","ir.action.act_window,70"],["icon","tryton-tree"],["name","Companies"],["parent",61]]	ir.ui.menu	company	f	[["action","ir.action.act_window,70"],["icon","tryton-tree"],["name","Companies"],["parent",61]]	\N	\N
859	2020-09-02 07:24:29.514826	0	71	act_company_list	[["name","Companies"],["res_model","company.company"]]	ir.action.act_window	company	f	[["name","Companies"],["res_model","company.company"]]	\N	\N
860	2020-09-02 07:24:29.514826	0	90	act_company_list_view1	[["act_window",71],["sequence",10],["view",145]]	ir.action.act_window.view	company	f	[["act_window",71],["sequence",10],["view",145]]	\N	\N
861	2020-09-02 07:24:29.514826	0	91	act_company_list_view2	[["act_window",71],["sequence",20],["view",143]]	ir.action.act_window.view	company	f	[["act_window",71],["sequence",20],["view",143]]	\N	\N
862	2020-09-02 07:24:29.514826	0	63	menu_company_list	[["action","ir.action.act_window,71"],["icon","tryton-list"],["name","Companies"],["parent",62]]	ir.ui.menu	company	f	[["action","ir.action.act_window,71"],["icon","tryton-list"],["name","Companies"],["parent",62]]	\N	\N
863	2020-09-02 07:24:29.514826	0	78	access_company	[["model",102],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	company	f	[["model",102],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
864	2020-09-02 07:24:29.514826	0	79	access_company_admin	[["group",4],["model",102],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	company	f	[["group",4],["model",102],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
865	2020-09-02 07:24:29.514826	0	146	user_view_form	[["inherit",99],["model","res.user"],["name","user_form"]]	ir.ui.view	company	f	[["inherit",99],["model","res.user"],["name","user_form"]]	\N	\N
866	2020-09-02 07:24:29.514826	0	147	user_view_form_preferences	[["inherit",100],["model","res.user"],["name","user_form_preferences"]]	ir.ui.view	company	f	[["inherit",100],["model","res.user"],["name","user_form_preferences"]]	\N	\N
867	2020-09-02 07:24:29.514826	0	148	company_config_start_view_form	[["model","company.company.config.start"],["name","company_config_start_form"],["type","form"]]	ir.ui.view	company	f	[["model","company.company.config.start"],["name","company_config_start_form"],["type","form"]]	\N	\N
868	2020-09-02 07:24:29.514826	0	72	act_company_config	[["name","Configure Company"],["window",true],["wiz_name","company.company.config"]]	ir.action.wizard	company	f	[["name","Configure Company"],["window",true],["wiz_name","company.company.config"]]	\N	\N
869	2020-09-02 07:24:29.514826	0	4	config_wizard_item_company	[["action",72]]	ir.module.config_wizard.item	company	f	[["action",72]]	\N	\N
870	2020-09-02 07:24:29.514826	0	149	employee_view_form	[["inherit",null],["model","company.employee"],["name","employee_form"],["priority",10],["type","form"]]	ir.ui.view	company	f	[["inherit",null],["model","company.employee"],["name","employee_form"],["priority",10],["type","form"]]	\N	\N
873	2020-09-02 07:24:29.514826	0	92	act_employee_form_view1	[["act_window",73],["sequence",10],["view",150]]	ir.action.act_window.view	company	f	[["act_window",73],["sequence",10],["view",150]]	\N	\N
874	2020-09-02 07:24:29.514826	0	93	act_employee_form_view2	[["act_window",73],["sequence",20],["view",149]]	ir.action.act_window.view	company	f	[["act_window",73],["sequence",20],["view",149]]	\N	\N
875	2020-09-02 07:24:29.514826	0	64	menu_employee_form	[["action","ir.action.act_window,73"],["icon","tryton-list"],["name","Employees"],["parent",61]]	ir.ui.menu	company	f	[["action","ir.action.act_window,73"],["icon","tryton-list"],["name","Employees"],["parent",61]]	\N	\N
876	2020-09-02 07:24:29.514826	0	74	act_employee_subordinates	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"supervisor\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"supervisor\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name","Supervised by"],["res_model","company.employee"]]	ir.action.act_window	company	f	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"supervisor\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"supervisor\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name","Supervised by"],["res_model","company.employee"]]	\N	\N
877	2020-09-02 07:24:29.514826	0	94	act_employee_subordinates_view1	[["act_window",74],["sequence",10],["view",150]]	ir.action.act_window.view	company	f	[["act_window",74],["sequence",10],["view",150]]	\N	\N
878	2020-09-02 07:24:29.514826	0	95	act_employee_subordinates_view2	[["act_window",74],["sequence",20],["view",149]]	ir.action.act_window.view	company	f	[["act_window",74],["sequence",20],["view",149]]	\N	\N
879	2020-09-02 07:24:29.514826	0	70	act_employee_subordinates_keyword1	[["action",74],["keyword","form_relate"],["model","company.employee,-1"]]	ir.action.keyword	company	f	[["action",74],["keyword","form_relate"],["model","company.employee,-1"]]	\N	\N
880	2020-09-02 07:24:29.514826	0	80	access_employee	[["model",103],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	company	f	[["model",103],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
881	2020-09-02 07:24:29.514826	0	81	access_employee_admin	[["group",5],["model",103],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	company	f	[["group",5],["model",103],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
882	2020-09-02 07:24:29.514826	0	75	report_letter	[["model","party.party"],["name","Letter"],["report","company/letter.fodt"],["report_name","party.letter"]]	ir.action.report	company	f	[["model","party.party"],["name","Letter"],["report","company/letter.fodt"],["report_name","party.letter"]]	\N	\N
883	2020-09-02 07:24:29.514826	0	71	report_letter_party	[["action",75],["keyword","form_print"],["model","party.party,-1"]]	ir.action.keyword	company	f	[["action",75],["keyword","form_print"],["model","party.party,-1"]]	\N	\N
884	2020-09-02 07:24:29.514826	0	151	sequence_view_form	[["inherit",51],["model","ir.sequence"],["name","sequence_form"]]	ir.ui.view	company	f	[["inherit",51],["model","ir.sequence"],["name","sequence_form"]]	\N	\N
885	2020-09-02 07:24:29.514826	0	152	sequence_view_tree	[["inherit",52],["model","ir.sequence"],["name","sequence_tree"]]	ir.ui.view	company	f	[["inherit",52],["model","ir.sequence"],["name","sequence_tree"]]	\N	\N
886	2020-09-02 07:24:29.514826	0	24	rule_group_sequence	[["global_p",true],["model",11],["name","User in company"]]	ir.rule.group	company	f	[["global_p",true],["model",11],["name","User in company"]]	\N	\N
887	2020-09-02 07:24:29.514826	0	34	rule_sequence1	[["domain","[[\\"company\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": null, \\"k\\": \\"company\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",24]]	ir.rule	company	f	[["domain","[[\\"company\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": null, \\"k\\": \\"company\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",24]]	\N	\N
888	2020-09-02 07:24:29.514826	0	35	rule_sequence2	[["domain","[[\\"company\\", \\"=\\", null]]"],["rule_group",24]]	ir.rule	company	f	[["domain","[[\\"company\\", \\"=\\", null]]"],["rule_group",24]]	\N	\N
889	2020-09-02 07:24:29.514826	0	25	rule_group_sequence_strict	[["global_p",true],["model",12],["name","User in company"]]	ir.rule.group	company	f	[["global_p",true],["model",12],["name","User in company"]]	\N	\N
890	2020-09-02 07:24:29.514826	0	36	rule_sequence_strict1	[["domain","[[\\"company\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": null, \\"k\\": \\"company\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",25]]	ir.rule	company	f	[["domain","[[\\"company\\", \\"=\\", {\\"__class__\\": \\"Get\\", \\"d\\": null, \\"k\\": \\"company\\", \\"v\\": {\\"__class__\\": \\"Eval\\", \\"d\\": {}, \\"v\\": \\"user\\"}}]]"],["rule_group",25]]	\N	\N
891	2020-09-02 07:24:29.514826	0	37	rule_sequence_strict2	[["domain","[[\\"company\\", \\"=\\", null]]"],["rule_group",25]]	ir.rule	company	f	[["domain","[[\\"company\\", \\"=\\", null]]"],["rule_group",25]]	\N	\N
892	2020-09-02 07:24:29.514826	0	153	cron_view_form	[["inherit",62],["model","ir.cron"],["name","cron_form"]]	ir.ui.view	company	f	[["inherit",62],["model","ir.cron"],["name","cron_form"]]	\N	\N
893	2020-09-02 07:24:32.977941	0	6	group_product_admin	[["name","Product Administration"]]	res.group	product	f	[["name","Product Administration"]]	\N	\N
894	2020-09-02 07:24:32.977941	0	6	user_admin_group_product_admin	[["group",6],["user",1]]	res.user-res.group	product	f	[["group",6],["user",1]]	\N	\N
895	2020-09-02 07:24:32.977941	0	13	product_icon	[["name","tryton-product"],["path","icons/tryton-product.svg"]]	ir.ui.icon	product	f	[["name","tryton-product"],["path","icons/tryton-product.svg"]]	\N	\N
896	2020-09-02 07:24:32.977941	0	65	menu_main_product	[["icon","tryton-product"],["name","Product"],["sequence",1]]	ir.ui.menu	product	f	[["icon","tryton-product"],["name","Product"],["sequence",1]]	\N	\N
897	2020-09-02 07:24:32.977941	0	66	menu_configuration	[["icon","tryton-settings"],["name","Configuration"],["parent",65],["sequence",0]]	ir.ui.menu	product	f	[["icon","tryton-settings"],["name","Configuration"],["parent",65],["sequence",0]]	\N	\N
898	2020-09-02 07:24:32.977941	0	47	menu_product_group_product_admin	[["group",6],["menu",66]]	ir.ui.menu-res.group	product	f	[["group",6],["menu",66]]	\N	\N
899	2020-09-02 07:24:32.977941	0	154	template_view_tree	[["model","product.template"],["name","template_tree"],["type","tree"]]	ir.ui.view	product	f	[["model","product.template"],["name","template_tree"],["type","tree"]]	\N	\N
900	2020-09-02 07:24:32.977941	0	155	template_view_form	[["model","product.template"],["name","template_form"],["type","form"]]	ir.ui.view	product	f	[["model","product.template"],["name","template_form"],["type","form"]]	\N	\N
901	2020-09-02 07:24:32.977941	0	76	act_template_form	[["name","Products"],["res_model","product.template"]]	ir.action.act_window	product	f	[["name","Products"],["res_model","product.template"]]	\N	\N
902	2020-09-02 07:24:32.977941	0	96	act_template_list_view	[["act_window",76],["sequence",10],["view",154]]	ir.action.act_window.view	product	f	[["act_window",76],["sequence",10],["view",154]]	\N	\N
903	2020-09-02 07:24:32.977941	0	97	act_template_form_view	[["act_window",76],["sequence",20],["view",155]]	ir.action.act_window.view	product	f	[["act_window",76],["sequence",20],["view",155]]	\N	\N
904	2020-09-02 07:24:32.977941	0	67	menu_template	[["action","ir.action.act_window,76"],["icon","tryton-list"],["name","Products"],["parent",65],["sequence",1]]	ir.ui.menu	product	f	[["action","ir.action.act_window,76"],["icon","tryton-list"],["name","Products"],["parent",65],["sequence",1]]	\N	\N
905	2020-09-02 07:24:32.977941	0	77	act_template_by_category	[["context","{\\"categories\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}"],["domain","[[\\"categories_all\\", \\"child_of\\", [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}], \\"parent\\"]]"],["name","Product by Category"],["res_model","product.template"]]	ir.action.act_window	product	f	[["context","{\\"categories\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}"],["domain","[[\\"categories_all\\", \\"child_of\\", [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}], \\"parent\\"]]"],["name","Product by Category"],["res_model","product.template"]]	\N	\N
906	2020-09-02 07:24:32.977941	0	98	act_template_by_category_view1	[["act_window",77],["sequence",10],["view",154]]	ir.action.act_window.view	product	f	[["act_window",77],["sequence",10],["view",154]]	\N	\N
907	2020-09-02 07:24:32.977941	0	99	act_template_by_category_view2	[["act_window",77],["sequence",20],["view",155]]	ir.action.act_window.view	product	f	[["act_window",77],["sequence",20],["view",155]]	\N	\N
908	2020-09-02 07:24:32.977941	0	73	act_template_by_category_keyword1	[["action",77],["keyword","tree_open"],["model","product.category,-1"]]	ir.action.keyword	product	f	[["action",77],["keyword","tree_open"],["model","product.category,-1"]]	\N	\N
909	2020-09-02 07:24:32.977941	0	156	product_view_tree	[["inherit",154],["model","product.product"],["name","product_tree"],["priority",10],["type",null]]	ir.ui.view	product	f	[["inherit",154],["model","product.product"],["name","product_tree"],["priority",10],["type",null]]	\N	\N
910	2020-09-02 07:24:32.977941	0	157	product_view_tree_simple	[["model","product.product"],["name","product_tree_simple"],["priority",20],["type","tree"]]	ir.ui.view	product	f	[["model","product.product"],["name","product_tree_simple"],["priority",20],["type","tree"]]	\N	\N
911	2020-09-02 07:24:32.977941	0	158	product_view_form	[["inherit",155],["model","product.product"],["name","product_form"],["priority",10],["type",null]]	ir.ui.view	product	f	[["inherit",155],["model","product.product"],["name","product_form"],["priority",10],["type",null]]	\N	\N
912	2020-09-02 07:24:32.977941	0	159	product_view_form_simple	[["model","product.product"],["name","product_form_simple"],["priority",20],["type","form"]]	ir.ui.view	product	f	[["model","product.product"],["name","product_form_simple"],["priority",20],["type","form"]]	\N	\N
913	2020-09-02 07:24:32.977941	0	78	act_product_form	[["name","Variants"],["res_model","product.product"]]	ir.action.act_window	product	f	[["name","Variants"],["res_model","product.product"]]	\N	\N
914	2020-09-02 07:24:32.977941	0	100	act_product_list_view	[["act_window",78],["sequence",10],["view",156]]	ir.action.act_window.view	product	f	[["act_window",78],["sequence",10],["view",156]]	\N	\N
915	2020-09-02 07:24:32.977941	0	101	act_product_form_view	[["act_window",78],["sequence",20],["view",158]]	ir.action.act_window.view	product	f	[["act_window",78],["sequence",20],["view",158]]	\N	\N
916	2020-09-02 07:24:32.977941	0	68	menu_product	[["action","ir.action.act_window,78"],["icon","tryton-list"],["name","Variants"],["parent",67],["sequence",1]]	ir.ui.menu	product	f	[["action","ir.action.act_window,78"],["icon","tryton-list"],["name","Variants"],["parent",67],["sequence",1]]	\N	\N
917	2020-09-02 07:24:32.977941	0	82	access_product	[["model",111],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	product	f	[["model",111],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
918	2020-09-02 07:24:32.977941	0	83	access_product_admin	[["group",6],["model",111],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	product	f	[["group",6],["model",111],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
919	2020-09-02 07:24:32.977941	0	84	access_product_template	[["model",110],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	product	f	[["model",110],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
920	2020-09-02 07:24:32.977941	0	85	access_product_template_admin	[["group",6],["model",110],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	product	f	[["group",6],["model",110],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
921	2020-09-02 07:24:32.977941	0	79	act_product_from_template	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"template\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"template\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name","Variants"],["res_model","product.product"]]	ir.action.act_window	product	f	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"template\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"template\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name","Variants"],["res_model","product.product"]]	\N	\N
922	2020-09-02 07:24:32.977941	0	102	act_product_from_template_list_view	[["act_window",79],["sequence",10],["view",156]]	ir.action.act_window.view	product	f	[["act_window",79],["sequence",10],["view",156]]	\N	\N
923	2020-09-02 07:24:32.977941	0	103	act_productfrom_template_form_view	[["act_window",79],["sequence",20],["view",158]]	ir.action.act_window.view	product	f	[["act_window",79],["sequence",20],["view",158]]	\N	\N
924	2020-09-02 07:24:32.977941	0	75	act_product_from_template_keyword1	[["action",79],["keyword","form_relate"],["model","product.template,-1"]]	ir.action.keyword	product	f	[["action",79],["keyword","form_relate"],["model","product.template,-1"]]	\N	\N
925	2020-09-02 07:24:32.977941	0	160	identifier_view_form	[["model","product.identifier"],["name","identifier_form"],["type","form"]]	ir.ui.view	product	f	[["model","product.identifier"],["name","identifier_form"],["type","form"]]	\N	\N
926	2020-09-02 07:24:32.977941	0	161	identifier_view_list	[["model","product.identifier"],["name","identifier_list"],["priority",10],["type","tree"]]	ir.ui.view	product	f	[["model","product.identifier"],["name","identifier_list"],["priority",10],["type","tree"]]	\N	\N
927	2020-09-02 07:24:32.977941	0	162	identifier_view_list_sequence	[["model","product.identifier"],["name","identifier_list_sequence"],["priority",20],["type","tree"]]	ir.ui.view	product	f	[["model","product.identifier"],["name","identifier_list_sequence"],["priority",20],["type","tree"]]	\N	\N
928	2020-09-02 07:24:32.977941	0	86	access_product_identifier	[["model",112],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	product	f	[["model",112],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
929	2020-09-02 07:24:32.977941	0	87	access_product_identifier_admin	[["group",6],["model",112],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	product	f	[["group",6],["model",112],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
930	2020-09-02 07:24:32.977941	0	2	sequence_type_product	[["code","product.product"],["name","Product"]]	ir.sequence.type	product	f	[["code","product.product"],["name","Product"]]	\N	\N
931	2020-09-02 07:24:32.977941	0	3	sequence_type_product_group_admin	[["group",1],["sequence_type",2]]	ir.sequence.type-res.group	product	f	[["group",1],["sequence_type",2]]	\N	\N
932	2020-09-02 07:24:32.977941	0	4	sequence_type_product_group_product_admin	[["group",6],["sequence_type",2]]	ir.sequence.type-res.group	product	f	[["group",6],["sequence_type",2]]	\N	\N
933	2020-09-02 07:24:32.977941	0	163	category_view_list	[["model","product.category"],["name","category_list"],["priority",10],["type","tree"]]	ir.ui.view	product	f	[["model","product.category"],["name","category_list"],["priority",10],["type","tree"]]	\N	\N
934	2020-09-02 07:24:32.977941	0	164	category_view_tree	[["field_childs","childs"],["model","product.category"],["name","category_tree"],["priority",20],["type","tree"]]	ir.ui.view	product	f	[["field_childs","childs"],["model","product.category"],["name","category_tree"],["priority",20],["type","tree"]]	\N	\N
935	2020-09-02 07:24:32.977941	0	165	category_view_form	[["model","product.category"],["name","category_form"],["type","form"]]	ir.ui.view	product	f	[["model","product.category"],["name","category_form"],["type","form"]]	\N	\N
936	2020-09-02 07:24:32.977941	0	80	act_category_tree	[["domain","[[\\"parent\\", \\"=\\", null]]"],["name","Categories"],["res_model","product.category"]]	ir.action.act_window	product	f	[["domain","[[\\"parent\\", \\"=\\", null]]"],["name","Categories"],["res_model","product.category"]]	\N	\N
937	2020-09-02 07:24:32.977941	0	104	act_category_tree_view1	[["act_window",80],["sequence",10],["view",164]]	ir.action.act_window.view	product	f	[["act_window",80],["sequence",10],["view",164]]	\N	\N
938	2020-09-02 07:24:32.977941	0	105	act_category_tree_view2	[["act_window",80],["sequence",20],["view",165]]	ir.action.act_window.view	product	f	[["act_window",80],["sequence",20],["view",165]]	\N	\N
939	2020-09-02 07:24:32.977941	0	69	menu_category_tree	[["action","ir.action.act_window,80"],["icon","tryton-tree"],["name","Categories"],["parent",65],["sequence",2]]	ir.ui.menu	product	f	[["action","ir.action.act_window,80"],["icon","tryton-tree"],["name","Categories"],["parent",65],["sequence",2]]	\N	\N
940	2020-09-02 07:24:32.977941	0	81	act_category_list	[["name","Categories"],["res_model","product.category"]]	ir.action.act_window	product	f	[["name","Categories"],["res_model","product.category"]]	\N	\N
941	2020-09-02 07:24:32.977941	0	106	act_category_list_view1	[["act_window",81],["sequence",10],["view",163]]	ir.action.act_window.view	product	f	[["act_window",81],["sequence",10],["view",163]]	\N	\N
942	2020-09-02 07:24:32.977941	0	107	act_category_list_view2	[["act_window",81],["sequence",20],["view",165]]	ir.action.act_window.view	product	f	[["act_window",81],["sequence",20],["view",165]]	\N	\N
943	2020-09-02 07:24:32.977941	0	70	menu_category_list	[["action","ir.action.act_window,81"],["icon","tryton-list"],["name","Categories"],["parent",69],["sequence",10]]	ir.ui.menu	product	f	[["action","ir.action.act_window,81"],["icon","tryton-list"],["name","Categories"],["parent",69],["sequence",10]]	\N	\N
944	2020-09-02 07:24:32.977941	0	88	access_product_category	[["model",109],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	product	f	[["model",109],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
945	2020-09-02 07:24:32.977941	0	89	access_product_category_admin	[["group",6],["model",109],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	product	f	[["group",6],["model",109],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
946	2020-09-02 07:24:32.977941	0	166	uom_view_tree	[["model","product.uom"],["name","uom_tree"],["type","tree"]]	ir.ui.view	product	f	[["model","product.uom"],["name","uom_tree"],["type","tree"]]	\N	\N
947	2020-09-02 07:24:32.977941	0	167	uom_view_form	[["model","product.uom"],["name","uom_form"],["type","form"]]	ir.ui.view	product	f	[["model","product.uom"],["name","uom_form"],["type","form"]]	\N	\N
948	2020-09-02 07:24:32.977941	0	82	act_uom_form	[["name","Units of Measure"],["res_model","product.uom"]]	ir.action.act_window	product	f	[["name","Units of Measure"],["res_model","product.uom"]]	\N	\N
949	2020-09-02 07:24:32.977941	0	108	act_uom_form_view1	[["act_window",82],["sequence",10],["view",166]]	ir.action.act_window.view	product	f	[["act_window",82],["sequence",10],["view",166]]	\N	\N
950	2020-09-02 07:24:32.977941	0	109	act_uom_form_view2	[["act_window",82],["sequence",20],["view",167]]	ir.action.act_window.view	product	f	[["act_window",82],["sequence",20],["view",167]]	\N	\N
951	2020-09-02 07:24:32.977941	0	71	menu_uom_form	[["action","ir.action.act_window,82"],["icon","tryton-list"],["name","Units of Measure"],["parent",65],["sequence",3]]	ir.ui.menu	product	f	[["action","ir.action.act_window,82"],["icon","tryton-list"],["name","Units of Measure"],["parent",65],["sequence",3]]	\N	\N
952	2020-09-02 07:24:32.977941	0	168	uom_category_view_tree	[["model","product.uom.category"],["name","uom_category_tree"],["type","tree"]]	ir.ui.view	product	f	[["model","product.uom.category"],["name","uom_category_tree"],["type","tree"]]	\N	\N
953	2020-09-02 07:24:32.977941	0	169	uom_category_view_form	[["model","product.uom.category"],["name","uom_category_form"],["type","form"]]	ir.ui.view	product	f	[["model","product.uom.category"],["name","uom_category_form"],["type","form"]]	\N	\N
954	2020-09-02 07:24:32.977941	0	83	act_uom_category_form	[["name","Categories of Unit of Measure"],["res_model","product.uom.category"]]	ir.action.act_window	product	f	[["name","Categories of Unit of Measure"],["res_model","product.uom.category"]]	\N	\N
955	2020-09-02 07:24:32.977941	0	110	act_uom_category_form_view1	[["act_window",83],["sequence",10],["view",168]]	ir.action.act_window.view	product	f	[["act_window",83],["sequence",10],["view",168]]	\N	\N
956	2020-09-02 07:24:32.977941	0	111	act_uom_category_form_view2	[["act_window",83],["sequence",20],["view",169]]	ir.action.act_window.view	product	f	[["act_window",83],["sequence",20],["view",169]]	\N	\N
989	2020-09-02 07:24:32.977941	0	26	uom_square_meter	[["category",6],["factor",1.0],["name","Square meter"],["rate",1.0],["symbol","m\\u00b2"]]	product.uom	product	f	[["category",6],["factor",1.0],["name","Square meter"],["rate",1.0],["symbol","m\\u00b2"]]	\N	\N
957	2020-09-02 07:24:32.977941	0	72	menu_uom_category_form	[["action","ir.action.act_window,83"],["icon","tryton-list"],["name","Categories"],["parent",71],["sequence",2]]	ir.ui.menu	product	f	[["action","ir.action.act_window,83"],["icon","tryton-list"],["name","Categories"],["parent",71],["sequence",2]]	\N	\N
958	2020-09-02 07:24:32.977941	0	1	uom_cat_unit	[["name","Units"]]	product.uom.category	product	f	[["name","Units"]]	\N	\N
959	2020-09-02 07:24:32.977941	0	1	uom_unit	[["category",1],["digits",0],["factor",1.0],["name","Unit"],["rate",1.0],["rounding",1.0],["symbol","u"]]	product.uom	product	f	[["category",1],["digits",0],["factor",1.0],["name","Unit"],["rate",1.0],["rounding",1.0],["symbol","u"]]	\N	\N
960	2020-09-02 07:24:32.977941	0	2	uom_cat_weight	[["name","Weight"]]	product.uom.category	product	f	[["name","Weight"]]	\N	\N
961	2020-09-02 07:24:32.977941	0	2	uom_kilogram	[["category",2],["factor",1.0],["name","Kilogram"],["rate",1.0],["symbol","kg"]]	product.uom	product	f	[["category",2],["factor",1.0],["name","Kilogram"],["rate",1.0],["symbol","kg"]]	\N	\N
962	2020-09-02 07:24:32.977941	0	3	uom_gram	[["category",2],["factor",0.001],["name","Gram"],["rate",1000.0],["symbol","g"]]	product.uom	product	f	[["category",2],["factor",0.001],["name","Gram"],["rate",1000.0],["symbol","g"]]	\N	\N
963	2020-09-02 07:24:32.977941	0	4	uom_carat	[["category",2],["factor",0.0002],["name","Carat"],["rate",5000.0],["symbol","c"]]	product.uom	product	f	[["category",2],["factor",0.0002],["name","Carat"],["rate",5000.0],["symbol","c"]]	\N	\N
964	2020-09-02 07:24:32.977941	0	5	uom_pound	[["category",2],["factor",0.45359237],["name","Pound"],["rate",2.204622621849],["symbol","lb"]]	product.uom	product	f	[["category",2],["factor",0.45359237],["name","Pound"],["rate",2.204622621849],["symbol","lb"]]	\N	\N
965	2020-09-02 07:24:32.977941	0	6	uom_ounce	[["category",2],["factor",0.028349523125],["name","Ounce"],["rate",35.27396194958],["symbol","oz"]]	product.uom	product	f	[["category",2],["factor",0.028349523125],["name","Ounce"],["rate",35.27396194958],["symbol","oz"]]	\N	\N
966	2020-09-02 07:24:32.977941	0	3	uom_cat_time	[["name","Time"]]	product.uom.category	product	f	[["name","Time"]]	\N	\N
967	2020-09-02 07:24:32.977941	0	7	uom_second	[["category",3],["factor",0.000277777778],["name","Second"],["rate",3600.0],["symbol","s"]]	product.uom	product	f	[["category",3],["factor",0.000277777778],["name","Second"],["rate",3600.0],["symbol","s"]]	\N	\N
968	2020-09-02 07:24:32.977941	0	8	uom_minute	[["category",3],["factor",0.016666666667],["name","Minute"],["rate",60.0],["symbol","min"]]	product.uom	product	f	[["category",3],["factor",0.016666666667],["name","Minute"],["rate",60.0],["symbol","min"]]	\N	\N
969	2020-09-02 07:24:32.977941	0	9	uom_hour	[["category",3],["factor",1.0],["name","Hour"],["rate",1.0],["symbol","h"]]	product.uom	product	f	[["category",3],["factor",1.0],["name","Hour"],["rate",1.0],["symbol","h"]]	\N	\N
970	2020-09-02 07:24:32.977941	0	10	uom_work_day	[["category",3],["factor",8.0],["name","Work Day"],["rate",0.125],["symbol","wd"]]	product.uom	product	f	[["category",3],["factor",8.0],["name","Work Day"],["rate",0.125],["symbol","wd"]]	\N	\N
971	2020-09-02 07:24:32.977941	0	11	uom_day	[["category",3],["factor",24.0],["name","Day"],["rate",0.041666666667],["symbol","d"]]	product.uom	product	f	[["category",3],["factor",24.0],["name","Day"],["rate",0.041666666667],["symbol","d"]]	\N	\N
972	2020-09-02 07:24:32.977941	0	4	uom_cat_length	[["name","Length"]]	product.uom.category	product	f	[["name","Length"]]	\N	\N
973	2020-09-02 07:24:32.977941	0	12	uom_meter	[["category",4],["factor",1.0],["name","Meter"],["rate",1.0],["symbol","m"]]	product.uom	product	f	[["category",4],["factor",1.0],["name","Meter"],["rate",1.0],["symbol","m"]]	\N	\N
974	2020-09-02 07:24:32.977941	0	13	uom_kilometer	[["category",4],["factor",1000.0],["name","Kilometer"],["rate",0.001],["symbol","km"]]	product.uom	product	f	[["category",4],["factor",1000.0],["name","Kilometer"],["rate",0.001],["symbol","km"]]	\N	\N
975	2020-09-02 07:24:32.977941	0	14	uom_centimeter	[["category",4],["factor",0.01],["name","centimeter"],["rate",100.0],["symbol","cm"]]	product.uom	product	f	[["category",4],["factor",0.01],["name","centimeter"],["rate",100.0],["symbol","cm"]]	\N	\N
976	2020-09-02 07:24:32.977941	0	15	uom_millimeter	[["category",4],["factor",0.001],["name","Millimeter"],["rate",1000.0],["symbol","mm"]]	product.uom	product	f	[["category",4],["factor",0.001],["name","Millimeter"],["rate",1000.0],["symbol","mm"]]	\N	\N
977	2020-09-02 07:24:32.977941	0	16	uom_foot	[["category",4],["factor",0.3048],["name","Foot"],["rate",3.280839895013],["symbol","ft"]]	product.uom	product	f	[["category",4],["factor",0.3048],["name","Foot"],["rate",3.280839895013],["symbol","ft"]]	\N	\N
978	2020-09-02 07:24:32.977941	0	17	uom_yard	[["category",4],["factor",0.9144],["name","Yard"],["rate",1.093613298338],["symbol","yd"]]	product.uom	product	f	[["category",4],["factor",0.9144],["name","Yard"],["rate",1.093613298338],["symbol","yd"]]	\N	\N
979	2020-09-02 07:24:32.977941	0	18	uom_inch	[["category",4],["factor",0.0254],["name","Inch"],["rate",39.370078740157],["symbol","in"]]	product.uom	product	f	[["category",4],["factor",0.0254],["name","Inch"],["rate",39.370078740157],["symbol","in"]]	\N	\N
980	2020-09-02 07:24:32.977941	0	19	uom_mile	[["category",4],["factor",1609.344],["name","Mile"],["rate",0.000621371192],["symbol","mi"]]	product.uom	product	f	[["category",4],["factor",1609.344],["name","Mile"],["rate",0.000621371192],["symbol","mi"]]	\N	\N
981	2020-09-02 07:24:32.977941	0	5	uom_cat_volume	[["name","Volume"]]	product.uom.category	product	f	[["name","Volume"]]	\N	\N
982	2020-09-02 07:24:32.977941	0	20	uom_cubic_meter	[["category",5],["factor",1000.0],["name","Cubic meter"],["rate",0.001],["symbol","m\\u00b3"]]	product.uom	product	f	[["category",5],["factor",1000.0],["name","Cubic meter"],["rate",0.001],["symbol","m\\u00b3"]]	\N	\N
983	2020-09-02 07:24:32.977941	0	21	uom_liter	[["category",5],["factor",1.0],["name","Liter"],["rate",1.0],["symbol","l"]]	product.uom	product	f	[["category",5],["factor",1.0],["name","Liter"],["rate",1.0],["symbol","l"]]	\N	\N
984	2020-09-02 07:24:32.977941	0	22	uom_cubic_centimeter	[["category",5],["factor",0.001],["name","Cubic centimeter"],["rate",1000.0],["symbol","cm\\u00b3"]]	product.uom	product	f	[["category",5],["factor",0.001],["name","Cubic centimeter"],["rate",1000.0],["symbol","cm\\u00b3"]]	\N	\N
985	2020-09-02 07:24:32.977941	0	23	uom_cubic_inch	[["category",5],["factor",0.016387064],["name","Cubic inch"],["rate",61.023744094732],["symbol","in\\u00b3"]]	product.uom	product	f	[["category",5],["factor",0.016387064],["name","Cubic inch"],["rate",61.023744094732],["symbol","in\\u00b3"]]	\N	\N
986	2020-09-02 07:24:32.977941	0	24	uom_cubic_foot	[["category",5],["factor",28.316846592],["name","Cubic foot"],["rate",0.035314666721],["symbol","ft\\u00b3"]]	product.uom	product	f	[["category",5],["factor",28.316846592],["name","Cubic foot"],["rate",0.035314666721],["symbol","ft\\u00b3"]]	\N	\N
987	2020-09-02 07:24:32.977941	0	25	uom_gallon	[["category",5],["factor",3.785411784],["name","Gallon"],["rate",0.264172052358],["symbol","gal"]]	product.uom	product	f	[["category",5],["factor",3.785411784],["name","Gallon"],["rate",0.264172052358],["symbol","gal"]]	\N	\N
988	2020-09-02 07:24:32.977941	0	6	uom_cat_surface	[["name","Surface"]]	product.uom.category	product	f	[["name","Surface"]]	\N	\N
990	2020-09-02 07:24:32.977941	0	27	uom_square_centimeter	[["category",6],["factor",0.0001],["name","Square centimeter"],["rate",10000.0],["symbol","cm\\u00b2"]]	product.uom	product	f	[["category",6],["factor",0.0001],["name","Square centimeter"],["rate",10000.0],["symbol","cm\\u00b2"]]	\N	\N
991	2020-09-02 07:24:32.977941	0	28	uom_are	[["category",6],["factor",100.0],["name","Are"],["rate",0.01],["symbol","a"]]	product.uom	product	f	[["category",6],["factor",100.0],["name","Are"],["rate",0.01],["symbol","a"]]	\N	\N
992	2020-09-02 07:24:32.977941	0	29	uom_hectare	[["category",6],["factor",10000.0],["name","Hectare"],["rate",0.0001],["symbol","ha"]]	product.uom	product	f	[["category",6],["factor",10000.0],["name","Hectare"],["rate",0.0001],["symbol","ha"]]	\N	\N
993	2020-09-02 07:24:32.977941	0	30	uom_square_inch	[["category",6],["factor",0.00064516],["name","Square inch"],["rate",1550.0031000062],["symbol","in\\u00b2"]]	product.uom	product	f	[["category",6],["factor",0.00064516],["name","Square inch"],["rate",1550.0031000062],["symbol","in\\u00b2"]]	\N	\N
994	2020-09-02 07:24:32.977941	0	31	uom_square_foot	[["category",6],["factor",0.09290304],["name","Square foot"],["rate",10.76391041671],["symbol","ft\\u00b2"]]	product.uom	product	f	[["category",6],["factor",0.09290304],["name","Square foot"],["rate",10.76391041671],["symbol","ft\\u00b2"]]	\N	\N
995	2020-09-02 07:24:32.977941	0	32	uom_square_yard	[["category",6],["factor",0.83612736],["name","Square yard"],["rate",1.195990046301],["symbol","yd\\u00b2"]]	product.uom	product	f	[["category",6],["factor",0.83612736],["name","Square yard"],["rate",1.195990046301],["symbol","yd\\u00b2"]]	\N	\N
996	2020-09-02 07:24:32.977941	0	90	access_uom	[["model",108],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	product	f	[["model",108],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
997	2020-09-02 07:24:32.977941	0	91	access_uom_admin	[["group",6],["model",108],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	product	f	[["group",6],["model",108],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
998	2020-09-02 07:24:32.977941	0	92	access_uom_category	[["model",107],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	product	f	[["model",107],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
999	2020-09-02 07:24:32.977941	0	93	access_uom_category_admin	[["group",6],["model",107],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	product	f	[["group",6],["model",107],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
1000	2020-09-02 07:24:32.977941	0	170	product_configuration_view_form	[["model","product.configuration"],["name","configuration_form"],["type","form"]]	ir.ui.view	product	f	[["model","product.configuration"],["name","configuration_form"],["type","form"]]	\N	\N
1001	2020-09-02 07:24:32.977941	0	84	act_product_configuration_form	[["name","Product Configuration"],["res_model","product.configuration"]]	ir.action.act_window	product	f	[["name","Product Configuration"],["res_model","product.configuration"]]	\N	\N
1002	2020-09-02 07:24:32.977941	0	112	act_product_configuration_form_view1	[["act_window",84],["sequence",10],["view",170]]	ir.action.act_window.view	product	f	[["act_window",84],["sequence",10],["view",170]]	\N	\N
1003	2020-09-02 07:24:32.977941	0	73	menu_product_configuration	[["action","ir.action.act_window,84"],["icon","tryton-list"],["name","Product Configuration"],["parent",66],["sequence",0]]	ir.ui.menu	product	f	[["action","ir.action.act_window,84"],["icon","tryton-list"],["name","Product Configuration"],["parent",66],["sequence",0]]	\N	\N
1004	2020-09-02 07:24:32.977941	0	48	menu_product_configuration_group_product_admin	[["group",6],["menu",73]]	ir.ui.menu-res.group	product	f	[["group",6],["menu",73]]	\N	\N
1005	2020-09-02 07:24:32.977941	0	94	access_product_configuration	[["model",118],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	ir.model.access	product	f	[["model",118],["perm_create",false],["perm_delete",false],["perm_read",true],["perm_write",false]]	\N	\N
1006	2020-09-02 07:24:32.977941	0	95	access_product_configuration_product_admin	[["group",6],["model",118],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	product	f	[["group",6],["model",118],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
1007	2020-09-02 07:24:32.977941	0	1	cost_price_method	[["default_cost_price_method","fixed"]]	product.configuration.default_cost_price_method	product	t	[["default_cost_price_method","fixed"]]	\N	\N
1008	2020-09-02 07:24:32.977941	0	113	msg_uom_modify_factor	[["text","You cannot modify the factor of UOM \\"%(uom)s\\"."]]	ir.message	product	f	[["text","You cannot modify the factor of UOM \\"%(uom)s\\"."]]	\N	\N
1009	2020-09-02 07:24:32.977941	0	114	msg_uom_modify_rate	[["text","You cannot modify the rate of UOM \\"%(uom)s\\"."]]	ir.message	product	f	[["text","You cannot modify the rate of UOM \\"%(uom)s\\"."]]	\N	\N
1010	2020-09-02 07:24:32.977941	0	115	msg_uom_modify_category	[["text","You cannot modify the category of UOM \\"%(uom)s\\"."]]	ir.message	product	f	[["text","You cannot modify the category of UOM \\"%(uom)s\\"."]]	\N	\N
1011	2020-09-02 07:24:32.977941	0	116	msg_uom_modify_options	[["text","If the UOM is still not used, you can delete it otherwise you can deactivate it and create a new one."]]	ir.message	product	f	[["text","If the UOM is still not used, you can delete it otherwise you can deactivate it and create a new one."]]	\N	\N
1012	2020-09-02 07:24:32.977941	0	117	msg_uom_incompatible_factor_rate	[["text","Incompatible factor and rate values on UOM \\"%(uom)s\\"."]]	ir.message	product	f	[["text","Incompatible factor and rate values on UOM \\"%(uom)s\\"."]]	\N	\N
1013	2020-09-02 07:24:32.977941	0	118	msg_uom_no_zero_factor_rate	[["text","Rate and factor can not be both equal to zero."]]	ir.message	product	f	[["text","Rate and factor can not be both equal to zero."]]	\N	\N
1014	2020-09-02 07:24:32.977941	0	119	msg_invalid_code	[["text","The %(type)s \\"%(code)s\\" for product \\"%(product)s\\" is not valid."]]	ir.message	product	f	[["text","The %(type)s \\"%(code)s\\" for product \\"%(product)s\\" is not valid."]]	\N	\N
1015	2020-09-02 07:24:32.977941	0	120	msg_product_code_unique	[["text","Code of active product must be unique."]]	ir.message	product	f	[["text","Code of active product must be unique."]]	\N	\N
1016	2020-09-02 07:40:09.139967	0	7	group_taller_admin	[["name","Taller Administration"]]	res.group	taller	f	[["name","Taller Administration"]]	\N	\N
1017	2020-09-02 07:40:09.139967	0	7	user_admin_group_taller_admin	[["group",7],["user",1]]	res.user-res.group	taller	f	[["group",7],["user",1]]	\N	\N
1018	2020-09-02 07:40:09.139967	0	8	group_taller	[["name","Taller "]]	res.group	taller	f	[["name","Taller "]]	\N	\N
1019	2020-09-02 07:40:09.139967	0	8	user_admin_group_taller	[["group",8],["user",1]]	res.user-res.group	taller	f	[["group",8],["user",1]]	\N	\N
1020	2020-09-02 07:56:32.835869	0	74	menu_taller	[["icon","tryton-folder"],["name","Taller"],["sequence",3]]	ir.ui.menu	taller	f	[["icon","tryton-folder"],["name","Taller"],["sequence",3]]	\N	\N
1021	2020-09-02 07:56:32.835869	0	171	marca_view_form	[["model","taller.marca"],["name","marca_form"],["type","form"]]	ir.ui.view	taller	f	[["model","taller.marca"],["name","marca_form"],["type","form"]]	\N	\N
1022	2020-09-02 07:56:32.835869	0	172	marca_view_list	[["model","taller.marca"],["name","marca_list"],["type","tree"]]	ir.ui.view	taller	f	[["model","taller.marca"],["name","marca_list"],["type","tree"]]	\N	\N
1023	2020-09-02 07:56:32.835869	0	85	act_marca_form	[["name","Marcas"],["res_model","taller.marca"]]	ir.action.act_window	taller	f	[["name","Marcas"],["res_model","taller.marca"]]	\N	\N
1024	2020-09-02 07:56:32.835869	0	113	act_marca_form_view1	[["act_window",85],["sequence",10],["view",172]]	ir.action.act_window.view	taller	f	[["act_window",85],["sequence",10],["view",172]]	\N	\N
1025	2020-09-02 07:56:32.835869	0	114	act_marca_form_view2	[["act_window",85],["sequence",20],["view",171]]	ir.action.act_window.view	taller	f	[["act_window",85],["sequence",20],["view",171]]	\N	\N
1026	2020-09-02 07:56:32.835869	0	75	menu_marca_form	[["action","ir.action.act_window,85"],["icon","tryton-list"],["name","Marcas"],["parent",74],["sequence",10]]	ir.ui.menu	taller	f	[["action","ir.action.act_window,85"],["icon","tryton-list"],["name","Marcas"],["parent",74],["sequence",10]]	\N	\N
1027	2020-09-02 08:45:11.63	0	173	modelo_view_form	[["model","taller.modelo"],["name","modelo_form"],["type","form"]]	ir.ui.view	taller	f	[["model","taller.modelo"],["name","modelo_form"],["type","form"]]	\N	\N
1028	2020-09-02 08:45:11.63	0	174	modelo_view_list	[["model","taller.modelo"],["name","modelo_list"],["type","tree"]]	ir.ui.view	taller	f	[["model","taller.modelo"],["name","modelo_list"],["type","tree"]]	\N	\N
1029	2020-09-02 08:45:11.63	0	86	act_modelo_form	[["name","Modelos"],["res_model","taller.modelo"]]	ir.action.act_window	taller	f	[["name","Modelos"],["res_model","taller.modelo"]]	\N	\N
1030	2020-09-02 08:45:11.63	0	115	act_modelo_form_view1	[["act_window",86],["sequence",10],["view",174]]	ir.action.act_window.view	taller	f	[["act_window",86],["sequence",10],["view",174]]	\N	\N
1031	2020-09-02 08:45:11.63	0	116	act_modelo_form_view2	[["act_window",86],["sequence",20],["view",173]]	ir.action.act_window.view	taller	f	[["act_window",86],["sequence",20],["view",173]]	\N	\N
1032	2020-09-02 08:45:11.63	0	76	menu_modelo_form	[["action","ir.action.act_window,86"],["icon","tryton-list"],["name","Modelos"],["parent",74],["sequence",10]]	ir.ui.menu	taller	f	[["action","ir.action.act_window,86"],["icon","tryton-list"],["name","Modelos"],["parent",74],["sequence",10]]	\N	\N
1033	2020-09-02 11:14:57.919973	0	175	coche_view_form	[["model","taller.coche"],["name","coche_form"],["type","form"]]	ir.ui.view	taller	f	[["model","taller.coche"],["name","coche_form"],["type","form"]]	\N	\N
1034	2020-09-02 11:14:57.919973	0	176	coche_view_list	[["model","taller.coche"],["name","coche_list"],["type","tree"]]	ir.ui.view	taller	f	[["model","taller.coche"],["name","coche_list"],["type","tree"]]	\N	\N
1035	2020-09-02 11:14:57.919973	0	87	act_coche_form	[["name","Coches"],["res_model","taller.coche"]]	ir.action.act_window	taller	f	[["name","Coches"],["res_model","taller.coche"]]	\N	\N
1036	2020-09-02 11:14:57.919973	0	117	act_coche_form_view1	[["act_window",87],["sequence",10],["view",176]]	ir.action.act_window.view	taller	f	[["act_window",87],["sequence",10],["view",176]]	\N	\N
1037	2020-09-02 11:14:57.919973	0	118	act_coche_form_view2	[["act_window",87],["sequence",20],["view",175]]	ir.action.act_window.view	taller	f	[["act_window",87],["sequence",20],["view",175]]	\N	\N
1038	2020-09-02 11:19:59.170481	0	77	menu_coche_form	[["action","ir.action.act_window,87"],["icon","tryton-list"],["name","Coches"],["parent",74],["sequence",10]]	ir.ui.menu	taller	f	[["action","ir.action.act_window,87"],["icon","tryton-list"],["name","Coches"],["parent",74],["sequence",10]]	\N	\N
1039	2020-09-04 07:51:32.521302	0	121	msg_coche_matricula_unique	[["text"," Matricula repetida "]]	ir.message	taller	f	[["text"," Matricula repetida "]]	\N	\N
1048	2020-09-08 08:59:28.457176	0	84	act_teclado_coches_list	[["action",88],["keyword","form_relate"],["model","party.party,-1"]]	ir.action.keyword	taller	f	[["action",88],["keyword","form_relate"],["model","party.party,-1"]]	\N	\N
1049	2020-09-09 07:20:33.036685	0	89	act_teclado_coches_productos	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"modelo.productosDelModelo\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"modelo.productosDelModelo\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name"," lista Coches productos "],["res_model","taller.coche"]]	ir.action.act_window	taller	f	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"modelo.productosDelModelo\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"modelo.productosDelModelo\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name"," lista Coches productos "],["res_model","taller.coche"]]	\N	\N
1045	2020-09-07 13:29:55.542439	0	182	party_view_form	[["inherit",122],["model","party.party"],["name","PartyCoches_Form"]]	ir.ui.view	taller	f	[["inherit",122],["model","party.party"],["name","PartyCoches_Form"]]	\N	\N
1050	2020-09-09 07:20:33.036685	0	85	act_teclado_coches_prod_list	[["action",89],["keyword","form_relate"],["model","product.template,-1"]]	ir.action.keyword	taller	f	[["action",89],["keyword","form_relate"],["model","product.template,-1"]]	\N	\N
1046	2020-09-07 14:00:06.849225	0	183	template_view_form	[["inherit",155],["model","product.template"],["name","productos_models_view_list"]]	ir.ui.view	taller	f	[["inherit",155],["model","product.template"],["name","productos_models_view_list"]]	\N	\N
1047	2020-09-08 08:59:28.457176	0	88	act_teclado_coches	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"propietario\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"propietario\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name"," lista Coches "],["res_model","taller.coche"]]	ir.action.act_window	taller	f	[["domain","[{\\"__class__\\": \\"If\\", \\"c\\": {\\"__class__\\": \\"Equal\\", \\"s1\\": {\\"__class__\\": \\"Eval\\", \\"d\\": [], \\"v\\": \\"active_ids\\"}, \\"s2\\": [{\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}, \\"e\\": [\\"propietario\\", \\"in\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_ids\\"}], \\"t\\": [\\"propietario\\", \\"=\\", {\\"__class__\\": \\"Eval\\", \\"d\\": \\"\\", \\"v\\": \\"active_id\\"}]}]"],["name"," lista Coches "],["res_model","taller.coche"]]	\N	\N
1054	2020-09-09 08:30:29.70123	0	186	coche_baja_start_form	[["model","taller.coche.baja.start"],["name","coche_baja_start"],["type","form"]]	ir.ui.view	taller	f	[["model","taller.coche.baja.start"],["name","coche_baja_start"],["type","form"]]	\N	\N
1055	2020-09-09 08:30:29.70123	0	187	coche_baja_result_form	[["model","taller.coche.baja.result"],["name","coche_baja_result"],["type","form"]]	ir.ui.view	taller	f	[["model","taller.coche.baja.result"],["name","coche_baja_result"],["type","form"]]	\N	\N
1057	2020-09-09 08:30:29.70123	0	87	act_teclado_coches_baja	[["action",91],["keyword","form_action"],["model","taller.coche,-1"]]	ir.action.keyword	taller	f	[["action",91],["keyword","form_action"],["model","taller.coche,-1"]]	\N	\N
1060	2020-09-14 06:53:25.827367	0	95	report_coche2	[["extension","csv"],["model","taller.coche"],["name","Report_lista_coches"],["report","taller/reports/lista_coches.ods"],["report_name","Lista_de_mis_coches"],["template_extension","ods"]]	ir.action.report	taller	f	[["extension","csv"],["model","taller.coche"],["name","Report_lista_coches"],["report","taller/reports/lista_coches.ods"],["report_name","Lista_de_mis_coches"],["template_extension","ods"]]	\N	\N
1061	2020-09-14 06:53:25.827367	0	89	report_coche_keyword2	[["action",95],["keyword","form_print"],["model","taller.coche,-1"]]	ir.action.keyword	taller	f	[["action",95],["keyword","form_print"],["model","taller.coche,-1"]]	\N	\N
1071	2020-09-16 08:36:14.823954	0	16	user_reset_password_button	[["model",127],["name","reset_password"],["string","Reset Password"]]	ir.model.button	web_user	f	[["model",127],["name","reset_password"],["string","Reset Password"]]	\N	\N
1072	2020-09-16 08:36:14.823954	0	97	report_email_validation	[["model","web.user"],["name","Email Validation"],["report","web_user/email_validation.html"],["report_name","web.user.email_validation"],["template_extension","html"]]	ir.action.report	web_user	f	[["model","web.user"],["name","Email Validation"],["report","web_user/email_validation.html"],["report_name","web.user.email_validation"],["template_extension","html"]]	\N	\N
1073	2020-09-16 08:36:14.823954	0	98	report_email_reset_password	[["model","web.user"],["name","Reset Password"],["report","web_user/email_reset_password.html"],["report_name","web.user.email_reset_password"],["template_extension","html"]]	ir.action.report	web_user	f	[["model","web.user"],["name","Reset Password"],["report","web_user/email_reset_password.html"],["report_name","web.user.email_reset_password"],["template_extension","html"]]	\N	\N
1074	2020-09-16 08:36:14.823954	0	122	msg_user_email_unique	[["text","E-mail of active web user must be unique."]]	ir.message	web_user	f	[["text","E-mail of active web user must be unique."]]	\N	\N
1075	2020-09-16 08:36:14.823954	0	123	msg_user_session_key_unique	[["text","Web user session key must be unique."]]	ir.message	web_user	f	[["text","Web user session key must be unique."]]	\N	\N
1058	2020-09-10 08:14:35.515414	0	92	report_coche	[["extension","pdf"],["model","taller.coche"],["name","Report_coche"],["report","taller/reports/ficha_tecnica.odt"],["report_name","ficha_tecnica_del_coche"],["template_extension","odt"]]	ir.action.report	taller	f	[["extension","pdf"],["model","taller.coche"],["name","Report_coche"],["report","taller/reports/ficha_tecnica.odt"],["report_name","ficha_tecnica_del_coche"],["template_extension","odt"]]	\N	\N
1059	2020-09-10 08:14:35.515414	0	88	report_coche_keyword	[["action",92],["keyword","form_print"],["model","taller.coche,-1"]]	ir.action.keyword	taller	f	[["action",92],["keyword","form_print"],["model","taller.coche,-1"]]	\N	\N
1056	2020-09-09 08:30:29.70123	0	91	coche_baja_transition	[["name","Baja Transition"],["wiz_name","taller.coche.baja"]]	ir.action.wizard	taller	f	[["name","Baja Transition"],["wiz_name","BajaCoche"]]	2020-09-29 10:24:50.5878	0
1062	2020-09-16 08:36:14.823954	0	188	user_view_form	[["model","web.user"],["name","user_form"],["type","form"]]	ir.ui.view	web_user	f	[["model","web.user"],["name","user_form"],["type","form"]]	\N	\N
1063	2020-09-16 08:36:14.823954	0	189	user_view_list	[["model","web.user"],["name","user_list"],["type","tree"]]	ir.ui.view	web_user	f	[["model","web.user"],["name","user_list"],["type","tree"]]	\N	\N
1064	2020-09-16 08:36:14.823954	0	96	act_user_form	[["name","Web Users"],["res_model","web.user"]]	ir.action.act_window	web_user	f	[["name","Web Users"],["res_model","web.user"]]	\N	\N
1065	2020-09-16 08:36:14.823954	0	119	act_user_form_view1	[["act_window",96],["sequence",10],["view",189]]	ir.action.act_window.view	web_user	f	[["act_window",96],["sequence",10],["view",189]]	\N	\N
1066	2020-09-16 08:36:14.823954	0	120	act_user_form_view2	[["act_window",96],["sequence",20],["view",188]]	ir.action.act_window.view	web_user	f	[["act_window",96],["sequence",20],["view",188]]	\N	\N
1067	2020-09-16 08:36:14.823954	0	78	menu_user_form	[["action","ir.action.act_window,96"],["icon","tryton-list"],["name","Web Users"],["parent",45],["sequence",30]]	ir.ui.menu	web_user	f	[["action","ir.action.act_window,96"],["icon","tryton-list"],["name","Web Users"],["parent",45],["sequence",30]]	\N	\N
1068	2020-09-16 08:36:14.823954	0	96	access_user	[["model",127],["perm_create",false],["perm_delete",false],["perm_read",false],["perm_write",false]]	ir.model.access	web_user	f	[["model",127],["perm_create",false],["perm_delete",false],["perm_read",false],["perm_write",false]]	\N	\N
1069	2020-09-16 08:36:14.823954	0	97	access_user_admin	[["group",1],["model",127],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	ir.model.access	web_user	f	[["group",1],["model",127],["perm_create",true],["perm_delete",true],["perm_read",true],["perm_write",true]]	\N	\N
1070	2020-09-16 08:36:14.823954	0	15	user_validate_email_button	[["model",127],["name","validate_email"],["string","Validate E-mail"]]	ir.model.button	web_user	f	[["model",127],["name","validate_email"],["string","Validate E-mail"]]	\N	\N
\.


--
-- Data for Name: ir_model_field; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_model_field (id, model, name, relation, field_description, ttype, help, module, create_date, create_uid, write_date, write_uid) FROM stdin;
821	82	active	\N	ir.msg_active	boolean	ir.msg_active_help	country	\N	\N	\N	\N
822	82	code	\N	Code	char	The 2 chars ISO country code.	country	\N	\N	\N	\N
823	82	code3	\N	3-letters Code	char	The 3 chars ISO country code.	country	\N	\N	\N	\N
824	82	code_numeric	\N	Numeric Code	char	The ISO numeric country code.	country	\N	\N	\N	\N
825	82	create_date	\N	ir.msg_created_by	timestamp		country	\N	\N	\N	\N
826	82	create_uid	res.user	ir.msg_created_by	many2one		country	\N	\N	\N	\N
827	82	id	\N	ir.msg_ID	integer		country	\N	\N	\N	\N
828	82	name	\N	Name	char	The main identifier of the country.	country	\N	\N	\N	\N
829	82	rec_name	\N	ir.msg_record_name	char		country	\N	\N	\N	\N
830	82	subdivisions	country.subdivision	Subdivisions	one2many		country	\N	\N	\N	\N
831	82	write_date	\N	ir.msg_edited_at	timestamp		country	\N	\N	\N	\N
832	82	write_uid	res.user	ir.msg_edited_by	many2one		country	\N	\N	\N	\N
833	83	active	\N	ir.msg_active	boolean	ir.msg_active_help	country	\N	\N	\N	\N
834	83	code	\N	Code	char	The ISO code of the subdivision.	country	\N	\N	\N	\N
835	83	country	country.country	Country	many2one	The country where this subdivision is.	country	\N	\N	\N	\N
836	83	create_date	\N	ir.msg_created_by	timestamp		country	\N	\N	\N	\N
837	83	create_uid	res.user	ir.msg_created_by	many2one		country	\N	\N	\N	\N
838	83	id	\N	ir.msg_ID	integer		country	\N	\N	\N	\N
839	83	name	\N	Name	char	The main identifier of the subdivision.	country	\N	\N	\N	\N
840	83	parent	country.subdivision	Parent	many2one	Add subdivision below the parent.	country	\N	\N	\N	\N
841	83	rec_name	\N	ir.msg_record_name	char		country	\N	\N	\N	\N
842	83	type	\N	Type	selection		country	\N	\N	\N	\N
843	83	write_date	\N	ir.msg_edited_at	timestamp		country	\N	\N	\N	\N
844	83	write_uid	res.user	ir.msg_edited_by	many2one		country	\N	\N	\N	\N
845	84	city	\N	City	char	The name of the city for the zip.	country	\N	\N	\N	\N
846	84	country	country.country	Country	many2one	The country where the zip is.	country	\N	\N	\N	\N
847	84	create_date	\N	ir.msg_created_by	timestamp		country	\N	\N	\N	\N
848	84	create_uid	res.user	ir.msg_created_by	many2one		country	\N	\N	\N	\N
849	84	id	\N	ir.msg_ID	integer		country	\N	\N	\N	\N
850	84	rec_name	\N	ir.msg_record_name	char		country	\N	\N	\N	\N
851	84	subdivision	country.subdivision	Subdivision	many2one	The subdivision where the zip is.	country	\N	\N	\N	\N
852	84	write_date	\N	ir.msg_edited_at	timestamp		country	\N	\N	\N	\N
853	84	write_uid	res.user	ir.msg_edited_by	many2one		country	\N	\N	\N	\N
854	84	zip	\N	Zip	char		country	\N	\N	\N	\N
855	85	active	\N	ir.msg_active	boolean	ir.msg_active_help	currency	\N	\N	\N	\N
856	85	code	\N	Code	char	The 3 chars ISO currency code.	currency	\N	\N	\N	\N
857	85	create_date	\N	ir.msg_created_by	timestamp		currency	\N	\N	\N	\N
858	85	create_uid	res.user	ir.msg_created_by	many2one		currency	\N	\N	\N	\N
859	85	digits	\N	Digits	integer	The number of digits to display after the decimal separator.	currency	\N	\N	\N	\N
860	85	id	\N	ir.msg_ID	integer		currency	\N	\N	\N	\N
861	85	name	\N	Name	char	The main identifier of the currency.	currency	\N	\N	\N	\N
862	85	numeric_code	\N	Numeric Code	char	The 3 digits ISO currency code.	currency	\N	\N	\N	\N
863	85	rate	\N	Current rate	numeric		currency	\N	\N	\N	\N
864	85	rates	currency.currency.rate	Rates	one2many	Add floating exchange rates for the currency.	currency	\N	\N	\N	\N
865	85	rec_name	\N	ir.msg_record_name	char		currency	\N	\N	\N	\N
866	85	rounding	\N	Rounding factor	numeric	The minimum amount which can be represented in this currency.	currency	\N	\N	\N	\N
867	85	symbol	\N	Symbol	char	The symbol used for currency formating.	currency	\N	\N	\N	\N
868	85	write_date	\N	ir.msg_edited_at	timestamp		currency	\N	\N	\N	\N
869	85	write_uid	res.user	ir.msg_edited_by	many2one		currency	\N	\N	\N	\N
870	86	create_date	\N	ir.msg_created_by	timestamp		currency	\N	\N	\N	\N
871	86	create_uid	res.user	ir.msg_created_by	many2one		currency	\N	\N	\N	\N
872	86	currency	currency.currency	Currency	many2one	The currency on which the rate applies.	currency	\N	\N	\N	\N
873	86	date	\N	Date	date	From when the rate applies.	currency	\N	\N	\N	\N
874	86	id	\N	ir.msg_ID	integer		currency	\N	\N	\N	\N
875	86	rate	\N	Rate	numeric	The floating exchange rate used to convert the currency.	currency	\N	\N	\N	\N
876	86	rec_name	\N	ir.msg_record_name	char		currency	\N	\N	\N	\N
877	86	write_date	\N	ir.msg_edited_at	timestamp		currency	\N	\N	\N	\N
878	86	write_uid	res.user	ir.msg_edited_by	many2one		currency	\N	\N	\N	\N
879	87	active	\N	ir.msg_active	boolean	ir.msg_active_help	party	\N	\N	\N	\N
880	87	childs	party.category	Children	one2many	Add children below the category.	party	\N	\N	\N	\N
881	87	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
882	87	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
883	87	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
884	87	name	\N	Name	char	The main identifier of the category.	party	\N	\N	\N	\N
885	87	parent	party.category	Parent	many2one	Add the category below the parent.	party	\N	\N	\N	\N
886	87	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
887	87	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
888	87	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
889	88	active	\N	ir.msg_active	boolean	ir.msg_active_help	party	\N	\N	\N	\N
890	88	addresses	party.address	Addresses	one2many		party	\N	\N	\N	\N
891	88	categories	party.party-party.category	Categories	many2many	The categories the party belongs to.	party	\N	\N	\N	\N
892	88	code	\N	Code	char	The unique identifier of the party.	party	\N	\N	\N	\N
893	88	code_readonly	\N	Code Readonly	boolean		party	\N	\N	\N	\N
894	88	contact_mechanisms	party.contact_mechanism	Contact Mechanisms	one2many		party	\N	\N	\N	\N
895	88	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
896	88	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
897	88	email	\N	E-Mail	char		party	\N	\N	\N	\N
898	88	fax	\N	Fax	char		party	\N	\N	\N	\N
899	88	full_name	\N	Full Name	char		party	\N	\N	\N	\N
900	88	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
901	88	identifiers	party.identifier	Identifiers	one2many	Add other identifiers of the party.	party	\N	\N	\N	\N
902	88	lang	ir.lang	Language	many2one	Used to translate communications with the party.	party	\N	\N	\N	\N
903	88	langs	party.party.lang	Languages	one2many		party	\N	\N	\N	\N
904	88	mobile	\N	Mobile	char		party	\N	\N	\N	\N
905	88	name	\N	Name	char	The main identifier of the party.	party	\N	\N	\N	\N
906	88	phone	\N	Phone	char		party	\N	\N	\N	\N
907	88	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
908	88	replaced_by	party.party	Replaced By	many2one	The party replacing this one.	party	\N	\N	\N	\N
909	88	tax_identifier	party.identifier	Tax Identifier	many2one	The identifier used for tax report.	party	\N	\N	\N	\N
910	88	website	\N	Website	char		party	\N	\N	\N	\N
911	88	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
912	88	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
913	89	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
914	89	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
915	89	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
916	89	lang	ir.lang	Language	many2one		party	\N	\N	\N	\N
917	89	party	party.party	Party	many2one		party	\N	\N	\N	\N
918	89	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
919	89	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
920	89	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
921	90	category	party.category	Category	many2one		party	\N	\N	\N	\N
922	90	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
923	90	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
924	90	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
925	90	party	party.party	Party	many2one		party	\N	\N	\N	\N
926	90	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
927	90	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
928	90	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
929	91	code	\N	Code	char		party	\N	\N	\N	\N
930	91	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
931	91	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
932	91	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
933	91	party	party.party	Party	many2one	The party identified by this record.	party	\N	\N	\N	\N
934	91	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
935	91	sequence	\N	ir.msg_sequence	integer		party	\N	\N	\N	\N
936	91	type	\N	Type	selection		party	\N	\N	\N	\N
937	91	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
938	91	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
939	92	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
940	92	parties_failed	party.party	Parties Failed	many2many		party	\N	\N	\N	\N
941	92	parties_succeed	party.party	Parties Succeed	many2many		party	\N	\N	\N	\N
942	93	destination	party.party	Destination	many2one	The party that replaces.	party	\N	\N	\N	\N
943	93	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
944	93	source	party.party	Source	many2one	The party to be replaced.	party	\N	\N	\N	\N
945	94	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
946	94	party	party.party	Party	many2one	The party to be erased.	party	\N	\N	\N	\N
947	95	active	\N	ir.msg_active	boolean	ir.msg_active_help	party	\N	\N	\N	\N
948	95	city	\N	City	char		party	\N	\N	\N	\N
949	95	country	country.country	Country	many2one		party	\N	\N	\N	\N
950	95	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
951	95	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
952	95	full_address	\N	Full Address	text		party	\N	\N	\N	\N
953	95	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
954	95	name	\N	Building Name	char		party	\N	\N	\N	\N
955	95	party	party.party	Party	many2one		party	\N	\N	\N	\N
956	95	party_name	\N	Party Name	char	If filled, replace the name of the party for address formatting	party	\N	\N	\N	\N
957	95	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
958	95	sequence	\N	ir.msg_sequence	integer		party	\N	\N	\N	\N
959	95	street	\N	Street	text		party	\N	\N	\N	\N
960	95	subdivision	country.subdivision	Subdivision	many2one		party	\N	\N	\N	\N
961	95	subdivision_types	\N	Subdivision Types	multiselection		party	\N	\N	\N	\N
962	95	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
963	95	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
964	95	zip	\N	Zip	char		party	\N	\N	\N	\N
965	96	active	\N	ir.msg_active	boolean	ir.msg_active_help	party	\N	\N	\N	\N
966	96	country_code	\N	Country Code	char		party	\N	\N	\N	\N
967	96	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
968	96	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
969	96	format_	\N	Format	text	Available variables (also in upper case):\n- ${party_name}\n- ${name}\n- ${attn}\n- ${street}\n- ${zip}\n- ${city}\n- ${subdivision}\n- ${subdivision_code}\n- ${country}\n- ${country_code}	party	\N	\N	\N	\N
970	96	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
971	96	language_code	\N	Language Code	char		party	\N	\N	\N	\N
972	96	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
973	96	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
974	96	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
975	97	active	\N	ir.msg_active	boolean	ir.msg_active_help	party	\N	\N	\N	\N
976	97	country_code	\N	Country Code	char		party	\N	\N	\N	\N
977	97	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
978	97	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
979	97	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
980	97	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
981	97	types	\N	Subdivision Types	multiselection		party	\N	\N	\N	\N
982	97	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
983	97	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
984	98	active	\N	ir.msg_active	boolean	ir.msg_active_help	party	\N	\N	\N	\N
985	98	comment	\N	Comment	text		party	\N	\N	\N	\N
986	98	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
987	98	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
988	98	email	\N	E-Mail	char		party	\N	\N	\N	\N
989	98	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
990	98	name	\N	Name	char		party	\N	\N	\N	\N
991	98	other_value	\N	Value	char		party	\N	\N	\N	\N
992	98	party	party.party	Party	many2one		party	\N	\N	\N	\N
993	98	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
994	98	sequence	\N	ir.msg_sequence	integer		party	\N	\N	\N	\N
995	98	sip	\N	SIP	char		party	\N	\N	\N	\N
996	98	skype	\N	Skype	char		party	\N	\N	\N	\N
997	98	type	\N	Type	selection		party	\N	\N	\N	\N
998	98	url	\N	URL	char		party	\N	\N	\N	\N
999	98	value	\N	Value	char		party	\N	\N	\N	\N
1000	98	value_compact	\N	Value Compact	char		party	\N	\N	\N	\N
1001	98	website	\N	Website	char		party	\N	\N	\N	\N
1002	98	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
1003	98	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
1004	99	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
1005	99	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
1006	99	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
1007	99	party_lang	ir.lang	Party Language	many2one	The default language for new parties.	party	\N	\N	\N	\N
1008	99	party_sequence	ir.sequence	Party Sequence	many2one	Used to generate the party code.	party	\N	\N	\N	\N
1009	99	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
1010	99	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
1011	99	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
1012	100	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
1013	100	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
1014	100	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
1015	100	party_sequence	ir.sequence	Party Sequence	many2one	Used to generate the party code.	party	\N	\N	\N	\N
1016	100	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
1017	100	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
1018	100	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
1019	101	create_date	\N	ir.msg_created_by	timestamp		party	\N	\N	\N	\N
1020	101	create_uid	res.user	ir.msg_created_by	many2one		party	\N	\N	\N	\N
1021	101	id	\N	ir.msg_ID	integer		party	\N	\N	\N	\N
1022	101	party_lang	ir.lang	Party Language	many2one	The default language for new parties.	party	\N	\N	\N	\N
1023	101	rec_name	\N	ir.msg_record_name	char		party	\N	\N	\N	\N
1024	101	write_date	\N	ir.msg_edited_at	timestamp		party	\N	\N	\N	\N
1025	101	write_uid	res.user	ir.msg_edited_by	many2one		party	\N	\N	\N	\N
1026	102	childs	company.company	Children	one2many	Add children below the company.	company	\N	\N	\N	\N
1027	102	create_date	\N	ir.msg_created_by	timestamp		company	\N	\N	\N	\N
1028	102	create_uid	res.user	ir.msg_created_by	many2one		company	\N	\N	\N	\N
1029	102	currency	currency.currency	Currency	many2one	The main currency for the company.	company	\N	\N	\N	\N
1030	102	employees	company.employee	Employees	one2many	Add employees to the company.	company	\N	\N	\N	\N
1031	102	footer	\N	Footer	text	The text to display on report footers.	company	\N	\N	\N	\N
1032	102	header	\N	Header	text	The text to display on report headers.	company	\N	\N	\N	\N
1033	102	id	\N	ir.msg_ID	integer		company	\N	\N	\N	\N
1034	102	parent	company.company	Parent	many2one	Add the company below the parent.	company	\N	\N	\N	\N
1035	102	party	party.party	Party	many2one		company	\N	\N	\N	\N
1036	102	rec_name	\N	ir.msg_record_name	char		company	\N	\N	\N	\N
1037	102	timezone	\N	Timezone	selection	Used to compute the today date.	company	\N	\N	\N	\N
1038	102	write_date	\N	ir.msg_edited_at	timestamp		company	\N	\N	\N	\N
1039	102	write_uid	res.user	ir.msg_edited_by	many2one		company	\N	\N	\N	\N
1040	103	company	company.company	Company	many2one	The company to which the employee belongs.	company	\N	\N	\N	\N
1041	103	create_date	\N	ir.msg_created_by	timestamp		company	\N	\N	\N	\N
1042	103	create_uid	res.user	ir.msg_created_by	many2one		company	\N	\N	\N	\N
1043	103	end_date	\N	End Date	date	When the employee leaves the company.	company	\N	\N	\N	\N
1044	103	id	\N	ir.msg_ID	integer		company	\N	\N	\N	\N
1045	103	party	party.party	Party	many2one	The party which represents the employee.	company	\N	\N	\N	\N
1046	103	rec_name	\N	ir.msg_record_name	char		company	\N	\N	\N	\N
1047	103	start_date	\N	Start Date	date	When the employee joins the company.	company	\N	\N	\N	\N
1048	103	subordinates	company.employee	Subordinates	one2many	The employees to be overseen by this employee.	company	\N	\N	\N	\N
1049	103	supervisor	company.employee	Supervisor	many2one	The employee who oversees this employee.	company	\N	\N	\N	\N
1050	103	write_date	\N	ir.msg_edited_at	timestamp		company	\N	\N	\N	\N
1051	103	write_uid	res.user	ir.msg_edited_by	many2one		company	\N	\N	\N	\N
1052	104	id	\N	ir.msg_ID	integer		company	\N	\N	\N	\N
1053	105	create_date	\N	ir.msg_created_by	timestamp		company	\N	\N	\N	\N
1054	105	create_uid	res.user	ir.msg_created_by	many2one		company	\N	\N	\N	\N
1055	105	employee	company.employee	Employee	many2one		company	\N	\N	\N	\N
1056	105	id	\N	ir.msg_ID	integer		company	\N	\N	\N	\N
1057	105	rec_name	\N	ir.msg_record_name	char		company	\N	\N	\N	\N
1058	105	user	res.user	User	many2one		company	\N	\N	\N	\N
1059	105	write_date	\N	ir.msg_edited_at	timestamp		company	\N	\N	\N	\N
1060	105	write_uid	res.user	ir.msg_edited_by	many2one		company	\N	\N	\N	\N
1061	68	companies	company.company	Companies	one2many		company	\N	\N	\N	\N
1062	68	company	company.company	Current Company	many2one	Select the company to work for.	company	\N	\N	\N	\N
1063	68	employee	company.employee	Current Employee	many2one	Select the employee to make the user behave as such.	company	\N	\N	\N	\N
1064	68	employees	res.user-company.employee	Employees	many2many	Add employees to grant the user access to them.	company	\N	\N	\N	\N
1065	68	main_company	company.company	Main Company	many2one	Grant access to the company and its children.	company	\N	\N	\N	\N
1066	11	company	company.company	Company	many2one	Restrict the sequence usage to the company.	company	\N	\N	\N	\N
1067	12	company	company.company	Company	many2one	Restrict the sequence usage to the company.	company	\N	\N	\N	\N
544	48	domain	\N	Domain	char	Domain is evaluated with a PYSON context containing:\n- "user" as the current user\n- "employee" from the current user	ir	\N	\N	\N	\N
1068	42	companies	ir.cron-company.company	Companies	many2many	Companies registered for this cron.	company	\N	\N	\N	\N
1069	106	company	company.company	Company	many2one		company	\N	\N	\N	\N
1070	106	create_date	\N	ir.msg_created_by	timestamp		company	\N	\N	\N	\N
1071	106	create_uid	res.user	ir.msg_created_by	many2one		company	\N	\N	\N	\N
1072	106	cron	ir.cron	Cron	many2one		company	\N	\N	\N	\N
1073	106	id	\N	ir.msg_ID	integer		company	\N	\N	\N	\N
1074	106	rec_name	\N	ir.msg_record_name	char		company	\N	\N	\N	\N
1075	106	write_date	\N	ir.msg_edited_at	timestamp		company	\N	\N	\N	\N
1076	106	write_uid	res.user	ir.msg_edited_by	many2one		company	\N	\N	\N	\N
1077	101	company	company.company	Company	many2one		company	\N	\N	\N	\N
1078	89	company	company.company	Company	many2one		company	\N	\N	\N	\N
1079	107	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1080	107	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1081	107	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1082	107	name	\N	Name	char		product	\N	\N	\N	\N
1083	107	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1084	107	uoms	product.uom	Units of Measure	one2many		product	\N	\N	\N	\N
1085	107	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1086	107	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1087	108	active	\N	ir.msg_active	boolean	ir.msg_active_help	product	\N	\N	\N	\N
1088	108	category	product.uom.category	Category	many2one	The category that contains the unit of measure.\nConversions between different units of measure can be done if they are in the same category.	product	\N	\N	\N	\N
1089	108	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1090	108	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1091	108	digits	\N	Display Digits	integer	The number of digits to display after the decimal separator.	product	\N	\N	\N	\N
1092	108	factor	\N	Factor	float	The coefficient for the formula:\ncoefficient (base unit) = 1 (this unit)	product	\N	\N	\N	\N
1093	108	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1094	108	name	\N	Name	char		product	\N	\N	\N	\N
1095	108	rate	\N	Rate	float	The coefficient for the formula:\n1 (base unit) = coef (this unit)	product	\N	\N	\N	\N
1096	108	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1097	108	rounding	\N	Rounding Precision	float	The accuracy to which values are rounded.	product	\N	\N	\N	\N
1098	108	symbol	\N	Symbol	char	The symbol that represents the unit of measure.	product	\N	\N	\N	\N
1099	108	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1100	108	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1101	109	childs	product.category	Children	one2many	Used to add structure below the category.	product	\N	\N	\N	\N
1102	109	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1103	109	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1104	109	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1105	109	name	\N	Name	char		product	\N	\N	\N	\N
1106	109	parent	product.category	Parent	many2one	Used to add structure above the category.	product	\N	\N	\N	\N
1107	109	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1108	109	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1109	109	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1110	110	active	\N	ir.msg_active	boolean	ir.msg_active_help	product	\N	\N	\N	\N
1111	110	categories	product.template-product.category	Categories	many2many	The categories that the product is in.\nUsed to group similar products together.	product	\N	\N	\N	\N
1112	110	categories_all	product.template-product.category.all	Categories	many2many		product	\N	\N	\N	\N
1113	110	code	\N	Code	char		product	\N	\N	\N	\N
1114	110	consumable	\N	Consumable	boolean	Check to allow stock moves to be assigned regardless of stock level.	product	\N	\N	\N	\N
1115	110	cost_price	\N	Cost Price	numeric	The amount it costs to purchase or make the product, or carry out the service.	product	\N	\N	\N	\N
1116	110	cost_price_method	\N	Cost Price Method	selection	The method used to calculate the cost price.	product	\N	\N	\N	\N
1117	110	cost_price_methods	product.cost_price_method	Cost Price Methods	one2many		product	\N	\N	\N	\N
1118	110	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1119	110	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1120	110	default_uom	product.uom	Default UOM	many2one	The standard unit of measure for the product.\nUsed internally when calculating the stock levels of goods and assets.	product	\N	\N	\N	\N
1121	110	default_uom_category	product.uom.category	Default UOM Category	many2one		product	\N	\N	\N	\N
1122	110	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1123	110	list_price	\N	List Price	numeric	The standard price the product is sold at.	product	\N	\N	\N	\N
1124	110	list_prices	product.list_price	List Prices	one2many		product	\N	\N	\N	\N
1125	110	name	\N	Name	char		product	\N	\N	\N	\N
1126	110	products	product.product	Variants	one2many	The different variants the product comes in.	product	\N	\N	\N	\N
1127	110	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1128	110	type	\N	Type	selection		product	\N	\N	\N	\N
1129	110	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1130	110	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1131	111	active	\N	ir.msg_active	boolean	ir.msg_active_help	product	\N	\N	\N	\N
1132	111	categories	product.template-product.category	Categories	many2many	The categories that the product is in.\nUsed to group similar products together.	product	\N	\N	\N	\N
1133	111	categories_all	product.template-product.category.all	Categories	many2many		product	\N	\N	\N	\N
1134	111	code	\N	Code	char	A unique identifier for the variant.	product	\N	\N	\N	\N
1135	111	code_readonly	\N	Code Readonly	boolean		product	\N	\N	\N	\N
1136	111	consumable	\N	Consumable	boolean	Check to allow stock moves to be assigned regardless of stock level.	product	\N	\N	\N	\N
1137	111	cost_price	\N	Cost Price	numeric	The amount it costs to purchase or make the variant, or carry out the service.	product	\N	\N	\N	\N
1138	111	cost_price_method	\N	Cost Price Method	selection	The method used to calculate the cost price.	product	\N	\N	\N	\N
1139	111	cost_price_methods	product.cost_price_method	Cost Price Methods	one2many		product	\N	\N	\N	\N
1140	111	cost_price_uom	\N	Cost Price	numeric		product	\N	\N	\N	\N
1141	111	cost_prices	product.cost_price	Cost Prices	one2many		product	\N	\N	\N	\N
1142	111	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1143	111	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1144	111	default_uom	product.uom	Default UOM	many2one	The standard unit of measure for the product.\nUsed internally when calculating the stock levels of goods and assets.	product	\N	\N	\N	\N
1145	111	default_uom_category	product.uom.category	Default UOM Category	many2one		product	\N	\N	\N	\N
1146	111	description	\N	Description	text		product	\N	\N	\N	\N
1147	111	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1148	111	identifiers	product.identifier	Identifiers	one2many	Other identifiers associated with the variant.	product	\N	\N	\N	\N
1149	111	list_price	\N	List Price	numeric	The standard price the product is sold at.	product	\N	\N	\N	\N
1150	111	list_price_uom	\N	List Price	numeric		product	\N	\N	\N	\N
1151	111	list_prices	product.list_price	List Prices	one2many		product	\N	\N	\N	\N
1152	111	name	\N	Name	char		product	\N	\N	\N	\N
1153	111	prefix_code	\N	Prefix Code	char		product	\N	\N	\N	\N
1154	111	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1155	111	suffix_code	\N	Suffix Code	char	The unique identifier for the product (aka SKU).	product	\N	\N	\N	\N
1156	111	template	product.template	Product Template	many2one	The product that defines the common properties inherited by the variant.	product	\N	\N	\N	\N
1157	111	type	\N	Type	selection		product	\N	\N	\N	\N
1158	111	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1159	111	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1160	112	code	\N	Code	char		product	\N	\N	\N	\N
1161	112	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1162	112	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1163	112	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1164	112	product	product.product	Product	many2one	The product identified by the code.	product	\N	\N	\N	\N
1165	112	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1166	112	sequence	\N	ir.msg_sequence	integer		product	\N	\N	\N	\N
1167	112	type	\N	Type	selection		product	\N	\N	\N	\N
1168	112	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1169	112	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1170	113	company	company.company	Company	many2one		product	\N	\N	\N	\N
1171	113	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1172	113	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1173	113	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1174	113	list_price	\N	List Price	numeric		product	\N	\N	\N	\N
1175	113	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1176	113	template	product.template	Template	many2one		product	\N	\N	\N	\N
1177	113	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1178	113	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1179	114	company	company.company	Company	many2one		product	\N	\N	\N	\N
1180	114	cost_price_method	\N	Cost Price Method	selection		product	\N	\N	\N	\N
1181	114	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1182	114	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1183	114	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1184	114	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1185	114	template	product.template	Template	many2one		product	\N	\N	\N	\N
1186	114	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1187	114	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1188	115	company	company.company	Company	many2one		product	\N	\N	\N	\N
1189	115	cost_price	\N	Cost Price	numeric		product	\N	\N	\N	\N
1190	115	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1191	115	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1192	115	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1193	115	product	product.product	Product	many2one		product	\N	\N	\N	\N
1194	115	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1195	115	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1196	115	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1197	116	category	product.category	Category	many2one		product	\N	\N	\N	\N
1198	116	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1199	116	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1200	116	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1201	116	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1202	116	template	product.template	Template	many2one		product	\N	\N	\N	\N
1203	116	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1204	116	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1205	117	category	product.category	Category	many2one		product	\N	\N	\N	\N
1206	117	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1207	117	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1208	117	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1209	117	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1210	117	template	product.template	Template	many2one		product	\N	\N	\N	\N
1211	117	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1212	117	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1213	118	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1214	118	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1215	118	default_cost_price_method	\N	Default Cost Method	selection	The default cost price method for new products.	product	\N	\N	\N	\N
1216	118	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1217	118	product_sequence	ir.sequence	Product Sequence	many2one	Used to generate the product code.	product	\N	\N	\N	\N
1218	118	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1219	118	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1220	118	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1221	119	create_date	\N	ir.msg_created_by	timestamp		product	\N	\N	\N	\N
1222	119	create_uid	res.user	ir.msg_created_by	many2one		product	\N	\N	\N	\N
1223	119	default_cost_price_method	\N	Default Cost Method	selection	The default cost price method for new products.	product	\N	\N	\N	\N
1224	119	id	\N	ir.msg_ID	integer		product	\N	\N	\N	\N
1225	119	rec_name	\N	ir.msg_record_name	char		product	\N	\N	\N	\N
1226	119	write_date	\N	ir.msg_edited_at	timestamp		product	\N	\N	\N	\N
1227	119	write_uid	res.user	ir.msg_edited_by	many2one		product	\N	\N	\N	\N
1228	120	create_date	\N	ir.msg_created_by	timestamp		taller	\N	\N	\N	\N
1229	120	create_uid	res.user	ir.msg_created_by	many2one		taller	\N	\N	\N	\N
1230	120	id	\N	ir.msg_ID	integer		taller	\N	\N	\N	\N
1231	120	name	\N	Nombre	char		taller	\N	\N	\N	\N
1232	120	rec_name	\N	ir.msg_record_name	char		taller	\N	\N	\N	\N
1233	120	write_date	\N	ir.msg_edited_at	timestamp		taller	\N	\N	\N	\N
1234	120	write_uid	res.user	ir.msg_edited_by	many2one		taller	\N	\N	\N	\N
1247	122	create_date	\N	ir.msg_created_by	timestamp		taller	\N	\N	\N	\N
1248	122	create_uid	res.user	ir.msg_created_by	many2one		taller	\N	\N	\N	\N
1249	122	fecha_baja	\N	Fecha baja	date		taller	\N	\N	\N	\N
1250	122	fecha_matriculacion	\N	Fecha matriculacion	date		taller	\N	\N	\N	\N
1251	122	id	\N	ir.msg_ID	integer		taller	\N	\N	\N	\N
1252	122	marca	taller.marca	Marca	many2one		taller	\N	\N	\N	\N
1253	122	matricula	\N	Matricula	char		taller	\N	\N	\N	\N
1255	122	precio	\N	precio	integer		taller	\N	\N	\N	\N
1257	122	rec_name	\N	ir.msg_record_name	char		taller	\N	\N	\N	\N
1258	122	write_date	\N	ir.msg_edited_at	timestamp		taller	\N	\N	\N	\N
1259	122	write_uid	res.user	ir.msg_edited_by	many2one		taller	\N	\N	\N	\N
1254	122	modelo	taller.modelo	Modelo	many2one		taller	\N	\N	\N	\N
1269	88	coches	taller.coche	Coches	one2many		taller	\N	\N	\N	\N
1235	121	caballos	\N	numero de caballos	integer		taller	\N	\N	\N	\N
1236	121	combustible	\N	tipo de combustible	selection		taller	\N	\N	\N	\N
1237	121	create_date	\N	ir.msg_created_by	timestamp		taller	\N	\N	\N	\N
1238	121	create_uid	res.user	ir.msg_created_by	many2one		taller	\N	\N	\N	\N
1239	121	fecha_lanz	\N	Fecha Lanzamiento	date		taller	\N	\N	\N	\N
1240	121	id	\N	ir.msg_ID	integer		taller	\N	\N	\N	\N
1241	121	marca	taller.marca	Marca	many2one		taller	\N	\N	\N	\N
1242	121	modelo	\N	Modelo	char		taller	\N	\N	\N	\N
1243	121	rec_name	\N	ir.msg_record_name	char		taller	\N	\N	\N	\N
1244	121	write_date	\N	ir.msg_edited_at	timestamp		taller	\N	\N	\N	\N
1245	121	write_uid	res.user	ir.msg_edited_by	many2one		taller	\N	\N	\N	\N
1260	122	propietario	party.party	Propietario	many2one		taller	\N	\N	\N	\N
1264	121	precioMod	\N	precio del modelo	integer		taller	\N	\N	\N	\N
1270	121	productosDelModelo	modelo-producto	productos disponibles	many2many		taller	\N	\N	\N	\N
1271	110	modelos_compatibles	modelo-producto	Modelos compatibles	many2many		taller	\N	\N	\N	\N
1290	125	fecha_baja	\N	Fecha baja	date		taller	\N	\N	\N	\N
1291	125	id	\N	ir.msg_ID	integer		taller	\N	\N	\N	\N
1292	126	id	\N	ir.msg_ID	integer		taller	\N	\N	\N	\N
1293	126	n_coches_adfectados	\N	numero de coches afectados	integer		taller	\N	\N	\N	\N
1294	127	active	\N	ir.msg_active	boolean	ir.msg_active_help	web_user	\N	\N	\N	\N
1295	127	create_date	\N	ir.msg_created_by	timestamp		web_user	\N	\N	\N	\N
1296	127	create_uid	res.user	ir.msg_created_by	many2one		web_user	\N	\N	\N	\N
1297	127	email	\N	E-mail	char		web_user	\N	\N	\N	\N
1298	127	email_token	\N	E-mail Token	char		web_user	\N	\N	\N	\N
1299	127	email_valid	\N	E-mail Valid	boolean		web_user	\N	\N	\N	\N
1300	127	id	\N	ir.msg_ID	integer		web_user	\N	\N	\N	\N
1301	127	party	party.party	Party	many2one		web_user	\N	\N	\N	\N
1302	127	password	\N	Password	char		web_user	\N	\N	\N	\N
1303	127	password_hash	\N	Password Hash	char		web_user	\N	\N	\N	\N
1304	127	rec_name	\N	ir.msg_record_name	char		web_user	\N	\N	\N	\N
1305	127	reset_password_token	\N	Reset Password Token	char		web_user	\N	\N	\N	\N
1306	127	reset_password_token_expire	\N	Reset Password Token Expire	timestamp		web_user	\N	\N	\N	\N
1307	127	write_date	\N	ir.msg_edited_at	timestamp		web_user	\N	\N	\N	\N
1308	127	write_uid	res.user	ir.msg_edited_by	many2one		web_user	\N	\N	\N	\N
1309	128	create_date	\N	ir.msg_created_by	timestamp		web_user	\N	\N	\N	\N
1310	128	create_uid	res.user	ir.msg_created_by	many2one		web_user	\N	\N	\N	\N
1311	128	id	\N	ir.msg_ID	integer		web_user	\N	\N	\N	\N
1312	128	ip_address	\N	IP Address	char		web_user	\N	\N	\N	\N
1313	128	ip_network	\N	IP Network	char		web_user	\N	\N	\N	\N
1314	128	login	\N	Login	char		web_user	\N	\N	\N	\N
1315	128	rec_name	\N	ir.msg_record_name	char		web_user	\N	\N	\N	\N
1316	128	write_date	\N	ir.msg_edited_at	timestamp		web_user	\N	\N	\N	\N
1317	128	write_uid	res.user	ir.msg_edited_by	many2one		web_user	\N	\N	\N	\N
1318	129	create_date	\N	ir.msg_created_by	timestamp		web_user	\N	\N	\N	\N
1319	129	create_uid	res.user	ir.msg_created_by	many2one		web_user	\N	\N	\N	\N
1320	129	id	\N	ir.msg_ID	integer		web_user	\N	\N	\N	\N
1321	129	key	\N	Key	char		web_user	\N	\N	\N	\N
1322	129	rec_name	\N	ir.msg_record_name	char		web_user	\N	\N	\N	\N
1323	129	user	web.user	User	many2one		web_user	\N	\N	\N	\N
1324	129	write_date	\N	ir.msg_edited_at	timestamp		web_user	\N	\N	\N	\N
1325	129	write_uid	res.user	ir.msg_edited_by	many2one		web_user	\N	\N	\N	\N
1	1	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
2	1	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
3	1	hostname	\N	Hostname	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
4	1	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
5	1	language	\N	language	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
6	1	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
7	1	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
8	1	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
9	2	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
10	2	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
11	2	fuzzy	\N	Fuzzy	boolean		ir	2020-09-01 08:50:03.838157	0	\N	\N
12	2	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
13	2	lang	\N	Language	selection		ir	2020-09-01 08:50:03.838157	0	\N	\N
14	2	model	\N	Model	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
15	2	module	\N	Module	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
16	2	name	\N	Field Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
17	2	overriding_module	\N	Overriding Module	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
18	2	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
19	2	res_id	\N	Resource ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
20	2	src	\N	Source	text		ir	2020-09-01 08:50:03.838157	0	\N	\N
21	2	type	\N	Type	selection		ir	2020-09-01 08:50:03.838157	0	\N	\N
22	2	value	\N	Translation Value	text		ir	2020-09-01 08:50:03.838157	0	\N	\N
23	2	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
24	2	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
25	3	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
26	4	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
27	5	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
28	6	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
29	7	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
30	7	language	ir.lang	Language	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
31	8	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
32	8	language	ir.lang	Language	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
33	8	module	ir.module	Module	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
34	9	file	\N	File	binary		ir	2020-09-01 08:50:03.838157	0	\N	\N
35	9	filename	\N	Filename	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
36	9	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
37	9	language	ir.lang	Language	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
38	9	module	ir.module	Module	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
39	10	code	\N	Sequence Code	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
40	10	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
41	10	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
42	10	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
43	10	name	\N	Sequence Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
44	10	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
45	10	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
46	10	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
47	11	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	2020-09-01 08:50:03.838157	0	\N	\N
48	11	code	\N	Sequence Code	selection		ir	2020-09-01 08:50:03.838157	0	\N	\N
49	11	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
50	11	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
51	11	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
52	11	last_timestamp	\N	Last Timestamp	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
53	11	name	\N	Sequence Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
54	11	number_increment	\N	Increment Number	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
55	11	number_next	\N	Next Number	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
56	11	number_next_internal	\N	Next Number	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
57	11	padding	\N	Number padding	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
58	11	prefix	\N	Prefix	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
59	11	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
60	11	suffix	\N	Suffix	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
61	11	timestamp_offset	\N	Timestamp Offset	float		ir	2020-09-01 08:50:03.838157	0	\N	\N
62	11	timestamp_rounding	\N	Timestamp Rounding	float		ir	2020-09-01 08:50:03.838157	0	\N	\N
63	11	type	\N	Type	selection		ir	2020-09-01 08:50:03.838157	0	\N	\N
64	11	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
65	11	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
66	12	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	2020-09-01 08:50:03.838157	0	\N	\N
67	12	code	\N	Sequence Code	selection		ir	2020-09-01 08:50:03.838157	0	\N	\N
68	12	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
69	12	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
70	12	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
71	12	last_timestamp	\N	Last Timestamp	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
72	12	name	\N	Sequence Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
73	12	number_increment	\N	Increment Number	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
74	12	number_next	\N	Next Number	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
75	12	number_next_internal	\N	Next Number	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
76	12	padding	\N	Number padding	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
77	12	prefix	\N	Prefix	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
78	12	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
79	12	suffix	\N	Suffix	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
80	12	timestamp_offset	\N	Timestamp Offset	float		ir	2020-09-01 08:50:03.838157	0	\N	\N
81	12	timestamp_rounding	\N	Timestamp Rounding	float		ir	2020-09-01 08:50:03.838157	0	\N	\N
82	12	type	\N	Type	selection		ir	2020-09-01 08:50:03.838157	0	\N	\N
83	12	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
84	12	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
85	13	action	\N	Action	reference		ir	2020-09-01 08:50:03.838157	0	\N	\N
86	13	action_keywords	ir.action.keyword	Action Keywords	one2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
87	13	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	2020-09-01 08:50:03.838157	0	\N	\N
88	13	childs	ir.ui.menu	Children	one2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
89	13	complete_name	\N	Complete Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
90	13	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
91	13	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
92	13	favorite	\N	Favorite	boolean		ir	2020-09-01 08:50:03.838157	0	\N	\N
93	13	groups	ir.ui.menu-res.group	Groups	many2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
94	13	icon	\N	Icon	selection		ir	2020-09-01 08:50:03.838157	0	\N	\N
95	13	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
96	13	name	\N	Menu	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
97	13	parent	ir.ui.menu	Parent Menu	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
98	13	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
99	13	sequence	\N	ir.msg_sequence	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
100	13	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
101	13	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
102	14	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
103	14	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
104	14	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
105	14	menu	ir.ui.menu	Menu	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
106	14	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
107	14	sequence	\N	ir.msg_sequence	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
108	14	user	res.user	User	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
109	14	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
110	14	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
111	15	arch	\N	View Architecture	text		ir	2020-09-01 08:50:03.838157	0	\N	\N
112	15	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
113	15	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
114	15	data	\N	Data	text		ir	2020-09-01 08:50:03.838157	0	\N	\N
115	15	domain	\N	Domain	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
116	15	field_childs	\N	Children Field	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
117	15	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
118	15	inherit	ir.ui.view	Inherited View	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
119	15	model	\N	Model	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
120	15	module	\N	Module	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
121	15	name	\N	Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
122	15	priority	\N	Priority	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
123	15	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
124	15	type	\N	View Type	selection		ir	2020-09-01 08:50:03.838157	0	\N	\N
125	15	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
126	15	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
127	16	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
128	17	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
129	17	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
130	17	field	\N	Field	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
131	17	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
132	17	model	\N	Model	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
133	17	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
134	17	user	res.user	User	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
135	17	width	\N	Width	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
136	17	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
137	17	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
138	18	child_name	\N	Child Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
139	18	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
140	18	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
141	18	domain	\N	Domain	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
142	18	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
143	18	model	\N	Model	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
144	18	nodes	\N	Expanded Nodes	text		ir	2020-09-01 08:50:03.838157	0	\N	\N
145	18	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
146	18	selected_nodes	\N	Selected Nodes	text		ir	2020-09-01 08:50:03.838157	0	\N	\N
147	18	user	res.user	User	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
148	18	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
149	18	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
150	19	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
151	19	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
152	19	domain	\N	Domain	char	The PYSON domain.	ir	2020-09-01 08:50:03.838157	0	\N	\N
153	19	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
154	19	model	\N	Model	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
155	19	name	\N	Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
156	19	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
157	19	user	res.user	User	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
158	19	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
159	19	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
160	20	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
161	20	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
162	20	icon	\N	Icon	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
163	20	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
164	20	module	\N	Module	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
165	20	name	\N	Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
166	20	path	\N	SVG Path	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
167	20	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
168	20	sequence	\N	ir.msg_sequence	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
247	24	res_model	\N	Model	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
169	20	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
170	20	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
171	21	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	2020-09-01 08:50:03.838157	0	\N	\N
172	21	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
173	21	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
174	21	groups	ir.action-res.group	Groups	many2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
175	21	icon	ir.ui.icon	Icon	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
176	21	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
177	21	keywords	ir.action.keyword	Keywords	one2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
178	21	name	\N	Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
179	21	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
180	21	type	\N	Type	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
181	21	usage	\N	Usage	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
182	21	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
183	21	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
184	22	action	ir.action	Action	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
185	22	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
186	22	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
187	22	groups	res.group	Groups	one2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
188	22	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
189	22	keyword	\N	Keyword	selection		ir	2020-09-01 08:50:03.838157	0	\N	\N
190	22	model	\N	Model	reference		ir	2020-09-01 08:50:03.838157	0	\N	\N
191	22	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
192	22	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
193	22	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
194	23	action	ir.action	Action	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
195	23	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	2020-09-01 08:50:03.838157	0	\N	\N
196	23	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
197	23	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
198	23	direct_print	\N	Direct Print	boolean		ir	2020-09-01 08:50:03.838157	0	\N	\N
199	23	email	\N	Email	char	Python dictonary where keys define "to" "cc" "subject"\nExample: {'to': 'test@example.com', 'cc': 'user@example.com'}	ir	2020-09-01 08:50:03.838157	0	\N	\N
200	23	extension	\N	Extension	selection	Leave empty for the same as template, see LibreOffice documentation for compatible format.	ir	2020-09-01 08:50:03.838157	0	\N	\N
201	23	groups	ir.action-res.group	Groups	many2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
202	23	icon	ir.ui.icon	Icon	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
203	23	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
204	23	is_custom	\N	Is Custom	boolean		ir	2020-09-01 08:50:03.838157	0	\N	\N
205	23	keywords	ir.action.keyword	Keywords	one2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
206	23	model	\N	Model	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
207	23	module	\N	Module	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
208	23	name	\N	Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
209	23	pyson_email	\N	PySON Email	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
210	23	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
211	23	report	\N	Path	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
212	23	report_content	\N	Content	binary		ir	2020-09-01 08:50:03.838157	0	\N	\N
213	23	report_content_custom	\N	Content	binary		ir	2020-09-01 08:50:03.838157	0	\N	\N
214	23	report_content_html	\N	Content HTML	binary		ir	2020-09-01 08:50:03.838157	0	\N	\N
215	23	report_content_name	\N	Content Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
216	23	report_name	\N	Internal Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
217	23	single	\N	Single	boolean	Check if the template works only for one record.	ir	2020-09-01 08:50:03.838157	0	\N	\N
218	23	template_extension	\N	Template Extension	selection		ir	2020-09-01 08:50:03.838157	0	\N	\N
219	23	translatable	\N	Translatable	boolean	Uncheck to disable translations for this report.	ir	2020-09-01 08:50:03.838157	0	\N	\N
220	23	type	\N	Type	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
221	23	usage	\N	Usage	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
222	23	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
223	23	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
224	24	act_window_domains	ir.action.act_window.domain	Domains	one2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
225	24	act_window_views	ir.action.act_window.view	Views	one2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
226	24	action	ir.action	Action	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
227	24	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	2020-09-01 08:50:03.838157	0	\N	\N
228	24	context	\N	Context Value	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
229	24	context_domain	\N	Context Domain	char	Part of the domain that will be evaluated on each refresh.	ir	2020-09-01 08:50:03.838157	0	\N	\N
230	24	context_model	\N	Context Model	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
231	24	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
232	24	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
233	24	domain	\N	Domain Value	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
234	24	domains	\N	Domains	binary		ir	2020-09-01 08:50:03.838157	0	\N	\N
235	24	groups	ir.action-res.group	Groups	many2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
236	24	icon	ir.ui.icon	Icon	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
237	24	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
238	24	keywords	ir.action.keyword	Keywords	one2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
239	24	limit	\N	Limit	integer	Default limit for the list view.	ir	2020-09-01 08:50:03.838157	0	\N	\N
240	24	name	\N	Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
241	24	order	\N	Order Value	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
242	24	pyson_context	\N	PySON Context	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
243	24	pyson_domain	\N	PySON Domain	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
244	24	pyson_order	\N	PySON Order	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
245	24	pyson_search_value	\N	PySON Search Criteria	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
246	24	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
248	24	search_value	\N	Search Criteria	char	Default search criteria for the list view.	ir	2020-09-01 08:50:03.838157	0	\N	\N
249	24	type	\N	Type	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
250	24	usage	\N	Usage	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
251	24	views	\N	Views	binary		ir	2020-09-01 08:50:03.838157	0	\N	\N
252	24	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
253	24	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
254	25	act_window	ir.action.act_window	Action	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
255	25	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	2020-09-01 08:50:03.838157	0	\N	\N
256	25	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
257	25	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
258	25	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
259	25	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
260	25	sequence	\N	ir.msg_sequence	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
261	25	view	ir.ui.view	View	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
262	25	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
263	25	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
264	26	act_window	ir.action.act_window	Action	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
265	26	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	2020-09-01 08:50:03.838157	0	\N	\N
266	26	count	\N	Count	boolean		ir	2020-09-01 08:50:03.838157	0	\N	\N
267	26	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
268	26	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
269	26	domain	\N	Domain	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
270	26	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
271	26	name	\N	Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
272	26	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
273	26	sequence	\N	ir.msg_sequence	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
274	26	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
275	26	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
276	27	action	ir.action	Action	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
277	27	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	2020-09-01 08:50:03.838157	0	\N	\N
278	27	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
279	27	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
280	27	email	\N	Email	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
281	27	groups	ir.action-res.group	Groups	many2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
282	27	icon	ir.ui.icon	Icon	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
283	27	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
284	27	keywords	ir.action.keyword	Keywords	one2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
285	27	model	\N	Model	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
286	27	name	\N	Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
287	27	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
288	27	type	\N	Type	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
289	27	usage	\N	Usage	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
290	27	window	\N	Window	boolean	Run wizard in a new window.	ir	2020-09-01 08:50:03.838157	0	\N	\N
291	27	wiz_name	\N	Wizard name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
292	27	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
293	27	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
294	28	action	ir.action	Action	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
295	28	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	2020-09-01 08:50:03.838157	0	\N	\N
296	28	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
297	28	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
298	28	groups	ir.action-res.group	Groups	many2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
299	28	icon	ir.ui.icon	Icon	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
300	28	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
301	28	keywords	ir.action.keyword	Keywords	one2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
302	28	name	\N	Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
303	28	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
304	28	type	\N	Type	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
305	28	url	\N	Action Url	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
306	28	usage	\N	Usage	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
307	28	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
308	28	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
309	29	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
310	29	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
311	29	fields	ir.model.field	Fields	one2many		ir	2020-09-01 08:50:03.838157	0	\N	\N
312	29	global_search_p	\N	Global Search	boolean		ir	2020-09-01 08:50:03.838157	0	\N	\N
313	29	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
314	29	info	\N	Information	text		ir	2020-09-01 08:50:03.838157	0	\N	\N
315	29	model	\N	Model Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
316	29	module	\N	Module	char	Module in which this model is defined.	ir	2020-09-01 08:50:03.838157	0	\N	\N
317	29	name	\N	Model Description	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
318	29	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
319	29	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
320	29	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
321	30	create_date	\N	ir.msg_created_by	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
322	30	create_uid	res.user	ir.msg_created_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
323	30	field_description	\N	Field Description	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
324	30	help	\N	Help	text		ir	2020-09-01 08:50:03.838157	0	\N	\N
325	30	id	\N	ir.msg_ID	integer		ir	2020-09-01 08:50:03.838157	0	\N	\N
326	30	model	ir.model	Model	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
327	30	module	\N	Module	char	Module in which this field is defined.	ir	2020-09-01 08:50:03.838157	0	\N	\N
328	30	name	\N	Name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
329	30	rec_name	\N	ir.msg_record_name	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
330	30	relation	\N	Model Relation	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
331	30	ttype	\N	Field Type	char		ir	2020-09-01 08:50:03.838157	0	\N	\N
332	30	write_date	\N	ir.msg_edited_at	timestamp		ir	2020-09-01 08:50:03.838157	0	\N	\N
333	30	write_uid	res.user	ir.msg_edited_by	many2one		ir	2020-09-01 08:50:03.838157	0	\N	\N
334	31	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	\N	\N	\N	\N
335	31	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
336	31	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
337	31	description	\N	Description	text		ir	\N	\N	\N	\N
338	31	group	res.group	Group	many2one		ir	\N	\N	\N	\N
339	31	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
340	31	model	ir.model	Model	many2one		ir	\N	\N	\N	\N
341	31	perm_create	\N	Create Access	boolean		ir	\N	\N	\N	\N
342	31	perm_delete	\N	Delete Access	boolean		ir	\N	\N	\N	\N
343	31	perm_read	\N	Read Access	boolean		ir	\N	\N	\N	\N
344	31	perm_write	\N	Write Access	boolean		ir	\N	\N	\N	\N
345	31	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
346	31	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
347	31	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
348	32	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	\N	\N	\N	\N
349	32	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
350	32	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
351	32	description	\N	Description	text		ir	\N	\N	\N	\N
352	32	field	ir.model.field	Field	many2one		ir	\N	\N	\N	\N
353	32	group	res.group	Group	many2one		ir	\N	\N	\N	\N
354	32	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
355	32	perm_create	\N	Create Access	boolean		ir	\N	\N	\N	\N
356	32	perm_delete	\N	Delete Access	boolean		ir	\N	\N	\N	\N
357	32	perm_read	\N	Read Access	boolean		ir	\N	\N	\N	\N
358	32	perm_write	\N	Write Access	boolean		ir	\N	\N	\N	\N
359	32	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
360	32	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
361	32	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
362	33	clicks	ir.model.button.click	Clicks	one2many		ir	\N	\N	\N	\N
363	33	confirm	\N	Confirm	text	Text to ask user confirmation when clicking the button.	ir	\N	\N	\N	\N
364	33	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
365	33	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
366	33	groups	ir.model.button-res.group	Groups	many2many		ir	\N	\N	\N	\N
367	33	help	\N	Help	text		ir	\N	\N	\N	\N
368	33	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
369	33	model	ir.model	Model	many2one		ir	\N	\N	\N	\N
370	33	name	\N	Name	char		ir	\N	\N	\N	\N
371	33	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
372	33	reset	ir.model.button-button.reset	Reset	many2many		ir	\N	\N	\N	\N
373	33	reset_by	ir.model.button-button.reset	Reset by	many2many	Button that should reset the rules.	ir	\N	\N	\N	\N
374	33	rules	ir.model.button.rule	Rules	one2many		ir	\N	\N	\N	\N
375	33	string	\N	Label	char		ir	\N	\N	\N	\N
376	33	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
377	33	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
378	34	button	ir.model.button	Button	many2one		ir	\N	\N	\N	\N
379	34	condition	\N	Condition	char	A PYSON statement evaluated with the record represented by "self"\nIt activate the rule if true.	ir	\N	\N	\N	\N
380	34	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
381	34	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
382	34	description	\N	Description	char		ir	\N	\N	\N	\N
383	34	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
384	34	number_user	\N	Number of User	integer		ir	\N	\N	\N	\N
385	34	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
386	34	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
387	34	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
388	35	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	\N	\N	\N	\N
389	35	button	ir.model.button	Button	many2one		ir	\N	\N	\N	\N
390	35	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
391	35	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
392	35	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
393	35	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
394	35	record_id	\N	Record ID	integer		ir	\N	\N	\N	\N
395	35	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
396	35	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
397	36	button	ir.model.button	Button	many2one		ir	\N	\N	\N	\N
398	36	button_ruled	ir.model.button	Button Ruled	many2one		ir	\N	\N	\N	\N
399	36	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
400	36	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
401	36	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
402	36	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
403	36	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
404	36	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
405	37	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
406	37	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
407	37	db_id	\N	Resource ID	integer	The id of the record in the database.	ir	\N	\N	\N	\N
408	37	fs_id	\N	Identifier on File System	char	The id of the record as known on the file system.	ir	\N	\N	\N	\N
409	37	fs_values	\N	Values on File System	text		ir	\N	\N	\N	\N
410	37	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
411	37	model	\N	Model	char		ir	\N	\N	\N	\N
412	37	module	\N	Module	char		ir	\N	\N	\N	\N
413	37	noupdate	\N	No Update	boolean		ir	\N	\N	\N	\N
414	37	out_of_sync	\N	Out of Sync	boolean		ir	\N	\N	\N	\N
415	37	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
416	37	values	\N	Values	text		ir	\N	\N	\N	\N
417	37	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
418	37	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
419	38	filter	\N	Filter	text	Entering a Python Regular Expression will exclude matching models from the graph.	ir	\N	\N	\N	\N
420	38	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
421	38	level	\N	Level	integer		ir	\N	\N	\N	\N
422	39	copy_to_resources	\N	Copy to Resources	multiselection		ir	\N	\N	\N	\N
423	39	copy_to_resources_visible	\N	Copy to Resources Visible	boolean		ir	\N	\N	\N	\N
424	39	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
425	39	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
426	39	data	\N	Data	binary		ir	\N	\N	\N	\N
427	39	data_size	\N	Data size	integer		ir	\N	\N	\N	\N
428	39	description	\N	Description	text		ir	\N	\N	\N	\N
429	39	file_id	\N	File ID	char		ir	\N	\N	\N	\N
430	39	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
431	39	last_modification	\N	Last Modification	datetime		ir	\N	\N	\N	\N
432	39	last_user	\N	Last User	char		ir	\N	\N	\N	\N
433	39	link	\N	Link	char		ir	\N	\N	\N	\N
434	39	name	\N	Name	char		ir	\N	\N	\N	\N
435	39	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
436	39	resource	\N	Resource	reference		ir	\N	\N	\N	\N
437	39	summary	\N	Summary	char		ir	\N	\N	\N	\N
438	39	type	\N	Type	selection		ir	\N	\N	\N	\N
439	39	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
440	39	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
441	40	copy_to_resources	\N	Copy to Resources	multiselection		ir	\N	\N	\N	\N
442	40	copy_to_resources_visible	\N	Copy to Resources Visible	boolean		ir	\N	\N	\N	\N
443	40	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
444	40	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
445	40	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
446	40	last_modification	\N	Last Modification	datetime		ir	\N	\N	\N	\N
447	40	last_user	\N	Last User	char		ir	\N	\N	\N	\N
448	40	message	\N	Message	text		ir	\N	\N	\N	\N
449	40	message_wrapped	\N	Message	text		ir	\N	\N	\N	\N
450	40	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
451	40	resource	\N	Resource	reference		ir	\N	\N	\N	\N
452	40	unread	\N	Unread	boolean		ir	\N	\N	\N	\N
453	40	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
454	40	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
455	41	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
456	41	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
457	41	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
458	41	note	ir.note	Note	many2one		ir	\N	\N	\N	\N
459	41	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
460	41	user	res.user	User	many2one		ir	\N	\N	\N	\N
461	41	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
462	41	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
463	42	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	\N	\N	\N	\N
464	42	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
465	42	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
466	42	day	\N	Day	integer		ir	\N	\N	\N	\N
467	42	hour	\N	Hour	integer		ir	\N	\N	\N	\N
468	42	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
469	42	interval_number	\N	Interval Number	integer		ir	\N	\N	\N	\N
470	42	interval_type	\N	Interval Type	selection		ir	\N	\N	\N	\N
471	42	method	\N	Method	selection		ir	\N	\N	\N	\N
472	42	minute	\N	Minute	integer		ir	\N	\N	\N	\N
473	42	next_call	\N	Next Call	datetime		ir	\N	\N	\N	\N
474	42	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
475	42	weekday	ir.calendar.day	Day of Week	many2one		ir	\N	\N	\N	\N
476	42	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
477	42	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
478	43	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	\N	\N	\N	\N
479	43	am	\N	AM	char		ir	\N	\N	\N	\N
480	43	code	\N	Code	char	RFC 4646 tag: http://tools.ietf.org/html/rfc4646	ir	\N	\N	\N	\N
481	43	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
482	43	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
483	43	date	\N	Date	char		ir	\N	\N	\N	\N
484	43	decimal_point	\N	Decimal Separator	char		ir	\N	\N	\N	\N
485	43	direction	\N	Direction	selection		ir	\N	\N	\N	\N
486	43	grouping	\N	Grouping	char		ir	\N	\N	\N	\N
487	43	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
488	43	mon_decimal_point	\N	Decimal Separator	char		ir	\N	\N	\N	\N
489	43	mon_grouping	\N	Grouping	char		ir	\N	\N	\N	\N
490	43	mon_thousands_sep	\N	Thousands Separator	char		ir	\N	\N	\N	\N
491	43	n_cs_precedes	\N	Negative Currency Symbol Precedes	boolean		ir	\N	\N	\N	\N
492	43	n_sep_by_space	\N	Negative Separate by Space	boolean		ir	\N	\N	\N	\N
493	43	n_sign_posn	\N	Negative Sign Position	integer		ir	\N	\N	\N	\N
494	43	name	\N	Name	char		ir	\N	\N	\N	\N
495	43	negative_sign	\N	Negative Sign	char		ir	\N	\N	\N	\N
496	43	p_cs_precedes	\N	Positive Currency Symbol Precedes	boolean		ir	\N	\N	\N	\N
497	43	p_sep_by_space	\N	Positive Separate by Space	boolean		ir	\N	\N	\N	\N
498	43	p_sign_posn	\N	Positive Sign Position	integer		ir	\N	\N	\N	\N
499	43	parent	\N	Parent Code	char	Code of the exceptional parent	ir	\N	\N	\N	\N
500	43	pm	\N	PM	char		ir	\N	\N	\N	\N
501	43	positive_sign	\N	Positive Sign	char		ir	\N	\N	\N	\N
502	43	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
503	43	thousands_sep	\N	Thousands Separator	char		ir	\N	\N	\N	\N
504	43	translatable	\N	Translatable	boolean		ir	\N	\N	\N	\N
505	43	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
506	43	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
507	44	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
508	44	languages	ir.lang	Languages	many2many		ir	\N	\N	\N	\N
509	45	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
510	45	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
511	45	export_fields	ir.export.line	Fields	one2many		ir	\N	\N	\N	\N
512	45	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
513	45	name	\N	Name	char		ir	\N	\N	\N	\N
514	45	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
515	45	resource	\N	Resource	char		ir	\N	\N	\N	\N
516	45	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
517	45	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
518	46	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
519	46	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
520	46	export	ir.export	Export	many2one		ir	\N	\N	\N	\N
521	46	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
522	46	name	\N	Name	char		ir	\N	\N	\N	\N
523	46	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
524	46	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
525	46	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
526	47	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
527	47	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
528	47	default_p	\N	Default	boolean	Add this rule to all users by default.	ir	\N	\N	\N	\N
529	47	global_p	\N	Global	boolean	Make the rule global \nso every users must follow this rule.	ir	\N	\N	\N	\N
530	47	groups	ir.rule.group-res.group	Groups	many2many		ir	\N	\N	\N	\N
531	47	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
532	47	model	ir.model	Model	many2one		ir	\N	\N	\N	\N
533	47	name	\N	Name	char	Displayed to users when access error is raised for this rule.	ir	\N	\N	\N	\N
534	47	perm_create	\N	Create Access	boolean		ir	\N	\N	\N	\N
535	47	perm_delete	\N	Delete Access	boolean		ir	\N	\N	\N	\N
536	47	perm_read	\N	Read Access	boolean		ir	\N	\N	\N	\N
537	47	perm_write	\N	Write Access	boolean		ir	\N	\N	\N	\N
538	47	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
539	47	rules	ir.rule	Tests	one2many	The rule is satisfied if at least one test is True.	ir	\N	\N	\N	\N
540	47	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
541	47	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
542	48	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
543	48	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
545	48	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
546	48	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
547	48	rule_group	ir.rule.group	Group	many2one		ir	\N	\N	\N	\N
548	48	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
549	48	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
550	49	childs	ir.module	Childs	one2many		ir	\N	\N	\N	\N
551	49	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
552	49	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
553	49	dependencies	ir.module.dependency	Dependencies	one2many		ir	\N	\N	\N	\N
554	49	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
555	49	name	\N	Name	char		ir	\N	\N	\N	\N
556	49	parents	ir.module	Parents	one2many		ir	\N	\N	\N	\N
557	49	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
558	49	state	\N	State	selection		ir	\N	\N	\N	\N
559	49	version	\N	Version	char		ir	\N	\N	\N	\N
560	49	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
561	49	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
562	50	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
563	50	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
564	50	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
565	50	module	ir.module	Module	many2one		ir	\N	\N	\N	\N
566	50	name	\N	Name	char		ir	\N	\N	\N	\N
567	50	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
568	50	state	\N	State	selection		ir	\N	\N	\N	\N
569	50	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
570	50	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
571	51	action	ir.action	Action	many2one		ir	\N	\N	\N	\N
572	51	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
573	51	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
574	51	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
575	51	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
576	51	sequence	\N	ir.msg_sequence	integer		ir	\N	\N	\N	\N
577	51	state	\N	State	selection		ir	\N	\N	\N	\N
578	51	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
579	51	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
580	52	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
581	53	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
582	53	percentage	\N	Percentage	float		ir	\N	\N	\N	\N
583	54	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
584	55	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
585	55	module_info	\N	Modules to update	text		ir	\N	\N	\N	\N
586	56	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
587	57	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
588	57	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
589	57	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
590	57	name	\N	Name	char		ir	\N	\N	\N	\N
591	57	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
592	57	timestamp	\N	Timestamp	datetime		ir	\N	\N	\N	\N
593	57	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
594	57	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
595	58	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
596	59	action	\N	Action	selection		ir	\N	\N	\N	\N
597	59	active	\N	ir.msg_active	boolean	ir.msg_active_help	ir	\N	\N	\N	\N
598	59	condition	\N	Condition	char	A PYSON statement evaluated with record represented by "self"\nIt triggers the action if true.	ir	\N	\N	\N	\N
599	59	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
600	59	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
601	59	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
602	59	limit_number	\N	Limit Number	integer	Limit the number of call to "Action Function" by records.\n0 for no limit.	ir	\N	\N	\N	\N
603	59	minimum_time_delay	\N	Minimum Delay	timedelta	Set a minimum time delay between call to "Action Function" for the same record.\nempty for no delay.	ir	\N	\N	\N	\N
604	59	model	ir.model	Model	many2one		ir	\N	\N	\N	\N
605	59	name	\N	Name	char		ir	\N	\N	\N	\N
606	59	on_create	\N	On Create	boolean		ir	\N	\N	\N	\N
607	59	on_delete	\N	On Delete	boolean		ir	\N	\N	\N	\N
608	59	on_time	\N	On Time	boolean		ir	\N	\N	\N	\N
609	59	on_write	\N	On Write	boolean		ir	\N	\N	\N	\N
610	59	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
611	59	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
612	59	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
613	60	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
614	60	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
615	60	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
616	60	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
617	60	record_id	\N	Record ID	integer		ir	\N	\N	\N	\N
618	60	trigger	ir.trigger	Trigger	many2one		ir	\N	\N	\N	\N
619	60	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
620	60	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
621	61	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
622	61	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
623	61	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
624	61	key	\N	Key	char		ir	\N	\N	\N	\N
625	61	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
626	61	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
627	61	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
628	62	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
629	62	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
630	62	data	\N	Data	text		ir	\N	\N	\N	\N
631	62	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
632	62	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
633	62	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
634	62	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
635	63	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
636	63	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
637	63	data	\N	Data	dict		ir	\N	\N	\N	\N
638	63	dequeued_at	\N	Dequeued at	timestamp		ir	\N	\N	\N	\N
639	63	enqueued_at	\N	Enqueued at	timestamp		ir	\N	\N	\N	\N
640	63	expected_at	\N	Expected at	timestamp	When the task should be done.	ir	\N	\N	\N	\N
641	63	finished_at	\N	Finished at	timestamp		ir	\N	\N	\N	\N
642	63	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
643	63	name	\N	Name	char		ir	\N	\N	\N	\N
644	63	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
645	63	scheduled_at	\N	Scheduled at	timestamp	When the task can start.	ir	\N	\N	\N	\N
646	63	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
647	63	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
648	64	abbreviation	\N	Abbreviation	char		ir	\N	\N	\N	\N
649	64	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
650	64	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
651	64	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
652	64	index	\N	Index	integer		ir	\N	\N	\N	\N
653	64	name	\N	Name	char		ir	\N	\N	\N	\N
654	64	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
655	64	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
656	64	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
657	65	abbreviation	\N	Abbreviation	char		ir	\N	\N	\N	\N
658	65	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
659	65	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
660	65	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
661	65	index	\N	Index	integer		ir	\N	\N	\N	\N
662	65	name	\N	Name	char		ir	\N	\N	\N	\N
663	65	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
664	65	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
665	65	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
666	66	create_date	\N	ir.msg_created_by	timestamp		ir	\N	\N	\N	\N
667	66	create_uid	res.user	ir.msg_created_by	many2one		ir	\N	\N	\N	\N
668	66	id	\N	ir.msg_ID	integer		ir	\N	\N	\N	\N
669	66	rec_name	\N	ir.msg_record_name	char		ir	\N	\N	\N	\N
670	66	text	\N	Text	text		ir	\N	\N	\N	\N
671	66	write_date	\N	ir.msg_edited_at	timestamp		ir	\N	\N	\N	\N
672	66	write_uid	res.user	ir.msg_edited_by	many2one		ir	\N	\N	\N	\N
673	67	active	\N	ir.msg_active	boolean	ir.msg_active_help	res	\N	\N	\N	\N
674	67	buttons	ir.model.button-res.group	Buttons	many2many		res	\N	\N	\N	\N
675	67	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
676	67	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
677	67	field_access	ir.model.field.access	Access Field	one2many		res	\N	\N	\N	\N
678	67	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
679	67	menu_access	ir.ui.menu-res.group	Access Menu	many2many		res	\N	\N	\N	\N
680	67	model_access	ir.model.access	Access Model	one2many		res	\N	\N	\N	\N
681	67	name	\N	Name	char		res	\N	\N	\N	\N
682	67	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
683	67	rule_groups	ir.rule.group-res.group	Rules	many2many		res	\N	\N	\N	\N
684	67	users	res.user-res.group	Users	many2many		res	\N	\N	\N	\N
685	67	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
686	67	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
687	68	actions	res.user-ir.action	Actions	many2many	Actions that will be run at login.	res	\N	\N	\N	\N
688	68	active	\N	ir.msg_active	boolean	ir.msg_active_help	res	\N	\N	\N	\N
689	68	applications	res.user.application	Applications	one2many		res	\N	\N	\N	\N
690	68	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
691	68	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
692	68	email	\N	Email	char		res	\N	\N	\N	\N
693	68	groups	res.user-res.group	Groups	many2many		res	\N	\N	\N	\N
694	68	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
695	68	language	ir.lang	Language	many2one		res	\N	\N	\N	\N
696	68	language_direction	\N	Language Direction	char		res	\N	\N	\N	\N
697	68	login	\N	Login	char		res	\N	\N	\N	\N
698	68	menu	ir.action	Menu Action	many2one		res	\N	\N	\N	\N
699	68	name	\N	Name	char		res	\N	\N	\N	\N
700	68	password	\N	Password	char		res	\N	\N	\N	\N
701	68	password_hash	\N	Password Hash	char		res	\N	\N	\N	\N
702	68	password_reset	\N	Reset Password	char		res	\N	\N	\N	\N
703	68	password_reset_expire	\N	Reset Password Expire	timestamp		res	\N	\N	\N	\N
704	68	pyson_menu	\N	PySON Menu	char		res	\N	\N	\N	\N
705	68	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
706	68	sessions	\N	Sessions	integer		res	\N	\N	\N	\N
707	68	signature	\N	Signature	text		res	\N	\N	\N	\N
708	68	status_bar	\N	Status Bar	char		res	\N	\N	\N	\N
709	68	warnings	res.user.warning	Warnings	one2many		res	\N	\N	\N	\N
710	68	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
711	68	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
712	69	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
713	69	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
714	69	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
715	69	ip_address	\N	IP Address	char		res	\N	\N	\N	\N
716	69	ip_network	\N	IP Network	char		res	\N	\N	\N	\N
717	69	login	\N	Login	char		res	\N	\N	\N	\N
718	69	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
719	69	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
720	69	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
721	70	action	ir.action	Action	many2one		res	\N	\N	\N	\N
722	70	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
723	70	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
724	70	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
725	70	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
726	70	user	res.user	User	many2one		res	\N	\N	\N	\N
727	70	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
728	70	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
729	71	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
730	71	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
731	71	group	res.group	Group	many2one		res	\N	\N	\N	\N
732	71	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
733	71	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
734	71	user	res.user	User	many2one		res	\N	\N	\N	\N
735	71	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
736	71	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
737	72	always	\N	Always	boolean		res	\N	\N	\N	\N
738	72	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
739	72	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
740	72	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
741	72	name	\N	Name	char		res	\N	\N	\N	\N
742	72	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
743	72	user	res.user	User	many2one		res	\N	\N	\N	\N
744	72	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
745	72	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
746	73	application	\N	Application	selection		res	\N	\N	\N	\N
747	73	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
748	73	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
749	73	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
750	73	key	\N	Key	char		res	\N	\N	\N	\N
751	73	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
752	73	state	\N	State	selection		res	\N	\N	\N	\N
753	73	user	res.user	User	many2one		res	\N	\N	\N	\N
754	73	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
755	73	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
756	74	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
757	75	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
758	75	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
759	75	group	res.group	Group	many2one		res	\N	\N	\N	\N
760	75	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
761	75	menu	ir.ui.menu	Menu	many2one		res	\N	\N	\N	\N
762	75	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
763	75	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
764	75	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
765	76	action	ir.action	Action	many2one		res	\N	\N	\N	\N
766	76	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
767	76	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
768	76	group	res.group	Group	many2one		res	\N	\N	\N	\N
769	76	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
770	76	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
771	76	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
772	76	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
773	77	active	\N	ir.msg_active	boolean	ir.msg_active_help	res	\N	\N	\N	\N
774	77	button	ir.model.button	Button	many2one		res	\N	\N	\N	\N
775	77	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
776	77	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
777	77	group	res.group	Group	many2one		res	\N	\N	\N	\N
778	77	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
779	77	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
780	77	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
781	77	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
782	34	group	res.group	Group	many2one		res	\N	\N	\N	\N
783	35	user	res.user	User	many2one		res	\N	\N	\N	\N
784	78	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
785	78	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
786	78	group	res.group	Group	many2one		res	\N	\N	\N	\N
787	78	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
788	78	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
789	78	rule_group	ir.rule.group	Rule Group	many2one		res	\N	\N	\N	\N
790	78	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
791	78	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
792	10	groups	ir.sequence.type-res.group	User Groups	many2many	Groups allowed to edit the sequences of this type.	res	\N	\N	\N	\N
793	79	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
794	79	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
795	79	group	res.group	User Groups	many2one		res	\N	\N	\N	\N
796	79	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
797	79	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
798	79	sequence_type	ir.sequence.type	Sequence Type	many2one		res	\N	\N	\N	\N
799	79	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
800	79	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
801	11	groups	res.group	User Groups	many2many		res	\N	\N	\N	\N
802	12	groups	res.group	User Groups	many2many		res	\N	\N	\N	\N
803	45	groups	ir.export-res.group	Groups	many2many	The user groups that can use the export.	res	\N	\N	\N	\N
804	45	write_groups	ir.export-write-res.group	Modification Groups	many2many	The user groups that can modify the export.	res	\N	\N	\N	\N
805	80	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
806	80	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
807	80	export	ir.export	Export	many2one		res	\N	\N	\N	\N
808	80	group	res.group	Group	many2one		res	\N	\N	\N	\N
809	80	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
810	80	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
811	80	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
812	80	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
813	81	create_date	\N	ir.msg_created_by	timestamp		res	\N	\N	\N	\N
814	81	create_uid	res.user	ir.msg_created_by	many2one		res	\N	\N	\N	\N
815	81	export	ir.export	Export	many2one		res	\N	\N	\N	\N
816	81	group	res.group	Group	many2one		res	\N	\N	\N	\N
817	81	id	\N	ir.msg_ID	integer		res	\N	\N	\N	\N
818	81	rec_name	\N	ir.msg_record_name	char		res	\N	\N	\N	\N
819	81	write_date	\N	ir.msg_edited_at	timestamp		res	\N	\N	\N	\N
820	81	write_uid	res.user	ir.msg_edited_by	many2one		res	\N	\N	\N	\N
1246	120	modelos	taller.modelo	Modelos	one2many		taller	\N	\N	\N	\N
1261	122	caballosImp	\N	nº caballos	integer		taller	\N	\N	\N	\N
1262	122	fechaImp	\N	fecha de lanzamiento	date		taller	\N	\N	\N	\N
1272	123	create_date	\N	ir.msg_created_by	timestamp		taller	\N	\N	\N	\N
1273	123	create_uid	res.user	ir.msg_created_by	many2one		taller	\N	\N	\N	\N
1274	123	id	\N	ir.msg_ID	integer		taller	\N	\N	\N	\N
1275	123	modelo	taller.modelo	modelo	many2one		taller	\N	\N	\N	\N
1276	123	producto	product.template	producto	many2one		taller	\N	\N	\N	\N
1277	123	rec_name	\N	ir.msg_record_name	char		taller	\N	\N	\N	\N
1278	123	write_date	\N	ir.msg_edited_at	timestamp		taller	\N	\N	\N	\N
1279	123	write_uid	res.user	ir.msg_edited_by	many2one		taller	\N	\N	\N	\N
\.


--
-- Data for Name: ir_model_field_access; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_model_field_access (id, active, create_date, create_uid, description, field, "group", perm_create, perm_delete, perm_read, perm_write, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_module; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_module (id, create_uid, create_date, write_date, write_uid, name, state) FROM stdin;
1	0	2020-09-01 08:49:55.447985	\N	\N	ir	activated
2	0	2020-09-01 08:49:55.447985	\N	\N	res	activated
7	0	2020-09-01 08:50:46.151904	\N	\N	tests	not activated
8	0	2020-09-01 08:50:46.151904	\N	\N	country	activated
3	0	2020-09-01 08:50:46.151904	\N	\N	currency	activated
4	0	2020-09-01 08:50:46.151904	\N	\N	party	activated
5	0	2020-09-01 08:50:46.151904	\N	\N	company	activated
6	0	2020-09-01 08:50:46.151904	\N	\N	product	activated
10	0	2020-09-16 08:36:14.823954	\N	\N	web_user	activated
9	0	2020-09-01 08:50:46.151904	\N	\N	taller	activated
\.


--
-- Data for Name: ir_module_config_wizard_item; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_module_config_wizard_item (id, action, create_date, create_uid, sequence, state, write_date, write_uid) FROM stdin;
1	33	2020-09-01 08:49:55.946968	0	10	open	\N	\N
2	47	2020-09-01 08:49:55.946968	0	10000	open	\N	\N
3	52	2020-09-01 08:50:28.180942	0	10	open	\N	\N
4	72	2020-09-02 07:24:29.514826	0	10	open	\N	\N
\.


--
-- Data for Name: ir_module_dependency; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_module_dependency (id, create_uid, create_date, write_date, write_uid, name, module) FROM stdin;
1	0	2020-09-01 08:49:55.447985	\N	\N	ir	2
2	0	2020-09-01 08:50:46.151904	\N	\N	ir	3
3	0	2020-09-01 08:50:46.151904	\N	\N	res	3
4	0	2020-09-01 08:50:46.151904	\N	\N	country	4
5	0	2020-09-01 08:50:46.151904	\N	\N	ir	4
6	0	2020-09-01 08:50:46.151904	\N	\N	res	4
7	0	2020-09-01 08:50:46.151904	\N	\N	currency	5
8	0	2020-09-01 08:50:46.151904	\N	\N	ir	5
9	0	2020-09-01 08:50:46.151904	\N	\N	party	5
10	0	2020-09-01 08:50:46.151904	\N	\N	res	5
11	0	2020-09-01 08:50:46.151904	\N	\N	company	6
12	0	2020-09-01 08:50:46.151904	\N	\N	ir	6
13	0	2020-09-01 08:50:46.151904	\N	\N	res	6
14	0	2020-09-01 08:50:46.151904	\N	\N	ir	7
15	0	2020-09-01 08:50:46.151904	\N	\N	res	7
16	0	2020-09-01 08:50:46.151904	\N	\N	ir	8
17	0	2020-09-01 08:50:46.151904	\N	\N	res	8
18	0	2020-09-01 08:50:46.151904	\N	\N	ir	9
19	0	2020-09-01 08:50:46.151904	\N	\N	party	9
20	0	2020-09-01 08:50:46.151904	\N	\N	product	9
21	0	2020-09-16 08:36:17.463064	\N	\N	web_user	9
22	0	2020-09-16 08:36:17.463064	\N	\N	ir	10
23	0	2020-09-16 08:36:17.463064	\N	\N	party	10
\.


--
-- Data for Name: ir_note; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_note (id, copy_to_resources, create_date, create_uid, message, resource, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_note_read; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_note_read (id, create_date, create_uid, note, "user", write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_queue; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_queue (id, create_date, create_uid, data, dequeued_at, enqueued_at, expected_at, finished_at, name, scheduled_at, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_rule; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_rule (id, create_date, create_uid, domain, rule_group, write_date, write_uid) FROM stdin;
1	2020-09-01 08:50:28.180942	0	[["user", "=", {"__class__": "Get", "d": -1, "k": "id", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	1	\N	\N
2	2020-09-01 08:50:28.180942	0	[["user", "=", {"__class__": "Get", "d": -1, "k": "id", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	2	\N	\N
3	2020-09-01 08:50:28.180942	0	[]	3	\N	\N
4	2020-09-01 08:50:28.180942	0	[["groups", "in", {"__class__": "Get", "d": [], "k": "groups", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	4	\N	\N
5	2020-09-01 08:50:28.180942	0	[["groups", "=", null]]	4	\N	\N
6	2020-09-01 08:50:28.180942	0	[["groups", "in", {"__class__": "Get", "d": [], "k": "groups", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	5	\N	\N
7	2020-09-01 08:50:28.180942	0	[["groups", "=", null]]	5	\N	\N
8	2020-09-01 08:50:28.180942	0	[["groups", "in", {"__class__": "Get", "d": [], "k": "groups", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	6	\N	\N
9	2020-09-01 08:50:28.180942	0	[["groups", "=", null]]	6	\N	\N
10	2020-09-01 08:50:28.180942	0	[["groups", "in", {"__class__": "Get", "d": [], "k": "groups", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	7	\N	\N
11	2020-09-01 08:50:28.180942	0	[["groups", "=", null]]	7	\N	\N
12	2020-09-01 08:50:28.180942	0	[["groups", "in", {"__class__": "Get", "d": [], "k": "groups", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	8	\N	\N
13	2020-09-01 08:50:28.180942	0	[["groups", "=", null]]	8	\N	\N
14	2020-09-01 08:50:28.180942	0	[["groups", "in", {"__class__": "Get", "d": [], "k": "groups", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	9	\N	\N
15	2020-09-01 08:50:28.180942	0	[["groups", "=", null]]	9	\N	\N
16	2020-09-01 08:50:28.180942	0	[["groups", "in", {"__class__": "Get", "d": [], "k": "groups", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	10	\N	\N
17	2020-09-01 08:50:28.180942	0	[["groups", "=", null]]	10	\N	\N
18	2020-09-01 08:50:28.180942	0	[["groups", "in", {"__class__": "Get", "d": [], "k": "groups", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	11	\N	\N
19	2020-09-01 08:50:28.180942	0	[["groups", "in", {"__class__": "Get", "d": [], "k": "groups", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	12	\N	\N
20	2020-09-01 08:50:28.180942	0	[["user", "=", {"__class__": "Get", "d": -1, "k": "id", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	13	\N	\N
21	2020-09-01 08:50:28.180942	0	[["create_uid", "=", {"__class__": "Get", "d": -1, "k": "id", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	15	\N	\N
22	2020-09-01 08:50:28.180942	0	[["create_uid", "=", {"__class__": "Get", "d": -1, "k": "id", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	16	\N	\N
23	2020-09-01 08:50:28.180942	0	[["create_uid", "=", {"__class__": "Get", "d": -1, "k": "id", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	17	\N	\N
24	2020-09-01 08:50:28.180942	0	[["groups", "in", {"__class__": "Eval", "d": [], "v": "user.groups"}]]	18	\N	\N
25	2020-09-01 08:50:28.180942	0	[["create_uid", "=", {"__class__": "Eval", "d": -1, "v": "user.id"}]]	18	\N	\N
26	2020-09-01 08:50:28.180942	0	[["write_groups", "in", {"__class__": "Eval", "d": [], "v": "user.groups"}]]	19	\N	\N
27	2020-09-01 08:50:28.180942	0	[["create_uid", "=", {"__class__": "Eval", "d": -1, "v": "user.id"}]]	19	\N	\N
28	2020-09-01 08:50:28.180942	0	[]	20	\N	\N
29	2020-09-01 08:50:28.180942	0	[["export.groups", "in", {"__class__": "Eval", "d": [], "v": "user.groups"}]]	21	\N	\N
30	2020-09-01 08:50:28.180942	0	[["export.create_uid", "=", {"__class__": "Eval", "d": -1, "v": "user.id"}]]	21	\N	\N
31	2020-09-01 08:50:28.180942	0	[["export.write_groups", "in", {"__class__": "Eval", "d": [], "v": "user.groups"}]]	22	\N	\N
32	2020-09-01 08:50:28.180942	0	[["export.create_uid", "=", {"__class__": "Eval", "d": -1, "v": "user.id"}]]	22	\N	\N
33	2020-09-01 08:50:28.180942	0	[]	23	\N	\N
34	2020-09-02 07:24:29.514826	0	[["company", "=", {"__class__": "Get", "d": null, "k": "company", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	24	\N	\N
35	2020-09-02 07:24:29.514826	0	[["company", "=", null]]	24	\N	\N
36	2020-09-02 07:24:29.514826	0	[["company", "=", {"__class__": "Get", "d": null, "k": "company", "v": {"__class__": "Eval", "d": {}, "v": "user"}}]]	25	\N	\N
37	2020-09-02 07:24:29.514826	0	[["company", "=", null]]	25	\N	\N
\.


--
-- Data for Name: ir_rule_group; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_rule_group (id, create_date, create_uid, default_p, global_p, model, name, perm_create, perm_delete, perm_read, perm_write, write_date, write_uid) FROM stdin;
1	2020-09-01 08:50:28.180942	0	f	t	72	Own warning	t	t	t	t	\N	\N
2	2020-09-01 08:50:28.180942	0	t	f	73	Own user application	t	t	t	t	\N	\N
3	2020-09-01 08:50:28.180942	0	f	f	73	Any user application	t	t	t	t	\N	\N
4	2020-09-01 08:50:28.180942	0	f	t	13	User in groups	t	t	t	t	\N	\N
5	2020-09-01 08:50:28.180942	0	f	t	21	User in groups	t	t	t	t	\N	\N
6	2020-09-01 08:50:28.180942	0	f	t	22	User in groups	t	t	t	t	\N	\N
7	2020-09-01 08:50:28.180942	0	f	t	23	User in groups	t	t	t	t	\N	\N
8	2020-09-01 08:50:28.180942	0	f	t	24	User in groups	t	t	t	t	\N	\N
9	2020-09-01 08:50:28.180942	0	f	t	27	User in groups	t	t	t	t	\N	\N
10	2020-09-01 08:50:28.180942	0	f	t	28	User in groups	t	t	t	t	\N	\N
11	2020-09-01 08:50:28.180942	0	f	t	11	User in groups	t	t	f	t	\N	\N
12	2020-09-01 08:50:28.180942	0	f	t	12	User in groups	t	t	f	t	\N	\N
13	2020-09-01 08:50:28.180942	0	t	f	19	Own view search	t	t	t	t	\N	\N
14	2020-09-01 08:50:28.180942	0	f	f	19	Any view search	t	t	t	t	\N	\N
15	2020-09-01 08:50:28.180942	0	f	t	61	Own session	t	t	t	t	\N	\N
16	2020-09-01 08:50:28.180942	0	f	t	62	Own session	t	t	t	t	\N	\N
17	2020-09-01 08:50:28.180942	0	f	t	14	Own favorite	t	t	t	t	\N	\N
18	2020-09-01 08:50:28.180942	0	t	f	45	User in groups	f	f	t	f	\N	\N
19	2020-09-01 08:50:28.180942	0	t	f	45	User in modification groups	t	t	t	t	\N	\N
20	2020-09-01 08:50:28.180942	0	f	f	45	Any export	t	t	t	t	\N	\N
21	2020-09-01 08:50:28.180942	0	t	f	46	User in groups	f	f	t	f	\N	\N
22	2020-09-01 08:50:28.180942	0	t	f	46	User in modification groups	t	t	t	t	\N	\N
23	2020-09-01 08:50:28.180942	0	f	f	46	Any export	t	t	t	t	\N	\N
24	2020-09-02 07:24:29.514826	0	f	t	11	User in company	t	t	t	t	\N	\N
25	2020-09-02 07:24:29.514826	0	f	t	12	User in company	t	t	t	t	\N	\N
\.


--
-- Data for Name: ir_rule_group-res_group; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."ir_rule_group-res_group" (id, create_date, create_uid, "group", rule_group, write_date, write_uid) FROM stdin;
1	2020-09-01 08:50:28.180942	0	1	3	\N	\N
2	2020-09-01 08:50:28.180942	0	1	14	\N	\N
3	2020-09-01 08:50:28.180942	0	1	20	\N	\N
4	2020-09-01 08:50:28.180942	0	1	23	\N	\N
\.


--
-- Data for Name: ir_sequence; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_sequence (id, active, code, create_date, create_uid, last_timestamp, name, number_increment, number_next_internal, padding, prefix, suffix, timestamp_offset, timestamp_rounding, type, write_date, write_uid, company) FROM stdin;
1	t	party.party	2020-09-02 07:24:24.586225	0	0	Party	1	1	0	\N	\N	946681200	1	incremental	2020-09-02 07:24:24.586225	0	\N
\.


--
-- Data for Name: ir_sequence_strict; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_sequence_strict (id, active, code, create_date, create_uid, last_timestamp, name, number_increment, number_next_internal, padding, prefix, suffix, timestamp_offset, timestamp_rounding, type, write_date, write_uid, company) FROM stdin;
\.


--
-- Data for Name: ir_sequence_type; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_sequence_type (id, code, create_date, create_uid, name, write_date, write_uid) FROM stdin;
1	party.party	2020-09-02 07:24:24.586225	0	Party	\N	\N
2	product.product	2020-09-02 07:24:32.977941	0	Product	\N	\N
\.


--
-- Data for Name: ir_sequence_type-res_group; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."ir_sequence_type-res_group" (id, create_date, create_uid, "group", sequence_type, write_date, write_uid) FROM stdin;
1	2020-09-02 07:24:24.586225	0	1	1	\N	\N
2	2020-09-02 07:24:24.586225	0	3	1	\N	\N
3	2020-09-02 07:24:32.977941	0	1	2	\N	\N
4	2020-09-02 07:24:32.977941	0	6	2	\N	\N
\.


--
-- Data for Name: ir_session; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_session (id, create_date, create_uid, key, write_date, write_uid) FROM stdin;
8	2020-09-29 10:26:07.772756	1	d3978e0db927e55364fec33d17105478da9e00067758e22f22a2d704a8bcbd2b	2020-09-29 10:27:08.797364	0
9	2020-09-29 10:26:22.745842	1	15c852cb984618e6cc32b951b7bf73a27324ac99b84974fe32bf9186e6ab703b	2020-09-29 10:26:34.617268	0
\.


--
-- Data for Name: ir_session_wizard; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_session_wizard (id, create_date, create_uid, data, write_date, write_uid) FROM stdin;
1	2020-09-09 08:33:00.344399	1	{}	\N	\N
19	2020-09-14 06:29:05.679587	1	{"result":{},"start":{"fecha_baja":{"__class__":"date","year":2020,"month":9,"day":14}}}	2020-09-14 06:29:07.903528	1
20	2020-09-14 06:33:11.42806	1	{"result":{},"start":{}}	2020-09-14 06:33:11.717147	1
6	2020-09-09 09:51:57.425129	1	{"result":{},"start":{"fecha_baja":{"__class__":"date","year":2020,"month":8,"day":9}}}	2020-09-09 09:52:35.362176	1
7	2020-09-09 09:53:01.352205	1	{"result":{},"start":{"fecha_baja":{"__class__":"date","year":2020,"month":9,"day":8}}}	2020-09-09 09:53:07.652922	1
9	2020-09-09 09:55:04.781463	1	{"result":{},"start":{"fecha_baja":{"__class__":"date","year":2020,"month":9,"day":9}}}	2020-09-09 09:55:06.904276	1
10	2020-09-09 09:56:19.196363	1	{"result":{},"start":{"fecha_baja":{"__class__":"date","year":2020,"month":9,"day":10}}}	2020-09-09 09:56:25.531262	1
13	2020-09-10 07:39:52.873898	1	{"result":{},"start":{"fecha_baja":{"__class__":"date","year":2020,"month":9,"day":10}}}	2020-09-10 07:39:54.708982	1
16	2020-09-14 06:27:58.886067	1	{"result":{},"start":{"fecha_baja":{"__class__":"date","year":2020,"month":9,"day":14}}}	2020-09-14 06:28:00.826825	1
17	2020-09-14 06:28:26.719915	1	{"result":{},"start":{"fecha_baja":{"__class__":"date","year":2020,"month":9,"day":14}}}	2020-09-14 06:28:28.580164	1
\.


--
-- Data for Name: ir_translation; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_translation (id, lang, src, name, res_id, value, type, module, fuzzy, create_date, create_uid, overriding_module, write_date, write_uid) FROM stdin;
1	en	Configuration	ir.configuration,name	-1		model	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
2	en	Hostname	ir.configuration,hostname	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
3	en	language	ir.configuration,language	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
4	en	Translation	ir.translation,name	-1		model	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
5	en	Fuzzy	ir.translation,fuzzy	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
6	en	Language	ir.translation,lang	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
7	en	Model	ir.translation,model	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
8	en	Module	ir.translation,module	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
9	en	Field Name	ir.translation,name	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
10	en	Overriding Module	ir.translation,overriding_module	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
11	en	Resource ID	ir.translation,res_id	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
12	en	Source	ir.translation,src	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
13	en	Type	ir.translation,type	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
14	en	Field	ir.translation,type	-1		selection	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
15	en	Model	ir.translation,type	-1		selection	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
16	en	Report	ir.translation,type	-1		selection	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
17	en	Selection	ir.translation,type	-1		selection	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
18	en	View	ir.translation,type	-1		selection	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
19	en	Wizard Button	ir.translation,type	-1		selection	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
20	en	Help	ir.translation,type	-1		selection	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
21	en	Translation Value	ir.translation,value	-1		field	ir	f	2020-09-01 08:49:56.475804	0	\N	\N	\N
22	en	Set Translation	ir.translation.set.start,name	-1		model	ir	f	\N	\N	\N	\N	\N
23	en	Set Translation	ir.translation.set.succeed,name	-1		model	ir	f	\N	\N	\N	\N	\N
24	en	Clean translation	ir.translation.clean.start,name	-1		model	ir	f	\N	\N	\N	\N	\N
25	en	Clean translation	ir.translation.clean.succeed,name	-1		model	ir	f	\N	\N	\N	\N	\N
26	en	Update translation	ir.translation.update.start,name	-1		model	ir	f	\N	\N	\N	\N	\N
27	en	Language	ir.translation.update.start,language	-1		field	ir	f	\N	\N	\N	\N	\N
28	en	Export translation	ir.translation.export.start,name	-1		model	ir	f	\N	\N	\N	\N	\N
29	en	Language	ir.translation.export.start,language	-1		field	ir	f	\N	\N	\N	\N	\N
30	en	Module	ir.translation.export.start,module	-1		field	ir	f	\N	\N	\N	\N	\N
31	en	Export translation	ir.translation.export.result,name	-1		model	ir	f	\N	\N	\N	\N	\N
32	en	File	ir.translation.export.result,file	-1		field	ir	f	\N	\N	\N	\N	\N
33	en	Filename	ir.translation.export.result,filename	-1		field	ir	f	\N	\N	\N	\N	\N
34	en	Language	ir.translation.export.result,language	-1		field	ir	f	\N	\N	\N	\N	\N
35	en	Module	ir.translation.export.result,module	-1		field	ir	f	\N	\N	\N	\N	\N
36	en	Sequence type	ir.sequence.type,name	-1		model	ir	f	\N	\N	\N	\N	\N
37	en	Sequence Code	ir.sequence.type,code	-1		field	ir	f	\N	\N	\N	\N	\N
38	en	Sequence Name	ir.sequence.type,name	-1		field	ir	f	\N	\N	\N	\N	\N
39	en	Sequence	ir.sequence,name	-1		model	ir	f	\N	\N	\N	\N	\N
40	en	Sequence Code	ir.sequence,code	-1		field	ir	f	\N	\N	\N	\N	\N
41	en	Last Timestamp	ir.sequence,last_timestamp	-1		field	ir	f	\N	\N	\N	\N	\N
42	en	Sequence Name	ir.sequence,name	-1		field	ir	f	\N	\N	\N	\N	\N
43	en	Increment Number	ir.sequence,number_increment	-1		field	ir	f	\N	\N	\N	\N	\N
44	en	Next Number	ir.sequence,number_next	-1		field	ir	f	\N	\N	\N	\N	\N
45	en	Next Number	ir.sequence,number_next_internal	-1		field	ir	f	\N	\N	\N	\N	\N
46	en	Number padding	ir.sequence,padding	-1		field	ir	f	\N	\N	\N	\N	\N
47	en	Prefix	ir.sequence,prefix	-1		field	ir	f	\N	\N	\N	\N	\N
48	en	Suffix	ir.sequence,suffix	-1		field	ir	f	\N	\N	\N	\N	\N
49	en	Timestamp Offset	ir.sequence,timestamp_offset	-1		field	ir	f	\N	\N	\N	\N	\N
50	en	Timestamp Rounding	ir.sequence,timestamp_rounding	-1		field	ir	f	\N	\N	\N	\N	\N
51	en	Type	ir.sequence,type	-1		field	ir	f	\N	\N	\N	\N	\N
52	en	Incremental	ir.sequence,type	-1		selection	ir	f	\N	\N	\N	\N	\N
53	en	Decimal Timestamp	ir.sequence,type	-1		selection	ir	f	\N	\N	\N	\N	\N
54	en	Hexadecimal Timestamp	ir.sequence,type	-1		selection	ir	f	\N	\N	\N	\N	\N
55	en	Sequence Strict	ir.sequence.strict,name	-1		model	ir	f	\N	\N	\N	\N	\N
56	en	Sequence Code	ir.sequence.strict,code	-1		field	ir	f	\N	\N	\N	\N	\N
57	en	Last Timestamp	ir.sequence.strict,last_timestamp	-1		field	ir	f	\N	\N	\N	\N	\N
58	en	Sequence Name	ir.sequence.strict,name	-1		field	ir	f	\N	\N	\N	\N	\N
59	en	Increment Number	ir.sequence.strict,number_increment	-1		field	ir	f	\N	\N	\N	\N	\N
60	en	Next Number	ir.sequence.strict,number_next	-1		field	ir	f	\N	\N	\N	\N	\N
61	en	Next Number	ir.sequence.strict,number_next_internal	-1		field	ir	f	\N	\N	\N	\N	\N
62	en	Number padding	ir.sequence.strict,padding	-1		field	ir	f	\N	\N	\N	\N	\N
63	en	Prefix	ir.sequence.strict,prefix	-1		field	ir	f	\N	\N	\N	\N	\N
64	en	Suffix	ir.sequence.strict,suffix	-1		field	ir	f	\N	\N	\N	\N	\N
65	en	Timestamp Offset	ir.sequence.strict,timestamp_offset	-1		field	ir	f	\N	\N	\N	\N	\N
66	en	Timestamp Rounding	ir.sequence.strict,timestamp_rounding	-1		field	ir	f	\N	\N	\N	\N	\N
67	en	Type	ir.sequence.strict,type	-1		field	ir	f	\N	\N	\N	\N	\N
68	en	Incremental	ir.sequence.strict,type	-1		selection	ir	f	\N	\N	\N	\N	\N
69	en	Decimal Timestamp	ir.sequence.strict,type	-1		selection	ir	f	\N	\N	\N	\N	\N
70	en	Hexadecimal Timestamp	ir.sequence.strict,type	-1		selection	ir	f	\N	\N	\N	\N	\N
71	en	UI menu	ir.ui.menu,name	-1		model	ir	f	\N	\N	\N	\N	\N
72	en	Action	ir.ui.menu,action	-1		field	ir	f	\N	\N	\N	\N	\N
73	en	Action Keywords	ir.ui.menu,action_keywords	-1		field	ir	f	\N	\N	\N	\N	\N
74	en	Children	ir.ui.menu,childs	-1		field	ir	f	\N	\N	\N	\N	\N
75	en	Complete Name	ir.ui.menu,complete_name	-1		field	ir	f	\N	\N	\N	\N	\N
76	en	Favorite	ir.ui.menu,favorite	-1		field	ir	f	\N	\N	\N	\N	\N
77	en	Groups	ir.ui.menu,groups	-1		field	ir	f	\N	\N	\N	\N	\N
78	en	Icon	ir.ui.menu,icon	-1		field	ir	f	\N	\N	\N	\N	\N
79	en	Menu	ir.ui.menu,name	-1		field	ir	f	\N	\N	\N	\N	\N
80	en	Parent Menu	ir.ui.menu,parent	-1		field	ir	f	\N	\N	\N	\N	\N
81	en	Menu Favorite	ir.ui.menu.favorite,name	-1		model	ir	f	\N	\N	\N	\N	\N
82	en	Menu	ir.ui.menu.favorite,menu	-1		field	ir	f	\N	\N	\N	\N	\N
83	en	User	ir.ui.menu.favorite,user	-1		field	ir	f	\N	\N	\N	\N	\N
84	en	View	ir.ui.view,name	-1		model	ir	f	\N	\N	\N	\N	\N
85	en	View Architecture	ir.ui.view,arch	-1		field	ir	f	\N	\N	\N	\N	\N
86	en	Data	ir.ui.view,data	-1		field	ir	f	\N	\N	\N	\N	\N
87	en	Domain	ir.ui.view,domain	-1		field	ir	f	\N	\N	\N	\N	\N
88	en	Children Field	ir.ui.view,field_childs	-1		field	ir	f	\N	\N	\N	\N	\N
89	en	Inherited View	ir.ui.view,inherit	-1		field	ir	f	\N	\N	\N	\N	\N
90	en	Model	ir.ui.view,model	-1		field	ir	f	\N	\N	\N	\N	\N
91	en	Module	ir.ui.view,module	-1		field	ir	f	\N	\N	\N	\N	\N
92	en	Name	ir.ui.view,name	-1		field	ir	f	\N	\N	\N	\N	\N
93	en	Priority	ir.ui.view,priority	-1		field	ir	f	\N	\N	\N	\N	\N
94	en	View Type	ir.ui.view,type	-1		field	ir	f	\N	\N	\N	\N	\N
95	en	Tree	ir.ui.view,type	-1		selection	ir	f	\N	\N	\N	\N	\N
96	en	Form	ir.ui.view,type	-1		selection	ir	f	\N	\N	\N	\N	\N
97	en	Graph	ir.ui.view,type	-1		selection	ir	f	\N	\N	\N	\N	\N
98	en	Calendar	ir.ui.view,type	-1		selection	ir	f	\N	\N	\N	\N	\N
99	en	Board	ir.ui.view,type	-1		selection	ir	f	\N	\N	\N	\N	\N
100	en	List Form	ir.ui.view,type	-1		selection	ir	f	\N	\N	\N	\N	\N
101	en	Show view	ir.ui.view.show.start,name	-1		model	ir	f	\N	\N	\N	\N	\N
102	en	View Tree Width	ir.ui.view_tree_width,name	-1		model	ir	f	\N	\N	\N	\N	\N
103	en	Field	ir.ui.view_tree_width,field	-1		field	ir	f	\N	\N	\N	\N	\N
104	en	Model	ir.ui.view_tree_width,model	-1		field	ir	f	\N	\N	\N	\N	\N
105	en	User	ir.ui.view_tree_width,user	-1		field	ir	f	\N	\N	\N	\N	\N
106	en	Width	ir.ui.view_tree_width,width	-1		field	ir	f	\N	\N	\N	\N	\N
107	en	View Tree State	ir.ui.view_tree_state,name	-1		model	ir	f	\N	\N	\N	\N	\N
108	en	Child Name	ir.ui.view_tree_state,child_name	-1		field	ir	f	\N	\N	\N	\N	\N
109	en	Domain	ir.ui.view_tree_state,domain	-1		field	ir	f	\N	\N	\N	\N	\N
110	en	Model	ir.ui.view_tree_state,model	-1		field	ir	f	\N	\N	\N	\N	\N
111	en	Expanded Nodes	ir.ui.view_tree_state,nodes	-1		field	ir	f	\N	\N	\N	\N	\N
112	en	Selected Nodes	ir.ui.view_tree_state,selected_nodes	-1		field	ir	f	\N	\N	\N	\N	\N
113	en	User	ir.ui.view_tree_state,user	-1		field	ir	f	\N	\N	\N	\N	\N
114	en	View Search	ir.ui.view_search,name	-1		model	ir	f	\N	\N	\N	\N	\N
115	en	Domain	ir.ui.view_search,domain	-1		field	ir	f	\N	\N	\N	\N	\N
116	en	The PYSON domain.	ir.ui.view_search,domain	-1		help	ir	f	\N	\N	\N	\N	\N
117	en	Model	ir.ui.view_search,model	-1		field	ir	f	\N	\N	\N	\N	\N
118	en	Name	ir.ui.view_search,name	-1		field	ir	f	\N	\N	\N	\N	\N
119	en	User	ir.ui.view_search,user	-1		field	ir	f	\N	\N	\N	\N	\N
120	en	Icon	ir.ui.icon,name	-1		model	ir	f	\N	\N	\N	\N	\N
121	en	Icon	ir.ui.icon,icon	-1		field	ir	f	\N	\N	\N	\N	\N
122	en	Module	ir.ui.icon,module	-1		field	ir	f	\N	\N	\N	\N	\N
123	en	Name	ir.ui.icon,name	-1		field	ir	f	\N	\N	\N	\N	\N
124	en	SVG Path	ir.ui.icon,path	-1		field	ir	f	\N	\N	\N	\N	\N
125	en	Action	ir.action,name	-1		model	ir	f	\N	\N	\N	\N	\N
126	en	Groups	ir.action,groups	-1		field	ir	f	\N	\N	\N	\N	\N
127	en	Icon	ir.action,icon	-1		field	ir	f	\N	\N	\N	\N	\N
128	en	Keywords	ir.action,keywords	-1		field	ir	f	\N	\N	\N	\N	\N
129	en	Name	ir.action,name	-1		field	ir	f	\N	\N	\N	\N	\N
130	en	Type	ir.action,type	-1		field	ir	f	\N	\N	\N	\N	\N
131	en	Usage	ir.action,usage	-1		field	ir	f	\N	\N	\N	\N	\N
132	en	Action keyword	ir.action.keyword,name	-1		model	ir	f	\N	\N	\N	\N	\N
133	en	Action	ir.action.keyword,action	-1		field	ir	f	\N	\N	\N	\N	\N
134	en	Groups	ir.action.keyword,groups	-1		field	ir	f	\N	\N	\N	\N	\N
135	en	Keyword	ir.action.keyword,keyword	-1		field	ir	f	\N	\N	\N	\N	\N
136	en	Open tree	ir.action.keyword,keyword	-1		selection	ir	f	\N	\N	\N	\N	\N
137	en	Print form	ir.action.keyword,keyword	-1		selection	ir	f	\N	\N	\N	\N	\N
138	en	Action form	ir.action.keyword,keyword	-1		selection	ir	f	\N	\N	\N	\N	\N
139	en	Form relate	ir.action.keyword,keyword	-1		selection	ir	f	\N	\N	\N	\N	\N
140	en	Open Graph	ir.action.keyword,keyword	-1		selection	ir	f	\N	\N	\N	\N	\N
141	en	Model	ir.action.keyword,model	-1		field	ir	f	\N	\N	\N	\N	\N
142	en	Action report	ir.action.report,name	-1		model	ir	f	\N	\N	\N	\N	\N
143	en	Action	ir.action.report,action	-1		field	ir	f	\N	\N	\N	\N	\N
144	en	Direct Print	ir.action.report,direct_print	-1		field	ir	f	\N	\N	\N	\N	\N
145	en	Email	ir.action.report,email	-1		field	ir	f	\N	\N	\N	\N	\N
146	en	Python dictonary where keys define "to" "cc" "subject"\nExample: {'to': 'test@example.com', 'cc': 'user@example.com'}	ir.action.report,email	-1		help	ir	f	\N	\N	\N	\N	\N
147	en	Extension	ir.action.report,extension	-1		field	ir	f	\N	\N	\N	\N	\N
148	en	Leave empty for the same as template, see LibreOffice documentation for compatible format.	ir.action.report,extension	-1		help	ir	f	\N	\N	\N	\N	\N
149	en	Groups	ir.action.report,groups	-1		field	ir	f	\N	\N	\N	\N	\N
150	en	Icon	ir.action.report,icon	-1		field	ir	f	\N	\N	\N	\N	\N
151	en	Is Custom	ir.action.report,is_custom	-1		field	ir	f	\N	\N	\N	\N	\N
152	en	Keywords	ir.action.report,keywords	-1		field	ir	f	\N	\N	\N	\N	\N
153	en	Model	ir.action.report,model	-1		field	ir	f	\N	\N	\N	\N	\N
154	en	Module	ir.action.report,module	-1		field	ir	f	\N	\N	\N	\N	\N
155	en	Name	ir.action.report,name	-1		field	ir	f	\N	\N	\N	\N	\N
156	en	PySON Email	ir.action.report,pyson_email	-1		field	ir	f	\N	\N	\N	\N	\N
157	en	Path	ir.action.report,report	-1		field	ir	f	\N	\N	\N	\N	\N
158	en	Content	ir.action.report,report_content	-1		field	ir	f	\N	\N	\N	\N	\N
159	en	Content	ir.action.report,report_content_custom	-1		field	ir	f	\N	\N	\N	\N	\N
160	en	Content HTML	ir.action.report,report_content_html	-1		field	ir	f	\N	\N	\N	\N	\N
161	en	Content Name	ir.action.report,report_content_name	-1		field	ir	f	\N	\N	\N	\N	\N
162	en	Internal Name	ir.action.report,report_name	-1		field	ir	f	\N	\N	\N	\N	\N
163	en	Single	ir.action.report,single	-1		field	ir	f	\N	\N	\N	\N	\N
164	en	Check if the template works only for one record.	ir.action.report,single	-1		help	ir	f	\N	\N	\N	\N	\N
165	en	Template Extension	ir.action.report,template_extension	-1		field	ir	f	\N	\N	\N	\N	\N
166	en	Translatable	ir.action.report,translatable	-1		field	ir	f	\N	\N	\N	\N	\N
167	en	Uncheck to disable translations for this report.	ir.action.report,translatable	-1		help	ir	f	\N	\N	\N	\N	\N
168	en	Type	ir.action.report,type	-1		field	ir	f	\N	\N	\N	\N	\N
169	en	Usage	ir.action.report,usage	-1		field	ir	f	\N	\N	\N	\N	\N
170	en	Action act window	ir.action.act_window,name	-1		model	ir	f	\N	\N	\N	\N	\N
171	en	Domains	ir.action.act_window,act_window_domains	-1		field	ir	f	\N	\N	\N	\N	\N
172	en	Views	ir.action.act_window,act_window_views	-1		field	ir	f	\N	\N	\N	\N	\N
173	en	Action	ir.action.act_window,action	-1		field	ir	f	\N	\N	\N	\N	\N
174	en	Context Value	ir.action.act_window,context	-1		field	ir	f	\N	\N	\N	\N	\N
175	en	Context Domain	ir.action.act_window,context_domain	-1		field	ir	f	\N	\N	\N	\N	\N
176	en	Part of the domain that will be evaluated on each refresh.	ir.action.act_window,context_domain	-1		help	ir	f	\N	\N	\N	\N	\N
177	en	Context Model	ir.action.act_window,context_model	-1		field	ir	f	\N	\N	\N	\N	\N
178	en	Domain Value	ir.action.act_window,domain	-1		field	ir	f	\N	\N	\N	\N	\N
179	en	Domains	ir.action.act_window,domains	-1		field	ir	f	\N	\N	\N	\N	\N
180	en	Groups	ir.action.act_window,groups	-1		field	ir	f	\N	\N	\N	\N	\N
181	en	Icon	ir.action.act_window,icon	-1		field	ir	f	\N	\N	\N	\N	\N
182	en	Keywords	ir.action.act_window,keywords	-1		field	ir	f	\N	\N	\N	\N	\N
183	en	Limit	ir.action.act_window,limit	-1		field	ir	f	\N	\N	\N	\N	\N
184	en	Default limit for the list view.	ir.action.act_window,limit	-1		help	ir	f	\N	\N	\N	\N	\N
185	en	Name	ir.action.act_window,name	-1		field	ir	f	\N	\N	\N	\N	\N
186	en	Order Value	ir.action.act_window,order	-1		field	ir	f	\N	\N	\N	\N	\N
187	en	PySON Context	ir.action.act_window,pyson_context	-1		field	ir	f	\N	\N	\N	\N	\N
188	en	PySON Domain	ir.action.act_window,pyson_domain	-1		field	ir	f	\N	\N	\N	\N	\N
189	en	PySON Order	ir.action.act_window,pyson_order	-1		field	ir	f	\N	\N	\N	\N	\N
190	en	PySON Search Criteria	ir.action.act_window,pyson_search_value	-1		field	ir	f	\N	\N	\N	\N	\N
191	en	Model	ir.action.act_window,res_model	-1		field	ir	f	\N	\N	\N	\N	\N
192	en	Search Criteria	ir.action.act_window,search_value	-1		field	ir	f	\N	\N	\N	\N	\N
193	en	Default search criteria for the list view.	ir.action.act_window,search_value	-1		help	ir	f	\N	\N	\N	\N	\N
194	en	Type	ir.action.act_window,type	-1		field	ir	f	\N	\N	\N	\N	\N
195	en	Usage	ir.action.act_window,usage	-1		field	ir	f	\N	\N	\N	\N	\N
196	en	Views	ir.action.act_window,views	-1		field	ir	f	\N	\N	\N	\N	\N
197	en	Action act window view	ir.action.act_window.view,name	-1		model	ir	f	\N	\N	\N	\N	\N
198	en	Action	ir.action.act_window.view,act_window	-1		field	ir	f	\N	\N	\N	\N	\N
199	en	View	ir.action.act_window.view,view	-1		field	ir	f	\N	\N	\N	\N	\N
200	en	Action act window domain	ir.action.act_window.domain,name	-1		model	ir	f	\N	\N	\N	\N	\N
201	en	Action	ir.action.act_window.domain,act_window	-1		field	ir	f	\N	\N	\N	\N	\N
202	en	Count	ir.action.act_window.domain,count	-1		field	ir	f	\N	\N	\N	\N	\N
203	en	Domain	ir.action.act_window.domain,domain	-1		field	ir	f	\N	\N	\N	\N	\N
204	en	Name	ir.action.act_window.domain,name	-1		field	ir	f	\N	\N	\N	\N	\N
205	en	Action wizard	ir.action.wizard,name	-1		model	ir	f	\N	\N	\N	\N	\N
206	en	Action	ir.action.wizard,action	-1		field	ir	f	\N	\N	\N	\N	\N
207	en	Email	ir.action.wizard,email	-1		field	ir	f	\N	\N	\N	\N	\N
208	en	Groups	ir.action.wizard,groups	-1		field	ir	f	\N	\N	\N	\N	\N
209	en	Icon	ir.action.wizard,icon	-1		field	ir	f	\N	\N	\N	\N	\N
210	en	Keywords	ir.action.wizard,keywords	-1		field	ir	f	\N	\N	\N	\N	\N
211	en	Model	ir.action.wizard,model	-1		field	ir	f	\N	\N	\N	\N	\N
212	en	Name	ir.action.wizard,name	-1		field	ir	f	\N	\N	\N	\N	\N
213	en	Type	ir.action.wizard,type	-1		field	ir	f	\N	\N	\N	\N	\N
214	en	Usage	ir.action.wizard,usage	-1		field	ir	f	\N	\N	\N	\N	\N
215	en	Window	ir.action.wizard,window	-1		field	ir	f	\N	\N	\N	\N	\N
216	en	Run wizard in a new window.	ir.action.wizard,window	-1		help	ir	f	\N	\N	\N	\N	\N
217	en	Wizard name	ir.action.wizard,wiz_name	-1		field	ir	f	\N	\N	\N	\N	\N
218	en	Action URL	ir.action.url,name	-1		model	ir	f	\N	\N	\N	\N	\N
219	en	Action	ir.action.url,action	-1		field	ir	f	\N	\N	\N	\N	\N
220	en	Groups	ir.action.url,groups	-1		field	ir	f	\N	\N	\N	\N	\N
221	en	Icon	ir.action.url,icon	-1		field	ir	f	\N	\N	\N	\N	\N
222	en	Keywords	ir.action.url,keywords	-1		field	ir	f	\N	\N	\N	\N	\N
223	en	Name	ir.action.url,name	-1		field	ir	f	\N	\N	\N	\N	\N
224	en	Type	ir.action.url,type	-1		field	ir	f	\N	\N	\N	\N	\N
225	en	Action Url	ir.action.url,url	-1		field	ir	f	\N	\N	\N	\N	\N
226	en	Usage	ir.action.url,usage	-1		field	ir	f	\N	\N	\N	\N	\N
227	en	Model	ir.model,name	-1		model	ir	f	\N	\N	\N	\N	\N
228	en	Fields	ir.model,fields	-1		field	ir	f	\N	\N	\N	\N	\N
229	en	Global Search	ir.model,global_search_p	-1		field	ir	f	\N	\N	\N	\N	\N
230	en	Information	ir.model,info	-1		field	ir	f	\N	\N	\N	\N	\N
231	en	Model Name	ir.model,model	-1		field	ir	f	\N	\N	\N	\N	\N
232	en	Module	ir.model,module	-1		field	ir	f	\N	\N	\N	\N	\N
233	en	Module in which this model is defined.	ir.model,module	-1		help	ir	f	\N	\N	\N	\N	\N
234	en	Model Description	ir.model,name	-1		field	ir	f	\N	\N	\N	\N	\N
235	en	Model field	ir.model.field,name	-1		model	ir	f	\N	\N	\N	\N	\N
236	en	Field Description	ir.model.field,field_description	-1		field	ir	f	\N	\N	\N	\N	\N
237	en	Help	ir.model.field,help	-1		field	ir	f	\N	\N	\N	\N	\N
238	en	Model	ir.model.field,model	-1		field	ir	f	\N	\N	\N	\N	\N
239	en	Module	ir.model.field,module	-1		field	ir	f	\N	\N	\N	\N	\N
240	en	Module in which this field is defined.	ir.model.field,module	-1		help	ir	f	\N	\N	\N	\N	\N
241	en	Name	ir.model.field,name	-1		field	ir	f	\N	\N	\N	\N	\N
242	en	Model Relation	ir.model.field,relation	-1		field	ir	f	\N	\N	\N	\N	\N
243	en	Field Type	ir.model.field,ttype	-1		field	ir	f	\N	\N	\N	\N	\N
244	en	Model access	ir.model.access,name	-1		model	ir	f	\N	\N	\N	\N	\N
245	en	Description	ir.model.access,description	-1		field	ir	f	\N	\N	\N	\N	\N
246	en	Group	ir.model.access,group	-1		field	ir	f	\N	\N	\N	\N	\N
247	en	Model	ir.model.access,model	-1		field	ir	f	\N	\N	\N	\N	\N
248	en	Create Access	ir.model.access,perm_create	-1		field	ir	f	\N	\N	\N	\N	\N
249	en	Delete Access	ir.model.access,perm_delete	-1		field	ir	f	\N	\N	\N	\N	\N
250	en	Read Access	ir.model.access,perm_read	-1		field	ir	f	\N	\N	\N	\N	\N
251	en	Write Access	ir.model.access,perm_write	-1		field	ir	f	\N	\N	\N	\N	\N
252	en	Model Field Access	ir.model.field.access,name	-1		model	ir	f	\N	\N	\N	\N	\N
253	en	Description	ir.model.field.access,description	-1		field	ir	f	\N	\N	\N	\N	\N
254	en	Field	ir.model.field.access,field	-1		field	ir	f	\N	\N	\N	\N	\N
255	en	Group	ir.model.field.access,group	-1		field	ir	f	\N	\N	\N	\N	\N
256	en	Create Access	ir.model.field.access,perm_create	-1		field	ir	f	\N	\N	\N	\N	\N
257	en	Delete Access	ir.model.field.access,perm_delete	-1		field	ir	f	\N	\N	\N	\N	\N
258	en	Read Access	ir.model.field.access,perm_read	-1		field	ir	f	\N	\N	\N	\N	\N
259	en	Write Access	ir.model.field.access,perm_write	-1		field	ir	f	\N	\N	\N	\N	\N
260	en	Model Button	ir.model.button,name	-1		model	ir	f	\N	\N	\N	\N	\N
261	en	Clicks	ir.model.button,clicks	-1		field	ir	f	\N	\N	\N	\N	\N
262	en	Confirm	ir.model.button,confirm	-1		field	ir	f	\N	\N	\N	\N	\N
263	en	Text to ask user confirmation when clicking the button.	ir.model.button,confirm	-1		help	ir	f	\N	\N	\N	\N	\N
264	en	Groups	ir.model.button,groups	-1		field	ir	f	\N	\N	\N	\N	\N
265	en	Help	ir.model.button,help	-1		field	ir	f	\N	\N	\N	\N	\N
266	en	Model	ir.model.button,model	-1		field	ir	f	\N	\N	\N	\N	\N
267	en	Name	ir.model.button,name	-1		field	ir	f	\N	\N	\N	\N	\N
268	en	Reset	ir.model.button,reset	-1		field	ir	f	\N	\N	\N	\N	\N
269	en	Reset by	ir.model.button,reset_by	-1		field	ir	f	\N	\N	\N	\N	\N
270	en	Button that should reset the rules.	ir.model.button,reset_by	-1		help	ir	f	\N	\N	\N	\N	\N
271	en	Rules	ir.model.button,rules	-1		field	ir	f	\N	\N	\N	\N	\N
272	en	Label	ir.model.button,string	-1		field	ir	f	\N	\N	\N	\N	\N
273	en	Model Button Rule	ir.model.button.rule,name	-1		model	ir	f	\N	\N	\N	\N	\N
274	en	Button	ir.model.button.rule,button	-1		field	ir	f	\N	\N	\N	\N	\N
275	en	Condition	ir.model.button.rule,condition	-1		field	ir	f	\N	\N	\N	\N	\N
276	en	A PYSON statement evaluated with the record represented by "self"\nIt activate the rule if true.	ir.model.button.rule,condition	-1		help	ir	f	\N	\N	\N	\N	\N
277	en	Description	ir.model.button.rule,description	-1		field	ir	f	\N	\N	\N	\N	\N
278	en	Number of User	ir.model.button.rule,number_user	-1		field	ir	f	\N	\N	\N	\N	\N
279	en	Model Button Click	ir.model.button.click,name	-1		model	ir	f	\N	\N	\N	\N	\N
280	en	Button	ir.model.button.click,button	-1		field	ir	f	\N	\N	\N	\N	\N
281	en	Record ID	ir.model.button.click,record_id	-1		field	ir	f	\N	\N	\N	\N	\N
282	en	Model Button Reset	ir.model.button-button.reset,name	-1		model	ir	f	\N	\N	\N	\N	\N
283	en	Button	ir.model.button-button.reset,button	-1		field	ir	f	\N	\N	\N	\N	\N
366	en	PM	ir.lang,pm	-1		field	ir	f	\N	\N	\N	\N	\N
284	en	Button Ruled	ir.model.button-button.reset,button_ruled	-1		field	ir	f	\N	\N	\N	\N	\N
285	en	Model data	ir.model.data,name	-1		model	ir	f	\N	\N	\N	\N	\N
286	en	Resource ID	ir.model.data,db_id	-1		field	ir	f	\N	\N	\N	\N	\N
287	en	The id of the record in the database.	ir.model.data,db_id	-1		help	ir	f	\N	\N	\N	\N	\N
288	en	Identifier on File System	ir.model.data,fs_id	-1		field	ir	f	\N	\N	\N	\N	\N
289	en	The id of the record as known on the file system.	ir.model.data,fs_id	-1		help	ir	f	\N	\N	\N	\N	\N
290	en	Values on File System	ir.model.data,fs_values	-1		field	ir	f	\N	\N	\N	\N	\N
291	en	Model	ir.model.data,model	-1		field	ir	f	\N	\N	\N	\N	\N
292	en	Module	ir.model.data,module	-1		field	ir	f	\N	\N	\N	\N	\N
293	en	No Update	ir.model.data,noupdate	-1		field	ir	f	\N	\N	\N	\N	\N
294	en	Out of Sync	ir.model.data,out_of_sync	-1		field	ir	f	\N	\N	\N	\N	\N
295	en	Values	ir.model.data,values	-1		field	ir	f	\N	\N	\N	\N	\N
296	en	Print Model Graph	ir.model.print_model_graph.start,name	-1		model	ir	f	\N	\N	\N	\N	\N
297	en	Filter	ir.model.print_model_graph.start,filter	-1		field	ir	f	\N	\N	\N	\N	\N
298	en	Entering a Python Regular Expression will exclude matching models from the graph.	ir.model.print_model_graph.start,filter	-1		help	ir	f	\N	\N	\N	\N	\N
299	en	Level	ir.model.print_model_graph.start,level	-1		field	ir	f	\N	\N	\N	\N	\N
300	en	Attachment	ir.attachment,name	-1		model	ir	f	\N	\N	\N	\N	\N
301	en	Copy to Resources	ir.attachment,copy_to_resources	-1		field	ir	f	\N	\N	\N	\N	\N
302	en	Copy to Resources Visible	ir.attachment,copy_to_resources_visible	-1		field	ir	f	\N	\N	\N	\N	\N
303	en	Data	ir.attachment,data	-1		field	ir	f	\N	\N	\N	\N	\N
304	en	Data size	ir.attachment,data_size	-1		field	ir	f	\N	\N	\N	\N	\N
305	en	Description	ir.attachment,description	-1		field	ir	f	\N	\N	\N	\N	\N
306	en	File ID	ir.attachment,file_id	-1		field	ir	f	\N	\N	\N	\N	\N
307	en	Last Modification	ir.attachment,last_modification	-1		field	ir	f	\N	\N	\N	\N	\N
308	en	Last User	ir.attachment,last_user	-1		field	ir	f	\N	\N	\N	\N	\N
309	en	Link	ir.attachment,link	-1		field	ir	f	\N	\N	\N	\N	\N
310	en	Name	ir.attachment,name	-1		field	ir	f	\N	\N	\N	\N	\N
311	en	Resource	ir.attachment,resource	-1		field	ir	f	\N	\N	\N	\N	\N
312	en	Summary	ir.attachment,summary	-1		field	ir	f	\N	\N	\N	\N	\N
313	en	Type	ir.attachment,type	-1		field	ir	f	\N	\N	\N	\N	\N
314	en	Data	ir.attachment,type	-1		selection	ir	f	\N	\N	\N	\N	\N
315	en	Link	ir.attachment,type	-1		selection	ir	f	\N	\N	\N	\N	\N
316	en	Note	ir.note,name	-1		model	ir	f	\N	\N	\N	\N	\N
317	en	Copy to Resources	ir.note,copy_to_resources	-1		field	ir	f	\N	\N	\N	\N	\N
318	en	Copy to Resources Visible	ir.note,copy_to_resources_visible	-1		field	ir	f	\N	\N	\N	\N	\N
319	en	Last Modification	ir.note,last_modification	-1		field	ir	f	\N	\N	\N	\N	\N
320	en	Last User	ir.note,last_user	-1		field	ir	f	\N	\N	\N	\N	\N
321	en	Message	ir.note,message	-1		field	ir	f	\N	\N	\N	\N	\N
322	en	Message	ir.note,message_wrapped	-1		field	ir	f	\N	\N	\N	\N	\N
323	en	Resource	ir.note,resource	-1		field	ir	f	\N	\N	\N	\N	\N
324	en	Unread	ir.note,unread	-1		field	ir	f	\N	\N	\N	\N	\N
325	en	Note Read	ir.note.read,name	-1		model	ir	f	\N	\N	\N	\N	\N
326	en	Note	ir.note.read,note	-1		field	ir	f	\N	\N	\N	\N	\N
327	en	User	ir.note.read,user	-1		field	ir	f	\N	\N	\N	\N	\N
328	en	Cron	ir.cron,name	-1		model	ir	f	\N	\N	\N	\N	\N
329	en	Day	ir.cron,day	-1		field	ir	f	\N	\N	\N	\N	\N
330	en	Hour	ir.cron,hour	-1		field	ir	f	\N	\N	\N	\N	\N
331	en	Interval Number	ir.cron,interval_number	-1		field	ir	f	\N	\N	\N	\N	\N
332	en	Interval Type	ir.cron,interval_type	-1		field	ir	f	\N	\N	\N	\N	\N
333	en	Minutes	ir.cron,interval_type	-1		selection	ir	f	\N	\N	\N	\N	\N
334	en	Hours	ir.cron,interval_type	-1		selection	ir	f	\N	\N	\N	\N	\N
335	en	Days	ir.cron,interval_type	-1		selection	ir	f	\N	\N	\N	\N	\N
336	en	Weeks	ir.cron,interval_type	-1		selection	ir	f	\N	\N	\N	\N	\N
337	en	Months	ir.cron,interval_type	-1		selection	ir	f	\N	\N	\N	\N	\N
338	en	Method	ir.cron,method	-1		field	ir	f	\N	\N	\N	\N	\N
339	en	Run On Time Triggers	ir.cron,method	-1		selection	ir	f	\N	\N	\N	\N	\N
340	en	Minute	ir.cron,minute	-1		field	ir	f	\N	\N	\N	\N	\N
341	en	Next Call	ir.cron,next_call	-1		field	ir	f	\N	\N	\N	\N	\N
342	en	Day of Week	ir.cron,weekday	-1		field	ir	f	\N	\N	\N	\N	\N
343	en	Language	ir.lang,name	-1		model	ir	f	\N	\N	\N	\N	\N
344	en	AM	ir.lang,am	-1		field	ir	f	\N	\N	\N	\N	\N
345	en	Code	ir.lang,code	-1		field	ir	f	\N	\N	\N	\N	\N
346	en	RFC 4646 tag: http://tools.ietf.org/html/rfc4646	ir.lang,code	-1		help	ir	f	\N	\N	\N	\N	\N
347	en	Date	ir.lang,date	-1		field	ir	f	\N	\N	\N	\N	\N
348	en	Decimal Separator	ir.lang,decimal_point	-1		field	ir	f	\N	\N	\N	\N	\N
349	en	Direction	ir.lang,direction	-1		field	ir	f	\N	\N	\N	\N	\N
350	en	Left-to-right	ir.lang,direction	-1		selection	ir	f	\N	\N	\N	\N	\N
351	en	Right-to-left	ir.lang,direction	-1		selection	ir	f	\N	\N	\N	\N	\N
352	en	Grouping	ir.lang,grouping	-1		field	ir	f	\N	\N	\N	\N	\N
353	en	Decimal Separator	ir.lang,mon_decimal_point	-1		field	ir	f	\N	\N	\N	\N	\N
354	en	Grouping	ir.lang,mon_grouping	-1		field	ir	f	\N	\N	\N	\N	\N
355	en	Thousands Separator	ir.lang,mon_thousands_sep	-1		field	ir	f	\N	\N	\N	\N	\N
356	en	Negative Currency Symbol Precedes	ir.lang,n_cs_precedes	-1		field	ir	f	\N	\N	\N	\N	\N
357	en	Negative Separate by Space	ir.lang,n_sep_by_space	-1		field	ir	f	\N	\N	\N	\N	\N
358	en	Negative Sign Position	ir.lang,n_sign_posn	-1		field	ir	f	\N	\N	\N	\N	\N
359	en	Name	ir.lang,name	-1		field	ir	f	\N	\N	\N	\N	\N
360	en	Negative Sign	ir.lang,negative_sign	-1		field	ir	f	\N	\N	\N	\N	\N
361	en	Positive Currency Symbol Precedes	ir.lang,p_cs_precedes	-1		field	ir	f	\N	\N	\N	\N	\N
362	en	Positive Separate by Space	ir.lang,p_sep_by_space	-1		field	ir	f	\N	\N	\N	\N	\N
363	en	Positive Sign Position	ir.lang,p_sign_posn	-1		field	ir	f	\N	\N	\N	\N	\N
364	en	Parent Code	ir.lang,parent	-1		field	ir	f	\N	\N	\N	\N	\N
365	en	Code of the exceptional parent	ir.lang,parent	-1		help	ir	f	\N	\N	\N	\N	\N
367	en	Positive Sign	ir.lang,positive_sign	-1		field	ir	f	\N	\N	\N	\N	\N
368	en	Thousands Separator	ir.lang,thousands_sep	-1		field	ir	f	\N	\N	\N	\N	\N
369	en	Translatable	ir.lang,translatable	-1		field	ir	f	\N	\N	\N	\N	\N
370	en	Language Configuration Start	ir.lang.config.start,name	-1		model	ir	f	\N	\N	\N	\N	\N
371	en	Languages	ir.lang.config.start,languages	-1		field	ir	f	\N	\N	\N	\N	\N
372	en	Export	ir.export,name	-1		model	ir	f	\N	\N	\N	\N	\N
373	en	Fields	ir.export,export_fields	-1		field	ir	f	\N	\N	\N	\N	\N
374	en	Name	ir.export,name	-1		field	ir	f	\N	\N	\N	\N	\N
375	en	Resource	ir.export,resource	-1		field	ir	f	\N	\N	\N	\N	\N
376	en	Export line	ir.export.line,name	-1		model	ir	f	\N	\N	\N	\N	\N
377	en	Export	ir.export.line,export	-1		field	ir	f	\N	\N	\N	\N	\N
378	en	Name	ir.export.line,name	-1		field	ir	f	\N	\N	\N	\N	\N
379	en	Rule group	ir.rule.group,name	-1		model	ir	f	\N	\N	\N	\N	\N
380	en	Default	ir.rule.group,default_p	-1		field	ir	f	\N	\N	\N	\N	\N
381	en	Add this rule to all users by default.	ir.rule.group,default_p	-1		help	ir	f	\N	\N	\N	\N	\N
382	en	Global	ir.rule.group,global_p	-1		field	ir	f	\N	\N	\N	\N	\N
383	en	Make the rule global \nso every users must follow this rule.	ir.rule.group,global_p	-1		help	ir	f	\N	\N	\N	\N	\N
384	en	Groups	ir.rule.group,groups	-1		field	ir	f	\N	\N	\N	\N	\N
385	en	Model	ir.rule.group,model	-1		field	ir	f	\N	\N	\N	\N	\N
386	en	Name	ir.rule.group,name	-1		field	ir	f	\N	\N	\N	\N	\N
387	en	Displayed to users when access error is raised for this rule.	ir.rule.group,name	-1		help	ir	f	\N	\N	\N	\N	\N
388	en	Create Access	ir.rule.group,perm_create	-1		field	ir	f	\N	\N	\N	\N	\N
389	en	Delete Access	ir.rule.group,perm_delete	-1		field	ir	f	\N	\N	\N	\N	\N
390	en	Read Access	ir.rule.group,perm_read	-1		field	ir	f	\N	\N	\N	\N	\N
391	en	Write Access	ir.rule.group,perm_write	-1		field	ir	f	\N	\N	\N	\N	\N
392	en	Tests	ir.rule.group,rules	-1		field	ir	f	\N	\N	\N	\N	\N
393	en	The rule is satisfied if at least one test is True.	ir.rule.group,rules	-1		help	ir	f	\N	\N	\N	\N	\N
394	en	Rule	ir.rule,name	-1		model	ir	f	\N	\N	\N	\N	\N
395	en	Domain	ir.rule,domain	-1		field	ir	f	\N	\N	\N	\N	\N
396	en	Domain is evaluated with a PYSON context containing:\n- "user" as the current user	ir.rule,domain	-1		help	ir	f	\N	\N	\N	\N	\N
397	en	Group	ir.rule,rule_group	-1		field	ir	f	\N	\N	\N	\N	\N
398	en	Module	ir.module,name	-1		model	ir	f	\N	\N	\N	\N	\N
399	en	Childs	ir.module,childs	-1		field	ir	f	\N	\N	\N	\N	\N
400	en	Dependencies	ir.module,dependencies	-1		field	ir	f	\N	\N	\N	\N	\N
401	en	Name	ir.module,name	-1		field	ir	f	\N	\N	\N	\N	\N
402	en	Parents	ir.module,parents	-1		field	ir	f	\N	\N	\N	\N	\N
403	en	State	ir.module,state	-1		field	ir	f	\N	\N	\N	\N	\N
404	en	Not Activated	ir.module,state	-1		selection	ir	f	\N	\N	\N	\N	\N
405	en	Activated	ir.module,state	-1		selection	ir	f	\N	\N	\N	\N	\N
406	en	To be upgraded	ir.module,state	-1		selection	ir	f	\N	\N	\N	\N	\N
407	en	To be removed	ir.module,state	-1		selection	ir	f	\N	\N	\N	\N	\N
408	en	To be activated	ir.module,state	-1		selection	ir	f	\N	\N	\N	\N	\N
409	en	Version	ir.module,version	-1		field	ir	f	\N	\N	\N	\N	\N
410	en	Module dependency	ir.module.dependency,name	-1		model	ir	f	\N	\N	\N	\N	\N
411	en	Module	ir.module.dependency,module	-1		field	ir	f	\N	\N	\N	\N	\N
412	en	Name	ir.module.dependency,name	-1		field	ir	f	\N	\N	\N	\N	\N
413	en	State	ir.module.dependency,state	-1		field	ir	f	\N	\N	\N	\N	\N
414	en	Not Activated	ir.module.dependency,state	-1		selection	ir	f	\N	\N	\N	\N	\N
415	en	Activated	ir.module.dependency,state	-1		selection	ir	f	\N	\N	\N	\N	\N
416	en	To be upgraded	ir.module.dependency,state	-1		selection	ir	f	\N	\N	\N	\N	\N
417	en	To be removed	ir.module.dependency,state	-1		selection	ir	f	\N	\N	\N	\N	\N
418	en	To be activated	ir.module.dependency,state	-1		selection	ir	f	\N	\N	\N	\N	\N
419	en	Unknown	ir.module.dependency,state	-1		selection	ir	f	\N	\N	\N	\N	\N
420	en	Config wizard to run after activating a module	ir.module.config_wizard.item,name	-1		model	ir	f	\N	\N	\N	\N	\N
421	en	Action	ir.module.config_wizard.item,action	-1		field	ir	f	\N	\N	\N	\N	\N
422	en	State	ir.module.config_wizard.item,state	-1		field	ir	f	\N	\N	\N	\N	\N
423	en	Open	ir.module.config_wizard.item,state	-1		selection	ir	f	\N	\N	\N	\N	\N
424	en	Done	ir.module.config_wizard.item,state	-1		selection	ir	f	\N	\N	\N	\N	\N
425	en	Module Config Wizard First	ir.module.config_wizard.first,name	-1		model	ir	f	\N	\N	\N	\N	\N
426	en	Module Config Wizard Other	ir.module.config_wizard.other,name	-1		model	ir	f	\N	\N	\N	\N	\N
427	en	Percentage	ir.module.config_wizard.other,percentage	-1		field	ir	f	\N	\N	\N	\N	\N
428	en	Module Config Wizard Done	ir.module.config_wizard.done,name	-1		model	ir	f	\N	\N	\N	\N	\N
429	en	Module Activate Upgrade Start	ir.module.activate_upgrade.start,name	-1		model	ir	f	\N	\N	\N	\N	\N
430	en	Modules to update	ir.module.activate_upgrade.start,module_info	-1		field	ir	f	\N	\N	\N	\N	\N
431	en	Module Activate Upgrade Done	ir.module.activate_upgrade.done,name	-1		model	ir	f	\N	\N	\N	\N	\N
432	en	Cache	ir.cache,name	-1		model	ir	f	\N	\N	\N	\N	\N
433	en	Name	ir.cache,name	-1		field	ir	f	\N	\N	\N	\N	\N
434	en	Timestamp	ir.cache,timestamp	-1		field	ir	f	\N	\N	\N	\N	\N
435	en	Date	ir.date,name	-1		model	ir	f	\N	\N	\N	\N	\N
436	en	Trigger	ir.trigger,name	-1		model	ir	f	\N	\N	\N	\N	\N
437	en	Action	ir.trigger,action	-1		field	ir	f	\N	\N	\N	\N	\N
438	en	Condition	ir.trigger,condition	-1		field	ir	f	\N	\N	\N	\N	\N
439	en	A PYSON statement evaluated with record represented by "self"\nIt triggers the action if true.	ir.trigger,condition	-1		help	ir	f	\N	\N	\N	\N	\N
440	en	Limit Number	ir.trigger,limit_number	-1		field	ir	f	\N	\N	\N	\N	\N
441	en	Limit the number of call to "Action Function" by records.\n0 for no limit.	ir.trigger,limit_number	-1		help	ir	f	\N	\N	\N	\N	\N
442	en	Minimum Delay	ir.trigger,minimum_time_delay	-1		field	ir	f	\N	\N	\N	\N	\N
443	en	Set a minimum time delay between call to "Action Function" for the same record.\nempty for no delay.	ir.trigger,minimum_time_delay	-1		help	ir	f	\N	\N	\N	\N	\N
444	en	Model	ir.trigger,model	-1		field	ir	f	\N	\N	\N	\N	\N
445	en	Name	ir.trigger,name	-1		field	ir	f	\N	\N	\N	\N	\N
446	en	On Create	ir.trigger,on_create	-1		field	ir	f	\N	\N	\N	\N	\N
447	en	On Delete	ir.trigger,on_delete	-1		field	ir	f	\N	\N	\N	\N	\N
448	en	On Time	ir.trigger,on_time	-1		field	ir	f	\N	\N	\N	\N	\N
449	en	On Write	ir.trigger,on_write	-1		field	ir	f	\N	\N	\N	\N	\N
450	en	Trigger Log	ir.trigger.log,name	-1		model	ir	f	\N	\N	\N	\N	\N
451	en	Record ID	ir.trigger.log,record_id	-1		field	ir	f	\N	\N	\N	\N	\N
452	en	Trigger	ir.trigger.log,trigger	-1		field	ir	f	\N	\N	\N	\N	\N
453	en	Session	ir.session,name	-1		model	ir	f	\N	\N	\N	\N	\N
454	en	Key	ir.session,key	-1		field	ir	f	\N	\N	\N	\N	\N
455	en	Session Wizard	ir.session.wizard,name	-1		model	ir	f	\N	\N	\N	\N	\N
456	en	Data	ir.session.wizard,data	-1		field	ir	f	\N	\N	\N	\N	\N
457	en	Queue	ir.queue,name	-1		model	ir	f	\N	\N	\N	\N	\N
458	en	Data	ir.queue,data	-1		field	ir	f	\N	\N	\N	\N	\N
459	en	Dequeued at	ir.queue,dequeued_at	-1		field	ir	f	\N	\N	\N	\N	\N
460	en	Enqueued at	ir.queue,enqueued_at	-1		field	ir	f	\N	\N	\N	\N	\N
461	en	Expected at	ir.queue,expected_at	-1		field	ir	f	\N	\N	\N	\N	\N
462	en	When the task should be done.	ir.queue,expected_at	-1		help	ir	f	\N	\N	\N	\N	\N
463	en	Finished at	ir.queue,finished_at	-1		field	ir	f	\N	\N	\N	\N	\N
464	en	Name	ir.queue,name	-1		field	ir	f	\N	\N	\N	\N	\N
465	en	Scheduled at	ir.queue,scheduled_at	-1		field	ir	f	\N	\N	\N	\N	\N
466	en	When the task can start.	ir.queue,scheduled_at	-1		help	ir	f	\N	\N	\N	\N	\N
467	en	Month	ir.calendar.month,name	-1		model	ir	f	\N	\N	\N	\N	\N
468	en	Abbreviation	ir.calendar.month,abbreviation	-1		field	ir	f	\N	\N	\N	\N	\N
469	en	Index	ir.calendar.month,index	-1		field	ir	f	\N	\N	\N	\N	\N
470	en	Name	ir.calendar.month,name	-1		field	ir	f	\N	\N	\N	\N	\N
471	en	Day	ir.calendar.day,name	-1		model	ir	f	\N	\N	\N	\N	\N
472	en	Abbreviation	ir.calendar.day,abbreviation	-1		field	ir	f	\N	\N	\N	\N	\N
473	en	Index	ir.calendar.day,index	-1		field	ir	f	\N	\N	\N	\N	\N
474	en	Name	ir.calendar.day,name	-1		field	ir	f	\N	\N	\N	\N	\N
475	en	Message	ir.message,name	-1		model	ir	f	\N	\N	\N	\N	\N
476	en	Text	ir.message,text	-1		field	ir	f	\N	\N	\N	\N	\N
477	en	Cancel	ir.translation.set,start,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
478	en	Set	ir.translation.set,start,set_	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
479	en	OK	ir.translation.set,succeed,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
480	en	Cancel	ir.translation.clean,start,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
481	en	Clean	ir.translation.clean,start,clean	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
482	en	OK	ir.translation.clean,succeed,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
483	en	Cancel	ir.translation.update,start,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
484	en	Update	ir.translation.update,start,update	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
485	en	Close	ir.translation.export,result,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
486	en	Cancel	ir.translation.export,start,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
487	en	Export	ir.translation.export,start,export	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
488	en	Close	ir.ui.view.show,start,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
489	en	Cancel	ir.model.print_model_graph,start,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
490	en	Print	ir.model.print_model_graph,start,print_	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
491	en	OK	ir.module.config_wizard,done,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
492	en	Cancel	ir.module.config_wizard,first,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
493	en	OK	ir.module.config_wizard,first,action	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
494	en	Cancel	ir.module.config_wizard,other,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
495	en	Next	ir.module.config_wizard,other,action	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
496	en	OK	ir.module.activate_upgrade,done,config	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
497	en	Cancel	ir.module.activate_upgrade,start,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
498	en	Start Upgrade	ir.module.activate_upgrade,start,upgrade	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
499	en	Cancel	ir.lang.config,start,end	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
500	en	Load	ir.lang.config,start,load	-1		wizard_button	ir	f	\N	\N	\N	\N	\N
501	en	English	ir.lang,name	1	English	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
502	en	Administration	ir.ui.menu,name	1	Administration	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
503	en	User Interface	ir.ui.menu,name	2	User Interface	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
504	en	Icons	ir.action,name	1	Icons	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
505	en	Icons	ir.ui.menu,name	3	Icons	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
506	en	Menu	ir.action,name	2	Menu	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
507	en	Menu	ir.action,name	3	Menu	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
508	en	Menu	ir.ui.menu,name	4	Menu	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
509	en	Show View	ir.action,name	4	Show View	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
510	en	Views	ir.action,name	5	Views	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
511	en	Views	ir.ui.menu,name	5	Views	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
512	en	Show	ir.model.button,string	1	Show	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
513	en	View Tree Width	ir.action,name	6	View Tree Width	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
514	en	View Tree Width	ir.ui.menu,name	6	View Tree Width	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
515	en	Tree State	ir.action,name	7	Tree State	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
516	en	Tree State	ir.ui.menu,name	7	Tree State	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
517	en	View Search	ir.action,name	8	View Search	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
518	en	View Search	ir.ui.menu,name	8	View Search	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
519	en	Actions	ir.ui.menu,name	9	Actions	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
520	en	Actions	ir.action,name	9	Actions	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
521	en	Actions	ir.ui.menu,name	10	Actions	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
522	en	Reports	ir.action,name	10	Reports	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
523	en	Reports	ir.ui.menu,name	11	Reports	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
524	en	Window Actions	ir.action,name	11	Window Actions	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
525	en	Window Actions	ir.ui.menu,name	12	Window Actions	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
526	en	Wizards	ir.action,name	12	Wizards	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
527	en	Wizards	ir.ui.menu,name	13	Wizards	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
528	en	URLs	ir.action,name	13	URLs	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
529	en	URLs	ir.ui.menu,name	14	URLs	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
530	en	Models	ir.ui.menu,name	15	Models	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
531	en	Models	ir.action,name	14	Models	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
532	en	Models	ir.ui.menu,name	16	Models	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
533	en	Fields	ir.action,name	15	Fields	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
534	en	Fields	ir.ui.menu,name	17	Fields	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
535	en	Models Access	ir.action,name	16	Models Access	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
536	en	Models Access	ir.ui.menu,name	18	Models Access	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
537	en	Access	ir.action,name	17	Access	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
538	en	Fields Access	ir.action,name	18	Fields Access	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
539	en	Fields Access	ir.ui.menu,name	19	Fields Access	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
540	en	Access	ir.action,name	19	Access	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
541	en	Graph	ir.action,name	20	Graph	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
542	en	Graph	ir.action,name	21	Graph	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
543	en	Workflow Graph	ir.action,name	22	Workflow Graph	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
544	en	Buttons	ir.action,name	23	Buttons	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
545	en	Buttons	ir.ui.menu,name	20	Buttons	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
546	en	Clicks	ir.action,name	24	Clicks	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
547	en	Data	ir.action,name	25	Data	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
548	en	Out of Sync	ir.action.act_window.domain,name	1	Out of Sync	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
549	en	All	ir.action.act_window.domain,name	2	All	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
550	en	Data	ir.ui.menu,name	21	Data	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
551	en	Sync	ir.model.button,string	2	Sync	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
552	en	Sequences	ir.ui.menu,name	22	Sequences	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
553	en	Sequences	ir.action,name	26	Sequences	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
554	en	Sequences	ir.ui.menu,name	23	Sequences	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
555	en	Sequences Strict	ir.action,name	27	Sequences Strict	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
556	en	Sequences Strict	ir.ui.menu,name	24	Sequences Strict	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
557	en	Sequence Types	ir.action,name	28	Sequence Types	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
558	en	Sequence Types	ir.ui.menu,name	25	Sequence Types	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
559	en	Attachments	ir.action,name	29	Attachments	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
560	en	Attachments	ir.ui.menu,name	26	Attachments	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
561	en	Notes	ir.action,name	30	Notes	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
769	en	User	res.user,name	-1		model	res	f	\N	\N	\N	\N	\N
770	en	Actions	res.user,actions	-1		field	res	f	\N	\N	\N	\N	\N
771	en	Actions that will be run at login.	res.user,actions	-1		help	res	f	\N	\N	\N	\N	\N
772	en	Applications	res.user,applications	-1		field	res	f	\N	\N	\N	\N	\N
773	en	Email	res.user,email	-1		field	res	f	\N	\N	\N	\N	\N
774	en	Groups	res.user,groups	-1		field	res	f	\N	\N	\N	\N	\N
775	en	Language	res.user,language	-1		field	res	f	\N	\N	\N	\N	\N
776	en	Language Direction	res.user,language_direction	-1		field	res	f	\N	\N	\N	\N	\N
777	en	Login	res.user,login	-1		field	res	f	\N	\N	\N	\N	\N
778	en	Menu Action	res.user,menu	-1		field	res	f	\N	\N	\N	\N	\N
779	en	Name	res.user,name	-1		field	res	f	\N	\N	\N	\N	\N
780	en	Password	res.user,password	-1		field	res	f	\N	\N	\N	\N	\N
781	en	Password Hash	res.user,password_hash	-1		field	res	f	\N	\N	\N	\N	\N
782	en	Reset Password	res.user,password_reset	-1		field	res	f	\N	\N	\N	\N	\N
783	en	Reset Password Expire	res.user,password_reset_expire	-1		field	res	f	\N	\N	\N	\N	\N
784	en	PySON Menu	res.user,pyson_menu	-1		field	res	f	\N	\N	\N	\N	\N
785	en	Sessions	res.user,sessions	-1		field	res	f	\N	\N	\N	\N	\N
786	en	Signature	res.user,signature	-1		field	res	f	\N	\N	\N	\N	\N
787	en	Status Bar	res.user,status_bar	-1		field	res	f	\N	\N	\N	\N	\N
788	en	Warnings	res.user,warnings	-1		field	res	f	\N	\N	\N	\N	\N
789	en	Login Attempt	res.user.login.attempt,name	-1		model	res	f	\N	\N	\N	\N	\N
790	en	IP Address	res.user.login.attempt,ip_address	-1		field	res	f	\N	\N	\N	\N	\N
791	en	IP Network	res.user.login.attempt,ip_network	-1		field	res	f	\N	\N	\N	\N	\N
792	en	Login	res.user.login.attempt,login	-1		field	res	f	\N	\N	\N	\N	\N
793	en	User - Action	res.user-ir.action,name	-1		model	res	f	\N	\N	\N	\N	\N
794	en	Action	res.user-ir.action,action	-1		field	res	f	\N	\N	\N	\N	\N
562	en	Notes	ir.ui.menu,name	27	Notes	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
563	en	Scheduler	ir.ui.menu,name	28	Scheduler	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
564	en	Scheduled Actions	ir.action,name	31	Scheduled Actions	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
565	en	Scheduled Actions	ir.ui.menu,name	29	Scheduled Actions	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
566	en	Run Once	ir.model.button,string	3	Run Once	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
567	en	Localization	ir.ui.menu,name	30	Localization	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
568	en	Bulgarian	ir.lang,name	2	Bulgarian	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
569	en	Català	ir.lang,name	3	Català	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
570	en	Czech	ir.lang,name	4	Czech	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
571	en	German	ir.lang,name	5	German	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
572	en	Spanish	ir.lang,name	6	Spanish	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
573	en	Spanish (Latin American)	ir.lang,name	7	Spanish (Latin American)	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
574	en	Estonian	ir.lang,name	8	Estonian	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
575	en	Persian	ir.lang,name	9	Persian	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
576	en	Finnish	ir.lang,name	10	Finnish	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
577	en	French	ir.lang,name	11	French	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
578	en	Hungarian	ir.lang,name	12	Hungarian	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
579	en	Indonesian	ir.lang,name	13	Indonesian	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
580	en	Italian	ir.lang,name	14	Italian	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
581	en	Lao	ir.lang,name	15	Lao	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
582	en	Lithuanian	ir.lang,name	16	Lithuanian	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
583	en	Dutch	ir.lang,name	17	Dutch	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
584	en	Polish	ir.lang,name	18	Polish	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
585	en	Portuguese	ir.lang,name	19	Portuguese	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
586	en	Russian	ir.lang,name	20	Russian	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
587	en	Slovenian	ir.lang,name	21	Slovenian	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
588	en	Turkish	ir.lang,name	22	Turkish	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
589	en	Chinese Simplified	ir.lang,name	23	Chinese Simplified	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
590	en	Languages	ir.action,name	32	Languages	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
591	en	Languages	ir.ui.menu,name	31	Languages	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
592	en	Load translations	ir.model.button,string	4	Load translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
593	en	Are you sure you want to load languages' translations?	ir.model.button,confirm	4	Are you sure you want to load languages' translations?	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
594	en	Unload translations	ir.model.button,string	5	Unload translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
595	en	Are you sure you want to remove languages' translations?	ir.model.button,confirm	5	Are you sure you want to remove languages' translations?	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
596	en	Configure Languages	ir.action,name	33	Configure Languages	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
597	en	Translations	ir.action,name	34	Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
598	en	Modules	ir.action.act_window.domain,name	3	Modules	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
599	en	Local	ir.action.act_window.domain,name	4	Local	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
600	en	Translations	ir.ui.menu,name	32	Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
601	en	Translations	ir.action,name	35	Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
602	en	Translations	ir.action,name	36	Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
603	en	Set Translations	ir.action,name	37	Set Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
604	en	Set Translations	ir.ui.menu,name	33	Set Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
605	en	Clean Translations	ir.action,name	38	Clean Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
606	en	Clean Translations	ir.ui.menu,name	34	Clean Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
607	en	Synchronize Translations	ir.action,name	39	Synchronize Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
608	en	Synchronize Translations	ir.ui.menu,name	35	Synchronize Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
609	en	Export Translations	ir.action,name	40	Export Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
610	en	Export Translations	ir.ui.menu,name	36	Export Translations	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
611	en	Exports	ir.action,name	41	Exports	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
612	en	Exports	ir.ui.menu,name	37	Exports	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
613	en	Record Rules	ir.action,name	42	Record Rules	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
614	en	Record Rules	ir.ui.menu,name	38	Record Rules	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
795	en	User	res.user-ir.action,user	-1		field	res	f	\N	\N	\N	\N	\N
796	en	User - Group	res.user-res.group,name	-1		model	res	f	\N	\N	\N	\N	\N
797	en	Group	res.user-res.group,group	-1		field	res	f	\N	\N	\N	\N	\N
798	en	User	res.user-res.group,user	-1		field	res	f	\N	\N	\N	\N	\N
799	en	User Warning	res.user.warning,name	-1		model	res	f	\N	\N	\N	\N	\N
800	en	Always	res.user.warning,always	-1		field	res	f	\N	\N	\N	\N	\N
801	en	Name	res.user.warning,name	-1		field	res	f	\N	\N	\N	\N	\N
802	en	User	res.user.warning,user	-1		field	res	f	\N	\N	\N	\N	\N
803	en	User Application	res.user.application,name	-1		model	res	f	\N	\N	\N	\N	\N
804	en	Application	res.user.application,application	-1		field	res	f	\N	\N	\N	\N	\N
805	en	Key	res.user.application,key	-1		field	res	f	\N	\N	\N	\N	\N
806	en	State	res.user.application,state	-1		field	res	f	\N	\N	\N	\N	\N
807	en	Requested	res.user.application,state	-1		selection	res	f	\N	\N	\N	\N	\N
808	en	Validated	res.user.application,state	-1		selection	res	f	\N	\N	\N	\N	\N
809	en	Cancelled	res.user.application,state	-1		selection	res	f	\N	\N	\N	\N	\N
615	en	Modules	ir.ui.menu,name	39	Modules	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
616	en	Modules	ir.action,name	43	Modules	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
617	en	Modules	ir.ui.menu,name	40	Modules	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
618	en	Mark for Activation	ir.model.button,string	6	Mark for Activation	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
619	en	Cancel Activation	ir.model.button,string	7	Cancel Activation	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
620	en	Mark for Deactivation (beta)	ir.model.button,string	8	Mark for Deactivation (beta)	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
621	en	Cancel Deactivation	ir.model.button,string	9	Cancel Deactivation	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
622	en	Mark for Upgrade	ir.model.button,string	10	Mark for Upgrade	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
623	en	Cancel Upgrade	ir.model.button,string	11	Cancel Upgrade	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
624	en	Config Wizard Items	ir.action,name	44	Config Wizard Items	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
625	en	Config Wizard Items	ir.ui.menu,name	41	Config Wizard Items	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
626	en	Module Configuration	ir.action,name	45	Module Configuration	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
627	en	Perform Pending Activation/Upgrade	ir.action,name	46	Perform Pending Activation/Upgrade	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
628	en	Perform Pending Activation/Upgrade	ir.ui.menu,name	42	Perform Pending Activation/Upgrade	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
629	en	Configure Modules	ir.action,name	47	Configure Modules	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
630	en	Triggers	ir.action,name	48	Triggers	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
631	en	Triggers	ir.ui.menu,name	43	Triggers	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
632	en	January	ir.calendar.month,name	1	January	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
633	en	February	ir.calendar.month,name	2	February	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
634	en	March	ir.calendar.month,name	3	March	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
635	en	April	ir.calendar.month,name	4	April	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
636	en	May	ir.calendar.month,name	5	May	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
637	en	June	ir.calendar.month,name	6	June	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
638	en	July	ir.calendar.month,name	7	July	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
639	en	August	ir.calendar.month,name	8	August	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
640	en	September	ir.calendar.month,name	9	September	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
641	en	October	ir.calendar.month,name	10	October	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
642	en	November	ir.calendar.month,name	11	November	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
643	en	December	ir.calendar.month,name	12	December	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
644	en	Jan	ir.calendar.month,abbreviation	1	Jan	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
645	en	Feb	ir.calendar.month,abbreviation	2	Feb	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
646	en	Mar	ir.calendar.month,abbreviation	3	Mar	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
647	en	Apr	ir.calendar.month,abbreviation	4	Apr	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
648	en	May	ir.calendar.month,abbreviation	5	May	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
649	en	Jun	ir.calendar.month,abbreviation	6	Jun	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
650	en	Jul	ir.calendar.month,abbreviation	7	Jul	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
651	en	Aug	ir.calendar.month,abbreviation	8	Aug	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
652	en	Sep	ir.calendar.month,abbreviation	9	Sep	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
653	en	Oct	ir.calendar.month,abbreviation	10	Oct	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
654	en	Nov	ir.calendar.month,abbreviation	11	Nov	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
655	en	Dec	ir.calendar.month,abbreviation	12	Dec	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
656	en	Monday	ir.calendar.day,name	1	Monday	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
657	en	Tuesday	ir.calendar.day,name	2	Tuesday	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
658	en	Wednesday	ir.calendar.day,name	3	Wednesday	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
659	en	Thursday	ir.calendar.day,name	4	Thursday	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
660	en	Friday	ir.calendar.day,name	5	Friday	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
661	en	Saturday	ir.calendar.day,name	6	Saturday	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
662	en	Sunday	ir.calendar.day,name	7	Sunday	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
663	en	Mon	ir.calendar.day,abbreviation	1	Mon	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
664	en	Tue	ir.calendar.day,abbreviation	2	Tue	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
665	en	Wed	ir.calendar.day,abbreviation	3	Wed	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
666	en	Thu	ir.calendar.day,abbreviation	4	Thu	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
667	en	Fri	ir.calendar.day,abbreviation	5	Fri	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
668	en	Sat	ir.calendar.day,abbreviation	6	Sat	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
669	en	Sun	ir.calendar.day,abbreviation	7	Sun	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
670	en	Message	ir.action,name	49	Message	model	ir	f	2020-09-01 08:49:55.946968	0	\N	2020-09-01 08:49:55.946968	0
671	en	Messages	ir.ui.menu,name	44	Messages	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
672	en	ID	ir.message,text	1	ID	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
673	en	Created by	ir.message,text	2	Created by	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
674	en	Created at	ir.message,text	3	Created at	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
675	en	Edited by	ir.message,text	4	Edited by	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
676	en	Edited at	ir.message,text	5	Edited at	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
677	en	Record Name	ir.message,text	6	Record Name	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
678	en	Active	ir.message,text	7	Active	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
679	en	Uncheck to exclude from future use.	ir.message,text	8	Uncheck to exclude from future use.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
680	en	Name	ir.message,text	9	Name	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
681	en	String	ir.message,text	10	String	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
682	en	Help	ir.message,text	11	Help	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
683	en	Type	ir.message,text	12	Type	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
684	en	Boolean	ir.message,text	13	Boolean	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
685	en	Integer	ir.message,text	14	Integer	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
686	en	Char	ir.message,text	15	Char	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
687	en	Float	ir.message,text	16	Float	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
688	en	Numeric	ir.message,text	17	Numeric	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
689	en	Date	ir.message,text	18	Date	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
690	en	DateTime	ir.message,text	19	DateTime	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
691	en	Selection	ir.message,text	20	Selection	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
692	en	MultiSelection	ir.message,text	21	MultiSelection	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
693	en	Digits	ir.message,text	22	Digits	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
694	en	Domain	ir.message,text	23	Domain	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
695	en	A couple of key and label separated by ":" per line.	ir.message,text	24	A couple of key and label separated by ":" per line.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
696	en	Selection Sorted	ir.message,text	25	Selection Sorted	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
697	en	If the selection must be sorted on label.	ir.message,text	26	If the selection must be sorted on label.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
698	en	Selection JSON	ir.message,text	27	Selection JSON	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
699	en	Sequence	ir.message,text	28	Sequence	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
700	en	ID must be positive.	ir.message,text	29	ID must be positive.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
701	en	You are not allowed to modify this record.	ir.message,text	30	You are not allowed to modify this record.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
702	en	You are not allowed to delete this record.	ir.message,text	31	You are not allowed to delete this record.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
703	en	This record is part of the base configuration.	ir.message,text	32	This record is part of the base configuration.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
704	en	Relation not found: "%(value)r" in "%(model)s".	ir.message,text	33	Relation not found: "%(value)r" in "%(model)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
705	en	Too many relations found: "%(value)r" in "%(model)s".	ir.message,text	34	Too many relations found: "%(value)r" in "%(model)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
706	en	Syntax error for reference: "%(value)r" in "%(field)s".	ir.message,text	35	Syntax error for reference: "%(value)r" in "%(field)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
707	en	Syntax error for XML id: "%(value)r" in "%(field)s".	ir.message,text	36	Syntax error for XML id: "%(value)r" in "%(field)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
708	en	The value for field "%(field)s" in "%(model)s" is not valid according to its domain.	ir.message,text	37	The value for field "%(field)s" in "%(model)s" is not valid according to its domain.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
709	en	A value is required for field "%(field)s" in "%(model)s".	ir.message,text	38	A value is required for field "%(field)s" in "%(model)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
710	en	The value for field "%(field)s" in "%(model)s" is too long (%(size)i > %(max_size)i).	ir.message,text	39	The value for field "%(field)s" in "%(model)s" is too long (%(size)i > %(max_size)i).	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
711	en	The number of digits in the value "%(value)s" for field "%(field)s" in "%(model)s" exceeds the limit of "%(digits)i".	ir.message,text	40	The number of digits in the value "%(value)s" for field "%(field)s" in "%(model)s" exceeds the limit of "%(digits)i".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
712	en	The value "%(value)s" for field "%(field)s" in "%(model)s" is not one of the allowed options.	ir.message,text	41	The value "%(value)s" for field "%(field)s" in "%(model)s" is not one of the allowed options.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
713	en	The time value "%(value)s" for field "%(field)s" in "%(model)s" is not valid.	ir.message,text	42	The time value "%(value)s" for field "%(field)s" in "%(model)s" is not valid.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
714	en	The value "%(value)s" for field "%(field)s" in "%(model)s" does not exist.	ir.message,text	43	The value "%(value)s" for field "%(field)s" in "%(model)s" does not exist.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
715	en	The records could not be deleted because they are used by field "%(field)s" of "%(model)s".	ir.message,text	44	The records could not be deleted because they are used by field "%(field)s" of "%(model)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
716	en	You are not allowed to access "%(model)s".	ir.message,text	45	You are not allowed to access "%(model)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
717	en	You are not allowed to access "%(model)s.%(field)s".	ir.message,text	46	You are not allowed to access "%(model)s.%(field)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
718	en	You are not allowed to create records of "%(model)s" because they fail on at least one of these rules:\n%(rules)s	ir.message,text	47	You are not allowed to create records of "%(model)s" because they fail on at least one of these rules:\n%(rules)s	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
719	en	You are not allowed to read records "%(ids)s" of "%(model)s" because of at least one of these rules:\n%(rules)s	ir.message,text	48	You are not allowed to read records "%(ids)s" of "%(model)s" because of at least one of these rules:\n%(rules)s	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
720	en	You are trying to read records "%(ids)s" of "%(model)s" that don't exist anymore.	ir.message,text	49	You are trying to read records "%(ids)s" of "%(model)s" that don't exist anymore.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
721	en	You are not allowed to write to records "%(ids)s" of "%(model)s" because of at least one of these rules:\n%(rules)s	ir.message,text	50	You are not allowed to write to records "%(ids)s" of "%(model)s" because of at least one of these rules:\n%(rules)s	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
722	en	You are trying to write to records "%(ids)s" of "%(model)s" that don't exist anymore.	ir.message,text	51	You are trying to write to records "%(ids)s" of "%(model)s" that don't exist anymore.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
723	en	You are not allowed to delete records "%(ids)s" of "%(model)s" because of at lease one of those rules:\n%(rules)s	ir.message,text	52	You are not allowed to delete records "%(ids)s" of "%(model)s" because of at lease one of those rules:\n%(rules)s	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
724	en	Invalid domain in schema "%(schema)s".	ir.message,text	53	Invalid domain in schema "%(schema)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
725	en	Invalid selection in schema "%(schema)s".	ir.message,text	54	Invalid selection in schema "%(schema)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
726	en	Recursion error: Record "%(rec_name)s" with parent "%(parent_rec_name)s" was configured as ancestor of itself.	ir.message,text	55	Recursion error: Record "%(rec_name)s" with parent "%(parent_rec_name)s" was configured as ancestor of itself.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
727	en	Missing search function for field "%(field)s" in "%(model)s".	ir.message,text	56	Missing search function for field "%(field)s" in "%(model)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
728	en	Missing setter function for field "%(field)s" in "%(model)s".	ir.message,text	57	Missing setter function for field "%(field)s" in "%(model)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
729	en	Calling button "%(button)s on "%(model)s" is not allowed.	ir.message,text	58	Calling button "%(button)s on "%(model)s" is not allowed.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
730	en	Invalid XML for view "%(name)s".	ir.message,text	59	Invalid XML for view "%(name)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
731	en	Wrong wizard model in keyword action "%(name)s".	ir.message,text	60	Wrong wizard model in keyword action "%(name)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
732	en	Invalid email definition for report "%(name)s".	ir.message,text	61	Invalid email definition for report "%(name)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
733	en	Invalid view "%(view)s" for action "%(action)s".	ir.message,text	62	Invalid view "%(view)s" for action "%(action)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
734	en	Invalid domain or search criteria "%(domain)s" for action "%(action)s".	ir.message,text	63	Invalid domain or search criteria "%(domain)s" for action "%(action)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
735	en	Invalid context "%(context)s" for action "%(action)s".	ir.message,text	64	Invalid context "%(context)s" for action "%(action)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
736	en	The condition "%(condition)s" is not a valid PYSON expression for button rule "%(rule)s".	ir.message,text	65	The condition "%(condition)s" is not a valid PYSON expression for button rule "%(rule)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
737	en	Missing sequence.	ir.message,text	66	Missing sequence.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
738	en	Invalid prefix "%(affix)s" for sequence "%(sequence)s".	ir.message,text	67	Invalid prefix "%(affix)s" for sequence "%(sequence)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
739	en	Invalid suffix "%(affix)s" for sequence "%(sequence)s".	ir.message,text	68	Invalid suffix "%(affix)s" for sequence "%(sequence)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
740	en	The "Last Timestamp" cannot be in the future for sequence "%s".	ir.message,text	69	The "Last Timestamp" cannot be in the future for sequence "%s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
741	en	Invalid grouping "%(grouping)s" for language "%(language)s".	ir.message,text	70	Invalid grouping "%(grouping)s" for language "%(language)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
742	en	Invalid date format "%(format)s" for language "%(language)s".	ir.message,text	71	Invalid date format "%(format)s" for language "%(language)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
743	en	The default language must be translatable.	ir.message,text	72	The default language must be translatable.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
744	en	The default language can not be deleted.	ir.message,text	73	The default language can not be deleted.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
745	en	Invalid domain in rule "%(name)s".	ir.message,text	74	Invalid domain in rule "%(name)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
746	en	You can not export translation "%(name)s" because it has been overridden by module "%(overriding_module)s".	ir.message,text	75	You can not export translation "%(name)s" because it has been overridden by module "%(overriding_module)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
747	en	You can not remove a module that is activated or that is about to be activated.	ir.message,text	76	You can not remove a module that is activated or that is about to be activated.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
748	en	Some activated modules depend on the ones you are trying to deactivate:	ir.message,text	77	Some activated modules depend on the ones you are trying to deactivate:	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
749	en	Condition "%(condition)s" is not a valid PYSON expression for trigger "%(trigger)s".	ir.message,text	78	Condition "%(condition)s" is not a valid PYSON expression for trigger "%(trigger)s".	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
750	en	Failed to save, please retry.	ir.message,text	79	Failed to save, please retry.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
751	en	Y	ir.message,text	80	Y	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
752	en	M	ir.message,text	81	M	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
753	en	w	ir.message,text	82	w	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
754	en	d	ir.message,text	83	d	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
755	en	h	ir.message,text	84	h	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
756	en	m	ir.message,text	85	m	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
757	en	s	ir.message,text	86	s	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
758	en	The resources to which this record must be copied.	ir.message,text	87	The resources to which this record must be copied.	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
759	en	Attachments	ir.message,text	88	Attachments	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
760	en	Notes	ir.message,text	89	Notes	model	ir	f	2020-09-01 08:49:55.946968	0	\N	\N	\N
761	en	Group	res.group,name	-1		model	res	f	\N	\N	\N	\N	\N
762	en	Buttons	res.group,buttons	-1		field	res	f	\N	\N	\N	\N	\N
763	en	Access Field	res.group,field_access	-1		field	res	f	\N	\N	\N	\N	\N
764	en	Access Menu	res.group,menu_access	-1		field	res	f	\N	\N	\N	\N	\N
765	en	Access Model	res.group,model_access	-1		field	res	f	\N	\N	\N	\N	\N
766	en	Name	res.group,name	-1		field	res	f	\N	\N	\N	\N	\N
767	en	Rules	res.group,rule_groups	-1		field	res	f	\N	\N	\N	\N	\N
768	en	Users	res.group,users	-1		field	res	f	\N	\N	\N	\N	\N
810	en	User	res.user.application,user	-1		field	res	f	\N	\N	\N	\N	\N
811	en	User Config Init	res.user.config.start,name	-1		model	res	f	\N	\N	\N	\N	\N
812	en	UI Menu - Group	ir.ui.menu-res.group,name	-1		model	res	f	\N	\N	\N	\N	\N
813	en	Group	ir.ui.menu-res.group,group	-1		field	res	f	\N	\N	\N	\N	\N
814	en	Menu	ir.ui.menu-res.group,menu	-1		field	res	f	\N	\N	\N	\N	\N
815	en	Action - Group	ir.action-res.group,name	-1		model	res	f	\N	\N	\N	\N	\N
816	en	Action	ir.action-res.group,action	-1		field	res	f	\N	\N	\N	\N	\N
817	en	Group	ir.action-res.group,group	-1		field	res	f	\N	\N	\N	\N	\N
818	en	Model Button - Group	ir.model.button-res.group,name	-1		model	res	f	\N	\N	\N	\N	\N
819	en	Button	ir.model.button-res.group,button	-1		field	res	f	\N	\N	\N	\N	\N
820	en	Group	ir.model.button-res.group,group	-1		field	res	f	\N	\N	\N	\N	\N
821	en	Group	ir.model.button.rule,group	-1		field	res	f	\N	\N	\N	\N	\N
822	en	User	ir.model.button.click,user	-1		field	res	f	\N	\N	\N	\N	\N
823	en	Rule Group - Group	ir.rule.group-res.group,name	-1		model	res	f	\N	\N	\N	\N	\N
824	en	Group	ir.rule.group-res.group,group	-1		field	res	f	\N	\N	\N	\N	\N
825	en	Rule Group	ir.rule.group-res.group,rule_group	-1		field	res	f	\N	\N	\N	\N	\N
826	en	User Groups	ir.sequence.type,groups	-1		field	res	f	\N	\N	\N	\N	\N
827	en	Groups allowed to edit the sequences of this type.	ir.sequence.type,groups	-1		help	res	f	\N	\N	\N	\N	\N
828	en	Sequence Type - Group	ir.sequence.type-res.group,name	-1		model	res	f	\N	\N	\N	\N	\N
829	en	User Groups	ir.sequence.type-res.group,group	-1		field	res	f	\N	\N	\N	\N	\N
830	en	Sequence Type	ir.sequence.type-res.group,sequence_type	-1		field	res	f	\N	\N	\N	\N	\N
831	en	User Groups	ir.sequence,groups	-1		field	res	f	\N	\N	\N	\N	\N
832	en	User Groups	ir.sequence.strict,groups	-1		field	res	f	\N	\N	\N	\N	\N
833	en	Groups	ir.export,groups	-1		field	res	f	\N	\N	\N	\N	\N
834	en	The user groups that can use the export.	ir.export,groups	-1		help	res	f	\N	\N	\N	\N	\N
835	en	Modification Groups	ir.export,write_groups	-1		field	res	f	\N	\N	\N	\N	\N
836	en	The user groups that can modify the export.	ir.export,write_groups	-1		help	res	f	\N	\N	\N	\N	\N
837	en	Export Group	ir.export-res.group,name	-1		model	res	f	\N	\N	\N	\N	\N
838	en	Export	ir.export-res.group,export	-1		field	res	f	\N	\N	\N	\N	\N
839	en	Group	ir.export-res.group,group	-1		field	res	f	\N	\N	\N	\N	\N
840	en	Export Modification Group	ir.export-write-res.group,name	-1		model	res	f	\N	\N	\N	\N	\N
841	en	Export	ir.export-write-res.group,export	-1		field	res	f	\N	\N	\N	\N	\N
842	en	Group	ir.export-write-res.group,group	-1		field	res	f	\N	\N	\N	\N	\N
843	en	Cancel	res.user.config,start,end	-1		wizard_button	res	f	\N	\N	\N	\N	\N
844	en	OK	res.user.config,start,user	-1		wizard_button	res	f	\N	\N	\N	\N	\N
845	en	End	res.user.config,user,end	-1		wizard_button	res	f	\N	\N	\N	\N	\N
846	en	Add	res.user.config,user,add	-1		wizard_button	res	f	\N	\N	\N	\N	\N
847	en	Users	ir.ui.menu,name	45	Users	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
848	en	Administration	res.group,name	1	Administration	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
849	en	Groups	ir.action,name	50	Groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	2020-09-01 08:50:28.180942	0
850	en	Groups	ir.ui.menu,name	46	Groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
851	en	Users	ir.action,name	51	Users	model	res	f	2020-09-01 08:50:28.180942	0	\N	2020-09-01 08:50:28.180942	0
852	en	Users	ir.ui.menu,name	47	Users	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
853	en	Reset Password	ir.model.button,string	12	Reset Password	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
854	en	Send by email a new temporary password to the user	ir.model.button,help	12	Send by email a new temporary password to the user	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
855	en	Configure Users	ir.action,name	52	Configure Users	model	res	f	2020-09-01 08:50:28.180942	0	\N	2020-09-01 08:50:28.180942	0
856	en	Own warning	ir.rule.group,name	1	Own warning	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
857	en	Own user application	ir.rule.group,name	2	Own user application	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
858	en	Any user application	ir.rule.group,name	3	Any user application	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
859	en	Reset Password	ir.action,name	53	Reset Password	model	res	f	2020-09-01 08:50:28.180942	0	\N	2020-09-01 08:50:28.180942	0
860	en	Validate	ir.model.button,string	13	Validate	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
861	en	Cancel	ir.model.button,string	14	Cancel	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
862	en	User in groups	ir.rule.group,name	4	User in groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
863	en	User in groups	ir.rule.group,name	5	User in groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
864	en	User in groups	ir.rule.group,name	6	User in groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
865	en	User in groups	ir.rule.group,name	7	User in groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
866	en	User in groups	ir.rule.group,name	8	User in groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
867	en	User in groups	ir.rule.group,name	9	User in groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
868	en	User in groups	ir.rule.group,name	10	User in groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
869	en	User in groups	ir.rule.group,name	11	User in groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
870	en	User in groups	ir.rule.group,name	12	User in groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
871	en	Own view search	ir.rule.group,name	13	Own view search	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
872	en	Any view search	ir.rule.group,name	14	Any view search	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
873	en	Own session	ir.rule.group,name	15	Own session	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
874	en	Own session	ir.rule.group,name	16	Own session	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
875	en	Own favorite	ir.rule.group,name	17	Own favorite	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
942	en	Commune	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
943	en	Constitutional province	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
944	en	Council area	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
945	en	Country	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
946	en	County	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
876	en	User in groups	ir.rule.group,name	18	User in groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
877	en	User in modification groups	ir.rule.group,name	19	User in modification groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
878	en	Any export	ir.rule.group,name	20	Any export	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
879	en	User in groups	ir.rule.group,name	21	User in groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
880	en	User in modification groups	ir.rule.group,name	22	User in modification groups	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
881	en	Any export	ir.rule.group,name	23	Any export	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
882	en	The password is too short.	ir.message,text	90	The password is too short.	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
883	en	The password is forbidden.	ir.message,text	91	The password is forbidden.	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
884	en	The same characters appear in the password too many times.	ir.message,text	92	The same characters appear in the password too many times.	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
885	en	The password cannot be the same as user's name.	ir.message,text	93	The password cannot be the same as user's name.	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
886	en	The password cannot be the same as user's login.	ir.message,text	94	The password cannot be the same as user's login.	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
887	en	The password cannot be the same as user's email address.	ir.message,text	95	The password cannot be the same as user's email address.	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
888	en	For logging purposes users cannot be deleted, instead they should be deactivated.	ir.message,text	96	For logging purposes users cannot be deleted, instead they should be deactivated.	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
889	en	Password for %(login)s	ir.message,text	97	Password for %(login)s	model	res	f	2020-09-01 08:50:28.180942	0	\N	\N	\N
890	en	Country	country.country,name	-1		model	country	f	\N	\N	\N	\N	\N
891	en	Code	country.country,code	-1		field	country	f	\N	\N	\N	\N	\N
892	en	The 2 chars ISO country code.	country.country,code	-1		help	country	f	\N	\N	\N	\N	\N
893	en	3-letters Code	country.country,code3	-1		field	country	f	\N	\N	\N	\N	\N
894	en	The 3 chars ISO country code.	country.country,code3	-1		help	country	f	\N	\N	\N	\N	\N
895	en	Numeric Code	country.country,code_numeric	-1		field	country	f	\N	\N	\N	\N	\N
896	en	The ISO numeric country code.	country.country,code_numeric	-1		help	country	f	\N	\N	\N	\N	\N
897	en	Name	country.country,name	-1		field	country	f	\N	\N	\N	\N	\N
898	en	The main identifier of the country.	country.country,name	-1		help	country	f	\N	\N	\N	\N	\N
899	en	Subdivisions	country.country,subdivisions	-1		field	country	f	\N	\N	\N	\N	\N
900	en	Subdivision	country.subdivision,name	-1		model	country	f	\N	\N	\N	\N	\N
901	en	Code	country.subdivision,code	-1		field	country	f	\N	\N	\N	\N	\N
902	en	The ISO code of the subdivision.	country.subdivision,code	-1		help	country	f	\N	\N	\N	\N	\N
903	en	Country	country.subdivision,country	-1		field	country	f	\N	\N	\N	\N	\N
904	en	The country where this subdivision is.	country.subdivision,country	-1		help	country	f	\N	\N	\N	\N	\N
905	en	Name	country.subdivision,name	-1		field	country	f	\N	\N	\N	\N	\N
906	en	The main identifier of the subdivision.	country.subdivision,name	-1		help	country	f	\N	\N	\N	\N	\N
907	en	Parent	country.subdivision,parent	-1		field	country	f	\N	\N	\N	\N	\N
908	en	Add subdivision below the parent.	country.subdivision,parent	-1		help	country	f	\N	\N	\N	\N	\N
909	en	Type	country.subdivision,type	-1		field	country	f	\N	\N	\N	\N	\N
910	en	Administration	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
911	en	Administrative area	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
912	en	Administrative atoll	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
913	en	Administrative Region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
914	en	Administrative Territory	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
915	en	Area	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
916	en	Atoll	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
917	en	Arctic Region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
918	en	Autonomous City	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
919	en	Autonomous Commune	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
920	en	Autonomous communities	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
921	en	Autonomous community	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
922	en	Autonomous District	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
923	en	Autonomous island	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
924	en	Autonomous monastic state	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
925	en	Autonomous municipality	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
926	en	Autonomous Province	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
927	en	Autonomous Region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
928	en	Autonomous republic	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
929	en	Autonomous sector	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
930	en	Autonomous territory	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
931	en	Autonomous territorial unit	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
932	en	Borough	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
933	en	Canton	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
934	en	Capital city	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
935	en	Capital District	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
936	en	Capital Metropolitan City	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
937	en	Capital Territory	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
938	en	Chains (of islands)	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
939	en	City	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
940	en	City corporation	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
941	en	City with county rights	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
947	en	Department	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
948	en	Dependency	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
949	en	Development region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
950	en	District	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
951	en	District council area	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
952	en	Division	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
953	en	Economic Prefecture	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
954	en	Economic region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
955	en	Emirate	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
956	en	Entity	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
957	en	Federal Dependency	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
958	en	Federal District	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
959	en	Federal Territories	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
960	en	Geographical entity	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
961	en	Geographical region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
962	en	Geographical unit	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
963	en	Governorate	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
964	en	Included for completeness	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
965	en	Indigenous region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
966	en	Island	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
967	en	Island council	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
968	en	Island group	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
969	en	Local council	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
970	en	London borough	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
971	en	Metropolitan cities	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
972	en	Metropolitan department	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
973	en	Metropolitan district	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
974	en	Metropolitan region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
975	en	Municipalities	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
976	en	Municipality	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
977	en	Nation	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
978	en	Oblast	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
979	en	Outlying area	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
980	en	Overseas department	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
981	en	Overseas region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
982	en	Overseas region/department	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
983	en	Overseas territorial collectivity	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
984	en	Parish	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
985	en	Popularates	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
986	en	Prefecture	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
987	en	Principality	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
988	en	Province	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
989	en	Quarter	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
990	en	Rayon	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
991	en	Region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
992	en	Regional council	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
993	en	Republic	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
994	en	Republican City	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
995	en	Self-governed part	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
996	en	Special administrative region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
997	en	Special city	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
998	en	Special District	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
999	en	Special island authority	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1000	en	Special Municipality	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1001	en	Special Region	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1002	en	Special zone	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1003	en	State	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1004	en	Territorial unit	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1005	en	Territory	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1006	en	Town council	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1007	en	Two-tier county	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1008	en	Union territory	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1009	en	Unitary authority	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1010	en	Unitary authority (england)	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1011	en	Unitary authority (wales)	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1012	en	zone	country.subdivision,type	-1		selection	country	f	\N	\N	\N	\N	\N
1013	en	Zip	country.zip,name	-1		model	country	f	\N	\N	\N	\N	\N
1014	en	City	country.zip,city	-1		field	country	f	\N	\N	\N	\N	\N
1015	en	The name of the city for the zip.	country.zip,city	-1		help	country	f	\N	\N	\N	\N	\N
1016	en	Country	country.zip,country	-1		field	country	f	\N	\N	\N	\N	\N
1017	en	The country where the zip is.	country.zip,country	-1		help	country	f	\N	\N	\N	\N	\N
1018	en	Subdivision	country.zip,subdivision	-1		field	country	f	\N	\N	\N	\N	\N
1019	en	The subdivision where the zip is.	country.zip,subdivision	-1		help	country	f	\N	\N	\N	\N	\N
1020	en	Zip	country.zip,zip	-1		field	country	f	\N	\N	\N	\N	\N
1021	en	Countries	ir.action,name	54	Countries	model	country	f	2020-09-02 07:24:22.107384	0	\N	2020-09-02 07:24:22.107384	0
1022	en	Countries	ir.ui.menu,name	48	Countries	model	country	f	2020-09-02 07:24:22.107384	0	\N	\N	\N
1023	en	Zip	ir.action,name	55	Zip	model	country	f	2020-09-02 07:24:22.107384	0	\N	2020-09-02 07:24:22.107384	0
1024	en	Currency	currency.currency,name	-1		model	currency	f	\N	\N	\N	\N	\N
1025	en	Code	currency.currency,code	-1		field	currency	f	\N	\N	\N	\N	\N
1026	en	The 3 chars ISO currency code.	currency.currency,code	-1		help	currency	f	\N	\N	\N	\N	\N
1027	en	Digits	currency.currency,digits	-1		field	currency	f	\N	\N	\N	\N	\N
1028	en	The number of digits to display after the decimal separator.	currency.currency,digits	-1		help	currency	f	\N	\N	\N	\N	\N
1029	en	Name	currency.currency,name	-1		field	currency	f	\N	\N	\N	\N	\N
1030	en	The main identifier of the currency.	currency.currency,name	-1		help	currency	f	\N	\N	\N	\N	\N
1031	en	Numeric Code	currency.currency,numeric_code	-1		field	currency	f	\N	\N	\N	\N	\N
1032	en	The 3 digits ISO currency code.	currency.currency,numeric_code	-1		help	currency	f	\N	\N	\N	\N	\N
1033	en	Current rate	currency.currency,rate	-1		field	currency	f	\N	\N	\N	\N	\N
1034	en	Rates	currency.currency,rates	-1		field	currency	f	\N	\N	\N	\N	\N
1035	en	Add floating exchange rates for the currency.	currency.currency,rates	-1		help	currency	f	\N	\N	\N	\N	\N
1036	en	Rounding factor	currency.currency,rounding	-1		field	currency	f	\N	\N	\N	\N	\N
1037	en	The minimum amount which can be represented in this currency.	currency.currency,rounding	-1		help	currency	f	\N	\N	\N	\N	\N
1038	en	Symbol	currency.currency,symbol	-1		field	currency	f	\N	\N	\N	\N	\N
1039	en	The symbol used for currency formating.	currency.currency,symbol	-1		help	currency	f	\N	\N	\N	\N	\N
1040	en	Currency Rate	currency.currency.rate,name	-1		model	currency	f	\N	\N	\N	\N	\N
1041	en	Currency	currency.currency.rate,currency	-1		field	currency	f	\N	\N	\N	\N	\N
1042	en	The currency on which the rate applies.	currency.currency.rate,currency	-1		help	currency	f	\N	\N	\N	\N	\N
1043	en	Date	currency.currency.rate,date	-1		field	currency	f	\N	\N	\N	\N	\N
1044	en	From when the rate applies.	currency.currency.rate,date	-1		help	currency	f	\N	\N	\N	\N	\N
1045	en	Rate	currency.currency.rate,rate	-1		field	currency	f	\N	\N	\N	\N	\N
1046	en	The floating exchange rate used to convert the currency.	currency.currency.rate,rate	-1		help	currency	f	\N	\N	\N	\N	\N
1047	en	Currency Administration	res.group,name	2	Currency Administration	model	currency	f	2020-09-02 07:24:23.639802	0	\N	\N	\N
1048	en	Currency	ir.ui.menu,name	49	Currency	model	currency	f	2020-09-02 07:24:23.639802	0	\N	\N	\N
1049	en	Currencies	ir.action,name	56	Currencies	model	currency	f	2020-09-02 07:24:23.639802	0	\N	2020-09-02 07:24:23.639802	0
1050	en	Currencies	ir.ui.menu,name	50	Currencies	model	currency	f	2020-09-02 07:24:23.639802	0	\N	\N	\N
1051	en	No rate found for currency "%(currency)s" on "%(date)s".	ir.message,text	98	No rate found for currency "%(currency)s" on "%(date)s".	model	currency	f	2020-09-02 07:24:23.639802	0	\N	\N	\N
1052	en	A currency can only have one rate by date.	ir.message,text	99	A currency can only have one rate by date.	model	currency	f	2020-09-02 07:24:23.639802	0	\N	\N	\N
1053	en	A currency rate must be positive.	ir.message,text	100	A currency rate must be positive.	model	currency	f	2020-09-02 07:24:23.639802	0	\N	\N	\N
1054	en	Category	party.category,name	-1		model	party	f	\N	\N	\N	\N	\N
1055	en	Children	party.category,childs	-1		field	party	f	\N	\N	\N	\N	\N
1056	en	Add children below the category.	party.category,childs	-1		help	party	f	\N	\N	\N	\N	\N
1057	en	Name	party.category,name	-1		field	party	f	\N	\N	\N	\N	\N
1058	en	The main identifier of the category.	party.category,name	-1		help	party	f	\N	\N	\N	\N	\N
1059	en	Parent	party.category,parent	-1		field	party	f	\N	\N	\N	\N	\N
1060	en	Add the category below the parent.	party.category,parent	-1		help	party	f	\N	\N	\N	\N	\N
1061	en	Party	party.party,name	-1		model	party	f	\N	\N	\N	\N	\N
1062	en	Addresses	party.party,addresses	-1		field	party	f	\N	\N	\N	\N	\N
1063	en	Categories	party.party,categories	-1		field	party	f	\N	\N	\N	\N	\N
1064	en	The categories the party belongs to.	party.party,categories	-1		help	party	f	\N	\N	\N	\N	\N
1065	en	Code	party.party,code	-1		field	party	f	\N	\N	\N	\N	\N
1066	en	The unique identifier of the party.	party.party,code	-1		help	party	f	\N	\N	\N	\N	\N
1067	en	Code Readonly	party.party,code_readonly	-1		field	party	f	\N	\N	\N	\N	\N
1068	en	Contact Mechanisms	party.party,contact_mechanisms	-1		field	party	f	\N	\N	\N	\N	\N
1069	en	E-Mail	party.party,email	-1		field	party	f	\N	\N	\N	\N	\N
1070	en	Fax	party.party,fax	-1		field	party	f	\N	\N	\N	\N	\N
1071	en	Full Name	party.party,full_name	-1		field	party	f	\N	\N	\N	\N	\N
1072	en	Identifiers	party.party,identifiers	-1		field	party	f	\N	\N	\N	\N	\N
1073	en	Add other identifiers of the party.	party.party,identifiers	-1		help	party	f	\N	\N	\N	\N	\N
1074	en	Language	party.party,lang	-1		field	party	f	\N	\N	\N	\N	\N
1075	en	Used to translate communications with the party.	party.party,lang	-1		help	party	f	\N	\N	\N	\N	\N
1076	en	Languages	party.party,langs	-1		field	party	f	\N	\N	\N	\N	\N
1077	en	Mobile	party.party,mobile	-1		field	party	f	\N	\N	\N	\N	\N
1078	en	Name	party.party,name	-1		field	party	f	\N	\N	\N	\N	\N
1079	en	The main identifier of the party.	party.party,name	-1		help	party	f	\N	\N	\N	\N	\N
1080	en	Phone	party.party,phone	-1		field	party	f	\N	\N	\N	\N	\N
1081	en	Replaced By	party.party,replaced_by	-1		field	party	f	\N	\N	\N	\N	\N
1082	en	The party replacing this one.	party.party,replaced_by	-1		help	party	f	\N	\N	\N	\N	\N
1083	en	Tax Identifier	party.party,tax_identifier	-1		field	party	f	\N	\N	\N	\N	\N
1084	en	The identifier used for tax report.	party.party,tax_identifier	-1		help	party	f	\N	\N	\N	\N	\N
1085	en	Website	party.party,website	-1		field	party	f	\N	\N	\N	\N	\N
1086	en	Party Lang	party.party.lang,name	-1		model	party	f	\N	\N	\N	\N	\N
1087	en	Language	party.party.lang,lang	-1		field	party	f	\N	\N	\N	\N	\N
1088	en	Party	party.party.lang,party	-1		field	party	f	\N	\N	\N	\N	\N
1089	en	Party - Category	party.party-party.category,name	-1		model	party	f	\N	\N	\N	\N	\N
1090	en	Category	party.party-party.category,category	-1		field	party	f	\N	\N	\N	\N	\N
1091	en	Party	party.party-party.category,party	-1		field	party	f	\N	\N	\N	\N	\N
1092	en	Party Identifier	party.identifier,name	-1		model	party	f	\N	\N	\N	\N	\N
1093	en	Code	party.identifier,code	-1		field	party	f	\N	\N	\N	\N	\N
1094	en	Party	party.identifier,party	-1		field	party	f	\N	\N	\N	\N	\N
1095	en	The party identified by this record.	party.identifier,party	-1		help	party	f	\N	\N	\N	\N	\N
1096	en	Type	party.identifier,type	-1		field	party	f	\N	\N	\N	\N	\N
1097	en	Albanian VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1098	en	Argentinian Tax Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1099	en	Austrian Company Register	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1100	en	Austrian Tax Identification	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1101	en	Australian Business Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1102	en	Australian Company Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1103	en	Australian Tax File Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1104	en	Belgian Enterprise Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1105	en	Bulgarian Personal Identity Codes	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1106	en	Bulgarian Number of a Foreigner	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1107	en	Bulgarian VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1108	en	Brazillian Company Identifier	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1109	en	Brazillian National Identifier	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1110	en	Canadian Business Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1111	en	Canadian Social Insurance Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1112	en	Swiss Social Security Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1113	en	Swiss Business Identifier	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1114	en	Swiss VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1115	en	Chilean National Tax Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1116	en	Chinese Resident Identity Card Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1117	en	Colombian Identity Code	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1118	en	Colombian Business Tax Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1119	en	Cypriot VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1120	en	Czech VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1121	en	Czech National Identifier	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1122	en	German Company Register Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1123	en	German Personal Tax Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1124	en	German Tax Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1125	en	German VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1126	en	Danish Citizen Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1127	en	Danish VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1128	en	Dominican Republic National Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1129	en	Dominican Republic Tax	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1130	en	Ecuadorian Personal Identity Code	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1131	en	Ecuadorian Tax Identification	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1132	en	Estonian Personcal ID number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1133	en	Estonian VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1134	en	Estonian Organisation Registration Code	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1135	en	Spanish Company Tax	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1136	en	Spanish Personal Identity Codes	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1137	en	Spanish Foreigner Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1138	en	Spanish VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1139	en	SEPA Identifier of the Creditor (AT-02)	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1140	en	European VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1141	en	Finnish VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1142	en	Finnish Association Identifier	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1143	en	Finnish Personal Identity Code	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1144	en	Finnish individual tax number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1145	en	Finnish Business Identifier	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1146	en	French Tax Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1147	en	French Personal Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1148	en	French VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1149	en	United Kingdom National Health Service Patient Identifier	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1150	en	English Unique Pupil Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1151	en	United Kingdom (and Isle of Man) VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1152	en	Greek VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1153	en	Croatian Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1154	en	Hungarian VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1155	en	Irish Personal Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1156	en	Irish VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1157	en	Indian Digital Resident Personal Identity Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1158	en	Indian Income Tax Identifier	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1346	en	Main Company	res.user,main_company	-1		field	company	f	\N	\N	\N	\N	\N
1159	en	Icelandic Personal and Organisation Identity Code	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1160	en	Icelandic VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1161	en	Italian Tax Code for Individuals	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1162	en	Italian VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1163	en	Lithuanian VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1164	en	Luxembourgian VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1165	en	Latvian VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1166	en	Monacan VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1167	en	Maltese VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1168	en	Mauritian National Identifier	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1169	en	Mexican Tax Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1170	en	Malaysian National Registration Identity Card Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1171	en	Dutch School Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1172	en	Dutch Citizen Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1173	en	Dutch VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1174	en	Dutch student identification number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1175	en	Norwegian VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1176	en	Norwegian Organisation Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1177	en	Polish VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1178	en	Polish National Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1179	en	Polish Register of Economic Units	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1180	en	Portuguese VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1181	en	Romanian VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1182	en	Romanian Numerical Personal Code	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1183	en	Serbian Tax Identification	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1184	en	Russian Tax identifier	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1185	en	Swedish Company Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1186	en	Swedish VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1187	en	Slovenian VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1188	en	Slovak VAT Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1189	en	Slovak Birth Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1190	en	San Marino National Tax Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1191	en	Turkish Personal Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1192	en	U.S. Adoption Taxpayer Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1193	en	U.S. Employer Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1194	en	U.S. Individual Taxpayer Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1195	en	U.S. Preparer Tax Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1196	en	U.S. Social Security Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1197	en	U.S. Taxpayer Identification Number	party.identifier,type	-1		selection	party	f	\N	\N	\N	\N	\N
1198	en	Check VIES	party.check_vies.result,name	-1		model	party	f	\N	\N	\N	\N	\N
1199	en	Parties Failed	party.check_vies.result,parties_failed	-1		field	party	f	\N	\N	\N	\N	\N
1200	en	Parties Succeed	party.check_vies.result,parties_succeed	-1		field	party	f	\N	\N	\N	\N	\N
1201	en	Replace Party	party.replace.ask,name	-1		model	party	f	\N	\N	\N	\N	\N
1202	en	Destination	party.replace.ask,destination	-1		field	party	f	\N	\N	\N	\N	\N
1203	en	The party that replaces.	party.replace.ask,destination	-1		help	party	f	\N	\N	\N	\N	\N
1204	en	Source	party.replace.ask,source	-1		field	party	f	\N	\N	\N	\N	\N
1205	en	The party to be replaced.	party.replace.ask,source	-1		help	party	f	\N	\N	\N	\N	\N
1206	en	Erase Party	party.erase.ask,name	-1		model	party	f	\N	\N	\N	\N	\N
1207	en	Party	party.erase.ask,party	-1		field	party	f	\N	\N	\N	\N	\N
1208	en	The party to be erased.	party.erase.ask,party	-1		help	party	f	\N	\N	\N	\N	\N
1209	en	Address	party.address,name	-1		model	party	f	\N	\N	\N	\N	\N
1210	en	City	party.address,city	-1		field	party	f	\N	\N	\N	\N	\N
1211	en	Country	party.address,country	-1		field	party	f	\N	\N	\N	\N	\N
1212	en	Full Address	party.address,full_address	-1		field	party	f	\N	\N	\N	\N	\N
1213	en	Building Name	party.address,name	-1		field	party	f	\N	\N	\N	\N	\N
1214	en	Party	party.address,party	-1		field	party	f	\N	\N	\N	\N	\N
1215	en	Party Name	party.address,party_name	-1		field	party	f	\N	\N	\N	\N	\N
1216	en	If filled, replace the name of the party for address formatting	party.address,party_name	-1		help	party	f	\N	\N	\N	\N	\N
1217	en	Street	party.address,street	-1		field	party	f	\N	\N	\N	\N	\N
1218	en	Subdivision	party.address,subdivision	-1		field	party	f	\N	\N	\N	\N	\N
1219	en	Subdivision Types	party.address,subdivision_types	-1		field	party	f	\N	\N	\N	\N	\N
1220	en	Zip	party.address,zip	-1		field	party	f	\N	\N	\N	\N	\N
1221	en	Address Format	party.address.format,name	-1		model	party	f	\N	\N	\N	\N	\N
1222	en	Country Code	party.address.format,country_code	-1		field	party	f	\N	\N	\N	\N	\N
1223	en	Format	party.address.format,format_	-1		field	party	f	\N	\N	\N	\N	\N
1224	en	Available variables (also in upper case):\n- ${party_name}\n- ${name}\n- ${attn}\n- ${street}\n- ${zip}\n- ${city}\n- ${subdivision}\n- ${subdivision_code}\n- ${country}\n- ${country_code}	party.address.format,format_	-1		help	party	f	\N	\N	\N	\N	\N
1225	en	Language Code	party.address.format,language_code	-1		field	party	f	\N	\N	\N	\N	\N
1226	en	Address Subdivision Type	party.address.subdivision_type,name	-1		model	party	f	\N	\N	\N	\N	\N
1227	en	Country Code	party.address.subdivision_type,country_code	-1		field	party	f	\N	\N	\N	\N	\N
1228	en	Subdivision Types	party.address.subdivision_type,types	-1		field	party	f	\N	\N	\N	\N	\N
1229	en	Contact Mechanism	party.contact_mechanism,name	-1		model	party	f	\N	\N	\N	\N	\N
1230	en	Comment	party.contact_mechanism,comment	-1		field	party	f	\N	\N	\N	\N	\N
1231	en	E-Mail	party.contact_mechanism,email	-1		field	party	f	\N	\N	\N	\N	\N
1232	en	Name	party.contact_mechanism,name	-1		field	party	f	\N	\N	\N	\N	\N
1233	en	Value	party.contact_mechanism,other_value	-1		field	party	f	\N	\N	\N	\N	\N
1234	en	Party	party.contact_mechanism,party	-1		field	party	f	\N	\N	\N	\N	\N
1235	en	SIP	party.contact_mechanism,sip	-1		field	party	f	\N	\N	\N	\N	\N
1236	en	Skype	party.contact_mechanism,skype	-1		field	party	f	\N	\N	\N	\N	\N
1237	en	Type	party.contact_mechanism,type	-1		field	party	f	\N	\N	\N	\N	\N
1238	en	Phone	party.contact_mechanism,type	-1		selection	party	f	\N	\N	\N	\N	\N
1239	en	Mobile	party.contact_mechanism,type	-1		selection	party	f	\N	\N	\N	\N	\N
1240	en	Fax	party.contact_mechanism,type	-1		selection	party	f	\N	\N	\N	\N	\N
1241	en	E-Mail	party.contact_mechanism,type	-1		selection	party	f	\N	\N	\N	\N	\N
1242	en	Website	party.contact_mechanism,type	-1		selection	party	f	\N	\N	\N	\N	\N
1243	en	Skype	party.contact_mechanism,type	-1		selection	party	f	\N	\N	\N	\N	\N
1244	en	SIP	party.contact_mechanism,type	-1		selection	party	f	\N	\N	\N	\N	\N
1245	en	IRC	party.contact_mechanism,type	-1		selection	party	f	\N	\N	\N	\N	\N
1246	en	Jabber	party.contact_mechanism,type	-1		selection	party	f	\N	\N	\N	\N	\N
1247	en	Other	party.contact_mechanism,type	-1		selection	party	f	\N	\N	\N	\N	\N
1248	en	URL	party.contact_mechanism,url	-1		field	party	f	\N	\N	\N	\N	\N
1249	en	Value	party.contact_mechanism,value	-1		field	party	f	\N	\N	\N	\N	\N
1250	en	Value Compact	party.contact_mechanism,value_compact	-1		field	party	f	\N	\N	\N	\N	\N
1251	en	Website	party.contact_mechanism,website	-1		field	party	f	\N	\N	\N	\N	\N
1252	en	Party Configuration	party.configuration,name	-1		model	party	f	\N	\N	\N	\N	\N
1253	en	Party Language	party.configuration,party_lang	-1		field	party	f	\N	\N	\N	\N	\N
1254	en	The default language for new parties.	party.configuration,party_lang	-1		help	party	f	\N	\N	\N	\N	\N
1255	en	Party Sequence	party.configuration,party_sequence	-1		field	party	f	\N	\N	\N	\N	\N
1256	en	Used to generate the party code.	party.configuration,party_sequence	-1		help	party	f	\N	\N	\N	\N	\N
1257	en	Party Configuration Sequence	party.configuration.party_sequence,name	-1		model	party	f	\N	\N	\N	\N	\N
1258	en	Party Sequence	party.configuration.party_sequence,party_sequence	-1		field	party	f	\N	\N	\N	\N	\N
1259	en	Used to generate the party code.	party.configuration.party_sequence,party_sequence	-1		help	party	f	\N	\N	\N	\N	\N
1260	en	Party Configuration Lang	party.configuration.party_lang,name	-1		model	party	f	\N	\N	\N	\N	\N
1261	en	Party Language	party.configuration.party_lang,party_lang	-1		field	party	f	\N	\N	\N	\N	\N
1262	en	The default language for new parties.	party.configuration.party_lang,party_lang	-1		help	party	f	\N	\N	\N	\N	\N
1263	en	OK	party.check_vies,result,end	-1		wizard_button	party	f	\N	\N	\N	\N	\N
1268	en	Party Administration	res.group,name	3	Party Administration	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1269	en	Party	ir.ui.menu,name	51	Party	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1270	en	Configuration	ir.ui.menu,name	52	Configuration	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1271	en	Parties	ir.action,name	57	Parties	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1272	en	Parties	ir.ui.menu,name	53	Parties	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1273	en	Parties by Category	ir.action,name	58	Parties by Category	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1274	en	Labels	ir.action,name	59	Labels	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1275	en	Party	ir.sequence.type,name	1	Party	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1276	en	Party	ir.sequence,name	1	Party	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1277	en	Check VIES	ir.action,name	60	Check VIES	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1278	en	Replace	ir.action,name	61	Replace	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1279	en	Erase	ir.action,name	62	Erase	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1280	en	Categories	ir.action,name	63	Categories	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1281	en	Categories	ir.ui.menu,name	54	Categories	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1282	en	Categories	ir.action,name	64	Categories	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1283	en	Categories	ir.ui.menu,name	55	Categories	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1284	en	Addresses	ir.action,name	65	Addresses	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1285	en	Addresses	ir.ui.menu,name	56	Addresses	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1286	en	Address Formats	ir.action,name	66	Address Formats	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1264	en	Cancel	party.replace,ask,end	-1		wizard_button	party	f	\N	\N	\N	\N	\N
1265	en	Replace	party.replace,ask,replace	-1		wizard_button	party	f	\N	\N	\N	\N	\N
1266	en	Cancel	party.erase,ask,end	-1		wizard_button	party	f	\N	\N	\N	\N	\N
1267	en	Erase	party.erase,ask,erase	-1		wizard_button	party	f	\N	\N	\N	\N	\N
1287	en	Address Formats	ir.ui.menu,name	57	Address Formats	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1288	en	Address Subdivision Types	ir.action,name	67	Address Subdivision Types	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1289	en	Address Subdivision Types	ir.ui.menu,name	58	Address Subdivision Types	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1290	en	Contact Mechanisms	ir.action,name	68	Contact Mechanisms	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1291	en	Contact Mechanisms	ir.ui.menu,name	59	Contact Mechanisms	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1292	en	Party Configuration	ir.action,name	69	Party Configuration	model	party	f	2020-09-02 07:24:24.586225	0	\N	2020-09-02 07:24:24.586225	0
1293	en	Party Configuration	ir.ui.menu,name	60	Party Configuration	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1294	en	The code on party must be unique.	ir.message,text	101	The code on party must be unique.	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1295	en	You cannot change the party of contact mechanism "%(contact)s".	ir.message,text	102	You cannot change the party of contact mechanism "%(contact)s".	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1296	en	The phone number "%(phone)s" for party "%(party)s" is not valid.	ir.message,text	103	The phone number "%(phone)s" for party "%(party)s" is not valid.	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1297	en	The %(type)s "%(code)s" for party "%(party)s" is not valid.	ir.message,text	104	The %(type)s "%(code)s" for party "%(party)s" is not valid.	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1298	en	The VIES service is unavailable, try again later.	ir.message,text	105	The VIES service is unavailable, try again later.	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1299	en	Parties have different names: "%(source_name)s" vs "%(destination_name)s".	ir.message,text	106	Parties have different names: "%(source_name)s" vs "%(destination_name)s".	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1300	en	Parties have different tax identifiers: "%(source_code)s" vs "%(destination_code)s".	ir.message,text	107	Parties have different tax identifiers: "%(source_code)s" vs "%(destination_code)s".	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1301	en	Party "%(party)s" cannot be erased because they are still active.	ir.message,text	108	Party "%(party)s" cannot be erased because they are still active.	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1302	en	You cannot change the party of address "%(address)s".	ir.message,text	109	You cannot change the party of address "%(address)s".	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1303	en	Invalid format "%(format)s" with exception "%(exception)s".	ir.message,text	110	Invalid format "%(format)s" with exception "%(exception)s".	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1304	en	The name of party category must be unique by parent.	ir.message,text	111	The name of party category must be unique by parent.	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1305	en	The country code on subdivision type must be unique.	ir.message,text	112	The country code on subdivision type must be unique.	model	party	f	2020-09-02 07:24:24.586225	0	\N	\N	\N
1306	en	Company	company.company,name	-1		model	company	f	\N	\N	\N	\N	\N
1307	en	Children	company.company,childs	-1		field	company	f	\N	\N	\N	\N	\N
1308	en	Add children below the company.	company.company,childs	-1		help	company	f	\N	\N	\N	\N	\N
1309	en	Currency	company.company,currency	-1		field	company	f	\N	\N	\N	\N	\N
1310	en	The main currency for the company.	company.company,currency	-1		help	company	f	\N	\N	\N	\N	\N
1311	en	Employees	company.company,employees	-1		field	company	f	\N	\N	\N	\N	\N
1312	en	Add employees to the company.	company.company,employees	-1		help	company	f	\N	\N	\N	\N	\N
1313	en	Footer	company.company,footer	-1		field	company	f	\N	\N	\N	\N	\N
1314	en	The text to display on report footers.	company.company,footer	-1		help	company	f	\N	\N	\N	\N	\N
1315	en	Header	company.company,header	-1		field	company	f	\N	\N	\N	\N	\N
1316	en	The text to display on report headers.	company.company,header	-1		help	company	f	\N	\N	\N	\N	\N
1317	en	Parent	company.company,parent	-1		field	company	f	\N	\N	\N	\N	\N
1318	en	Add the company below the parent.	company.company,parent	-1		help	company	f	\N	\N	\N	\N	\N
1319	en	Party	company.company,party	-1		field	company	f	\N	\N	\N	\N	\N
1320	en	Timezone	company.company,timezone	-1		field	company	f	\N	\N	\N	\N	\N
1321	en	Used to compute the today date.	company.company,timezone	-1		help	company	f	\N	\N	\N	\N	\N
1322	en	Employee	company.employee,name	-1		model	company	f	\N	\N	\N	\N	\N
1323	en	Company	company.employee,company	-1		field	company	f	\N	\N	\N	\N	\N
1324	en	The company to which the employee belongs.	company.employee,company	-1		help	company	f	\N	\N	\N	\N	\N
1325	en	End Date	company.employee,end_date	-1		field	company	f	\N	\N	\N	\N	\N
1326	en	When the employee leaves the company.	company.employee,end_date	-1		help	company	f	\N	\N	\N	\N	\N
1327	en	Party	company.employee,party	-1		field	company	f	\N	\N	\N	\N	\N
1328	en	The party which represents the employee.	company.employee,party	-1		help	company	f	\N	\N	\N	\N	\N
1329	en	Start Date	company.employee,start_date	-1		field	company	f	\N	\N	\N	\N	\N
1330	en	When the employee joins the company.	company.employee,start_date	-1		help	company	f	\N	\N	\N	\N	\N
1331	en	Subordinates	company.employee,subordinates	-1		field	company	f	\N	\N	\N	\N	\N
1332	en	The employees to be overseen by this employee.	company.employee,subordinates	-1		help	company	f	\N	\N	\N	\N	\N
1333	en	Supervisor	company.employee,supervisor	-1		field	company	f	\N	\N	\N	\N	\N
1334	en	The employee who oversees this employee.	company.employee,supervisor	-1		help	company	f	\N	\N	\N	\N	\N
1335	en	Company Config	company.company.config.start,name	-1		model	company	f	\N	\N	\N	\N	\N
1336	en	User - Employee	res.user-company.employee,name	-1		model	company	f	\N	\N	\N	\N	\N
1337	en	Employee	res.user-company.employee,employee	-1		field	company	f	\N	\N	\N	\N	\N
1338	en	User	res.user-company.employee,user	-1		field	company	f	\N	\N	\N	\N	\N
1339	en	Companies	res.user,companies	-1		field	company	f	\N	\N	\N	\N	\N
1340	en	Current Company	res.user,company	-1		field	company	f	\N	\N	\N	\N	\N
1341	en	Select the company to work for.	res.user,company	-1		help	company	f	\N	\N	\N	\N	\N
1342	en	Current Employee	res.user,employee	-1		field	company	f	\N	\N	\N	\N	\N
1343	en	Select the employee to make the user behave as such.	res.user,employee	-1		help	company	f	\N	\N	\N	\N	\N
1344	en	Employees	res.user,employees	-1		field	company	f	\N	\N	\N	\N	\N
1345	en	Add employees to grant the user access to them.	res.user,employees	-1		help	company	f	\N	\N	\N	\N	\N
1347	en	Grant access to the company and its children.	res.user,main_company	-1		help	company	f	\N	\N	\N	\N	\N
1348	en	Company	ir.sequence,company	-1		field	company	f	\N	\N	\N	\N	\N
1349	en	Restrict the sequence usage to the company.	ir.sequence,company	-1		help	company	f	\N	\N	\N	\N	\N
1350	en	Company	ir.sequence.strict,company	-1		field	company	f	\N	\N	\N	\N	\N
1351	en	Restrict the sequence usage to the company.	ir.sequence.strict,company	-1		help	company	f	\N	\N	\N	\N	\N
1352	en	\n- "employee" from the current user	ir.rule,domain	-1		help	company	f	\N	\N	\N	\N	\N
1353	en	Companies	ir.cron,companies	-1		field	company	f	\N	\N	\N	\N	\N
1354	en	Companies registered for this cron.	ir.cron,companies	-1		help	company	f	\N	\N	\N	\N	\N
1355	en	Cron - Company	ir.cron-company.company,name	-1		model	company	f	\N	\N	\N	\N	\N
1356	en	Company	ir.cron-company.company,company	-1		field	company	f	\N	\N	\N	\N	\N
1357	en	Cron	ir.cron-company.company,cron	-1		field	company	f	\N	\N	\N	\N	\N
1358	en	Company	party.configuration.party_lang,company	-1		field	company	f	\N	\N	\N	\N	\N
1359	en	Company	party.party.lang,company	-1		field	company	f	\N	\N	\N	\N	\N
1360	en	Cancel	company.company.config,company,end	-1		wizard_button	company	f	\N	\N	\N	\N	\N
1361	en	Add	company.company.config,company,add	-1		wizard_button	company	f	\N	\N	\N	\N	\N
1362	en	Cancel	company.company.config,start,end	-1		wizard_button	company	f	\N	\N	\N	\N	\N
1363	en	OK	company.company.config,start,company	-1		wizard_button	company	f	\N	\N	\N	\N	\N
1364	en	Company Administration	res.group,name	4	Company Administration	model	company	f	2020-09-02 07:24:29.514826	0	\N	\N	\N
1365	en	Employee Administration	res.group,name	5	Employee Administration	model	company	f	2020-09-02 07:24:29.514826	0	\N	\N	\N
1366	en	Company	ir.ui.menu,name	61	Company	model	company	f	2020-09-02 07:24:29.514826	0	\N	\N	\N
1367	en	Companies	ir.action,name	70	Companies	model	company	f	2020-09-02 07:24:29.514826	0	\N	2020-09-02 07:24:29.514826	0
1368	en	Companies	ir.ui.menu,name	62	Companies	model	company	f	2020-09-02 07:24:29.514826	0	\N	\N	\N
1369	en	Companies	ir.action,name	71	Companies	model	company	f	2020-09-02 07:24:29.514826	0	\N	2020-09-02 07:24:29.514826	0
1370	en	Companies	ir.ui.menu,name	63	Companies	model	company	f	2020-09-02 07:24:29.514826	0	\N	\N	\N
1371	en	Configure Company	ir.action,name	72	Configure Company	model	company	f	2020-09-02 07:24:29.514826	0	\N	2020-09-02 07:24:29.514826	0
1372	en	Employees	ir.action,name	73	Employees	model	company	f	2020-09-02 07:24:29.514826	0	\N	2020-09-02 07:24:29.514826	0
1373	en	Employees	ir.ui.menu,name	64	Employees	model	company	f	2020-09-02 07:24:29.514826	0	\N	\N	\N
1374	en	Supervised by	ir.action,name	74	Supervised by	model	company	f	2020-09-02 07:24:29.514826	0	\N	2020-09-02 07:24:29.514826	0
1375	en	Letter	ir.action,name	75	Letter	model	company	f	2020-09-02 07:24:29.514826	0	\N	2020-09-02 07:24:29.514826	0
1376	en	User in company	ir.rule.group,name	24	User in company	model	company	f	2020-09-02 07:24:29.514826	0	\N	\N	\N
1377	en	User in company	ir.rule.group,name	25	User in company	model	company	f	2020-09-02 07:24:29.514826	0	\N	\N	\N
1378	en	Unit of Measure Category	product.uom.category,name	-1		model	product	f	\N	\N	\N	\N	\N
1379	en	Name	product.uom.category,name	-1		field	product	f	\N	\N	\N	\N	\N
1380	en	Units of Measure	product.uom.category,uoms	-1		field	product	f	\N	\N	\N	\N	\N
1381	en	Unit of Measure	product.uom,name	-1		model	product	f	\N	\N	\N	\N	\N
1382	en	Category	product.uom,category	-1		field	product	f	\N	\N	\N	\N	\N
1383	en	The category that contains the unit of measure.\nConversions between different units of measure can be done if they are in the same category.	product.uom,category	-1		help	product	f	\N	\N	\N	\N	\N
1384	en	Display Digits	product.uom,digits	-1		field	product	f	\N	\N	\N	\N	\N
1385	en	The number of digits to display after the decimal separator.	product.uom,digits	-1		help	product	f	\N	\N	\N	\N	\N
1386	en	Factor	product.uom,factor	-1		field	product	f	\N	\N	\N	\N	\N
1387	en	The coefficient for the formula:\ncoefficient (base unit) = 1 (this unit)	product.uom,factor	-1		help	product	f	\N	\N	\N	\N	\N
1388	en	Name	product.uom,name	-1		field	product	f	\N	\N	\N	\N	\N
1389	en	Rate	product.uom,rate	-1		field	product	f	\N	\N	\N	\N	\N
1390	en	The coefficient for the formula:\n1 (base unit) = coef (this unit)	product.uom,rate	-1		help	product	f	\N	\N	\N	\N	\N
1391	en	Rounding Precision	product.uom,rounding	-1		field	product	f	\N	\N	\N	\N	\N
1392	en	The accuracy to which values are rounded.	product.uom,rounding	-1		help	product	f	\N	\N	\N	\N	\N
1393	en	Symbol	product.uom,symbol	-1		field	product	f	\N	\N	\N	\N	\N
1394	en	The symbol that represents the unit of measure.	product.uom,symbol	-1		help	product	f	\N	\N	\N	\N	\N
1395	en	Product Category	product.category,name	-1		model	product	f	\N	\N	\N	\N	\N
1396	en	Children	product.category,childs	-1		field	product	f	\N	\N	\N	\N	\N
1397	en	Used to add structure below the category.	product.category,childs	-1		help	product	f	\N	\N	\N	\N	\N
1398	en	Name	product.category,name	-1		field	product	f	\N	\N	\N	\N	\N
1399	en	Parent	product.category,parent	-1		field	product	f	\N	\N	\N	\N	\N
1400	en	Used to add structure above the category.	product.category,parent	-1		help	product	f	\N	\N	\N	\N	\N
1401	en	Product Template	product.template,name	-1		model	product	f	\N	\N	\N	\N	\N
1402	en	Categories	product.template,categories	-1		field	product	f	\N	\N	\N	\N	\N
1403	en	The categories that the product is in.\nUsed to group similar products together.	product.template,categories	-1		help	product	f	\N	\N	\N	\N	\N
1404	en	Categories	product.template,categories_all	-1		field	product	f	\N	\N	\N	\N	\N
1405	en	Code	product.template,code	-1		field	product	f	\N	\N	\N	\N	\N
1406	en	Consumable	product.template,consumable	-1		field	product	f	\N	\N	\N	\N	\N
1407	en	Check to allow stock moves to be assigned regardless of stock level.	product.template,consumable	-1		help	product	f	\N	\N	\N	\N	\N
1408	en	Cost Price	product.template,cost_price	-1		field	product	f	\N	\N	\N	\N	\N
1409	en	The amount it costs to purchase or make the product, or carry out the service.	product.template,cost_price	-1		help	product	f	\N	\N	\N	\N	\N
1410	en	Cost Price Method	product.template,cost_price_method	-1		field	product	f	\N	\N	\N	\N	\N
1411	en	The method used to calculate the cost price.	product.template,cost_price_method	-1		help	product	f	\N	\N	\N	\N	\N
1412	en	Fixed	product.template,cost_price_method	-1		selection	product	f	\N	\N	\N	\N	\N
1413	en	Average	product.template,cost_price_method	-1		selection	product	f	\N	\N	\N	\N	\N
1414	en	Cost Price Methods	product.template,cost_price_methods	-1		field	product	f	\N	\N	\N	\N	\N
1415	en	Default UOM	product.template,default_uom	-1		field	product	f	\N	\N	\N	\N	\N
1416	en	The standard unit of measure for the product.\nUsed internally when calculating the stock levels of goods and assets.	product.template,default_uom	-1		help	product	f	\N	\N	\N	\N	\N
1417	en	Default UOM Category	product.template,default_uom_category	-1		field	product	f	\N	\N	\N	\N	\N
1418	en	List Price	product.template,list_price	-1		field	product	f	\N	\N	\N	\N	\N
1419	en	The standard price the product is sold at.	product.template,list_price	-1		help	product	f	\N	\N	\N	\N	\N
1420	en	List Prices	product.template,list_prices	-1		field	product	f	\N	\N	\N	\N	\N
1421	en	Name	product.template,name	-1		field	product	f	\N	\N	\N	\N	\N
1422	en	Variants	product.template,products	-1		field	product	f	\N	\N	\N	\N	\N
1423	en	The different variants the product comes in.	product.template,products	-1		help	product	f	\N	\N	\N	\N	\N
1424	en	Type	product.template,type	-1		field	product	f	\N	\N	\N	\N	\N
1425	en	Goods	product.template,type	-1		selection	product	f	\N	\N	\N	\N	\N
1426	en	Assets	product.template,type	-1		selection	product	f	\N	\N	\N	\N	\N
1427	en	Service	product.template,type	-1		selection	product	f	\N	\N	\N	\N	\N
1428	en	Product Variant	product.product,name	-1		model	product	f	\N	\N	\N	\N	\N
1429	en	Categories	product.product,categories	-1		field	product	f	\N	\N	\N	\N	\N
1430	en	The categories that the product is in.\nUsed to group similar products together.	product.product,categories	-1		help	product	f	\N	\N	\N	\N	\N
1431	en	Categories	product.product,categories_all	-1		field	product	f	\N	\N	\N	\N	\N
1432	en	Code	product.product,code	-1		field	product	f	\N	\N	\N	\N	\N
1433	en	A unique identifier for the variant.	product.product,code	-1		help	product	f	\N	\N	\N	\N	\N
1434	en	Code Readonly	product.product,code_readonly	-1		field	product	f	\N	\N	\N	\N	\N
1435	en	Consumable	product.product,consumable	-1		field	product	f	\N	\N	\N	\N	\N
1436	en	Check to allow stock moves to be assigned regardless of stock level.	product.product,consumable	-1		help	product	f	\N	\N	\N	\N	\N
1437	en	Cost Price	product.product,cost_price	-1		field	product	f	\N	\N	\N	\N	\N
1438	en	The amount it costs to purchase or make the variant, or carry out the service.	product.product,cost_price	-1		help	product	f	\N	\N	\N	\N	\N
1439	en	Cost Price Method	product.product,cost_price_method	-1		field	product	f	\N	\N	\N	\N	\N
1440	en	The method used to calculate the cost price.	product.product,cost_price_method	-1		help	product	f	\N	\N	\N	\N	\N
1441	en	Fixed	product.product,cost_price_method	-1		selection	product	f	\N	\N	\N	\N	\N
1442	en	Average	product.product,cost_price_method	-1		selection	product	f	\N	\N	\N	\N	\N
1443	en	Cost Price Methods	product.product,cost_price_methods	-1		field	product	f	\N	\N	\N	\N	\N
1444	en	Cost Price	product.product,cost_price_uom	-1		field	product	f	\N	\N	\N	\N	\N
1445	en	Cost Prices	product.product,cost_prices	-1		field	product	f	\N	\N	\N	\N	\N
1446	en	Default UOM	product.product,default_uom	-1		field	product	f	\N	\N	\N	\N	\N
1447	en	The standard unit of measure for the product.\nUsed internally when calculating the stock levels of goods and assets.	product.product,default_uom	-1		help	product	f	\N	\N	\N	\N	\N
1448	en	Default UOM Category	product.product,default_uom_category	-1		field	product	f	\N	\N	\N	\N	\N
1449	en	Description	product.product,description	-1		field	product	f	\N	\N	\N	\N	\N
1450	en	Identifiers	product.product,identifiers	-1		field	product	f	\N	\N	\N	\N	\N
1451	en	Other identifiers associated with the variant.	product.product,identifiers	-1		help	product	f	\N	\N	\N	\N	\N
1452	en	List Price	product.product,list_price	-1		field	product	f	\N	\N	\N	\N	\N
1453	en	The standard price the product is sold at.	product.product,list_price	-1		help	product	f	\N	\N	\N	\N	\N
1454	en	List Price	product.product,list_price_uom	-1		field	product	f	\N	\N	\N	\N	\N
1455	en	List Prices	product.product,list_prices	-1		field	product	f	\N	\N	\N	\N	\N
1456	en	Name	product.product,name	-1		field	product	f	\N	\N	\N	\N	\N
1457	en	Prefix Code	product.product,prefix_code	-1		field	product	f	\N	\N	\N	\N	\N
1458	en	Suffix Code	product.product,suffix_code	-1		field	product	f	\N	\N	\N	\N	\N
1459	en	The unique identifier for the product (aka SKU).	product.product,suffix_code	-1		help	product	f	\N	\N	\N	\N	\N
1460	en	Product Template	product.product,template	-1		field	product	f	\N	\N	\N	\N	\N
1461	en	The product that defines the common properties inherited by the variant.	product.product,template	-1		help	product	f	\N	\N	\N	\N	\N
1462	en	Type	product.product,type	-1		field	product	f	\N	\N	\N	\N	\N
1463	en	Goods	product.product,type	-1		selection	product	f	\N	\N	\N	\N	\N
1464	en	Assets	product.product,type	-1		selection	product	f	\N	\N	\N	\N	\N
1465	en	Service	product.product,type	-1		selection	product	f	\N	\N	\N	\N	\N
1466	en	Product Identifier	product.identifier,name	-1		model	product	f	\N	\N	\N	\N	\N
1467	en	Code	product.identifier,code	-1		field	product	f	\N	\N	\N	\N	\N
1468	en	Product	product.identifier,product	-1		field	product	f	\N	\N	\N	\N	\N
1469	en	The product identified by the code.	product.identifier,product	-1		help	product	f	\N	\N	\N	\N	\N
1470	en	Type	product.identifier,type	-1		field	product	f	\N	\N	\N	\N	\N
1471	en	International Article Number	product.identifier,type	-1		selection	product	f	\N	\N	\N	\N	\N
1472	en	International Standard Audiovisual Number	product.identifier,type	-1		selection	product	f	\N	\N	\N	\N	\N
1473	en	International Standard Book Number	product.identifier,type	-1		selection	product	f	\N	\N	\N	\N	\N
1474	en	International Standard Identifier for Libraries	product.identifier,type	-1		selection	product	f	\N	\N	\N	\N	\N
1475	en	International Securities Identification Number	product.identifier,type	-1		selection	product	f	\N	\N	\N	\N	\N
1476	en	International Standard Music Number	product.identifier,type	-1		selection	product	f	\N	\N	\N	\N	\N
1477	en	Product List Price	product.list_price,name	-1		model	product	f	\N	\N	\N	\N	\N
1478	en	Company	product.list_price,company	-1		field	product	f	\N	\N	\N	\N	\N
1479	en	List Price	product.list_price,list_price	-1		field	product	f	\N	\N	\N	\N	\N
1480	en	Template	product.list_price,template	-1		field	product	f	\N	\N	\N	\N	\N
1481	en	Product Cost Price Method	product.cost_price_method,name	-1		model	product	f	\N	\N	\N	\N	\N
1482	en	Company	product.cost_price_method,company	-1		field	product	f	\N	\N	\N	\N	\N
1483	en	Cost Price Method	product.cost_price_method,cost_price_method	-1		field	product	f	\N	\N	\N	\N	\N
1484	en	Template	product.cost_price_method,template	-1		field	product	f	\N	\N	\N	\N	\N
1485	en	Product Cost Price	product.cost_price,name	-1		model	product	f	\N	\N	\N	\N	\N
1486	en	Company	product.cost_price,company	-1		field	product	f	\N	\N	\N	\N	\N
1487	en	Cost Price	product.cost_price,cost_price	-1		field	product	f	\N	\N	\N	\N	\N
1488	en	Product	product.cost_price,product	-1		field	product	f	\N	\N	\N	\N	\N
1489	en	Template - Category	product.template-product.category,name	-1		model	product	f	\N	\N	\N	\N	\N
1490	en	Category	product.template-product.category,category	-1		field	product	f	\N	\N	\N	\N	\N
1491	en	Template	product.template-product.category,template	-1		field	product	f	\N	\N	\N	\N	\N
1492	en	Template - Category All	product.template-product.category.all,name	-1		model	product	f	\N	\N	\N	\N	\N
1493	en	Category	product.template-product.category.all,category	-1		field	product	f	\N	\N	\N	\N	\N
1494	en	Template	product.template-product.category.all,template	-1		field	product	f	\N	\N	\N	\N	\N
1495	en	Product Configuration	product.configuration,name	-1		model	product	f	\N	\N	\N	\N	\N
1496	en	Default Cost Method	product.configuration,default_cost_price_method	-1		field	product	f	\N	\N	\N	\N	\N
1497	en	The default cost price method for new products.	product.configuration,default_cost_price_method	-1		help	product	f	\N	\N	\N	\N	\N
1498	en	Product Sequence	product.configuration,product_sequence	-1		field	product	f	\N	\N	\N	\N	\N
1499	en	Used to generate the product code.	product.configuration,product_sequence	-1		help	product	f	\N	\N	\N	\N	\N
1500	en	Product Configuration Default Cost Price Method	product.configuration.default_cost_price_method,name	-1		model	product	f	\N	\N	\N	\N	\N
1501	en	Default Cost Method	product.configuration.default_cost_price_method,default_cost_price_method	-1		field	product	f	\N	\N	\N	\N	\N
1502	en	The default cost price method for new products.	product.configuration.default_cost_price_method,default_cost_price_method	-1		help	product	f	\N	\N	\N	\N	\N
1503	en	Product Administration	res.group,name	6	Product Administration	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1504	en	Product	ir.ui.menu,name	65	Product	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1505	en	Configuration	ir.ui.menu,name	66	Configuration	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1506	en	Products	ir.action,name	76	Products	model	product	f	2020-09-02 07:24:32.977941	0	\N	2020-09-02 07:24:32.977941	0
1507	en	Products	ir.ui.menu,name	67	Products	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1508	en	Product by Category	ir.action,name	77	Product by Category	model	product	f	2020-09-02 07:24:32.977941	0	\N	2020-09-02 07:24:32.977941	0
1509	en	Variants	ir.action,name	78	Variants	model	product	f	2020-09-02 07:24:32.977941	0	\N	2020-09-02 07:24:32.977941	0
1510	en	Variants	ir.ui.menu,name	68	Variants	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1511	en	Variants	ir.action,name	79	Variants	model	product	f	2020-09-02 07:24:32.977941	0	\N	2020-09-02 07:24:32.977941	0
1512	en	Product	ir.sequence.type,name	2	Product	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1513	en	Categories	ir.action,name	80	Categories	model	product	f	2020-09-02 07:24:32.977941	0	\N	2020-09-02 07:24:32.977941	0
1514	en	Categories	ir.ui.menu,name	69	Categories	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1515	en	Categories	ir.action,name	81	Categories	model	product	f	2020-09-02 07:24:32.977941	0	\N	2020-09-02 07:24:32.977941	0
1516	en	Categories	ir.ui.menu,name	70	Categories	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1517	en	Units of Measure	ir.action,name	82	Units of Measure	model	product	f	2020-09-02 07:24:32.977941	0	\N	2020-09-02 07:24:32.977941	0
1518	en	Units of Measure	ir.ui.menu,name	71	Units of Measure	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1519	en	Categories of Unit of Measure	ir.action,name	83	Categories of Unit of Measure	model	product	f	2020-09-02 07:24:32.977941	0	\N	2020-09-02 07:24:32.977941	0
1520	en	Categories	ir.ui.menu,name	72	Categories	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1521	en	Units	product.uom.category,name	1	Units	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1522	en	Unit	product.uom,name	1	Unit	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1523	en	u	product.uom,symbol	1	u	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1524	en	Weight	product.uom.category,name	2	Weight	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1525	en	Kilogram	product.uom,name	2	Kilogram	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1526	en	kg	product.uom,symbol	2	kg	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1527	en	Gram	product.uom,name	3	Gram	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1528	en	g	product.uom,symbol	3	g	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1529	en	Carat	product.uom,name	4	Carat	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1530	en	c	product.uom,symbol	4	c	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1531	en	Pound	product.uom,name	5	Pound	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1532	en	lb	product.uom,symbol	5	lb	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1533	en	Ounce	product.uom,name	6	Ounce	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1534	en	oz	product.uom,symbol	6	oz	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1535	en	Time	product.uom.category,name	3	Time	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1536	en	Second	product.uom,name	7	Second	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1537	en	s	product.uom,symbol	7	s	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1538	en	Minute	product.uom,name	8	Minute	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1539	en	min	product.uom,symbol	8	min	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1540	en	Hour	product.uom,name	9	Hour	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1541	en	h	product.uom,symbol	9	h	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1542	en	Work Day	product.uom,name	10	Work Day	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1543	en	wd	product.uom,symbol	10	wd	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1544	en	Day	product.uom,name	11	Day	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1545	en	d	product.uom,symbol	11	d	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1546	en	Length	product.uom.category,name	4	Length	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1547	en	Meter	product.uom,name	12	Meter	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1548	en	m	product.uom,symbol	12	m	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1549	en	Kilometer	product.uom,name	13	Kilometer	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1550	en	km	product.uom,symbol	13	km	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1551	en	centimeter	product.uom,name	14	centimeter	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1552	en	cm	product.uom,symbol	14	cm	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1553	en	Millimeter	product.uom,name	15	Millimeter	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1554	en	mm	product.uom,symbol	15	mm	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1555	en	Foot	product.uom,name	16	Foot	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1556	en	ft	product.uom,symbol	16	ft	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1557	en	Yard	product.uom,name	17	Yard	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1558	en	yd	product.uom,symbol	17	yd	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1559	en	Inch	product.uom,name	18	Inch	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1560	en	in	product.uom,symbol	18	in	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1561	en	Mile	product.uom,name	19	Mile	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1562	en	mi	product.uom,symbol	19	mi	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1563	en	Volume	product.uom.category,name	5	Volume	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1564	en	Cubic meter	product.uom,name	20	Cubic meter	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1565	en	m³	product.uom,symbol	20	m³	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1566	en	Liter	product.uom,name	21	Liter	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1567	en	l	product.uom,symbol	21	l	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1568	en	Cubic centimeter	product.uom,name	22	Cubic centimeter	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1569	en	cm³	product.uom,symbol	22	cm³	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1570	en	Cubic inch	product.uom,name	23	Cubic inch	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1571	en	in³	product.uom,symbol	23	in³	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1572	en	Cubic foot	product.uom,name	24	Cubic foot	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1573	en	ft³	product.uom,symbol	24	ft³	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1574	en	Gallon	product.uom,name	25	Gallon	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1575	en	gal	product.uom,symbol	25	gal	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1576	en	Surface	product.uom.category,name	6	Surface	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1577	en	Square meter	product.uom,name	26	Square meter	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1578	en	m²	product.uom,symbol	26	m²	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1579	en	Square centimeter	product.uom,name	27	Square centimeter	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1580	en	cm²	product.uom,symbol	27	cm²	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1581	en	Are	product.uom,name	28	Are	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1582	en	a	product.uom,symbol	28	a	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1583	en	Hectare	product.uom,name	29	Hectare	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1584	en	ha	product.uom,symbol	29	ha	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1585	en	Square inch	product.uom,name	30	Square inch	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1586	en	in²	product.uom,symbol	30	in²	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1587	en	Square foot	product.uom,name	31	Square foot	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1588	en	ft²	product.uom,symbol	31	ft²	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1589	en	Square yard	product.uom,name	32	Square yard	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1590	en	yd²	product.uom,symbol	32	yd²	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1591	en	Product Configuration	ir.action,name	84	Product Configuration	model	product	f	2020-09-02 07:24:32.977941	0	\N	2020-09-02 07:24:32.977941	0
1592	en	Product Configuration	ir.ui.menu,name	73	Product Configuration	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1593	en	You cannot modify the factor of UOM "%(uom)s".	ir.message,text	113	You cannot modify the factor of UOM "%(uom)s".	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1594	en	You cannot modify the rate of UOM "%(uom)s".	ir.message,text	114	You cannot modify the rate of UOM "%(uom)s".	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1595	en	You cannot modify the category of UOM "%(uom)s".	ir.message,text	115	You cannot modify the category of UOM "%(uom)s".	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1596	en	If the UOM is still not used, you can delete it otherwise you can deactivate it and create a new one.	ir.message,text	116	If the UOM is still not used, you can delete it otherwise you can deactivate it and create a new one.	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1597	en	Incompatible factor and rate values on UOM "%(uom)s".	ir.message,text	117	Incompatible factor and rate values on UOM "%(uom)s".	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1598	en	Rate and factor can not be both equal to zero.	ir.message,text	118	Rate and factor can not be both equal to zero.	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1599	en	The %(type)s "%(code)s" for product "%(product)s" is not valid.	ir.message,text	119	The %(type)s "%(code)s" for product "%(product)s" is not valid.	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1600	en	Code of active product must be unique.	ir.message,text	120	Code of active product must be unique.	model	product	f	2020-09-02 07:24:32.977941	0	\N	\N	\N
1602	en	Nombre	taller.marca,name	-1		field	taller	f	\N	\N	\N	\N	\N
1642	en	tubo de escape	product.template,name	1	tubo de escape	model	\N	f	2020-09-07 09:12:25.291892	1	\N	\N	\N
1603	en	Taller Administration	res.group,name	7	Taller Administration	model	taller	f	2020-09-02 07:40:09.139967	0	\N	\N	\N
1604	en	Taller 	res.group,name	8	Taller 	model	taller	f	2020-09-02 07:40:09.139967	0	\N	\N	\N
1635	en	nº caballos	taller.coche,caballosImp	-1		field	taller	f	\N	\N	\N	\N	\N
1605	en	Taller	ir.ui.menu,name	74	Taller	model	taller	f	2020-09-02 07:56:32.835869	0	\N	\N	\N
1606	en	Marcas	ir.action,name	85	Marcas	model	taller	f	2020-09-02 07:56:32.835869	0	\N	2020-09-02 07:56:32.835869	0
1607	en	Marcas	ir.ui.menu,name	75	Marcas	model	taller	f	2020-09-02 07:56:32.835869	0	\N	\N	\N
1636	en	fecha de lanzamiento	taller.coche,fechaImp	-1		field	taller	f	\N	\N	\N	\N	\N
1637	en	fecha2	taller.coche,fechaImp2	-1		field	taller	f	\N	\N	\N	\N	\N
1609	en	numero de caballos	taller.modelo,caballos	-1		field	taller	f	\N	\N	\N	\N	\N
1610	en	tipo de combustible	taller.modelo,combustible	-1		field	taller	f	\N	\N	\N	\N	\N
1611	en	gasolina	taller.modelo,combustible	-1		selection	taller	f	\N	\N	\N	\N	\N
1612	en	diesel	taller.modelo,combustible	-1		selection	taller	f	\N	\N	\N	\N	\N
1613	en	hibrido	taller.modelo,combustible	-1		selection	taller	f	\N	\N	\N	\N	\N
1614	en	electrico	taller.modelo,combustible	-1		selection	taller	f	\N	\N	\N	\N	\N
1615	en	Fecha Lanzamiento	taller.modelo,fecha_lanz	-1		field	taller	f	\N	\N	\N	\N	\N
1616	en	Marca	taller.modelo,marca	-1		field	taller	f	\N	\N	\N	\N	\N
1617	en	Modelo	taller.modelo,modelo	-1		field	taller	f	\N	\N	\N	\N	\N
1618	en	Modelos	ir.action,name	86	Modelos	model	taller	f	2020-09-02 08:45:11.63	0	\N	2020-09-02 08:45:11.63	0
1619	en	Modelos	ir.ui.menu,name	76	Modelos	model	taller	f	2020-09-02 08:45:11.63	0	\N	\N	\N
1646	en	Coches compatibles:	product.template,cochesCompatibles	-1		field	taller	f	\N	\N	\N	\N	\N
1620	en	Modelos	taller.marca,modelos	-1		field	taller	f	\N	\N	\N	\N	\N
1656	en	Modelos compatibles	product.template,coches_compatibles	-1		field	taller	f	\N	\N	\N	\N	\N
1621	en	Coches	ir.action,name	87	Coches	model	taller	f	2020-09-02 11:14:57.919973	0	\N	2020-09-02 11:14:57.919973	0
1647	en	ruedas	product.template,name	2	ruedas	model	\N	f	2020-09-07 13:43:27.782244	1	\N	\N	\N
1623	en	Fecha baja	taller.coche,fecha_baja	-1		field	taller	f	\N	\N	\N	\N	\N
1624	en	Fecha matriculacion	taller.coche,fecha_matriculacion	-1		field	taller	f	\N	\N	\N	\N	\N
1625	en	Marca	taller.coche,marca	-1		field	taller	f	\N	\N	\N	\N	\N
1626	en	Matricula	taller.coche,matricula	-1		field	taller	f	\N	\N	\N	\N	\N
1627	en	Marca	taller.coche,modelo	-1		field	taller	f	\N	\N	\N	\N	\N
1628	en	precio	taller.coche,precio	-1		field	taller	f	\N	\N	\N	\N	\N
1629	en	Party	taller.coche,propietario	-1		field	taller	f	\N	\N	\N	\N	\N
1657	en	coche-producto	coche-producto,name	-1		model	taller	f	\N	\N	\N	\N	\N
1638	en	 Matricula repetida 	ir.message,text	121	 Matricula repetida 	model	taller	f	2020-09-04 07:51:32.521302	0	\N	\N	\N
1630	en	Coches	ir.ui.menu,name	77	Coches	model	taller	f	2020-09-02 11:19:59.170481	0	\N	\N	\N
1658	en	modelo	coche-producto,coche	-1		field	taller	f	\N	\N	\N	\N	\N
1601	en	Marca	taller.marca,name	-1		model	taller	f	\N	\N	\N	\N	\N
1608	en	Modelo	taller.modelo,name	-1		model	taller	f	\N	\N	\N	\N	\N
1622	en	Coche	taller.coche,name	-1		model	taller	f	\N	\N	\N	\N	\N
1645	en	Coches	party.party,coches	-1		field	taller	f	\N	\N	\N	\N	\N
1648	en	productos disponibles	taller.modelo,productosDelModelo	-1		field	taller	f	\N	\N	\N	\N	\N
1650	en	modelo - producto	modelo-producto,name	-1		model	taller	f	\N	\N	\N	\N	\N
1649	en	Modelos compatibles	product.template,modelos_compatibles	-1		field	taller	f	\N	\N	\N	\N	\N
1639	en	precio del modelo serie	taller.modelo,precioMod	-1		field	taller	f	\N	\N	\N	\N	\N
1631	en	Modelo	taller.coche,modelo	-1		field	taller	f	\N	\N	\N	\N	\N
1651	en	modelo	modelo-producto,modelo	-1		field	taller	f	\N	\N	\N	\N	\N
1632	en	numero de caballos	taller.coche,caballosImp	-1		field	taller	f	\N	\N	\N	\N	\N
1633	en	fecha lanz	taller.coche,fechaImp	-1		field	taller	f	\N	\N	\N	\N	\N
1634	en	Propietario	taller.coche,propietario	-1		field	taller	f	\N	\N	\N	\N	\N
1654	en	 lista Coches 	ir.action,name	88	 lista Coches 	model	taller	f	2020-09-08 08:59:28.457176	0	\N	2020-09-08 08:59:28.457176	0
1652	en	producto	modelo-producto,producto	-1		field	taller	f	\N	\N	\N	\N	\N
1653	en	cambio de aceite	product.template,name	3	cambio de aceite	model	\N	f	2020-09-08 07:44:04.847033	1	\N	\N	\N
1655	en	productos compatibles con su coche	taller.coche,productos_coche	-1		field	taller	f	\N	\N	\N	\N	\N
1640	en	precio del modelo	taller.modelo,precioMod	-1		field	taller	f	\N	\N	\N	\N	\N
1659	en	producto	coche-producto,producto	-1		field	taller	f	\N	\N	\N	\N	\N
1641	en	Modelos	party.party,coches	-1		field	taller	f	\N	\N	\N	\N	\N
1660	en	 lista Coches productos 	ir.action,name	89	 lista Coches productos 	model	taller	f	2020-09-09 07:20:33.036685	0	\N	2020-09-09 07:20:33.036685	0
1662	en	Fecha baja	taller.coche.baja.start,fecha_baja	-1		field	taller	f	\N	\N	\N	\N	\N
1664	en	numero de coches afectados	taller.coche.baja.result,n_coches_adfectados	-1		field	taller	f	\N	\N	\N	\N	\N
1661	en	baja de coche	taller.coche.baja.start,name	-1		model	taller	f	\N	\N	\N	\N	\N
1663	en	resultado baja de coche	taller.coche.baja.result,name	-1		model	taller	f	\N	\N	\N	\N	\N
1665	en	Close	taller.coche.baja,result,end	-1		wizard_button	taller	f	\N	\N	\N	\N	\N
1669	en	Baja Transition	ir.action,name	91	Baja Transition	model	taller	f	2020-09-09 08:30:29.70123	0	\N	2020-09-09 08:30:29.70123	0
1666	en	Cancel	taller.coche.baja,start,end	-1		wizard_button	taller	f	\N	\N	\N	\N	\N
1667	en	Baja	taller.coche.baja,start,baja	-1		wizard_button	taller	f	\N	\N	\N	\N	\N
1675	en	Web User	web.user,name	-1		model	web_user	f	\N	\N	\N	\N	\N
1676	en	E-mail	web.user,email	-1		field	web_user	f	\N	\N	\N	\N	\N
1677	en	E-mail Token	web.user,email_token	-1		field	web_user	f	\N	\N	\N	\N	\N
1678	en	E-mail Valid	web.user,email_valid	-1		field	web_user	f	\N	\N	\N	\N	\N
1674	en	Report_lista_coches	ir.action,name	95	Report_lista_coches	model	taller	f	2020-09-14 06:53:25.827367	0	\N	2020-09-14 06:53:25.827367	0
1679	en	Party	web.user,party	-1		field	web_user	f	\N	\N	\N	\N	\N
1680	en	Password	web.user,password	-1		field	web_user	f	\N	\N	\N	\N	\N
1681	en	Password Hash	web.user,password_hash	-1		field	web_user	f	\N	\N	\N	\N	\N
1671	en	euro	currency.currency,name	1	euro	model	\N	f	2020-09-10 09:01:59.038499	1	\N	\N	\N
1682	en	Reset Password Token	web.user,reset_password_token	-1		field	web_user	f	\N	\N	\N	\N	\N
1683	en	Reset Password Token Expire	web.user,reset_password_token_expire	-1		field	web_user	f	\N	\N	\N	\N	\N
1684	en	Web User Authenticate Attempt	web.user.authenticate.attempt,name	-1		model	web_user	f	\N	\N	\N	\N	\N
1685	en	IP Address	web.user.authenticate.attempt,ip_address	-1		field	web_user	f	\N	\N	\N	\N	\N
1686	en	IP Network	web.user.authenticate.attempt,ip_network	-1		field	web_user	f	\N	\N	\N	\N	\N
1687	en	Login	web.user.authenticate.attempt,login	-1		field	web_user	f	\N	\N	\N	\N	\N
1688	en	Web User Session	web.user.session,name	-1		model	web_user	f	\N	\N	\N	\N	\N
1689	en	Key	web.user.session,key	-1		field	web_user	f	\N	\N	\N	\N	\N
1690	en	User	web.user.session,user	-1		field	web_user	f	\N	\N	\N	\N	\N
1691	en	Web Users	ir.action,name	96	Web Users	model	web_user	f	2020-09-16 08:36:14.823954	0	\N	2020-09-16 08:36:14.823954	0
1692	en	Web Users	ir.ui.menu,name	78	Web Users	model	web_user	f	2020-09-16 08:36:14.823954	0	\N	\N	\N
1670	en	Report_coche	ir.action,name	92	Report_coche	model	taller	f	2020-09-10 08:14:35.515414	0	\N	2020-09-10 08:14:35.515414	0
1693	en	Validate E-mail	ir.model.button,string	15	Validate E-mail	model	web_user	f	2020-09-16 08:36:14.823954	0	\N	\N	\N
1694	en	Reset Password	ir.model.button,string	16	Reset Password	model	web_user	f	2020-09-16 08:36:14.823954	0	\N	\N	\N
1695	en	Email Validation	ir.action,name	97	Email Validation	model	web_user	f	2020-09-16 08:36:14.823954	0	\N	2020-09-16 08:36:14.823954	0
1696	en	Reset Password	ir.action,name	98	Reset Password	model	web_user	f	2020-09-16 08:36:14.823954	0	\N	2020-09-16 08:36:14.823954	0
1697	en	E-mail of active web user must be unique.	ir.message,text	122	E-mail of active web user must be unique.	model	web_user	f	2020-09-16 08:36:14.823954	0	\N	\N	\N
1698	en	Web user session key must be unique.	ir.message,text	123	Web user session key must be unique.	model	web_user	f	2020-09-16 08:36:14.823954	0	\N	\N	\N
\.


--
-- Data for Name: ir_trigger; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_trigger (id, action, active, condition, create_date, create_uid, limit_number, minimum_time_delay, model, name, on_create, on_delete, on_time, on_write, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_trigger__history; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_trigger__history (id, __id) FROM stdin;
\.


--
-- Data for Name: ir_trigger_log; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_trigger_log (id, create_date, create_uid, record_id, trigger, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_ui_icon; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_ui_icon (id, create_date, create_uid, module, name, path, sequence, write_date, write_uid) FROM stdin;
1	2020-09-01 08:49:55.946968	0	ir	tryton-board	ui/icons/tryton-board.svg	10	\N	\N
2	2020-09-01 08:49:55.946968	0	ir	tryton-calendar	ui/icons/tryton-calendar.svg	10	\N	\N
3	2020-09-01 08:49:55.946968	0	ir	tryton-folder	ui/icons/tryton-folder.svg	10	\N	\N
4	2020-09-01 08:49:55.946968	0	ir	tryton-form	ui/icons/tryton-form.svg	10	\N	\N
5	2020-09-01 08:49:55.946968	0	ir	tryton-graph	ui/icons/tryton-graph.svg	10	\N	\N
6	2020-09-01 08:49:55.946968	0	ir	tryton-list	ui/icons/tryton-list.svg	10	\N	\N
7	2020-09-01 08:49:55.946968	0	ir	tryton-settings	ui/icons/tryton-settings.svg	10	\N	\N
8	2020-09-01 08:49:55.946968	0	ir	tryton-tree	ui/icons/tryton-tree.svg	10	\N	\N
9	2020-09-02 07:24:22.107384	0	country	tryton-country	icons/tryton-country.svg	10	\N	\N
10	2020-09-02 07:24:23.639802	0	currency	tryton-currency	icons/tryton-currency.svg	10	\N	\N
11	2020-09-02 07:24:24.586225	0	party	tryton-party	icons/tryton-party.svg	10	\N	\N
12	2020-09-02 07:24:29.514826	0	company	tryton-company	icons/tryton-company.svg	10	\N	\N
13	2020-09-02 07:24:32.977941	0	product	tryton-product	icons/tryton-product.svg	10	\N	\N
\.


--
-- Data for Name: ir_ui_menu; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_ui_menu (id, parent, name, icon, active, create_date, create_uid, sequence, write_date, write_uid) FROM stdin;
1	\N	Administration	tryton-settings	t	2020-09-01 08:49:55.946968	0	9999	\N	\N
2	1	User Interface	tryton-folder	t	2020-09-01 08:49:55.946968	0	10	\N	\N
3	2	Icons	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
4	2	Menu	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
5	2	Views	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
6	2	View Tree Width	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
7	2	Tree State	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
8	2	View Search	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
9	2	Actions	tryton-folder	t	2020-09-01 08:49:55.946968	0	10	\N	\N
10	9	Actions	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
11	9	Reports	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
12	9	Window Actions	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
13	9	Wizards	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
14	9	URLs	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
15	1	Models	tryton-folder	t	2020-09-01 08:49:55.946968	0	10	\N	\N
16	15	Models	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
17	15	Fields	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
18	15	Models Access	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
19	18	Fields Access	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
20	18	Buttons	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
21	16	Data	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
22	1	Sequences	tryton-folder	t	2020-09-01 08:49:55.946968	0	10	\N	\N
23	22	Sequences	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
24	22	Sequences Strict	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
25	22	Sequence Types	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
26	15	Attachments	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
27	15	Notes	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
28	1	Scheduler	tryton-folder	t	2020-09-01 08:49:55.946968	0	10	\N	\N
29	28	Scheduled Actions	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
30	1	Localization	tryton-folder	t	2020-09-01 08:49:55.946968	0	10	\N	\N
31	30	Languages	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
32	30	Translations	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
33	30	Set Translations	tryton-launch	t	2020-09-01 08:49:55.946968	0	20	\N	\N
34	30	Clean Translations	tryton-launch	t	2020-09-01 08:49:55.946968	0	30	\N	\N
35	30	Synchronize Translations	tryton-launch	t	2020-09-01 08:49:55.946968	0	40	\N	\N
36	30	Export Translations	tryton-launch	t	2020-09-01 08:49:55.946968	0	50	\N	\N
37	15	Exports	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
38	15	Record Rules	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
39	1	Modules	tryton-folder	t	2020-09-01 08:49:55.946968	0	10	\N	\N
40	39	Modules	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
41	39	Config Wizard Items	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
42	39	Perform Pending Activation/Upgrade	tryton-launch	t	2020-09-01 08:49:55.946968	0	10	\N	\N
43	15	Triggers	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
44	30	Messages	tryton-list	t	2020-09-01 08:49:55.946968	0	10	\N	\N
45	1	Users	tryton-folder	t	2020-09-01 08:50:28.180942	0	10	\N	\N
46	45	Groups	tryton-list	t	2020-09-01 08:50:28.180942	0	10	\N	\N
47	45	Users	tryton-list	t	2020-09-01 08:50:28.180942	0	10	\N	\N
48	1	Countries	tryton-country	t	2020-09-02 07:24:22.107384	0	10	\N	\N
49	\N	Currency	tryton-currency	t	2020-09-02 07:24:23.639802	0	3	\N	\N
50	49	Currencies	tryton-list	t	2020-09-02 07:24:23.639802	0	10	\N	\N
51	\N	Party	tryton-party	t	2020-09-02 07:24:24.586225	0	0	\N	\N
52	51	Configuration	tryton-settings	t	2020-09-02 07:24:24.586225	0	0	\N	\N
53	51	Parties	tryton-list	t	2020-09-02 07:24:24.586225	0	1	\N	\N
54	51	Categories	tryton-tree	t	2020-09-02 07:24:24.586225	0	10	\N	\N
55	54	Categories	tryton-list	t	2020-09-02 07:24:24.586225	0	10	\N	\N
56	51	Addresses	tryton-list	t	2020-09-02 07:24:24.586225	0	2	\N	\N
57	52	Address Formats	tryton-list	t	2020-09-02 07:24:24.586225	0	10	\N	\N
58	52	Address Subdivision Types	tryton-list	t	2020-09-02 07:24:24.586225	0	20	\N	\N
59	51	Contact Mechanisms	tryton-list	t	2020-09-02 07:24:24.586225	0	3	\N	\N
60	52	Party Configuration	tryton-list	t	2020-09-02 07:24:24.586225	0	0	\N	\N
61	\N	Company	tryton-company	t	2020-09-02 07:24:29.514826	0	2	\N	\N
62	61	Companies	tryton-tree	t	2020-09-02 07:24:29.514826	0	10	\N	\N
63	62	Companies	tryton-list	t	2020-09-02 07:24:29.514826	0	10	\N	\N
64	61	Employees	tryton-list	t	2020-09-02 07:24:29.514826	0	10	\N	\N
65	\N	Product	tryton-product	t	2020-09-02 07:24:32.977941	0	1	\N	\N
66	65	Configuration	tryton-settings	t	2020-09-02 07:24:32.977941	0	0	\N	\N
67	65	Products	tryton-list	t	2020-09-02 07:24:32.977941	0	1	\N	\N
68	67	Variants	tryton-list	t	2020-09-02 07:24:32.977941	0	1	\N	\N
69	65	Categories	tryton-tree	t	2020-09-02 07:24:32.977941	0	2	\N	\N
70	69	Categories	tryton-list	t	2020-09-02 07:24:32.977941	0	10	\N	\N
71	65	Units of Measure	tryton-list	t	2020-09-02 07:24:32.977941	0	3	\N	\N
72	71	Categories	tryton-list	t	2020-09-02 07:24:32.977941	0	2	\N	\N
73	66	Product Configuration	tryton-list	t	2020-09-02 07:24:32.977941	0	0	\N	\N
74	\N	Taller	tryton-folder	t	2020-09-02 07:56:32.835869	0	3	\N	\N
75	74	Marcas	tryton-list	t	2020-09-02 07:56:32.835869	0	10	\N	\N
76	74	Modelos	tryton-list	t	2020-09-02 08:45:11.63	0	10	\N	\N
77	74	Coches	tryton-list	t	2020-09-02 11:19:59.170481	0	10	\N	\N
78	45	Web Users	tryton-list	t	2020-09-16 08:36:14.823954	0	30	\N	\N
\.


--
-- Data for Name: ir_ui_menu-res_group; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."ir_ui_menu-res_group" (id, create_date, create_uid, "group", menu, write_date, write_uid) FROM stdin;
1	2020-09-01 08:50:28.180942	0	1	46	\N	\N
2	2020-09-01 08:50:28.180942	0	1	47	\N	\N
3	2020-09-01 08:50:28.180942	0	1	1	\N	\N
4	2020-09-01 08:50:28.180942	0	1	2	\N	\N
5	2020-09-01 08:50:28.180942	0	1	3	\N	\N
6	2020-09-01 08:50:28.180942	0	1	4	\N	\N
7	2020-09-01 08:50:28.180942	0	1	5	\N	\N
8	2020-09-01 08:50:28.180942	0	1	6	\N	\N
9	2020-09-01 08:50:28.180942	0	1	9	\N	\N
10	2020-09-01 08:50:28.180942	0	1	10	\N	\N
11	2020-09-01 08:50:28.180942	0	1	11	\N	\N
12	2020-09-01 08:50:28.180942	0	1	12	\N	\N
13	2020-09-01 08:50:28.180942	0	1	13	\N	\N
14	2020-09-01 08:50:28.180942	0	1	14	\N	\N
15	2020-09-01 08:50:28.180942	0	1	15	\N	\N
16	2020-09-01 08:50:28.180942	0	1	16	\N	\N
17	2020-09-01 08:50:28.180942	0	1	17	\N	\N
18	2020-09-01 08:50:28.180942	0	1	18	\N	\N
19	2020-09-01 08:50:28.180942	0	1	19	\N	\N
20	2020-09-01 08:50:28.180942	0	1	22	\N	\N
21	2020-09-01 08:50:28.180942	0	1	23	\N	\N
22	2020-09-01 08:50:28.180942	0	1	24	\N	\N
23	2020-09-01 08:50:28.180942	0	1	25	\N	\N
24	2020-09-01 08:50:28.180942	0	1	26	\N	\N
25	2020-09-01 08:50:28.180942	0	1	28	\N	\N
26	2020-09-01 08:50:28.180942	0	1	29	\N	\N
27	2020-09-01 08:50:28.180942	0	1	30	\N	\N
28	2020-09-01 08:50:28.180942	0	1	31	\N	\N
29	2020-09-01 08:50:28.180942	0	1	32	\N	\N
30	2020-09-01 08:50:28.180942	0	1	33	\N	\N
31	2020-09-01 08:50:28.180942	0	1	34	\N	\N
32	2020-09-01 08:50:28.180942	0	1	35	\N	\N
33	2020-09-01 08:50:28.180942	0	1	36	\N	\N
34	2020-09-01 08:50:28.180942	0	1	37	\N	\N
35	2020-09-01 08:50:28.180942	0	1	38	\N	\N
36	2020-09-01 08:50:28.180942	0	1	39	\N	\N
37	2020-09-01 08:50:28.180942	0	1	40	\N	\N
38	2020-09-01 08:50:28.180942	0	1	41	\N	\N
39	2020-09-01 08:50:28.180942	0	1	42	\N	\N
40	2020-09-01 08:50:28.180942	0	1	43	\N	\N
41	2020-09-02 07:24:22.107384	0	1	48	\N	\N
42	2020-09-02 07:24:23.639802	0	2	49	\N	\N
43	2020-09-02 07:24:24.586225	0	3	52	\N	\N
44	2020-09-02 07:24:24.586225	0	3	60	\N	\N
45	2020-09-02 07:24:29.514826	0	4	61	\N	\N
46	2020-09-02 07:24:29.514826	0	5	61	\N	\N
47	2020-09-02 07:24:32.977941	0	6	66	\N	\N
48	2020-09-02 07:24:32.977941	0	6	73	\N	\N
\.


--
-- Data for Name: ir_ui_menu_favorite; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_ui_menu_favorite (id, create_date, create_uid, menu, sequence, "user", write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_ui_view; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_ui_view (id, model, type, data, field_childs, priority, create_date, create_uid, domain, inherit, module, name, write_date, write_uid) FROM stdin;
1	ir.ui.icon	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	icon_view_list	\N	\N
2	ir.ui.icon	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	icon_view_form	\N	\N
3	ir.ui.menu	tree	\N	childs	20	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_menu_tree	\N	\N
4	ir.ui.menu	tree	\N	\N	10	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_menu_list	\N	\N
5	ir.ui.menu	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_menu_form	\N	\N
6	ir.ui.menu.favorite	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_menu_favorite_list	\N	\N
7	ir.ui.menu.favorite	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_menu_favorite_form	\N	\N
8	ir.ui.view	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_view_form	\N	\N
9	ir.ui.view	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_view_list	\N	\N
10	ir.ui.view_tree_width	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_view_tree_width_form	\N	\N
11	ir.ui.view_tree_width	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_view_tree_width_list	\N	\N
12	ir.ui.view_tree_state	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_view_tree_state_form	\N	\N
13	ir.ui.view_tree_state	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_view_tree_state_list	\N	\N
14	ir.ui.view_search	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_view_search_form	\N	\N
15	ir.ui.view_search	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	ui_view_search_list	\N	\N
16	ir.action	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_form	\N	\N
17	ir.action	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_list	\N	\N
18	ir.action.keyword	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_keyword_list	\N	\N
19	ir.action.keyword	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_keyword_form	\N	\N
20	ir.action.report	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_report_form	\N	\N
21	ir.action.report	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_report_list	\N	\N
22	ir.action.act_window	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_act_window_form	\N	\N
23	ir.action.act_window	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_act_window_list	\N	\N
24	ir.action.act_window.view	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_act_window_view_form	\N	\N
25	ir.action.act_window.view	tree	\N	\N	10	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_act_window_view_list	\N	\N
26	ir.action.act_window.view	tree	\N	\N	20	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_act_window_view_list2	\N	\N
27	ir.action.act_window.domain	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_act_window_domain_form	\N	\N
28	ir.action.act_window.domain	tree	\N	\N	10	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_act_window_domain_list	\N	\N
29	ir.action.act_window.domain	tree	\N	\N	20	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_act_window_domain_list2	\N	\N
30	ir.action.wizard	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_wizard_form	\N	\N
31	ir.action.wizard	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_wizard_list	\N	\N
32	ir.action.url	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_url_form	\N	\N
33	ir.action.url	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	action_url_list	\N	\N
34	ir.model	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_form	\N	\N
35	ir.model	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_list	\N	\N
36	ir.model.field	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_field_form	\N	\N
37	ir.model.field	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_field_list	\N	\N
38	ir.model.access	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_access_list	\N	\N
39	ir.model.access	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_access_form	\N	\N
40	ir.model.field.access	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_field_access_list	\N	\N
41	ir.model.field.access	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_field_access_form	\N	\N
42	ir.model.print_model_graph.start	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_print_model_graph_start_form	\N	\N
43	ir.model.button	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_button_list	\N	\N
44	ir.model.button	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_button_form	\N	\N
45	ir.model.button.rule	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_button_rule_list	\N	\N
46	ir.model.button.rule	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_button_rule_form	\N	\N
47	ir.model.button.click	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_button_click_list	\N	\N
48	ir.model.button.click	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_button_click_form	\N	\N
49	ir.model.data	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_data_list	\N	\N
50	ir.model.data	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	model_data_form	\N	\N
51	ir.sequence	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	sequence_form	\N	\N
52	ir.sequence	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	sequence_list	\N	\N
53	ir.sequence.strict	\N	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	51	ir	\N	\N	\N
54	ir.sequence.strict	\N	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	52	ir	\N	\N	\N
55	ir.sequence.type	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	sequence_type_form	\N	\N
56	ir.sequence.type	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	sequence_type_list	\N	\N
57	ir.attachment	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	attachment_form	\N	\N
58	ir.attachment	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	attachment_list	\N	\N
59	ir.note	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	note_form	\N	\N
60	ir.note	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	note_list	\N	\N
61	ir.cron	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	cron_list	\N	\N
62	ir.cron	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	cron_form	\N	\N
63	ir.lang	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	lang_list	\N	\N
64	ir.lang	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	lang_form	\N	\N
65	ir.lang.config.start	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	lang_config_start_form	\N	\N
66	ir.translation	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	translation_form	\N	\N
67	ir.translation	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	translation_list	\N	\N
68	ir.translation.set.start	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	translation_set_start_form	\N	\N
69	ir.translation.set.succeed	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	translation_set_succeed_form	\N	\N
70	ir.translation.clean.start	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	translation_clean_start_form	\N	\N
71	ir.translation.clean.succeed	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	translation_clean_succeed_form	\N	\N
72	ir.translation.update.start	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	translation_update_start_form	\N	\N
73	ir.translation.export.start	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	translation_export_start_form	\N	\N
74	ir.translation.export.result	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	translation_export_result_form	\N	\N
75	ir.export	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	export_form	\N	\N
76	ir.export	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	export_list	\N	\N
77	ir.export.line	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	export_line_form	\N	\N
78	ir.export.line	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	export_line_list	\N	\N
79	ir.rule.group	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	rule_group_form	\N	\N
80	ir.rule.group	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	rule_group_list	\N	\N
81	ir.rule	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	rule_form	\N	\N
82	ir.rule	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	rule_list	\N	\N
83	ir.module	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	module_form	\N	\N
84	ir.module	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	module_list	\N	\N
85	ir.module.dependency	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	module_dependency_form	\N	\N
86	ir.module.dependency	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	module_dependency_list	\N	\N
87	ir.module.config_wizard.item	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	module_config_wizard_item_list	\N	\N
88	ir.module.config_wizard.first	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	module_config_wizard_first_form	\N	\N
89	ir.module.config_wizard.other	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	module_config_wizard_other_form	\N	\N
90	ir.module.config_wizard.done	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	module_config_wizard_done_form	\N	\N
91	ir.module.activate_upgrade.start	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	module_activate_upgrade_start_form	\N	\N
92	ir.module.activate_upgrade.done	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	module_activate_upgrade_done_form	\N	\N
93	ir.trigger	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	trigger_form	\N	\N
94	ir.trigger	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	trigger_list	\N	\N
95	ir.message	tree	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	message_list	\N	\N
96	ir.message	form	\N	\N	16	2020-09-01 08:49:55.946968	0	\N	\N	ir	message_form	\N	\N
97	res.group	form	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	\N	res	group_form	\N	\N
98	res.group	tree	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	\N	res	group_list	\N	\N
99	res.user	form	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	\N	res	user_form	\N	\N
100	res.user	form	\N	\N	20	2020-09-01 08:50:28.180942	0	\N	\N	res	user_form_preferences	\N	\N
101	res.user	tree	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	\N	res	user_list	\N	\N
102	res.user.config.start	form	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	\N	res	user_config_start_form	\N	\N
103	res.user.warning	form	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	\N	res	user_warning_form	\N	\N
104	res.user.warning	tree	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	\N	res	user_warning_tree	\N	\N
105	res.user.application	form	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	\N	res	user_application_form	\N	\N
106	res.user.application	tree	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	\N	res	user_application_list	\N	\N
107	ir.sequence.type	\N	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	55	res	sequence_type_form	\N	\N
108	ir.export	\N	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	75	res	export_form	\N	\N
109	ir.export	\N	\N	\N	16	2020-09-01 08:50:28.180942	0	\N	76	res	export_list	\N	\N
110	country.country	form	\N	\N	16	2020-09-02 07:24:22.107384	0	\N	\N	country	country_form	\N	\N
111	country.country	tree	\N	\N	16	2020-09-02 07:24:22.107384	0	\N	\N	country	country_tree	\N	\N
112	country.subdivision	form	\N	\N	16	2020-09-02 07:24:22.107384	0	\N	\N	country	subdivision_form	\N	\N
113	country.subdivision	tree	\N	\N	16	2020-09-02 07:24:22.107384	0	\N	\N	country	subdivision_tree	\N	\N
114	country.zip	form	\N	\N	16	2020-09-02 07:24:22.107384	0	\N	\N	country	zip_form	\N	\N
115	country.zip	tree	\N	\N	16	2020-09-02 07:24:22.107384	0	\N	\N	country	zip_list	\N	\N
116	currency.currency	form	\N	\N	16	2020-09-02 07:24:23.639802	0	\N	\N	currency	currency_form	\N	\N
117	currency.currency	tree	\N	\N	16	2020-09-02 07:24:23.639802	0	\N	\N	currency	currency_tree	\N	\N
118	currency.currency.rate	tree	\N	\N	16	2020-09-02 07:24:23.639802	0	\N	\N	currency	currency_rate_list	\N	\N
119	currency.currency.rate	form	\N	\N	16	2020-09-02 07:24:23.639802	0	\N	\N	currency	currency_rate_form	\N	\N
120	currency.currency.rate	graph	\N	\N	16	2020-09-02 07:24:23.639802	0	\N	\N	currency	currency_rate_graph	\N	\N
121	party.party	tree	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	party_tree	\N	\N
122	party.party	form	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	party_form	\N	\N
123	party.identifier	form	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	identifier_form	\N	\N
124	party.identifier	tree	\N	\N	10	2020-09-02 07:24:24.586225	0	\N	\N	party	identifier_list	\N	\N
125	party.identifier	tree	\N	\N	20	2020-09-02 07:24:24.586225	0	\N	\N	party	identifier_list_sequence	\N	\N
126	party.check_vies.result	form	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	check_vies_result	\N	\N
127	party.replace.ask	form	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	replace_ask_form	\N	\N
128	party.erase.ask	form	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	erase_ask_form	\N	\N
129	party.category	form	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	category_form	\N	\N
130	party.category	tree	\N	childs	16	2020-09-02 07:24:24.586225	0	\N	\N	party	category_tree	\N	\N
131	party.category	tree	\N	\N	10	2020-09-02 07:24:24.586225	0	\N	\N	party	category_list	\N	\N
132	party.address	tree	\N	\N	10	2020-09-02 07:24:24.586225	0	\N	\N	party	address_tree	\N	\N
133	party.address	tree	\N	\N	20	2020-09-02 07:24:24.586225	0	\N	\N	party	address_tree_sequence	\N	\N
134	party.address	form	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	address_form	\N	\N
135	party.address.format	tree	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	address_format_list	\N	\N
136	party.address.format	form	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	address_format_form	\N	\N
137	party.address.subdivision_type	tree	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	address_subdivision_type_list	\N	\N
138	party.address.subdivision_type	form	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	address_subdivision_type_form	\N	\N
139	party.contact_mechanism	tree	\N	\N	10	2020-09-02 07:24:24.586225	0	\N	\N	party	contact_mechanism_tree	\N	\N
140	party.contact_mechanism	tree	\N	\N	20	2020-09-02 07:24:24.586225	0	\N	\N	party	contact_mechanism_tree_sequence	\N	\N
141	party.contact_mechanism	form	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	contact_mechanism_form	\N	\N
142	party.configuration	form	\N	\N	16	2020-09-02 07:24:24.586225	0	\N	\N	party	configuration_form	\N	\N
143	company.company	form	\N	\N	16	2020-09-02 07:24:29.514826	0	\N	\N	company	company_form	\N	\N
144	company.company	tree	\N	childs	16	2020-09-02 07:24:29.514826	0	\N	\N	company	company_tree	\N	\N
145	company.company	tree	\N	\N	10	2020-09-02 07:24:29.514826	0	\N	\N	company	company_list	\N	\N
146	res.user	\N	\N	\N	16	2020-09-02 07:24:29.514826	0	\N	99	company	user_form	\N	\N
147	res.user	\N	\N	\N	16	2020-09-02 07:24:29.514826	0	\N	100	company	user_form_preferences	\N	\N
148	company.company.config.start	form	\N	\N	16	2020-09-02 07:24:29.514826	0	\N	\N	company	company_config_start_form	\N	\N
149	company.employee	form	\N	\N	10	2020-09-02 07:24:29.514826	0	\N	\N	company	employee_form	\N	\N
150	company.employee	tree	\N	\N	16	2020-09-02 07:24:29.514826	0	\N	\N	company	employee_tree	\N	\N
151	ir.sequence	\N	\N	\N	16	2020-09-02 07:24:29.514826	0	\N	51	company	sequence_form	\N	\N
152	ir.sequence	\N	\N	\N	16	2020-09-02 07:24:29.514826	0	\N	52	company	sequence_tree	\N	\N
153	ir.cron	\N	\N	\N	16	2020-09-02 07:24:29.514826	0	\N	62	company	cron_form	\N	\N
154	product.template	tree	\N	\N	16	2020-09-02 07:24:32.977941	0	\N	\N	product	template_tree	\N	\N
155	product.template	form	\N	\N	16	2020-09-02 07:24:32.977941	0	\N	\N	product	template_form	\N	\N
156	product.product	\N	\N	\N	10	2020-09-02 07:24:32.977941	0	\N	154	product	product_tree	\N	\N
157	product.product	tree	\N	\N	20	2020-09-02 07:24:32.977941	0	\N	\N	product	product_tree_simple	\N	\N
158	product.product	\N	\N	\N	10	2020-09-02 07:24:32.977941	0	\N	155	product	product_form	\N	\N
159	product.product	form	\N	\N	20	2020-09-02 07:24:32.977941	0	\N	\N	product	product_form_simple	\N	\N
160	product.identifier	form	\N	\N	16	2020-09-02 07:24:32.977941	0	\N	\N	product	identifier_form	\N	\N
161	product.identifier	tree	\N	\N	10	2020-09-02 07:24:32.977941	0	\N	\N	product	identifier_list	\N	\N
162	product.identifier	tree	\N	\N	20	2020-09-02 07:24:32.977941	0	\N	\N	product	identifier_list_sequence	\N	\N
163	product.category	tree	\N	\N	10	2020-09-02 07:24:32.977941	0	\N	\N	product	category_list	\N	\N
164	product.category	tree	\N	childs	20	2020-09-02 07:24:32.977941	0	\N	\N	product	category_tree	\N	\N
165	product.category	form	\N	\N	16	2020-09-02 07:24:32.977941	0	\N	\N	product	category_form	\N	\N
166	product.uom	tree	\N	\N	16	2020-09-02 07:24:32.977941	0	\N	\N	product	uom_tree	\N	\N
167	product.uom	form	\N	\N	16	2020-09-02 07:24:32.977941	0	\N	\N	product	uom_form	\N	\N
168	product.uom.category	tree	\N	\N	16	2020-09-02 07:24:32.977941	0	\N	\N	product	uom_category_tree	\N	\N
169	product.uom.category	form	\N	\N	16	2020-09-02 07:24:32.977941	0	\N	\N	product	uom_category_form	\N	\N
170	product.configuration	form	\N	\N	16	2020-09-02 07:24:32.977941	0	\N	\N	product	configuration_form	\N	\N
171	taller.marca	form	\N	\N	16	2020-09-02 07:56:32.835869	0	\N	\N	taller	marca_form	\N	\N
172	taller.marca	tree	\N	\N	16	2020-09-02 07:56:32.835869	0	\N	\N	taller	marca_list	\N	\N
173	taller.modelo	form	\N	\N	16	2020-09-02 08:45:11.63	0	\N	\N	taller	modelo_form	\N	\N
174	taller.modelo	tree	\N	\N	16	2020-09-02 08:45:11.63	0	\N	\N	taller	modelo_list	\N	\N
175	taller.coche	form	\N	\N	16	2020-09-02 11:14:57.919973	0	\N	\N	taller	coche_form	\N	\N
176	taller.coche	tree	\N	\N	16	2020-09-02 11:14:57.919973	0	\N	\N	taller	coche_list	\N	\N
182	party.party	\N	\N	\N	16	2020-09-07 13:29:55.542439	0	\N	122	taller	PartyCoches_Form	\N	\N
183	product.template	\N	\N	\N	16	2020-09-07 14:00:06.849225	0	\N	155	taller	productos_models_view_list	\N	\N
186	taller.coche.baja.start	form	\N	\N	16	2020-09-09 08:30:29.70123	0	\N	\N	taller	coche_baja_start	\N	\N
187	taller.coche.baja.result	form	\N	\N	16	2020-09-09 08:30:29.70123	0	\N	\N	taller	coche_baja_result	\N	\N
188	web.user	form	\N	\N	16	2020-09-16 08:36:14.823954	0	\N	\N	web_user	user_form	\N	\N
189	web.user	tree	\N	\N	16	2020-09-16 08:36:14.823954	0	\N	\N	web_user	user_list	\N	\N
\.


--
-- Data for Name: ir_ui_view_search; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_ui_view_search (id, create_date, create_uid, domain, model, name, "user", write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: ir_ui_view_tree_state; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_ui_view_tree_state (id, child_name, create_date, create_uid, domain, model, nodes, selected_nodes, "user", write_date, write_uid) FROM stdin;
179	childs	2020-09-29 10:26:34.506917	1	[["parent","=",null]]	ir.ui.menu	[[51],[65],[65,67],[61],[61,62],[74],[1],[1,15],[1,15,16],[1,39],[1,45]]	[]	1	\N	\N
\.


--
-- Data for Name: ir_ui_view_tree_width; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.ir_ui_view_tree_width (id, create_date, create_uid, field, model, "user", width, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: modelo-producto; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."modelo-producto" (id, create_date, create_uid, modelo, producto, write_date, write_uid) FROM stdin;
1	2020-09-08 07:40:25.358536	1	1	2	\N	\N
2	2020-09-08 07:40:25.358536	1	1	1	\N	\N
3	2020-09-08 07:40:46.383543	1	3	2	\N	\N
4	2020-09-08 14:02:57.667366	1	3	1	\N	\N
5	2020-09-08 14:04:25.735747	1	4	2	\N	\N
6	2020-09-08 14:04:25.735747	1	5	2	\N	\N
7	2020-09-08 14:04:25.735747	1	6	2	\N	\N
\.


--
-- Data for Name: party_address; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_address (id, active, city, country, create_date, create_uid, name, party, party_name, sequence, street, subdivision, write_date, write_uid, zip) FROM stdin;
1	t		\N	2020-09-03 07:25:21.235575	1		1		\N		\N	\N	\N	
2	t		\N	2020-09-07 08:16:01.62006	1		2		\N		\N	\N	\N	
3	t		\N	2020-09-07 08:23:07.490849	1		3		\N		\N	\N	\N	
4	t		\N	2020-09-10 09:02:23.019278	1		4		\N		\N	\N	\N	
\.


--
-- Data for Name: party_address_format; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_address_format (id, active, country_code, create_date, create_uid, format_, language_code, write_date, write_uid) FROM stdin;
1	t	AR	2020-09-02 07:24:24.586225	0	${party_name}\n${name}\n${street}\n${subdivision}\n${ZIP}, ${city}\n${COUNTRY}	\N	\N	\N
2	t	AU	2020-09-02 07:24:24.586225	0	${party_name}\n${attn}\n${name}\n${street}\n${subdivision}\n${CITY} ${SUBDIVISION} ${ZIP}\n${COUNTRY}	\N	\N	\N
3	t	AT	2020-09-02 07:24:24.586225	0	${party_name}\n${attn}\n${name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
4	t	BD	2020-09-02 07:24:24.586225	0	${party_name}\n${attn}\n${name}\n${street}\n${city}-${zip}\n${COUNTRY}	\N	\N	\N
5	t	BY	2020-09-02 07:24:24.586225	0	${party_name}\n${name}\n${street}\n${zip}, ${city}\n${subdivision}\n${COUNTRY}	\N	\N	\N
6	t	BE	2020-09-02 07:24:24.586225	0	${attn}\n${party_name}\n${name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
7	t	BR	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${name}\n${city} - ${subdivision_code}\n${zip}\n${COUNTRY}	\N	\N	\N
8	t	BG	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${name}\n${zip} ${city}\n${subdivision}\n${COUNTRY}	\N	\N	\N
9	t	CA	2020-09-02 07:24:24.586225	0	${attn}\n${party_name}\n${name}\n${street}\n${city} (${subdivision}) ${zip}\n${COUNTRY}	fr	\N	\N
10	t	CA	2020-09-02 07:24:24.586225	0	${ATTN}\n${PARTY_NAME}\n${NAME}\n${STREET}\n${CITY} ${SUBDIVISION_CODE} ${ZIP}\n${COUNTRY}	\N	\N	\N
11	t	CL	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${name}\n${zip}\n${city}\n${COUNTRY}	\N	\N	\N
12	t	CN	2020-09-02 07:24:24.586225	0	${COUNTRY} ${ZIP}\n${subdivision}${city}${street}${name}\n${party_name}	zh	\N	\N
13	t	CN	2020-09-02 07:24:24.586225	0	${COUNTRY} ${ZIP}\n${subdivision}, ${city}, ${street}, ${name}\n${party_name}	\N	\N	\N
14	t	HR	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${COUNTRY_CODE}-${ZIP} ${city}\n${COUNTRY}	\N	\N	\N
15	t	CZ	2020-09-02 07:24:24.586225	0	${party_name}\n${attn}\n${street}\n${COUNTRY_CODE}-${ZIP} ${city}\n${COUNTRY}	\N	\N	\N
16	t	DK	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
17	t	EE	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
18	t	FI	2020-09-02 07:24:24.586225	0	${attn}\n${party_name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
19	t	FR	2020-09-02 07:24:24.586225	0	${party_name}\n${attn}\n${name}\n${street}\n${ZIP} ${CITY}\n${COUNTRY}	\N	\N	\N
20	t	DE	2020-09-02 07:24:24.586225	0	${party_name}\n${attn}\n${name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
21	t	GR	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${COUNTRY_CODE}-${ZIP} ${CITY}\n${COUNTRY}	\N	\N	\N
22	t	HK	2020-09-02 07:24:24.586225	0	${party_name}\n${name}\n${street}\n${subdivision}\n${COUNTRY}	\N	\N	\N
23	t	HU	2020-09-02 07:24:24.586225	0	${party_name}\n${city}\n${street}\n${zip}\n${COUNTRY}	\N	\N	\N
24	t	IS	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${name}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
25	t	IN	2020-09-02 07:24:24.586225	0	${party_name}\n${name}\n${street}\n${CITY} ${zip}\n${subdivision}\n${COUNTRY}	\N	\N	\N
26	t	ID	2020-09-02 07:24:24.586225	0	${party_name}\n${name}\n${street}\n${city} ${zip}\n${subdivision}\n${COUNTRY}	\N	\N	\N
27	t	IR	2020-09-02 07:24:24.586225	0	${party_name}\n${name}\n${city}\n${street}\n${subdivision}\n${zip}\n${COUNTRY}	\N	\N	\N
28	t	IQ	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${name}\n${subdivision}\n${zip}\n${COUNTRY}	\N	\N	\N
29	t	IE	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city} ${zip}\n${COUNTRY}	\N	\N	\N
30	t	IL	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city} ${zip}\n${COUNTRY}	\N	\N	\N
31	t	IT	2020-09-02 07:24:24.586225	0	${party_name}\n${attn}\n${name}\n${street}\n${zip} ${city} ${SUBDIVISION_CODE}\n${COUNTRY}	\N	\N	\N
32	t	JP	2020-09-02 07:24:24.586225	0	${COUNTRY}\n${zip}\n${subdivision}${city}${street}\n${party_name}	jp	\N	\N
33	t	JP	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city}, ${SUBDIVISION} ${zip}\n${COUNTRY}	\N	\N	\N
34	t	KR	2020-09-02 07:24:24.586225	0	${COUNTRY}\n${street}\n${party_name}\n${zip}	ko	\N	\N
35	t	KR	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city}, ${subdivision} ${zip}\n${COUNTRY}	\N	\N	\N
36	t	LV	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city}\n${subdivision}\n${COUNTRY_CODE}-${ZIP}\n${COUNTRY}	\N	\N	\N
37	t	MO	2020-09-02 07:24:24.586225	0	${COUNTRY}\n${city}\n${street}\n${party_name}	zh	\N	\N
38	t	MO	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city}\n${COUNTRY}	\N	\N	\N
39	t	MY	2020-09-02 07:24:24.586225	0	${attn}\n${party_name}\n${name}\n${street}\n${zip} ${CITY}\n${SUBDIVISION}\n${COUNTRY}	\N	\N	\N
40	t	MX	2020-09-02 07:24:24.586225	0	${attn}\n${party_name}\n${street}\n${name}\n${zip}, ${city}, ${subdivision}\n${COUNTRY}	\N	\N	\N
41	t	NL	2020-09-02 07:24:24.586225	0	${party_name}\n${attn}\n${name}\n${street}\n${zip} ${CITY}\n${COUNTRY}	\N	\N	\N
42	t	NZ	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city} ${zip}\n${COUNTRY}	\N	\N	\N
43	t	NO	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${zip} ${CITY}\n${COUNTRY}	\N	\N	\N
44	t	OM	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city}\n${COUNTRY}	\N	\N	\N
45	t	PK	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city}\n${zip}\n${subdivision}\n${COUNTRY}	\N	\N	\N
46	t	PE	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${name}\n${city}\n${subdivision}\n${COUNTRY}	\N	\N	\N
47	t	PH	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${zip} ${CITY}\n${COUNTRY}	\N	\N	\N
48	t	PL	2020-09-02 07:24:24.586225	0	${attn} ${party_name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
49	t	PT	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
50	t	QA	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city}\n${COUNTRY}	\N	\N	\N
51	t	RO	2020-09-02 07:24:24.586225	0	${attn} ${party_name}\n${street}\n${city}\n${zip}\n${COUNTRY}	\N	\N	\N
52	t	RU	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city}\n${subdivision}\n${COUNTRY}\n${zip}	\N	\N	\N
53	t	SA	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city} ${zip}\n${COUNTRY}	\N	\N	\N
54	t	RS	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
55	t	SG	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${name}\n${CITY} ${ZIP}\n${COUNTRY}	\N	\N	\N
56	t	SK	2020-09-02 07:24:24.586225	0	${attn}\n${party_name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
57	t	SL	2020-09-02 07:24:24.586225	0	${party_name}\n${attn}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
58	t	ES	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${zip} ${city}\n${subdivision}\n${COUNTRY}	\N	\N	\N
59	t	LK	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${CITY}\n${zip}\n${COUNTRY}	\N	\N	\N
60	t	SE	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
61	t	CH	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${zip} ${city}\n${COUNTRY}	\N	\N	\N
62	t	TW	2020-09-02 07:24:24.586225	0	${COUNTRY}\n${zip}\n${street}\n${party_name}	zh	\N	\N
63	t	TW	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city}, ${subdivision} ${zip}\n${COUNTRY}	\N	\N	\N
64	t	TH	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${name}\n${subdivision}\n${COUNTRY}\n${zip}	\N	\N	\N
65	t	TR	2020-09-02 07:24:24.586225	0	${party_name}\n${attn}\n${street}\n${name}\n${zip} ${city} ${subdivision}\n${COUNTRY}	\N	\N	\N
66	t	UA	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city}\n${subdivision}\n${zip}\n${COUNTRY}	\N	\N	\N
67	t	GB	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${CITY}\n${zip}\n${COUNTRY}	\N	\N	\N
68	t	US	2020-09-02 07:24:24.586225	0	${attn}\n${party_name}\n${street}\n${city}, ${subdivision_code} ${zip}\n${COUNTRY}	\N	\N	\N
69	t	VN	2020-09-02 07:24:24.586225	0	${party_name}\n${street}\n${city}\n${subdivision}\n${COUNTRY}	\N	\N	\N
\.


--
-- Data for Name: party_address_subdivision_type; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_address_subdivision_type (id, active, country_code, create_date, create_uid, types, write_date, write_uid) FROM stdin;
1	t	ID	2020-09-02 07:24:24.586225	0	["autonomous province","province","special district","special region"]	\N	\N
2	t	IT	2020-09-02 07:24:24.586225	0	["province"]	\N	\N
3	t	ES	2020-09-02 07:24:24.586225	0	["province"]	\N	\N
\.


--
-- Data for Name: party_category; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_category (id, active, create_date, create_uid, name, parent, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: party_category_rel; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_category_rel (id, category, create_date, create_uid, party, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: party_configuration; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_configuration (id, create_date, create_uid, write_date, write_uid) FROM stdin;
1	2020-09-08 08:42:32.74241	1	\N	\N
\.


--
-- Data for Name: party_configuration_party_lang; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_configuration_party_lang (id, create_date, create_uid, party_lang, write_date, write_uid, company) FROM stdin;
1	2020-09-08 08:42:32.74241	1	\N	\N	\N	\N
\.


--
-- Data for Name: party_configuration_party_sequence; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_configuration_party_sequence (id, create_date, create_uid, party_sequence, write_date, write_uid) FROM stdin;
1	2020-09-02 07:24:24.586225	0	1	2020-09-08 08:42:32.74241	1
\.


--
-- Data for Name: party_contact_mechanism; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_contact_mechanism (id, active, comment, create_date, create_uid, name, party, sequence, type, value, value_compact, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: party_identifier; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_identifier (id, code, create_date, create_uid, party, sequence, type, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: party_party; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_party (id, active, code, create_date, create_uid, name, replaced_by, write_date, write_uid) FROM stdin;
1	t	1	2020-09-03 07:25:21.235575	1	iñigo	\N	\N	\N
2	t	2	2020-09-07 08:16:01.62006	1	Ivan	\N	\N	\N
4	t	4	2020-09-10 09:02:23.019278	1	gesalaga	\N	\N	\N
3	t	3	2020-09-07 08:23:07.490849	1	Sergi	\N	2020-09-17 07:11:58.36689	1
5	t	5	2020-09-17 07:23:37.467229	0	Aingeru	\N	\N	\N
\.


--
-- Data for Name: party_party_lang; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.party_party_lang (id, create_date, create_uid, lang, party, write_date, write_uid, company) FROM stdin;
1	2020-09-03 07:25:21.235575	1	\N	1	\N	\N	\N
2	2020-09-07 08:16:01.62006	1	\N	2	\N	\N	\N
3	2020-09-07 08:23:07.490849	1	\N	3	\N	\N	\N
4	2020-09-10 09:02:23.019278	1	\N	4	\N	\N	\N
5	2020-09-17 07:23:37.467229	0	\N	5	\N	\N	\N
\.


--
-- Data for Name: product_category; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.product_category (id, create_date, create_uid, name, parent, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: product_configuration; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.product_configuration (id, create_date, create_uid, product_sequence, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: product_configuration_default_cost_price_method; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.product_configuration_default_cost_price_method (id, create_date, create_uid, default_cost_price_method, write_date, write_uid) FROM stdin;
1	2020-09-02 07:24:32.977941	0	fixed	\N	\N
\.


--
-- Data for Name: product_cost_price; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.product_cost_price (id, company, cost_price, create_date, create_uid, product, write_date, write_uid) FROM stdin;
1	\N	0	2020-09-07 09:12:25.291892	1	1	\N	\N
2	\N	0	2020-09-07 13:43:27.782244	1	2	\N	\N
3	\N	0	2020-09-08 07:44:04.847033	1	3	\N	\N
\.


--
-- Data for Name: product_cost_price_method; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.product_cost_price_method (id, company, cost_price_method, create_date, create_uid, template, write_date, write_uid) FROM stdin;
1	\N	fixed	2020-09-07 09:12:25.291892	1	1	\N	\N
2	\N	fixed	2020-09-07 13:43:27.782244	1	2	\N	\N
3	\N	fixed	2020-09-08 07:44:04.847033	1	3	\N	\N
\.


--
-- Data for Name: product_identifier; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.product_identifier (id, code, create_date, create_uid, product, sequence, type, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: product_list_price; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.product_list_price (id, company, create_date, create_uid, list_price, template, write_date, write_uid) FROM stdin;
1	\N	2020-09-07 09:12:25.291892	1	200	1	\N	\N
2	\N	2020-09-07 13:43:27.782244	1	350	2	\N	\N
3	\N	2020-09-08 07:44:04.847033	1	160	3	\N	\N
\.


--
-- Data for Name: product_product; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.product_product (id, active, code, create_date, create_uid, description, suffix_code, template, write_date, write_uid) FROM stdin;
1	t	\N	2020-09-07 09:12:25.291892	1		\N	1	\N	\N
2	t	\N	2020-09-07 13:43:27.782244	1		\N	2	\N	\N
3	t	\N	2020-09-08 07:44:04.847033	1		\N	3	\N	\N
\.


--
-- Data for Name: product_template; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.product_template (id, active, code, consumable, create_date, create_uid, default_uom, name, type, write_date, write_uid) FROM stdin;
3	t		f	2020-09-08 07:44:04.847033	1	1	cambio de aceite	service	\N	\N
1	t		f	2020-09-07 09:12:25.291892	1	1	tubo de escape	goods	2020-09-08 14:02:57.667366	1
2	t		f	2020-09-07 13:43:27.782244	1	1	ruedas	goods	2020-09-08 14:04:25.735747	1
\.


--
-- Data for Name: product_template-product_category; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."product_template-product_category" (id, category, create_date, create_uid, template, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: product_uom; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.product_uom (id, active, category, create_date, create_uid, digits, factor, name, rate, rounding, symbol, write_date, write_uid) FROM stdin;
1	t	1	2020-09-02 07:24:32.977941	0	0	1	Unit	1	1	u	\N	\N
2	t	2	2020-09-02 07:24:32.977941	0	2	1	Kilogram	1	0.01	kg	\N	\N
3	t	2	2020-09-02 07:24:32.977941	0	2	0.001	Gram	1000	0.01	g	\N	\N
4	t	2	2020-09-02 07:24:32.977941	0	2	0.0002	Carat	5000	0.01	c	\N	\N
5	t	2	2020-09-02 07:24:32.977941	0	2	0.45359237	Pound	2.204622621849	0.01	lb	\N	\N
6	t	2	2020-09-02 07:24:32.977941	0	2	0.028349523125	Ounce	35.27396194958	0.01	oz	\N	\N
7	t	3	2020-09-02 07:24:32.977941	0	2	0.000277777778	Second	3600	0.01	s	\N	\N
8	t	3	2020-09-02 07:24:32.977941	0	2	0.016666666667	Minute	60	0.01	min	\N	\N
9	t	3	2020-09-02 07:24:32.977941	0	2	1	Hour	1	0.01	h	\N	\N
10	t	3	2020-09-02 07:24:32.977941	0	2	8	Work Day	0.125	0.01	wd	\N	\N
11	t	3	2020-09-02 07:24:32.977941	0	2	24	Day	0.041666666667	0.01	d	\N	\N
12	t	4	2020-09-02 07:24:32.977941	0	2	1	Meter	1	0.01	m	\N	\N
13	t	4	2020-09-02 07:24:32.977941	0	2	1000	Kilometer	0.001	0.01	km	\N	\N
14	t	4	2020-09-02 07:24:32.977941	0	2	0.01	centimeter	100	0.01	cm	\N	\N
15	t	4	2020-09-02 07:24:32.977941	0	2	0.001	Millimeter	1000	0.01	mm	\N	\N
16	t	4	2020-09-02 07:24:32.977941	0	2	0.3048	Foot	3.280839895013	0.01	ft	\N	\N
17	t	4	2020-09-02 07:24:32.977941	0	2	0.9144	Yard	1.093613298338	0.01	yd	\N	\N
18	t	4	2020-09-02 07:24:32.977941	0	2	0.0254	Inch	39.370078740157	0.01	in	\N	\N
19	t	4	2020-09-02 07:24:32.977941	0	2	1609.344	Mile	0.000621371192	0.01	mi	\N	\N
20	t	5	2020-09-02 07:24:32.977941	0	2	1000	Cubic meter	0.001	0.01	m³	\N	\N
21	t	5	2020-09-02 07:24:32.977941	0	2	1	Liter	1	0.01	l	\N	\N
22	t	5	2020-09-02 07:24:32.977941	0	2	0.001	Cubic centimeter	1000	0.01	cm³	\N	\N
23	t	5	2020-09-02 07:24:32.977941	0	2	0.016387064	Cubic inch	61.023744094732	0.01	in³	\N	\N
24	t	5	2020-09-02 07:24:32.977941	0	2	28.316846592	Cubic foot	0.035314666721	0.01	ft³	\N	\N
25	t	5	2020-09-02 07:24:32.977941	0	2	3.785411784	Gallon	0.264172052358	0.01	gal	\N	\N
26	t	6	2020-09-02 07:24:32.977941	0	2	1	Square meter	1	0.01	m²	\N	\N
27	t	6	2020-09-02 07:24:32.977941	0	2	0.0001	Square centimeter	10000	0.01	cm²	\N	\N
28	t	6	2020-09-02 07:24:32.977941	0	2	100	Are	0.01	0.01	a	\N	\N
29	t	6	2020-09-02 07:24:32.977941	0	2	10000	Hectare	0.0001	0.01	ha	\N	\N
30	t	6	2020-09-02 07:24:32.977941	0	2	0.00064516	Square inch	1550.0031000062	0.01	in²	\N	\N
31	t	6	2020-09-02 07:24:32.977941	0	2	0.09290304	Square foot	10.76391041671	0.01	ft²	\N	\N
32	t	6	2020-09-02 07:24:32.977941	0	2	0.83612736	Square yard	1.195990046301	0.01	yd²	\N	\N
\.


--
-- Data for Name: product_uom_category; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.product_uom_category (id, create_date, create_uid, name, write_date, write_uid) FROM stdin;
1	2020-09-02 07:24:32.977941	0	Units	\N	\N
2	2020-09-02 07:24:32.977941	0	Weight	\N	\N
3	2020-09-02 07:24:32.977941	0	Time	\N	\N
4	2020-09-02 07:24:32.977941	0	Length	\N	\N
5	2020-09-02 07:24:32.977941	0	Volume	\N	\N
6	2020-09-02 07:24:32.977941	0	Surface	\N	\N
\.


--
-- Data for Name: res_group; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.res_group (id, name, active, create_date, create_uid, write_date, write_uid) FROM stdin;
1	Administration	t	2020-09-01 08:50:28.180942	0	\N	\N
2	Currency Administration	t	2020-09-02 07:24:23.639802	0	\N	\N
3	Party Administration	t	2020-09-02 07:24:24.586225	0	\N	\N
4	Company Administration	t	2020-09-02 07:24:29.514826	0	\N	\N
5	Employee Administration	t	2020-09-02 07:24:29.514826	0	\N	\N
6	Product Administration	t	2020-09-02 07:24:32.977941	0	\N	\N
7	Taller Administration	t	2020-09-02 07:40:09.139967	0	\N	\N
8	Taller 	t	2020-09-02 07:40:09.139967	0	\N	\N
\.


--
-- Data for Name: res_user; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.res_user (id, name, active, login, password, create_date, create_uid, email, language, menu, password_hash, password_reset, password_reset_expire, signature, write_date, write_uid, company, employee, main_company) FROM stdin;
0	Root	f	root	\N	2020-09-01 08:50:29.004597	0	\N	\N	2	\N	\N	\N	\N	\N	\N	\N	\N	\N
1	Administrator	t	admin	\N	2020-09-01 08:50:28.180942	0	inigo.larrranaga@gesalaga.net	\N	2	$pbkdf2-sha512$25000$AGCMMQZgzNkb4/x/LyWkdA$vsb5fYVDsUk2jHe8pmnVD9GwVxQxbIOJWnJxlRc0XQyIqoaqSB9rLVXiPyqv2WgNqt.6TsDh4yJc.DDe76wtUg	\N	\N	Administrator	2020-09-29 10:22:48.015388	0	1	\N	1
\.


--
-- Data for Name: res_user-company_employee; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."res_user-company_employee" (id, create_date, create_uid, employee, "user", write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: res_user-ir_action; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."res_user-ir_action" (id, action, create_date, create_uid, "user", write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: res_user-res_group; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public."res_user-res_group" (id, "user", "group", create_date, create_uid, write_date, write_uid) FROM stdin;
1	1	1	2020-09-01 08:50:28.180942	0	\N	\N
2	1	2	2020-09-02 07:24:23.639802	0	\N	\N
3	1	3	2020-09-02 07:24:24.586225	0	\N	\N
4	1	4	2020-09-02 07:24:29.514826	0	\N	\N
5	1	5	2020-09-02 07:24:29.514826	0	\N	\N
6	1	6	2020-09-02 07:24:32.977941	0	\N	\N
7	1	7	2020-09-02 07:40:09.139967	0	\N	\N
8	1	8	2020-09-02 07:40:09.139967	0	\N	\N
\.


--
-- Data for Name: res_user_application; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.res_user_application (id, application, create_date, create_uid, key, state, "user", write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: res_user_login_attempt; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.res_user_login_attempt (id, create_date, create_uid, ip_address, ip_network, login, write_date, write_uid) FROM stdin;
5	2020-09-29 10:23:51.509362	0	127.0.0.1	127.0.0.1/32	Admin	\N	\N
6	2020-09-29 10:23:55.224786	0	127.0.0.1	127.0.0.1/32	Admin	\N	\N
\.


--
-- Data for Name: res_user_warning; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.res_user_warning (id, always, create_date, create_uid, name, "user", write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: taller_coche; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.taller_coche (id, create_date, create_uid, fecha_baja, fecha_matriculacion, marca, matricula, modelo, precio, propietario, write_date, write_uid) FROM stdin;
5	2020-09-04 07:44:30.807542	1	\N	\N	1	1234r	6	\N	1	2020-09-09 07:40:05.953966	1
4	2020-09-03 08:55:01.693219	1	2020-09-14	\N	3	123r	1	\N	1	2020-09-14 06:28:28.580164	1
2	2020-09-03 07:25:28.056767	1	2020-09-14	2020-09-01	3	3444jk	1	1	2	2020-09-14 06:33:48.505276	1
3	2020-09-03 08:46:58.626954	1	2020-09-14	2020-09-08	2	3232tt	3	2	1	2020-09-14 06:33:48.505276	1
15	2020-09-15 08:42:33.275108	0	\N	\N	3	1234rrer	1	\N	4	\N	\N
18	2020-09-15 08:49:08.259502	0	\N	\N	3	1234rrer2	1	\N	4	\N	\N
19	2020-09-15 08:50:41.270211	0	\N	\N	3	1234rrer23	1	\N	4	\N	\N
20	2020-09-15 10:09:34.494676	0	\N	\N	1	222re3	6	\N	3	\N	\N
21	2020-09-16 07:35:32.783661	0	\N	\N	7	222re3rr	10	\N	4	\N	\N
22	2020-09-16 07:44:50.383039	0	\N	\N	9	zcccc	11	\N	4	\N	\N
23	2020-09-16 07:53:58.688906	0	\N	\N	7	ddddd	12	\N	4	\N	\N
25	2020-09-17 07:03:19.440779	0	\N	\N	3	aaar333	13	\N	2	\N	\N
26	2020-09-17 07:07:23.235572	0	\N	\N	3	rrtr5	13	\N	1	\N	\N
27	2020-09-17 07:38:37.435249	0	\N	\N	4	2345tt	4	\N	5	\N	\N
28	2020-09-17 07:38:53.011485	0	\N	\N	4	3445y	4	\N	2	\N	\N
29	2020-09-17 07:39:31.430188	0	\N	\N	2	7878i	3	\N	5	\N	\N
\.


--
-- Data for Name: taller_marca; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.taller_marca (id, create_date, create_uid, name, write_date, write_uid) FROM stdin;
1	2020-09-02 08:05:04.984702	1	subaru	\N	\N
3	2020-09-02 08:05:22.503979	1	bmw	\N	\N
4	2020-09-03 08:07:30.706092	1	ford	\N	\N
2	2020-09-02 08:05:17.242776	1	mercedes	2020-09-07 06:38:00.316767	1
5	2020-09-15 11:42:18.027969	0	toyota	\N	\N
6	2020-09-15 13:12:53.8477	0	hyundai	\N	\N
7	2020-09-15 13:14:44.186304	0	opel	\N	\N
8	2020-09-16 06:23:47.31403	0	fiat	\N	\N
9	2020-09-16 07:44:37.07863	0	zcxcc	\N	\N
10	2020-09-22 08:26:07.601201	1	Honda	\N	\N
12	2020-09-22 09:03:45.322393	1	Infiniti	\N	\N
\.


--
-- Data for Name: taller_modelo; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.taller_modelo (id, caballos, combustible, create_date, create_uid, fecha_lanz, marca, modelo, write_date, write_uid, "precioMod", "productosDelModelo") FROM stdin;
4	\N	\N	2020-09-03 08:07:58.472014	1	2020-09-03	4	tourneo	\N	\N	\N	\N
5	190	g	2020-09-03 12:28:31.391635	1	2020-08-31	3	e36 525	\N	\N	\N	\N
1	300	g	2020-09-02 08:46:15.856562	1	\N	3	335i	2020-09-08 07:40:25.358536	1	2000	\N
3	\N	d	2020-09-02 08:49:19.868252	1	2020-09-08	2	CLS 220	2020-09-08 07:40:46.383543	1	17000	\N
6	300	\N	2020-09-08 09:01:13.586758	1	2020-09-08	1	impreza	\N	\N	\N	\N
7	\N	g	2020-09-14 10:16:27.180485	1	2020-09-14	1	legacy	\N	\N	55000	\N
10	\N	\N	2020-09-16 07:35:19.214623	0	2020-09-16	7	vectra	\N	\N	\N	\N
11	\N	\N	2020-09-16 07:44:44.450475	0	2020-09-16	9	zxcccc	\N	\N	\N	\N
12	\N	\N	2020-09-16 07:53:52.48973	0	2020-09-16	7	astra	\N	\N	\N	\N
13	\N	\N	2020-09-17 06:54:58.929884	0	2020-09-17	3	323 i	\N	\N	\N	\N
14	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Accord	\N	\N	\N	\N
15	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Airwave	\N	\N	\N	\N
16	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Ascot	\N	\N	\N	\N
17	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Avancier	\N	\N	\N	\N
18	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Beat	\N	\N	\N	\N
19	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Capa	\N	\N	\N	\N
20	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	City	\N	\N	\N	\N
21	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Civic	\N	\N	\N	\N
22	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Concerto	\N	\N	\N	\N
23	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	CR-V	\N	\N	\N	\N
24	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	CR-X	\N	\N	\N	\N
25	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	CR-Z	\N	\N	\N	\N
26	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Crossroad	\N	\N	\N	\N
27	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Crosstour	\N	\N	\N	\N
28	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Domani	\N	\N	\N	\N
29	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Edix	\N	\N	\N	\N
30	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Element	\N	\N	\N	\N
31	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Elysion	\N	\N	\N	\N
32	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	FCX Clarity	\N	\N	\N	\N
33	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Fit Aria	\N	\N	\N	\N
34	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Fit	\N	\N	\N	\N
35	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	FR-V	\N	\N	\N	\N
36	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Freed	\N	\N	\N	\N
37	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	HR-V	\N	\N	\N	\N
38	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Insight	\N	\N	\N	\N
39	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Inspire	\N	\N	\N	\N
40	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Integra	\N	\N	\N	\N
41	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Integra SJ	\N	\N	\N	\N
42	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Jazz	\N	\N	\N	\N
43	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Legend	\N	\N	\N	\N
44	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Life	\N	\N	\N	\N
45	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Logo	\N	\N	\N	\N
46	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	MDX	\N	\N	\N	\N
47	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Mobilio	\N	\N	\N	\N
48	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	NSX	\N	\N	\N	\N
49	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Odyssey	\N	\N	\N	\N
50	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Orthia	\N	\N	\N	\N
51	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Partner	\N	\N	\N	\N
52	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Passport	\N	\N	\N	\N
53	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Pilot	\N	\N	\N	\N
54	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Prelude	\N	\N	\N	\N
55	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Quint	\N	\N	\N	\N
56	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Rafaga	\N	\N	\N	\N
57	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Ridgeline	\N	\N	\N	\N
58	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	S-MX	\N	\N	\N	\N
59	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	S2000	\N	\N	\N	\N
60	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Saber	\N	\N	\N	\N
61	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Shuttle	\N	\N	\N	\N
62	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Stepwgn	\N	\N	\N	\N
63	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Stream	\N	\N	\N	\N
64	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	That's	\N	\N	\N	\N
65	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Today	\N	\N	\N	\N
66	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Torneo	\N	\N	\N	\N
67	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Vamos	\N	\N	\N	\N
68	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Vigor	\N	\N	\N	\N
69	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Z	\N	\N	\N	\N
70	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Zest	\N	\N	\N	\N
71	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	EX-Series	\N	\N	\N	\N
72	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	FX-Series	\N	\N	\N	\N
73	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	G-Series	\N	\N	\N	\N
74	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	J30	\N	\N	\N	\N
75	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	JX-Series	\N	\N	\N	\N
76	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	M-Series	\N	\N	\N	\N
77	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	Q45	\N	\N	\N	\N
78	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	Q50	\N	\N	\N	\N
79	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	Q60	\N	\N	\N	\N
80	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	QX-Series	\N	\N	\N	\N
81	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	QX50	\N	\N	\N	\N
82	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	QX60	\N	\N	\N	\N
83	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	QX70	\N	\N	\N	\N
84	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	QX80	\N	\N	\N	\N
85	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	Q70	\N	\N	\N	\N
86	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	Q40	\N	\N	\N	\N
87	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	Q30	\N	\N	\N	\N
88	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Acty	\N	\N	\N	\N
89	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	QX30	\N	\N	\N	\N
90	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Ascot Innova	\N	\N	\N	\N
91	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	1300	\N	\N	\N	\N
92	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	145	\N	\N	\N	\N
93	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Amaze	\N	\N	\N	\N
94	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Ballade	\N	\N	\N	\N
95	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Brio	\N	\N	\N	\N
96	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Ciimo	\N	\N	\N	\N
97	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Crider	\N	\N	\N	\N
98	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Fit Shuttle	\N	\N	\N	\N
99	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Lagreat	\N	\N	\N	\N
100	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Mobilio Spike	\N	\N	\N	\N
101	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	MR-V	\N	\N	\N	\N
102	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	N360	\N	\N	\N	\N
103	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	N600	\N	\N	\N	\N
104	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	N Box	\N	\N	\N	\N
105	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	N-One	\N	\N	\N	\N
106	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Quintet	\N	\N	\N	\N
107	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	S500	\N	\N	\N	\N
108	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	S600	\N	\N	\N	\N
109	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	S800	\N	\N	\N	\N
110	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Spirior	\N	\N	\N	\N
111	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	12	I-Series	\N	\N	\N	\N
112	\N	\N	2020-09-22 09:08:48.919686	1	2020-09-22	10	Jade	\N	\N	\N	\N
\.


--
-- Data for Name: web_user; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.web_user (id, active, create_date, create_uid, email, email_token, email_valid, party, password_hash, reset_password_token, reset_password_token_expire, write_date, write_uid) FROM stdin;
1	t	2020-09-16 08:40:01.142214	1	inigo.larranaga@gesalaga.net	\N	f	1	$pbkdf2-sha512$25000$7b0XwjjHGCNkzNlbq/Ue4w$9Two1LDj8XJKGrbBO5/xMtbmTNfdN4fRGNUUdxla25d9HsV7iMGXHUiMzQHkF1qrI/Oms2qadlrbJMKh7G0WeA	\N	\N	2020-09-16 08:40:01.142214	1
2	t	2020-09-17 07:23:37.467229	0	aingeru@gmail.com	\N	f	5	$pbkdf2-sha512$25000$AuCc8977HyMEgHBuzTknZA$NsewySQveO1hHqvU1pQIfEipLhToKmIn0vczIhA9n/psz6uH6QChmZJt3GwdAvDHHH9ifRQ3Lxp2LYG4zCFm3w	\N	\N	2020-09-17 07:23:37.467229	0
\.


--
-- Data for Name: web_user_authenticate_attempt; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.web_user_authenticate_attempt (id, create_date, create_uid, ip_address, ip_network, login, write_date, write_uid) FROM stdin;
\.


--
-- Data for Name: web_user_session; Type: TABLE DATA; Schema: public; Owner: tryton
--

COPY public.web_user_session (id, create_date, create_uid, key, "user", write_date, write_uid) FROM stdin;
2	2020-09-16 08:44:14.537969	0	60ee52cc5b89c131b274af66ce2cc46d0c7ade617396ef54824d28f736d59c1c	1	\N	\N
6	2020-09-16 12:26:43.666485	0	b7ccaff07a9b46a09c13884d3dd96cde0da55084a795620cd19007945f1cdbcb	1	\N	\N
7	2020-09-16 12:29:49.055767	0	4c85ae3c5cd6effe0f2697b5a28d66ada3bcd7b5167be9c95cf6cd8c3954172a	1	\N	\N
20	2020-09-17 07:39:15.717939	0	5ea4e18370d449f761c1abcfcfdeb9e72db7e8fc2c039f2d53a12bef1e88c384	2	\N	\N
\.


--
-- Name: coche-producto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."coche-producto_id_seq"', 1, false);


--
-- Name: company_company_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.company_company_id_seq', 1, true);


--
-- Name: company_employee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.company_employee_id_seq', 1, false);


--
-- Name: country_country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.country_country_id_seq', 1, false);


--
-- Name: country_subdivision_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.country_subdivision_id_seq', 1, false);


--
-- Name: country_zip_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.country_zip_id_seq', 1, false);


--
-- Name: cron_company_rel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.cron_company_rel_id_seq', 1, false);


--
-- Name: currency_currency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.currency_currency_id_seq', 1, true);


--
-- Name: currency_currency_rate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.currency_currency_rate_id_seq', 1, false);


--
-- Name: ir_action-res_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."ir_action-res_group_id_seq"', 6, true);


--
-- Name: ir_action_act_window_domain_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_action_act_window_domain_id_seq', 4, true);


--
-- Name: ir_action_act_window_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_action_act_window_id_seq', 96, true);


--
-- Name: ir_action_act_window_view_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_action_act_window_view_id_seq', 120, true);


--
-- Name: ir_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_action_id_seq', 98, true);


--
-- Name: ir_action_keyword_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_action_keyword_id_seq', 90, true);


--
-- Name: ir_action_report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_action_report_id_seq', 98, true);


--
-- Name: ir_action_url_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_action_url_id_seq', 1, false);


--
-- Name: ir_action_wizard_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_action_wizard_id_seq', 91, true);


--
-- Name: ir_attachment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_attachment_id_seq', 1, false);


--
-- Name: ir_cache_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_cache_id_seq', 16, true);


--
-- Name: ir_calendar_day_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_calendar_day_id_seq', 7, true);


--
-- Name: ir_calendar_month_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_calendar_month_id_seq', 12, true);


--
-- Name: ir_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_configuration_id_seq', 1, true);


--
-- Name: ir_cron_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_cron_id_seq', 1, true);


--
-- Name: ir_export-res_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."ir_export-res_group_id_seq"', 1, false);


--
-- Name: ir_export-write-res_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."ir_export-write-res_group_id_seq"', 1, false);


--
-- Name: ir_export_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_export_id_seq', 1, false);


--
-- Name: ir_export_line_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_export_line_id_seq', 1, false);


--
-- Name: ir_lang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_lang_id_seq', 23, true);


--
-- Name: ir_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_message_id_seq', 123, true);


--
-- Name: ir_model_access_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_model_access_id_seq', 97, true);


--
-- Name: ir_model_button-button_reset_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."ir_model_button-button_reset_id_seq"', 1, false);


--
-- Name: ir_model_button-res_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."ir_model_button-res_group_id_seq"', 9, true);


--
-- Name: ir_model_button_click_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_model_button_click_id_seq', 1, false);


--
-- Name: ir_model_button_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_model_button_id_seq', 16, true);


--
-- Name: ir_model_button_rule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_model_button_rule_id_seq', 1, false);


--
-- Name: ir_model_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_model_data_id_seq', 1075, true);


--
-- Name: ir_model_field_access_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_model_field_access_id_seq', 1, false);


--
-- Name: ir_model_field_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_model_field_id_seq', 1325, true);


--
-- Name: ir_model_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_model_id_seq', 129, true);


--
-- Name: ir_module_config_wizard_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_module_config_wizard_item_id_seq', 4, true);


--
-- Name: ir_module_dependency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_module_dependency_id_seq', 23, true);


--
-- Name: ir_module_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_module_id_seq', 10, true);


--
-- Name: ir_note_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_note_id_seq', 1, false);


--
-- Name: ir_note_read_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_note_read_id_seq', 1, false);


--
-- Name: ir_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_queue_id_seq', 1, false);


--
-- Name: ir_rule_group-res_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."ir_rule_group-res_group_id_seq"', 4, true);


--
-- Name: ir_rule_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_rule_group_id_seq', 25, true);


--
-- Name: ir_rule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_rule_id_seq', 37, true);


--
-- Name: ir_sequence_1; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_sequence_1', 5, true);


--
-- Name: ir_sequence_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_sequence_id_seq', 1, true);


--
-- Name: ir_sequence_strict_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_sequence_strict_id_seq', 1, false);


--
-- Name: ir_sequence_type-res_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."ir_sequence_type-res_group_id_seq"', 4, true);


--
-- Name: ir_sequence_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_sequence_type_id_seq', 2, true);


--
-- Name: ir_session_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_session_id_seq', 9, true);


--
-- Name: ir_session_wizard_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_session_wizard_id_seq', 22, true);


--
-- Name: ir_translation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_translation_id_seq', 1698, true);


--
-- Name: ir_trigger__history___id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_trigger__history___id_seq', 1, false);


--
-- Name: ir_trigger_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_trigger_id_seq', 1, false);


--
-- Name: ir_trigger_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_trigger_log_id_seq', 1, false);


--
-- Name: ir_ui_icon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_ui_icon_id_seq', 13, true);


--
-- Name: ir_ui_menu-res_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."ir_ui_menu-res_group_id_seq"', 48, true);


--
-- Name: ir_ui_menu_favorite_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_ui_menu_favorite_id_seq', 1, false);


--
-- Name: ir_ui_menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_ui_menu_id_seq', 78, true);


--
-- Name: ir_ui_view_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_ui_view_id_seq', 189, true);


--
-- Name: ir_ui_view_search_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_ui_view_search_id_seq', 1, false);


--
-- Name: ir_ui_view_tree_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_ui_view_tree_state_id_seq', 179, true);


--
-- Name: ir_ui_view_tree_width_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.ir_ui_view_tree_width_id_seq', 1, false);


--
-- Name: modelo-producto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."modelo-producto_id_seq"', 7, true);


--
-- Name: party_address_format_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_address_format_id_seq', 69, true);


--
-- Name: party_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_address_id_seq', 4, true);


--
-- Name: party_address_subdivision_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_address_subdivision_type_id_seq', 3, true);


--
-- Name: party_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_category_id_seq', 1, false);


--
-- Name: party_category_rel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_category_rel_id_seq', 1, false);


--
-- Name: party_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_configuration_id_seq', 1, true);


--
-- Name: party_configuration_party_lang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_configuration_party_lang_id_seq', 1, true);


--
-- Name: party_configuration_party_sequence_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_configuration_party_sequence_id_seq', 1, true);


--
-- Name: party_contact_mechanism_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_contact_mechanism_id_seq', 1, false);


--
-- Name: party_identifier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_identifier_id_seq', 1, false);


--
-- Name: party_party_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_party_id_seq', 5, true);


--
-- Name: party_party_lang_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.party_party_lang_id_seq', 5, true);


--
-- Name: product_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.product_category_id_seq', 1, false);


--
-- Name: product_configuration_default_cost_price_method_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.product_configuration_default_cost_price_method_id_seq', 1, true);


--
-- Name: product_configuration_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.product_configuration_id_seq', 1, false);


--
-- Name: product_cost_price_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.product_cost_price_id_seq', 3, true);


--
-- Name: product_cost_price_method_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.product_cost_price_method_id_seq', 3, true);


--
-- Name: product_identifier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.product_identifier_id_seq', 1, false);


--
-- Name: product_list_price_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.product_list_price_id_seq', 3, true);


--
-- Name: product_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.product_product_id_seq', 3, true);


--
-- Name: product_template-product_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."product_template-product_category_id_seq"', 1, false);


--
-- Name: product_template_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.product_template_id_seq', 3, true);


--
-- Name: product_uom_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.product_uom_category_id_seq', 6, true);


--
-- Name: product_uom_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.product_uom_id_seq', 32, true);


--
-- Name: res_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.res_group_id_seq', 8, true);


--
-- Name: res_user-company_employee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."res_user-company_employee_id_seq"', 1, false);


--
-- Name: res_user-ir_action_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."res_user-ir_action_id_seq"', 1, false);


--
-- Name: res_user-res_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public."res_user-res_group_id_seq"', 8, true);


--
-- Name: res_user_application_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.res_user_application_id_seq', 1, false);


--
-- Name: res_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.res_user_id_seq', 1, true);


--
-- Name: res_user_login_attempt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.res_user_login_attempt_id_seq', 6, true);


--
-- Name: res_user_warning_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.res_user_warning_id_seq', 1, false);


--
-- Name: taller_coche_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.taller_coche_id_seq', 30, true);


--
-- Name: taller_marca_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.taller_marca_id_seq', 12, true);


--
-- Name: taller_modelo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.taller_modelo_id_seq', 112, true);


--
-- Name: web_user_authenticate_attempt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.web_user_authenticate_attempt_id_seq', 1, false);


--
-- Name: web_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.web_user_id_seq', 2, true);


--
-- Name: web_user_session_id_seq; Type: SEQUENCE SET; Schema: public; Owner: tryton
--

SELECT pg_catalog.setval('public.web_user_session_id_seq', 20, true);


--
-- Name: coche-producto coche-producto_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."coche-producto"
    ADD CONSTRAINT "coche-producto_pkey" PRIMARY KEY (id);


--
-- Name: company_company company_company_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.company_company
    ADD CONSTRAINT company_company_pkey PRIMARY KEY (id);


--
-- Name: company_employee company_employee_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.company_employee
    ADD CONSTRAINT company_employee_pkey PRIMARY KEY (id);


--
-- Name: country_country country_country_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.country_country
    ADD CONSTRAINT country_country_pkey PRIMARY KEY (id);


--
-- Name: country_subdivision country_subdivision_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.country_subdivision
    ADD CONSTRAINT country_subdivision_pkey PRIMARY KEY (id);


--
-- Name: country_zip country_zip_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.country_zip
    ADD CONSTRAINT country_zip_pkey PRIMARY KEY (id);


--
-- Name: cron_company_rel cron_company_rel_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.cron_company_rel
    ADD CONSTRAINT cron_company_rel_pkey PRIMARY KEY (id);


--
-- Name: currency_currency currency_currency_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.currency_currency
    ADD CONSTRAINT currency_currency_pkey PRIMARY KEY (id);


--
-- Name: currency_currency_rate currency_currency_rate_date_currency_uniq; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.currency_currency_rate
    ADD CONSTRAINT currency_currency_rate_date_currency_uniq UNIQUE (date, currency);


--
-- Name: currency_currency_rate currency_currency_rate_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.currency_currency_rate
    ADD CONSTRAINT currency_currency_rate_pkey PRIMARY KEY (id);


--
-- Name: ir_action-res_group ir_action-res_group_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_action-res_group"
    ADD CONSTRAINT "ir_action-res_group_pkey" PRIMARY KEY (id);


--
-- Name: ir_action_act_window_domain ir_action_act_window_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_act_window_domain
    ADD CONSTRAINT ir_action_act_window_domain_pkey PRIMARY KEY (id);


--
-- Name: ir_action_act_window ir_action_act_window_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_act_window
    ADD CONSTRAINT ir_action_act_window_pkey PRIMARY KEY (id);


--
-- Name: ir_action_act_window_view ir_action_act_window_view_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_act_window_view
    ADD CONSTRAINT ir_action_act_window_view_pkey PRIMARY KEY (id);


--
-- Name: ir_action_keyword ir_action_keyword_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_keyword
    ADD CONSTRAINT ir_action_keyword_pkey PRIMARY KEY (id);


--
-- Name: ir_action ir_action_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action
    ADD CONSTRAINT ir_action_pkey PRIMARY KEY (id);


--
-- Name: ir_action_report ir_action_report_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_report
    ADD CONSTRAINT ir_action_report_pkey PRIMARY KEY (id);


--
-- Name: ir_action_url ir_action_url_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_url
    ADD CONSTRAINT ir_action_url_pkey PRIMARY KEY (id);


--
-- Name: ir_action_wizard ir_action_wizard_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_wizard
    ADD CONSTRAINT ir_action_wizard_pkey PRIMARY KEY (id);


--
-- Name: ir_attachment ir_attachment_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_attachment
    ADD CONSTRAINT ir_attachment_pkey PRIMARY KEY (id);


--
-- Name: ir_calendar_day ir_calendar_day_index_unique; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_calendar_day
    ADD CONSTRAINT ir_calendar_day_index_unique UNIQUE (index);


--
-- Name: ir_calendar_day ir_calendar_day_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_calendar_day
    ADD CONSTRAINT ir_calendar_day_pkey PRIMARY KEY (id);


--
-- Name: ir_calendar_month ir_calendar_month_index_unique; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_calendar_month
    ADD CONSTRAINT ir_calendar_month_index_unique UNIQUE (index);


--
-- Name: ir_calendar_month ir_calendar_month_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_calendar_month
    ADD CONSTRAINT ir_calendar_month_pkey PRIMARY KEY (id);


--
-- Name: ir_configuration ir_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_configuration
    ADD CONSTRAINT ir_configuration_pkey PRIMARY KEY (id);


--
-- Name: ir_cron ir_cron_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_cron
    ADD CONSTRAINT ir_cron_pkey PRIMARY KEY (id);


--
-- Name: ir_export-res_group ir_export-res_group_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_export-res_group"
    ADD CONSTRAINT "ir_export-res_group_pkey" PRIMARY KEY (id);


--
-- Name: ir_export-write-res_group ir_export-write-res_group_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_export-write-res_group"
    ADD CONSTRAINT "ir_export-write-res_group_pkey" PRIMARY KEY (id);


--
-- Name: ir_export_line ir_export_line_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_export_line
    ADD CONSTRAINT ir_export_line_pkey PRIMARY KEY (id);


--
-- Name: ir_export ir_export_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_export
    ADD CONSTRAINT ir_export_pkey PRIMARY KEY (id);


--
-- Name: ir_lang ir_lang_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_lang
    ADD CONSTRAINT ir_lang_pkey PRIMARY KEY (id);


--
-- Name: ir_message ir_message_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_message
    ADD CONSTRAINT ir_message_pkey PRIMARY KEY (id);


--
-- Name: ir_model_access ir_model_access_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_access
    ADD CONSTRAINT ir_model_access_pkey PRIMARY KEY (id);


--
-- Name: ir_model_button-button_reset ir_model_button-button_reset_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_model_button-button_reset"
    ADD CONSTRAINT "ir_model_button-button_reset_pkey" PRIMARY KEY (id);


--
-- Name: ir_model_button-res_group ir_model_button-res_group_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_model_button-res_group"
    ADD CONSTRAINT "ir_model_button-res_group_pkey" PRIMARY KEY (id);


--
-- Name: ir_model_button_click ir_model_button_click_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_button_click
    ADD CONSTRAINT ir_model_button_click_pkey PRIMARY KEY (id);


--
-- Name: ir_model_button ir_model_button_name_model_uniq; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_button
    ADD CONSTRAINT ir_model_button_name_model_uniq UNIQUE (name, model);


--
-- Name: ir_model_button ir_model_button_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_button
    ADD CONSTRAINT ir_model_button_pkey PRIMARY KEY (id);


--
-- Name: ir_model_button_rule ir_model_button_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_button_rule
    ADD CONSTRAINT ir_model_button_rule_pkey PRIMARY KEY (id);


--
-- Name: ir_model_data ir_model_data_fs_id_module_model_uniq; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_data
    ADD CONSTRAINT ir_model_data_fs_id_module_model_uniq UNIQUE (fs_id, module, model);


--
-- Name: ir_model_data ir_model_data_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_data
    ADD CONSTRAINT ir_model_data_pkey PRIMARY KEY (id);


--
-- Name: ir_model_field_access ir_model_field_access_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_field_access
    ADD CONSTRAINT ir_model_field_access_pkey PRIMARY KEY (id);


--
-- Name: ir_model_field ir_model_field_name_model_uniq; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_field
    ADD CONSTRAINT ir_model_field_name_model_uniq UNIQUE (name, model);


--
-- Name: ir_model_field ir_model_field_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_field
    ADD CONSTRAINT ir_model_field_pkey PRIMARY KEY (id);


--
-- Name: ir_model ir_model_model_uniq; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model
    ADD CONSTRAINT ir_model_model_uniq UNIQUE (model);


--
-- Name: ir_model ir_model_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model
    ADD CONSTRAINT ir_model_pkey PRIMARY KEY (id);


--
-- Name: ir_module_config_wizard_item ir_module_config_wizard_item_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_module_config_wizard_item
    ADD CONSTRAINT ir_module_config_wizard_item_pkey PRIMARY KEY (id);


--
-- Name: ir_module_dependency ir_module_dependency_name_module_uniq; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_module_dependency
    ADD CONSTRAINT ir_module_dependency_name_module_uniq UNIQUE (name, module);


--
-- Name: ir_module_dependency ir_module_dependency_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_module_dependency
    ADD CONSTRAINT ir_module_dependency_pkey PRIMARY KEY (id);


--
-- Name: ir_module ir_module_name_uniq; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_module
    ADD CONSTRAINT ir_module_name_uniq UNIQUE (name);


--
-- Name: ir_module ir_module_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_module
    ADD CONSTRAINT ir_module_pkey PRIMARY KEY (id);


--
-- Name: ir_note ir_note_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_note
    ADD CONSTRAINT ir_note_pkey PRIMARY KEY (id);


--
-- Name: ir_note_read ir_note_read_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_note_read
    ADD CONSTRAINT ir_note_read_pkey PRIMARY KEY (id);


--
-- Name: ir_queue ir_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_queue
    ADD CONSTRAINT ir_queue_pkey PRIMARY KEY (id);


--
-- Name: ir_rule_group-res_group ir_rule_group-res_group_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_rule_group-res_group"
    ADD CONSTRAINT "ir_rule_group-res_group_pkey" PRIMARY KEY (id);


--
-- Name: ir_rule_group ir_rule_group_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_rule_group
    ADD CONSTRAINT ir_rule_group_pkey PRIMARY KEY (id);


--
-- Name: ir_rule ir_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_rule
    ADD CONSTRAINT ir_rule_pkey PRIMARY KEY (id);


--
-- Name: ir_sequence ir_sequence_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_sequence
    ADD CONSTRAINT ir_sequence_pkey PRIMARY KEY (id);


--
-- Name: ir_sequence_strict ir_sequence_strict_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_sequence_strict
    ADD CONSTRAINT ir_sequence_strict_pkey PRIMARY KEY (id);


--
-- Name: ir_sequence_type-res_group ir_sequence_type-res_group_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_sequence_type-res_group"
    ADD CONSTRAINT "ir_sequence_type-res_group_pkey" PRIMARY KEY (id);


--
-- Name: ir_sequence_type ir_sequence_type_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_sequence_type
    ADD CONSTRAINT ir_sequence_type_pkey PRIMARY KEY (id);


--
-- Name: ir_session ir_session_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_session
    ADD CONSTRAINT ir_session_pkey PRIMARY KEY (id);


--
-- Name: ir_session_wizard ir_session_wizard_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_session_wizard
    ADD CONSTRAINT ir_session_wizard_pkey PRIMARY KEY (id);


--
-- Name: ir_translation ir_translation_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_translation
    ADD CONSTRAINT ir_translation_pkey PRIMARY KEY (id);


--
-- Name: ir_trigger__history ir_trigger__history_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_trigger__history
    ADD CONSTRAINT ir_trigger__history_pkey PRIMARY KEY (__id);


--
-- Name: ir_trigger_log ir_trigger_log_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_trigger_log
    ADD CONSTRAINT ir_trigger_log_pkey PRIMARY KEY (id);


--
-- Name: ir_trigger ir_trigger_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_trigger
    ADD CONSTRAINT ir_trigger_pkey PRIMARY KEY (id);


--
-- Name: ir_ui_icon ir_ui_icon_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_icon
    ADD CONSTRAINT ir_ui_icon_pkey PRIMARY KEY (id);


--
-- Name: ir_ui_menu-res_group ir_ui_menu-res_group_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_ui_menu-res_group"
    ADD CONSTRAINT "ir_ui_menu-res_group_pkey" PRIMARY KEY (id);


--
-- Name: ir_ui_menu_favorite ir_ui_menu_favorite_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_menu_favorite
    ADD CONSTRAINT ir_ui_menu_favorite_pkey PRIMARY KEY (id);


--
-- Name: ir_ui_menu ir_ui_menu_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_menu
    ADD CONSTRAINT ir_ui_menu_pkey PRIMARY KEY (id);


--
-- Name: ir_ui_view ir_ui_view_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_view
    ADD CONSTRAINT ir_ui_view_pkey PRIMARY KEY (id);


--
-- Name: ir_ui_view_search ir_ui_view_search_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_view_search
    ADD CONSTRAINT ir_ui_view_search_pkey PRIMARY KEY (id);


--
-- Name: ir_ui_view_tree_state ir_ui_view_tree_state_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_view_tree_state
    ADD CONSTRAINT ir_ui_view_tree_state_pkey PRIMARY KEY (id);


--
-- Name: ir_ui_view_tree_width ir_ui_view_tree_width_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_view_tree_width
    ADD CONSTRAINT ir_ui_view_tree_width_pkey PRIMARY KEY (id);


--
-- Name: modelo-producto modelo-producto_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."modelo-producto"
    ADD CONSTRAINT "modelo-producto_pkey" PRIMARY KEY (id);


--
-- Name: party_address_format party_address_format_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_address_format
    ADD CONSTRAINT party_address_format_pkey PRIMARY KEY (id);


--
-- Name: party_address party_address_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_address
    ADD CONSTRAINT party_address_pkey PRIMARY KEY (id);


--
-- Name: party_address_subdivision_type party_address_subdivision_type_country_code_unique; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_address_subdivision_type
    ADD CONSTRAINT party_address_subdivision_type_country_code_unique EXCLUDE USING btree (country_code WITH =) WHERE ((active = true));


--
-- Name: party_address_subdivision_type party_address_subdivision_type_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_address_subdivision_type
    ADD CONSTRAINT party_address_subdivision_type_pkey PRIMARY KEY (id);


--
-- Name: party_category party_category_name_parent_exclude; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_category
    ADD CONSTRAINT party_category_name_parent_exclude EXCLUDE USING btree (name WITH =, COALESCE(parent, '-1'::integer) WITH =);


--
-- Name: party_category party_category_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_category
    ADD CONSTRAINT party_category_pkey PRIMARY KEY (id);


--
-- Name: party_category_rel party_category_rel_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_category_rel
    ADD CONSTRAINT party_category_rel_pkey PRIMARY KEY (id);


--
-- Name: party_configuration_party_lang party_configuration_party_lang_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_configuration_party_lang
    ADD CONSTRAINT party_configuration_party_lang_pkey PRIMARY KEY (id);


--
-- Name: party_configuration_party_sequence party_configuration_party_sequence_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_configuration_party_sequence
    ADD CONSTRAINT party_configuration_party_sequence_pkey PRIMARY KEY (id);


--
-- Name: party_configuration party_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_configuration
    ADD CONSTRAINT party_configuration_pkey PRIMARY KEY (id);


--
-- Name: party_contact_mechanism party_contact_mechanism_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_contact_mechanism
    ADD CONSTRAINT party_contact_mechanism_pkey PRIMARY KEY (id);


--
-- Name: party_identifier party_identifier_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_identifier
    ADD CONSTRAINT party_identifier_pkey PRIMARY KEY (id);


--
-- Name: party_party party_party_code_uniq; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_party
    ADD CONSTRAINT party_party_code_uniq UNIQUE (code);


--
-- Name: party_party_lang party_party_lang_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_party_lang
    ADD CONSTRAINT party_party_lang_pkey PRIMARY KEY (id);


--
-- Name: party_party party_party_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_party
    ADD CONSTRAINT party_party_pkey PRIMARY KEY (id);


--
-- Name: product_category product_category_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_pkey PRIMARY KEY (id);


--
-- Name: product_configuration_default_cost_price_method product_configuration_default_cost_price_method_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_configuration_default_cost_price_method
    ADD CONSTRAINT product_configuration_default_cost_price_method_pkey PRIMARY KEY (id);


--
-- Name: product_configuration product_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_configuration
    ADD CONSTRAINT product_configuration_pkey PRIMARY KEY (id);


--
-- Name: product_cost_price_method product_cost_price_method_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_cost_price_method
    ADD CONSTRAINT product_cost_price_method_pkey PRIMARY KEY (id);


--
-- Name: product_cost_price product_cost_price_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_cost_price
    ADD CONSTRAINT product_cost_price_pkey PRIMARY KEY (id);


--
-- Name: product_identifier product_identifier_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_identifier
    ADD CONSTRAINT product_identifier_pkey PRIMARY KEY (id);


--
-- Name: product_list_price product_list_price_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_list_price
    ADD CONSTRAINT product_list_price_pkey PRIMARY KEY (id);


--
-- Name: product_product product_product_code_exclude; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_code_exclude EXCLUDE USING btree (code WITH =) WHERE (((active = true) AND ((code)::text <> ''::text)));


--
-- Name: product_product product_product_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_pkey PRIMARY KEY (id);


--
-- Name: product_template-product_category product_template-product_category_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."product_template-product_category"
    ADD CONSTRAINT "product_template-product_category_pkey" PRIMARY KEY (id);


--
-- Name: product_template product_template_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_template
    ADD CONSTRAINT product_template_pkey PRIMARY KEY (id);


--
-- Name: product_uom_category product_uom_category_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_uom_category
    ADD CONSTRAINT product_uom_category_pkey PRIMARY KEY (id);


--
-- Name: product_uom product_uom_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_uom
    ADD CONSTRAINT product_uom_pkey PRIMARY KEY (id);


--
-- Name: res_group res_group_name_uniq; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_group
    ADD CONSTRAINT res_group_name_uniq UNIQUE (name);


--
-- Name: res_group res_group_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_group
    ADD CONSTRAINT res_group_pkey PRIMARY KEY (id);


--
-- Name: res_user-company_employee res_user-company_employee_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."res_user-company_employee"
    ADD CONSTRAINT "res_user-company_employee_pkey" PRIMARY KEY (id);


--
-- Name: res_user-ir_action res_user-ir_action_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."res_user-ir_action"
    ADD CONSTRAINT "res_user-ir_action_pkey" PRIMARY KEY (id);


--
-- Name: res_user-res_group res_user-res_group_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."res_user-res_group"
    ADD CONSTRAINT "res_user-res_group_pkey" PRIMARY KEY (id);


--
-- Name: res_user_application res_user_application_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user_application
    ADD CONSTRAINT res_user_application_pkey PRIMARY KEY (id);


--
-- Name: res_user_login_attempt res_user_login_attempt_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user_login_attempt
    ADD CONSTRAINT res_user_login_attempt_pkey PRIMARY KEY (id);


--
-- Name: res_user res_user_login_key; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user
    ADD CONSTRAINT res_user_login_key UNIQUE (login);


--
-- Name: res_user res_user_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user
    ADD CONSTRAINT res_user_pkey PRIMARY KEY (id);


--
-- Name: res_user_warning res_user_warning_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user_warning
    ADD CONSTRAINT res_user_warning_pkey PRIMARY KEY (id);


--
-- Name: taller_coche taller_coche_matricula unica; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.taller_coche
    ADD CONSTRAINT "taller_coche_matricula unica" UNIQUE (matricula);


--
-- Name: taller_coche taller_coche_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.taller_coche
    ADD CONSTRAINT taller_coche_pkey PRIMARY KEY (id);


--
-- Name: taller_marca taller_marca_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.taller_marca
    ADD CONSTRAINT taller_marca_pkey PRIMARY KEY (id);


--
-- Name: taller_modelo taller_modelo_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.taller_modelo
    ADD CONSTRAINT taller_modelo_pkey PRIMARY KEY (id);


--
-- Name: web_user_authenticate_attempt web_user_authenticate_attempt_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.web_user_authenticate_attempt
    ADD CONSTRAINT web_user_authenticate_attempt_pkey PRIMARY KEY (id);


--
-- Name: web_user web_user_email_exclude; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.web_user
    ADD CONSTRAINT web_user_email_exclude EXCLUDE USING btree (email WITH =) WHERE ((active = true));


--
-- Name: web_user web_user_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.web_user
    ADD CONSTRAINT web_user_pkey PRIMARY KEY (id);


--
-- Name: web_user_session web_user_session_key_unique; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.web_user_session
    ADD CONSTRAINT web_user_session_key_unique UNIQUE (key);


--
-- Name: web_user_session web_user_session_pkey; Type: CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.web_user_session
    ADD CONSTRAINT web_user_session_pkey PRIMARY KEY (id);


--
-- Name: coche-producto_coche_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "coche-producto_coche_index" ON public."coche-producto" USING btree (coche);


--
-- Name: coche-producto_producto_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "coche-producto_producto_index" ON public."coche-producto" USING btree (producto);


--
-- Name: country_country_code3_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX country_country_code3_index ON public.country_country USING btree (code3);


--
-- Name: country_country_code_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX country_country_code_index ON public.country_country USING btree (code);


--
-- Name: country_country_code_numeric_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX country_country_code_numeric_index ON public.country_country USING btree (code_numeric);


--
-- Name: country_country_name_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX country_country_name_index ON public.country_country USING btree (name);


--
-- Name: country_subdivision_code_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX country_subdivision_code_index ON public.country_subdivision USING btree (code);


--
-- Name: country_subdivision_country_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX country_subdivision_country_index ON public.country_subdivision USING btree (country);


--
-- Name: country_subdivision_name_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX country_subdivision_name_index ON public.country_subdivision USING btree (name);


--
-- Name: country_zip_country_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX country_zip_country_index ON public.country_zip USING btree (country);


--
-- Name: country_zip_subdivision_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX country_zip_subdivision_index ON public.country_zip USING btree (subdivision);


--
-- Name: cron_company_rel_company_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX cron_company_rel_company_index ON public.cron_company_rel USING btree (company);


--
-- Name: cron_company_rel_cron_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX cron_company_rel_cron_index ON public.cron_company_rel USING btree (cron);


--
-- Name: currency_currency_rate_date_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX currency_currency_rate_date_index ON public.currency_currency_rate USING btree (date);


--
-- Name: e58f52dd207acfc3836ab6ec0b9aa054e35cbce07f3cfb9bc5a06e2d89daf2f; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX e58f52dd207acfc3836ab6ec0b9aa054e35cbce07f3cfb9bc5a06e2d89daf2f ON public.ir_queue USING btree (scheduled_at NULLS FIRST, expected_at NULLS FIRST, dequeued_at, name);


--
-- Name: ir_action-res_group_action_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_action-res_group_action_index" ON public."ir_action-res_group" USING btree (action);


--
-- Name: ir_action-res_group_group_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_action-res_group_group_index" ON public."ir_action-res_group" USING btree ("group");


--
-- Name: ir_action_act_window_domain_act_window_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_action_act_window_domain_act_window_index ON public.ir_action_act_window_domain USING btree (act_window);


--
-- Name: ir_action_keyword_action_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_action_keyword_action_index ON public.ir_action_keyword USING btree (action);


--
-- Name: ir_action_keyword_keyword_model_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_action_keyword_keyword_model_index ON public.ir_action_keyword USING btree (keyword, model);


--
-- Name: ir_action_report_module_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_action_report_module_index ON public.ir_action_report USING btree (module);


--
-- Name: ir_attachment_resource_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_attachment_resource_index ON public.ir_attachment USING btree (resource);


--
-- Name: ir_cron_next_call_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_cron_next_call_index ON public.ir_cron USING btree (next_call);


--
-- Name: ir_export-res_group_export_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_export-res_group_export_index" ON public."ir_export-res_group" USING btree (export);


--
-- Name: ir_export-write-res_group_export_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_export-write-res_group_export_index" ON public."ir_export-write-res_group" USING btree (export);


--
-- Name: ir_export_line_export_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_export_line_export_index ON public.ir_export_line USING btree (export);


--
-- Name: ir_model_button-button_reset_button_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_model_button-button_reset_button_index" ON public."ir_model_button-button_reset" USING btree (button);


--
-- Name: ir_model_button-button_reset_button_ruled_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_model_button-button_reset_button_ruled_index" ON public."ir_model_button-button_reset" USING btree (button_ruled);


--
-- Name: ir_model_button-res_group_button_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_model_button-res_group_button_index" ON public."ir_model_button-res_group" USING btree (button);


--
-- Name: ir_model_button-res_group_group_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_model_button-res_group_group_index" ON public."ir_model_button-res_group" USING btree ("group");


--
-- Name: ir_model_button_model_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_model_button_model_index ON public.ir_model_button USING btree (model);


--
-- Name: ir_model_data_db_id_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_model_data_db_id_index ON public.ir_model_data USING btree (db_id);


--
-- Name: ir_model_data_fs_id_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_model_data_fs_id_index ON public.ir_model_data USING btree (fs_id);


--
-- Name: ir_model_data_model_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_model_data_model_index ON public.ir_model_data USING btree (model);


--
-- Name: ir_model_data_module_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_model_data_module_index ON public.ir_model_data USING btree (module);


--
-- Name: ir_model_field_model_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_model_field_model_index ON public.ir_model_field USING btree (model);


--
-- Name: ir_module_config_wizard_item_state_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_module_config_wizard_item_state_index ON public.ir_module_config_wizard_item USING btree (state);


--
-- Name: ir_module_dependency_module_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_module_dependency_module_index ON public.ir_module_dependency USING btree (module);


--
-- Name: ir_note_resource_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_note_resource_index ON public.ir_note USING btree (resource);


--
-- Name: ir_rule_group-res_group_group_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_rule_group-res_group_group_index" ON public."ir_rule_group-res_group" USING btree ("group");


--
-- Name: ir_rule_group-res_group_rule_group_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_rule_group-res_group_rule_group_index" ON public."ir_rule_group-res_group" USING btree (rule_group);


--
-- Name: ir_rule_group_default_p_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_rule_group_default_p_index ON public.ir_rule_group USING btree (default_p);


--
-- Name: ir_rule_group_global_p_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_rule_group_global_p_index ON public.ir_rule_group USING btree (global_p);


--
-- Name: ir_rule_group_model_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_rule_group_model_index ON public.ir_rule_group USING btree (model);


--
-- Name: ir_rule_group_name_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_rule_group_name_index ON public.ir_rule_group USING btree (name);


--
-- Name: ir_rule_rule_group_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_rule_rule_group_index ON public.ir_rule USING btree (rule_group);


--
-- Name: ir_sequence_type-res_group_group_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_sequence_type-res_group_group_index" ON public."ir_sequence_type-res_group" USING btree ("group");


--
-- Name: ir_sequence_type-res_group_sequence_type_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_sequence_type-res_group_sequence_type_index" ON public."ir_sequence_type-res_group" USING btree (sequence_type);


--
-- Name: ir_session_create_uid_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_session_create_uid_index ON public.ir_session USING btree (create_uid);


--
-- Name: ir_session_key_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_session_key_index ON public.ir_session USING btree (key);


--
-- Name: ir_translation_lang_type_name_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_translation_lang_type_name_index ON public.ir_translation USING btree (lang, type, name);


--
-- Name: ir_translation_res_id_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_translation_res_id_index ON public.ir_translation USING btree (res_id);


--
-- Name: ir_trigger_log_trigger_record_id_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_trigger_log_trigger_record_id_index ON public.ir_trigger_log USING btree (trigger, record_id);


--
-- Name: ir_trigger_model_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_trigger_model_index ON public.ir_trigger USING btree (model);


--
-- Name: ir_trigger_on_create_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_trigger_on_create_index ON public.ir_trigger USING btree (on_create);


--
-- Name: ir_trigger_on_delete_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_trigger_on_delete_index ON public.ir_trigger USING btree (on_delete);


--
-- Name: ir_trigger_on_time_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_trigger_on_time_index ON public.ir_trigger USING btree (on_time);


--
-- Name: ir_trigger_on_write_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_trigger_on_write_index ON public.ir_trigger USING btree (on_write);


--
-- Name: ir_ui_icon_name_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_ui_icon_name_index ON public.ir_ui_icon USING btree (name);


--
-- Name: ir_ui_menu-res_group_group_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_ui_menu-res_group_group_index" ON public."ir_ui_menu-res_group" USING btree ("group");


--
-- Name: ir_ui_menu-res_group_menu_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "ir_ui_menu-res_group_menu_index" ON public."ir_ui_menu-res_group" USING btree (menu);


--
-- Name: ir_ui_menu_parent_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_ui_menu_parent_index ON public.ir_ui_menu USING btree (parent);


--
-- Name: ir_ui_view_inherit_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_ui_view_inherit_index ON public.ir_ui_view USING btree (inherit);


--
-- Name: ir_ui_view_model_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_ui_view_model_index ON public.ir_ui_view USING btree (model);


--
-- Name: ir_ui_view_priority_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_ui_view_priority_index ON public.ir_ui_view USING btree (priority);


--
-- Name: ir_ui_view_tree_state_model_domain_user_child_name_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_ui_view_tree_state_model_domain_user_child_name_index ON public.ir_ui_view_tree_state USING btree (model, domain, "user", child_name);


--
-- Name: ir_ui_view_tree_width_field_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_ui_view_tree_width_field_index ON public.ir_ui_view_tree_width USING btree (field);


--
-- Name: ir_ui_view_tree_width_model_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_ui_view_tree_width_model_index ON public.ir_ui_view_tree_width USING btree (model);


--
-- Name: ir_ui_view_tree_width_user_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_ui_view_tree_width_user_index ON public.ir_ui_view_tree_width USING btree ("user");


--
-- Name: ir_ui_view_type_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX ir_ui_view_type_index ON public.ir_ui_view USING btree (type);


--
-- Name: modelo-producto_modelo_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "modelo-producto_modelo_index" ON public."modelo-producto" USING btree (modelo);


--
-- Name: modelo-producto_producto_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "modelo-producto_producto_index" ON public."modelo-producto" USING btree (producto);


--
-- Name: party_address_party_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_address_party_index ON public.party_address USING btree (party);


--
-- Name: party_category_parent_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_category_parent_index ON public.party_category USING btree (parent);


--
-- Name: party_category_rel_category_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_category_rel_category_index ON public.party_category_rel USING btree (category);


--
-- Name: party_category_rel_party_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_category_rel_party_index ON public.party_category_rel USING btree (party);


--
-- Name: party_configuration_party_lang_company_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_configuration_party_lang_company_index ON public.party_configuration_party_lang USING btree (company);


--
-- Name: party_contact_mechanism_party_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_contact_mechanism_party_index ON public.party_contact_mechanism USING btree (party);


--
-- Name: party_contact_mechanism_value_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_contact_mechanism_value_index ON public.party_contact_mechanism USING btree (value);


--
-- Name: party_identifier_party_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_identifier_party_index ON public.party_identifier USING btree (party);


--
-- Name: party_party_code_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_party_code_index ON public.party_party USING btree (code);


--
-- Name: party_party_lang_company_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_party_lang_company_index ON public.party_party_lang USING btree (company);


--
-- Name: party_party_lang_party_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_party_lang_party_index ON public.party_party_lang USING btree (party);


--
-- Name: party_party_name_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX party_party_name_index ON public.party_party USING btree (name);


--
-- Name: product_category_parent_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_category_parent_index ON public.product_category USING btree (parent);


--
-- Name: product_cost_price_company_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_cost_price_company_index ON public.product_cost_price USING btree (company);


--
-- Name: product_cost_price_method_company_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_cost_price_method_company_index ON public.product_cost_price_method USING btree (company);


--
-- Name: product_cost_price_method_template_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_cost_price_method_template_index ON public.product_cost_price_method USING btree (template);


--
-- Name: product_cost_price_product_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_cost_price_product_index ON public.product_cost_price USING btree (product);


--
-- Name: product_identifier_product_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_identifier_product_index ON public.product_identifier USING btree (product);


--
-- Name: product_list_price_company_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_list_price_company_index ON public.product_list_price USING btree (company);


--
-- Name: product_list_price_template_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_list_price_template_index ON public.product_list_price USING btree (template);


--
-- Name: product_product_code_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_product_code_index ON public.product_product USING btree (code);


--
-- Name: product_product_template_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_product_template_index ON public.product_product USING btree (template);


--
-- Name: product_template-product_category_category_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "product_template-product_category_category_index" ON public."product_template-product_category" USING btree (category);


--
-- Name: product_template-product_category_template_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "product_template-product_category_template_index" ON public."product_template-product_category" USING btree (template);


--
-- Name: product_template_code_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_template_code_index ON public.product_template USING btree (code);


--
-- Name: product_template_name_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX product_template_name_index ON public.product_template USING btree (name);


--
-- Name: res_group_name_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX res_group_name_index ON public.res_group USING btree (name);


--
-- Name: res_user-company_employee_employee_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "res_user-company_employee_employee_index" ON public."res_user-company_employee" USING btree (employee);


--
-- Name: res_user-company_employee_user_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "res_user-company_employee_user_index" ON public."res_user-company_employee" USING btree ("user");


--
-- Name: res_user-ir_action_action_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "res_user-ir_action_action_index" ON public."res_user-ir_action" USING btree (action);


--
-- Name: res_user-ir_action_user_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "res_user-ir_action_user_index" ON public."res_user-ir_action" USING btree ("user");


--
-- Name: res_user-res_group_group_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "res_user-res_group_group_index" ON public."res_user-res_group" USING btree ("group");


--
-- Name: res_user-res_group_user_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX "res_user-res_group_user_index" ON public."res_user-res_group" USING btree ("user");


--
-- Name: res_user_application_key_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX res_user_application_key_index ON public.res_user_application USING btree (key);


--
-- Name: res_user_application_user_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX res_user_application_user_index ON public.res_user_application USING btree ("user");


--
-- Name: res_user_name_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX res_user_name_index ON public.res_user USING btree (name);


--
-- Name: res_user_warning_name_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX res_user_warning_name_index ON public.res_user_warning USING btree (name);


--
-- Name: res_user_warning_user_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX res_user_warning_user_index ON public.res_user_warning USING btree ("user");


--
-- Name: web_user_email_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX web_user_email_index ON public.web_user USING btree (email);


--
-- Name: web_user_email_token_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX web_user_email_token_index ON public.web_user USING btree (email_token);


--
-- Name: web_user_reset_password_token_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX web_user_reset_password_token_index ON public.web_user USING btree (reset_password_token);


--
-- Name: web_user_session_key_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX web_user_session_key_index ON public.web_user_session USING btree (key);


--
-- Name: web_user_session_user_index; Type: INDEX; Schema: public; Owner: tryton
--

CREATE INDEX web_user_session_user_index ON public.web_user_session USING btree ("user");


--
-- Name: coche-producto coche-producto_coche_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."coche-producto"
    ADD CONSTRAINT "coche-producto_coche_fkey" FOREIGN KEY (coche) REFERENCES public.taller_coche(id) ON DELETE CASCADE;


--
-- Name: coche-producto coche-producto_producto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."coche-producto"
    ADD CONSTRAINT "coche-producto_producto_fkey" FOREIGN KEY (producto) REFERENCES public.product_template(id) ON DELETE CASCADE;


--
-- Name: company_company company_company_currency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.company_company
    ADD CONSTRAINT company_company_currency_fkey FOREIGN KEY (currency) REFERENCES public.currency_currency(id) ON DELETE RESTRICT;


--
-- Name: company_company company_company_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.company_company
    ADD CONSTRAINT company_company_parent_fkey FOREIGN KEY (parent) REFERENCES public.company_company(id) ON DELETE SET NULL;


--
-- Name: company_company company_company_party_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.company_company
    ADD CONSTRAINT company_company_party_fkey FOREIGN KEY (party) REFERENCES public.party_party(id) ON DELETE CASCADE;


--
-- Name: company_employee company_employee_company_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.company_employee
    ADD CONSTRAINT company_employee_company_fkey FOREIGN KEY (company) REFERENCES public.company_company(id) ON DELETE RESTRICT;


--
-- Name: company_employee company_employee_party_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.company_employee
    ADD CONSTRAINT company_employee_party_fkey FOREIGN KEY (party) REFERENCES public.party_party(id) ON DELETE RESTRICT;


--
-- Name: company_employee company_employee_supervisor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.company_employee
    ADD CONSTRAINT company_employee_supervisor_fkey FOREIGN KEY (supervisor) REFERENCES public.company_employee(id) ON DELETE SET NULL;


--
-- Name: country_subdivision country_subdivision_country_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.country_subdivision
    ADD CONSTRAINT country_subdivision_country_fkey FOREIGN KEY (country) REFERENCES public.country_country(id) ON DELETE RESTRICT;


--
-- Name: country_subdivision country_subdivision_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.country_subdivision
    ADD CONSTRAINT country_subdivision_parent_fkey FOREIGN KEY (parent) REFERENCES public.country_subdivision(id) ON DELETE SET NULL;


--
-- Name: country_zip country_zip_country_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.country_zip
    ADD CONSTRAINT country_zip_country_fkey FOREIGN KEY (country) REFERENCES public.country_country(id) ON DELETE CASCADE;


--
-- Name: country_zip country_zip_subdivision_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.country_zip
    ADD CONSTRAINT country_zip_subdivision_fkey FOREIGN KEY (subdivision) REFERENCES public.country_subdivision(id) ON DELETE CASCADE;


--
-- Name: cron_company_rel cron_company_rel_company_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.cron_company_rel
    ADD CONSTRAINT cron_company_rel_company_fkey FOREIGN KEY (company) REFERENCES public.company_company(id) ON DELETE CASCADE;


--
-- Name: cron_company_rel cron_company_rel_cron_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.cron_company_rel
    ADD CONSTRAINT cron_company_rel_cron_fkey FOREIGN KEY (cron) REFERENCES public.ir_cron(id) ON DELETE CASCADE;


--
-- Name: currency_currency_rate currency_currency_rate_currency_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.currency_currency_rate
    ADD CONSTRAINT currency_currency_rate_currency_fkey FOREIGN KEY (currency) REFERENCES public.currency_currency(id) ON DELETE CASCADE;


--
-- Name: ir_action-res_group ir_action-res_group_action_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_action-res_group"
    ADD CONSTRAINT "ir_action-res_group_action_fkey" FOREIGN KEY (action) REFERENCES public.ir_action(id) ON DELETE CASCADE;


--
-- Name: ir_action-res_group ir_action-res_group_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_action-res_group"
    ADD CONSTRAINT "ir_action-res_group_group_fkey" FOREIGN KEY ("group") REFERENCES public.res_group(id) ON DELETE CASCADE;


--
-- Name: ir_action_act_window ir_action_act_window_action_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_act_window
    ADD CONSTRAINT ir_action_act_window_action_fkey FOREIGN KEY (action) REFERENCES public.ir_action(id) ON DELETE CASCADE;


--
-- Name: ir_action_act_window_domain ir_action_act_window_domain_act_window_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_act_window_domain
    ADD CONSTRAINT ir_action_act_window_domain_act_window_fkey FOREIGN KEY (act_window) REFERENCES public.ir_action_act_window(id) ON DELETE CASCADE;


--
-- Name: ir_action_act_window_view ir_action_act_window_view_act_window_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_act_window_view
    ADD CONSTRAINT ir_action_act_window_view_act_window_fkey FOREIGN KEY (act_window) REFERENCES public.ir_action_act_window(id) ON DELETE CASCADE;


--
-- Name: ir_action_act_window_view ir_action_act_window_view_view_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_act_window_view
    ADD CONSTRAINT ir_action_act_window_view_view_fkey FOREIGN KEY (view) REFERENCES public.ir_ui_view(id) ON DELETE CASCADE;


--
-- Name: ir_action ir_action_icon_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action
    ADD CONSTRAINT ir_action_icon_fkey FOREIGN KEY (icon) REFERENCES public.ir_ui_icon(id) ON DELETE SET NULL;


--
-- Name: ir_action_keyword ir_action_keyword_action_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_keyword
    ADD CONSTRAINT ir_action_keyword_action_fkey FOREIGN KEY (action) REFERENCES public.ir_action(id) ON DELETE CASCADE;


--
-- Name: ir_action_report ir_action_report_action_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_report
    ADD CONSTRAINT ir_action_report_action_fkey FOREIGN KEY (action) REFERENCES public.ir_action(id) ON DELETE CASCADE;


--
-- Name: ir_action_url ir_action_url_action_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_url
    ADD CONSTRAINT ir_action_url_action_fkey FOREIGN KEY (action) REFERENCES public.ir_action(id) ON DELETE CASCADE;


--
-- Name: ir_action_wizard ir_action_wizard_action_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_action_wizard
    ADD CONSTRAINT ir_action_wizard_action_fkey FOREIGN KEY (action) REFERENCES public.ir_action(id) ON DELETE CASCADE;


--
-- Name: ir_cron ir_cron_weekday_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_cron
    ADD CONSTRAINT ir_cron_weekday_fkey FOREIGN KEY (weekday) REFERENCES public.ir_calendar_day(id) ON DELETE SET NULL;


--
-- Name: ir_export-res_group ir_export-res_group_export_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_export-res_group"
    ADD CONSTRAINT "ir_export-res_group_export_fkey" FOREIGN KEY (export) REFERENCES public.ir_export(id) ON DELETE CASCADE;


--
-- Name: ir_export-res_group ir_export-res_group_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_export-res_group"
    ADD CONSTRAINT "ir_export-res_group_group_fkey" FOREIGN KEY ("group") REFERENCES public.res_group(id) ON DELETE CASCADE;


--
-- Name: ir_export-write-res_group ir_export-write-res_group_export_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_export-write-res_group"
    ADD CONSTRAINT "ir_export-write-res_group_export_fkey" FOREIGN KEY (export) REFERENCES public.ir_export(id) ON DELETE CASCADE;


--
-- Name: ir_export-write-res_group ir_export-write-res_group_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_export-write-res_group"
    ADD CONSTRAINT "ir_export-write-res_group_group_fkey" FOREIGN KEY ("group") REFERENCES public.res_group(id) ON DELETE CASCADE;


--
-- Name: ir_export_line ir_export_line_export_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_export_line
    ADD CONSTRAINT ir_export_line_export_fkey FOREIGN KEY (export) REFERENCES public.ir_export(id) ON DELETE CASCADE;


--
-- Name: ir_model_access ir_model_access_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_access
    ADD CONSTRAINT ir_model_access_group_fkey FOREIGN KEY ("group") REFERENCES public.res_group(id) ON DELETE CASCADE;


--
-- Name: ir_model_access ir_model_access_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_access
    ADD CONSTRAINT ir_model_access_model_fkey FOREIGN KEY (model) REFERENCES public.ir_model(id) ON DELETE CASCADE;


--
-- Name: ir_model_button-button_reset ir_model_button-button_reset_button_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_model_button-button_reset"
    ADD CONSTRAINT "ir_model_button-button_reset_button_fkey" FOREIGN KEY (button) REFERENCES public.ir_model_button(id) ON DELETE CASCADE;


--
-- Name: ir_model_button-button_reset ir_model_button-button_reset_button_ruled_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_model_button-button_reset"
    ADD CONSTRAINT "ir_model_button-button_reset_button_ruled_fkey" FOREIGN KEY (button_ruled) REFERENCES public.ir_model_button(id) ON DELETE CASCADE;


--
-- Name: ir_model_button-res_group ir_model_button-res_group_button_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_model_button-res_group"
    ADD CONSTRAINT "ir_model_button-res_group_button_fkey" FOREIGN KEY (button) REFERENCES public.ir_model_button(id) ON DELETE CASCADE;


--
-- Name: ir_model_button-res_group ir_model_button-res_group_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_model_button-res_group"
    ADD CONSTRAINT "ir_model_button-res_group_group_fkey" FOREIGN KEY ("group") REFERENCES public.res_group(id) ON DELETE CASCADE;


--
-- Name: ir_model_button_click ir_model_button_click_button_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_button_click
    ADD CONSTRAINT ir_model_button_click_button_fkey FOREIGN KEY (button) REFERENCES public.ir_model_button(id) ON DELETE CASCADE;


--
-- Name: ir_model_button_click ir_model_button_click_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_button_click
    ADD CONSTRAINT ir_model_button_click_user_fkey FOREIGN KEY ("user") REFERENCES public.res_user(id) ON DELETE CASCADE;


--
-- Name: ir_model_button ir_model_button_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_button
    ADD CONSTRAINT ir_model_button_model_fkey FOREIGN KEY (model) REFERENCES public.ir_model(id) ON DELETE CASCADE;


--
-- Name: ir_model_button_rule ir_model_button_rule_button_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_button_rule
    ADD CONSTRAINT ir_model_button_rule_button_fkey FOREIGN KEY (button) REFERENCES public.ir_model_button(id) ON DELETE CASCADE;


--
-- Name: ir_model_button_rule ir_model_button_rule_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_button_rule
    ADD CONSTRAINT ir_model_button_rule_group_fkey FOREIGN KEY ("group") REFERENCES public.res_group(id) ON DELETE CASCADE;


--
-- Name: ir_model_field_access ir_model_field_access_field_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_field_access
    ADD CONSTRAINT ir_model_field_access_field_fkey FOREIGN KEY (field) REFERENCES public.ir_model_field(id) ON DELETE CASCADE;


--
-- Name: ir_model_field_access ir_model_field_access_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_field_access
    ADD CONSTRAINT ir_model_field_access_group_fkey FOREIGN KEY ("group") REFERENCES public.res_group(id) ON DELETE CASCADE;


--
-- Name: ir_model_field ir_model_field_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_model_field
    ADD CONSTRAINT ir_model_field_model_fkey FOREIGN KEY (model) REFERENCES public.ir_model(id) ON DELETE CASCADE;


--
-- Name: ir_module_config_wizard_item ir_module_config_wizard_item_action_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_module_config_wizard_item
    ADD CONSTRAINT ir_module_config_wizard_item_action_fkey FOREIGN KEY (action) REFERENCES public.ir_action(id) ON DELETE RESTRICT;


--
-- Name: ir_module_dependency ir_module_dependency_module_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_module_dependency
    ADD CONSTRAINT ir_module_dependency_module_fkey FOREIGN KEY (module) REFERENCES public.ir_module(id) ON DELETE CASCADE;


--
-- Name: ir_note_read ir_note_read_note_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_note_read
    ADD CONSTRAINT ir_note_read_note_fkey FOREIGN KEY (note) REFERENCES public.ir_note(id) ON DELETE CASCADE;


--
-- Name: ir_note_read ir_note_read_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_note_read
    ADD CONSTRAINT ir_note_read_user_fkey FOREIGN KEY ("user") REFERENCES public.res_user(id) ON DELETE CASCADE;


--
-- Name: ir_rule_group-res_group ir_rule_group-res_group_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_rule_group-res_group"
    ADD CONSTRAINT "ir_rule_group-res_group_group_fkey" FOREIGN KEY ("group") REFERENCES public.res_group(id) ON DELETE CASCADE;


--
-- Name: ir_rule_group-res_group ir_rule_group-res_group_rule_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_rule_group-res_group"
    ADD CONSTRAINT "ir_rule_group-res_group_rule_group_fkey" FOREIGN KEY (rule_group) REFERENCES public.ir_rule_group(id) ON DELETE CASCADE;


--
-- Name: ir_rule_group ir_rule_group_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_rule_group
    ADD CONSTRAINT ir_rule_group_model_fkey FOREIGN KEY (model) REFERENCES public.ir_model(id) ON DELETE CASCADE;


--
-- Name: ir_rule ir_rule_rule_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_rule
    ADD CONSTRAINT ir_rule_rule_group_fkey FOREIGN KEY (rule_group) REFERENCES public.ir_rule_group(id) ON DELETE CASCADE;


--
-- Name: ir_sequence ir_sequence_company_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_sequence
    ADD CONSTRAINT ir_sequence_company_fkey FOREIGN KEY (company) REFERENCES public.company_company(id) ON DELETE SET NULL;


--
-- Name: ir_sequence_strict ir_sequence_strict_company_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_sequence_strict
    ADD CONSTRAINT ir_sequence_strict_company_fkey FOREIGN KEY (company) REFERENCES public.company_company(id) ON DELETE SET NULL;


--
-- Name: ir_sequence_type-res_group ir_sequence_type-res_group_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_sequence_type-res_group"
    ADD CONSTRAINT "ir_sequence_type-res_group_group_fkey" FOREIGN KEY ("group") REFERENCES public.res_group(id) ON DELETE CASCADE;


--
-- Name: ir_sequence_type-res_group ir_sequence_type-res_group_sequence_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_sequence_type-res_group"
    ADD CONSTRAINT "ir_sequence_type-res_group_sequence_type_fkey" FOREIGN KEY (sequence_type) REFERENCES public.ir_sequence_type(id) ON DELETE CASCADE;


--
-- Name: ir_trigger_log ir_trigger_log_trigger_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_trigger_log
    ADD CONSTRAINT ir_trigger_log_trigger_fkey FOREIGN KEY (trigger) REFERENCES public.ir_trigger(id) ON DELETE CASCADE;


--
-- Name: ir_trigger ir_trigger_model_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_trigger
    ADD CONSTRAINT ir_trigger_model_fkey FOREIGN KEY (model) REFERENCES public.ir_model(id) ON DELETE RESTRICT;


--
-- Name: ir_ui_menu-res_group ir_ui_menu-res_group_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_ui_menu-res_group"
    ADD CONSTRAINT "ir_ui_menu-res_group_group_fkey" FOREIGN KEY ("group") REFERENCES public.res_group(id) ON DELETE CASCADE;


--
-- Name: ir_ui_menu-res_group ir_ui_menu-res_group_menu_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."ir_ui_menu-res_group"
    ADD CONSTRAINT "ir_ui_menu-res_group_menu_fkey" FOREIGN KEY (menu) REFERENCES public.ir_ui_menu(id) ON DELETE CASCADE;


--
-- Name: ir_ui_menu_favorite ir_ui_menu_favorite_menu_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_menu_favorite
    ADD CONSTRAINT ir_ui_menu_favorite_menu_fkey FOREIGN KEY (menu) REFERENCES public.ir_ui_menu(id) ON DELETE CASCADE;


--
-- Name: ir_ui_menu_favorite ir_ui_menu_favorite_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_menu_favorite
    ADD CONSTRAINT ir_ui_menu_favorite_user_fkey FOREIGN KEY ("user") REFERENCES public.res_user(id) ON DELETE CASCADE;


--
-- Name: ir_ui_menu ir_ui_menu_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_menu
    ADD CONSTRAINT ir_ui_menu_parent_fkey FOREIGN KEY (parent) REFERENCES public.ir_ui_menu(id) ON DELETE CASCADE;


--
-- Name: ir_ui_view ir_ui_view_inherit_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_view
    ADD CONSTRAINT ir_ui_view_inherit_fkey FOREIGN KEY (inherit) REFERENCES public.ir_ui_view(id) ON DELETE CASCADE;


--
-- Name: ir_ui_view_search ir_ui_view_search_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_view_search
    ADD CONSTRAINT ir_ui_view_search_user_fkey FOREIGN KEY ("user") REFERENCES public.res_user(id) ON DELETE CASCADE;


--
-- Name: ir_ui_view_tree_state ir_ui_view_tree_state_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_view_tree_state
    ADD CONSTRAINT ir_ui_view_tree_state_user_fkey FOREIGN KEY ("user") REFERENCES public.res_user(id) ON DELETE CASCADE;


--
-- Name: ir_ui_view_tree_width ir_ui_view_tree_width_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.ir_ui_view_tree_width
    ADD CONSTRAINT ir_ui_view_tree_width_user_fkey FOREIGN KEY ("user") REFERENCES public.res_user(id) ON DELETE CASCADE;


--
-- Name: modelo-producto modelo-producto_modelo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."modelo-producto"
    ADD CONSTRAINT "modelo-producto_modelo_fkey" FOREIGN KEY (modelo) REFERENCES public.taller_modelo(id) ON DELETE CASCADE;


--
-- Name: modelo-producto modelo-producto_producto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."modelo-producto"
    ADD CONSTRAINT "modelo-producto_producto_fkey" FOREIGN KEY (producto) REFERENCES public.product_template(id) ON DELETE CASCADE;


--
-- Name: party_address party_address_country_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_address
    ADD CONSTRAINT party_address_country_fkey FOREIGN KEY (country) REFERENCES public.country_country(id) ON DELETE SET NULL;


--
-- Name: party_address party_address_party_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_address
    ADD CONSTRAINT party_address_party_fkey FOREIGN KEY (party) REFERENCES public.party_party(id) ON DELETE CASCADE;


--
-- Name: party_address party_address_subdivision_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_address
    ADD CONSTRAINT party_address_subdivision_fkey FOREIGN KEY (subdivision) REFERENCES public.country_subdivision(id) ON DELETE SET NULL;


--
-- Name: party_category party_category_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_category
    ADD CONSTRAINT party_category_parent_fkey FOREIGN KEY (parent) REFERENCES public.party_category(id) ON DELETE SET NULL;


--
-- Name: party_category_rel party_category_rel_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_category_rel
    ADD CONSTRAINT party_category_rel_category_fkey FOREIGN KEY (category) REFERENCES public.party_category(id) ON DELETE CASCADE;


--
-- Name: party_category_rel party_category_rel_party_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_category_rel
    ADD CONSTRAINT party_category_rel_party_fkey FOREIGN KEY (party) REFERENCES public.party_party(id) ON DELETE CASCADE;


--
-- Name: party_configuration_party_lang party_configuration_party_lang_company_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_configuration_party_lang
    ADD CONSTRAINT party_configuration_party_lang_company_fkey FOREIGN KEY (company) REFERENCES public.company_company(id) ON DELETE CASCADE;


--
-- Name: party_configuration_party_lang party_configuration_party_lang_party_lang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_configuration_party_lang
    ADD CONSTRAINT party_configuration_party_lang_party_lang_fkey FOREIGN KEY (party_lang) REFERENCES public.ir_lang(id) ON DELETE SET NULL;


--
-- Name: party_configuration_party_sequence party_configuration_party_sequence_party_sequence_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_configuration_party_sequence
    ADD CONSTRAINT party_configuration_party_sequence_party_sequence_fkey FOREIGN KEY (party_sequence) REFERENCES public.ir_sequence(id) ON DELETE SET NULL;


--
-- Name: party_contact_mechanism party_contact_mechanism_party_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_contact_mechanism
    ADD CONSTRAINT party_contact_mechanism_party_fkey FOREIGN KEY (party) REFERENCES public.party_party(id) ON DELETE CASCADE;


--
-- Name: party_identifier party_identifier_party_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_identifier
    ADD CONSTRAINT party_identifier_party_fkey FOREIGN KEY (party) REFERENCES public.party_party(id) ON DELETE CASCADE;


--
-- Name: party_party_lang party_party_lang_company_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_party_lang
    ADD CONSTRAINT party_party_lang_company_fkey FOREIGN KEY (company) REFERENCES public.company_company(id) ON DELETE CASCADE;


--
-- Name: party_party_lang party_party_lang_lang_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_party_lang
    ADD CONSTRAINT party_party_lang_lang_fkey FOREIGN KEY (lang) REFERENCES public.ir_lang(id) ON DELETE SET NULL;


--
-- Name: party_party_lang party_party_lang_party_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_party_lang
    ADD CONSTRAINT party_party_lang_party_fkey FOREIGN KEY (party) REFERENCES public.party_party(id) ON DELETE CASCADE;


--
-- Name: party_party party_party_replaced_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.party_party
    ADD CONSTRAINT party_party_replaced_by_fkey FOREIGN KEY (replaced_by) REFERENCES public.party_party(id) ON DELETE SET NULL;


--
-- Name: product_category product_category_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_category
    ADD CONSTRAINT product_category_parent_fkey FOREIGN KEY (parent) REFERENCES public.product_category(id) ON DELETE SET NULL;


--
-- Name: product_configuration product_configuration_product_sequence_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_configuration
    ADD CONSTRAINT product_configuration_product_sequence_fkey FOREIGN KEY (product_sequence) REFERENCES public.ir_sequence(id) ON DELETE SET NULL;


--
-- Name: product_cost_price product_cost_price_company_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_cost_price
    ADD CONSTRAINT product_cost_price_company_fkey FOREIGN KEY (company) REFERENCES public.company_company(id) ON DELETE CASCADE;


--
-- Name: product_cost_price_method product_cost_price_method_company_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_cost_price_method
    ADD CONSTRAINT product_cost_price_method_company_fkey FOREIGN KEY (company) REFERENCES public.company_company(id) ON DELETE CASCADE;


--
-- Name: product_cost_price_method product_cost_price_method_template_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_cost_price_method
    ADD CONSTRAINT product_cost_price_method_template_fkey FOREIGN KEY (template) REFERENCES public.product_template(id) ON DELETE CASCADE;


--
-- Name: product_cost_price product_cost_price_product_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_cost_price
    ADD CONSTRAINT product_cost_price_product_fkey FOREIGN KEY (product) REFERENCES public.product_product(id) ON DELETE CASCADE;


--
-- Name: product_identifier product_identifier_product_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_identifier
    ADD CONSTRAINT product_identifier_product_fkey FOREIGN KEY (product) REFERENCES public.product_product(id) ON DELETE CASCADE;


--
-- Name: product_list_price product_list_price_company_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_list_price
    ADD CONSTRAINT product_list_price_company_fkey FOREIGN KEY (company) REFERENCES public.company_company(id) ON DELETE CASCADE;


--
-- Name: product_list_price product_list_price_template_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_list_price
    ADD CONSTRAINT product_list_price_template_fkey FOREIGN KEY (template) REFERENCES public.product_template(id) ON DELETE CASCADE;


--
-- Name: product_product product_product_template_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_template_fkey FOREIGN KEY (template) REFERENCES public.product_template(id) ON DELETE CASCADE;


--
-- Name: product_template-product_category product_template-product_category_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."product_template-product_category"
    ADD CONSTRAINT "product_template-product_category_category_fkey" FOREIGN KEY (category) REFERENCES public.product_category(id) ON DELETE CASCADE;


--
-- Name: product_template-product_category product_template-product_category_template_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."product_template-product_category"
    ADD CONSTRAINT "product_template-product_category_template_fkey" FOREIGN KEY (template) REFERENCES public.product_template(id) ON DELETE CASCADE;


--
-- Name: product_template product_template_default_uom_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_template
    ADD CONSTRAINT product_template_default_uom_fkey FOREIGN KEY (default_uom) REFERENCES public.product_uom(id) ON DELETE RESTRICT;


--
-- Name: product_uom product_uom_category_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.product_uom
    ADD CONSTRAINT product_uom_category_fkey FOREIGN KEY (category) REFERENCES public.product_uom_category(id) ON DELETE RESTRICT;


--
-- Name: res_user-company_employee res_user-company_employee_employee_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."res_user-company_employee"
    ADD CONSTRAINT "res_user-company_employee_employee_fkey" FOREIGN KEY (employee) REFERENCES public.company_employee(id) ON DELETE CASCADE;


--
-- Name: res_user-company_employee res_user-company_employee_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."res_user-company_employee"
    ADD CONSTRAINT "res_user-company_employee_user_fkey" FOREIGN KEY ("user") REFERENCES public.res_user(id) ON DELETE CASCADE;


--
-- Name: res_user-ir_action res_user-ir_action_action_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."res_user-ir_action"
    ADD CONSTRAINT "res_user-ir_action_action_fkey" FOREIGN KEY (action) REFERENCES public.ir_action(id) ON DELETE CASCADE;


--
-- Name: res_user-ir_action res_user-ir_action_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."res_user-ir_action"
    ADD CONSTRAINT "res_user-ir_action_user_fkey" FOREIGN KEY ("user") REFERENCES public.res_user(id) ON DELETE CASCADE;


--
-- Name: res_user-res_group res_user-res_group_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."res_user-res_group"
    ADD CONSTRAINT "res_user-res_group_group_fkey" FOREIGN KEY ("group") REFERENCES public.res_group(id) ON DELETE CASCADE;


--
-- Name: res_user-res_group res_user-res_group_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public."res_user-res_group"
    ADD CONSTRAINT "res_user-res_group_user_fkey" FOREIGN KEY ("user") REFERENCES public.res_user(id) ON DELETE CASCADE;


--
-- Name: res_user_application res_user_application_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user_application
    ADD CONSTRAINT res_user_application_user_fkey FOREIGN KEY ("user") REFERENCES public.res_user(id) ON DELETE SET NULL;


--
-- Name: res_user res_user_company_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user
    ADD CONSTRAINT res_user_company_fkey FOREIGN KEY (company) REFERENCES public.company_company(id) ON DELETE SET NULL;


--
-- Name: res_user res_user_employee_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user
    ADD CONSTRAINT res_user_employee_fkey FOREIGN KEY (employee) REFERENCES public.company_employee(id) ON DELETE SET NULL;


--
-- Name: res_user res_user_language_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user
    ADD CONSTRAINT res_user_language_fkey FOREIGN KEY (language) REFERENCES public.ir_lang(id) ON DELETE SET NULL;


--
-- Name: res_user res_user_main_company_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user
    ADD CONSTRAINT res_user_main_company_fkey FOREIGN KEY (main_company) REFERENCES public.company_company(id) ON DELETE SET NULL;


--
-- Name: res_user res_user_menu_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user
    ADD CONSTRAINT res_user_menu_fkey FOREIGN KEY (menu) REFERENCES public.ir_action(id) ON DELETE RESTRICT;


--
-- Name: res_user_warning res_user_warning_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.res_user_warning
    ADD CONSTRAINT res_user_warning_user_fkey FOREIGN KEY ("user") REFERENCES public.res_user(id) ON DELETE RESTRICT;


--
-- Name: taller_coche taller_coche_marca_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.taller_coche
    ADD CONSTRAINT taller_coche_marca_fkey FOREIGN KEY (marca) REFERENCES public.taller_marca(id) ON DELETE CASCADE;


--
-- Name: taller_coche taller_coche_modelo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.taller_coche
    ADD CONSTRAINT taller_coche_modelo_fkey FOREIGN KEY (modelo) REFERENCES public.taller_modelo(id) ON DELETE CASCADE;


--
-- Name: taller_coche taller_coche_propietario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.taller_coche
    ADD CONSTRAINT taller_coche_propietario_fkey FOREIGN KEY (propietario) REFERENCES public.party_party(id) ON DELETE CASCADE;


--
-- Name: taller_modelo taller_modelo_marca_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.taller_modelo
    ADD CONSTRAINT taller_modelo_marca_fkey FOREIGN KEY (marca) REFERENCES public.taller_marca(id) ON DELETE CASCADE;


--
-- Name: taller_modelo taller_modelo_productosDelModelo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.taller_modelo
    ADD CONSTRAINT "taller_modelo_productosDelModelo_fkey" FOREIGN KEY ("productosDelModelo") REFERENCES public.product_template(id) ON DELETE CASCADE;


--
-- Name: web_user web_user_party_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.web_user
    ADD CONSTRAINT web_user_party_fkey FOREIGN KEY (party) REFERENCES public.party_party(id) ON DELETE RESTRICT;


--
-- Name: web_user_session web_user_session_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: tryton
--

ALTER TABLE ONLY public.web_user_session
    ADD CONSTRAINT web_user_session_user_fkey FOREIGN KEY ("user") REFERENCES public.web_user(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

