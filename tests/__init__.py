try:
    from trytond.modules.taller.tests.test_taller import suite  # noqa: E501
except ImportError:
    from .test_taller import suite

__all__ = ['suite']
