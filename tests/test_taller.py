import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class TallerTestCase(ModuleTestCase):
    'Test Taller module'
    module = 'taller'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            TallerTestCase))
    return suite
